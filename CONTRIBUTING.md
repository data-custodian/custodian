# Contributing

We deeply appreciate your interest in contributing to the Swiss Data Custodian
Project. Here’s a step-by-step guide to assist you through the process of
contributing:

### Step 1: Sign the Appropriate CLA

Before you can contribute, you need to sign a Contributor License Agreement
(CLA). The CLA clarifies the intellectual property licenses granted with
contributions.

- **Individual Contributors (CLAI):** If you're contributing independently (not
  on behalf of a company), sign the
  [Individual Contributor License Agreement (CLAI)](https://gitlab.com/data-custodian/custodian/-/blob/main/docs/license/CLAI.txt).
- **Corporate Contributors (CLAC):** If contributions are made by employees on
  behalf of a company, the company should sign the
  [Corporate Contributor License Agreement (CLAC)](https://gitlab.com/data-custodian/custodian/-/blob/main/docs/license/CLAC.txt).

### **Step 2: Decide How to Contribute**

There are various ways to support the project:

- **Write Code:** Engage in the development process by fixing bugs or creating
  new features.
- **Review Pull Requests:** Review proposed changes from peers and provide
  constructive feedback and insights.
- **Maintain and Improve Our Website:** Enhance the project’s online visibility
  and usability.
- **Outreach and Onboarding:** Participate in outreach initiatives and support
  the onboarding process for new contributors.
- **Write Grant Proposals:** Assist with writing grant proposals and participate
  in other fundraising endeavors.

For minor improvements or fixes, consider opening a new issue or commenting on
an existing relevant issue. If you plan to make significant contributions to the
source code or would like to have a discussion before contributing, feel free to
contact our coordinators at
[contact@datascience.ch](mailto:contact@datascience.ch).

### **Step 3: Follow Our Code of Conduct and Contribution Guidelines**

We're committed to building a supportive and inclusive Open Source community. By
following our
[Code of Conduct](https://gitlab.com/data-custodian/custodian/-/blob/main/CODE_OF_CONDUCT.md),
you're helping create a space where everyone feels welcome and can contribute
their best work. Our Code of Conduct is based on the following key principles:

- **Inclusivity & Empathy**: Everyone is welcome. We value empathy, kindness,
  and patience.
- **Collaborative Spirit**: Resolve conflicts together, assume good intentions,
  and respect each other’s work.
- **Openness**: Encourage public communication for project-related discussions,
  reserving private channels for sensitive matters.
- **Responsibility & Respect**: Be thoughtful in communication. Harassment,
  insults, or exclusionary behavior are not tolerated.
- **Diversity & Participation**: We welcome diverse backgrounds and strive to
  make everyone feel valued.
- **Language**: English is used for project development. Help maintain this
  standard.

Check additional information about contributions to Custodian development in our
[documentation](https://data-custodian.gitlab.io/custodian/development).

Thank you for your interest and support! Happy contributing!
