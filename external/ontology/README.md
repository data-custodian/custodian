# Data Governance Contract Ontology

## Introduction

The Swiss Data Custodian leverages semantic digital contracts to dynamically describe, manage, and enforce data governance contracts. To achieve this, the Swiss Data Custodian uses a contract ontology, a formal representation of the concepts and relationships relevant to data governance contracts. The contract ontology is designed for data governance contracts, primarily focusing on the entities involved in _data handling_, the _data handling processes_, and the _privacy protection measures_ implemented. This ontology outlines the structured framework for representing and validating data contracts by defining expected properties, relationships, and constraints. 

Employing the Resource Description Framework (RDF), Web Ontology Language (OWL), and integrating the Data Privacy Vocabulary (DPV), the semantic model of the contract ontology offers precision and consistency in articulating data privacy-related contract aspects. Refer to `contract_shape_ontology.ttl` for more details.

## Benefits of Semantic Digital Contracts

* **Consistency:** The ontology offers a uniform approach to detailing contracts across diverse domains or initiatives, facilitating the sharing and reuse of contract information.

* **Accuracy:** Built upon RDF and OWL standards, the ontology ensures the descriptions of contracts are accurate and unambiguous.

* **Extensibility:** With its extensible design, the contract ontology allows the incorporation of new contract entities and relationships as necessary, making it adaptable to a wide range of contracts.

---

## Key Automation Capabilities

* **Contract Generation:** Create tailored templates for data governance contracts to meet the specific needs of involved parties.
* **Contract Review:** Automatically review contracts for compliance with relevant laws and regulations.
* **Contract Monitoring:** Track and ensure ongoing compliance and implementation of data governance contracts over time.

## Core Elements

### Entities with Legal Roles in Data Handling

* Data Controller (`dpv:DataController`): The entity determining the purpose and means of personal data processing.
* Data Subject (`dpv:DataSubject`): The individual whose personal data is being processed.
* Data Processor (`dpv:DataProcessor`): The entity processing data on behalf of the data controller.

### Process

* Process (`dpv:Process`): This class represents the comprehensive process of handling data. It encapsulates various aspects and stages of data processing, from the role of the Data Controller to that of the Data Processor.

### Privacy Protection Measures

* **Organisational Measures:** Initiatives implemented organizationally to safeguard data privacy, e.g., Cybersecurity Training, Data Protection Impact Assessment, Non-Disclosure Agreement (NDA).
* **Technical Measures:** Technical solutions and protocols designed to protect data, e.g., Encryption, Access Control Methods, Data Sanitization Techniques.

## Ontology Overview

* **Node Shapes:** These define expected properties and constraints for nodes in the data graph, providing clear targets and specifications for data governance contracts.
* **Semantic Information:** The ontology provides clear and explicit semantic definitions for classes, properties, and concepts, facilitating their understanding and interpretation.
* **Validation Components:** The ontology introduces custom validation components and validators to enforce constraints on property values, enhancing data reliability and integrity. For example, there is a custom validation constraint component, `ex:NotExpiredComponent`, with an associated SPARQL validator, `ex:NotExpiredValidator`, ensuring date values are not expired.

## Interface for Semantic Contract Creation

The ontology framework integrates with user-friendly visual flow-based programming interfaces for efficient and intuitive contract creation. With their preconfigured settings, these interfaces offer a simplified and accelerated process for semantic contract creation, ensuring the generated contracts are compliant and aligned with data governance standards. Below is an illustrative example of contract creation using Barfi: [Watch Contract Creation Demo](https://youtu.be/A7By31ZDaUU) 

## Usage

To effectively use this ontology, reference the appropriate classes, properties, and shapes per your data handling and contractual requirements. Adhering to the defined constraints and validation rules is crucial for accurate and compliant data representation. For a comprehensive understanding and application, refer to the detailed documentation provided.

___

## Templates for organizational and technical measures
### Building Block for Container Security Scans. 
The security scan ontology shape serves as a fundamental building block within our broader Contract Ontology, specializing in defining technical measures for container security scans. It outlines SHACL shapes and constraints specific to the results of security scans for vulnerabilities. When dealing with data related to container security scans within the Contract Ontology, refer to and utilize the `securityscan_shape_ontology.ttl` for guidance on validation, representation, and documentation of the respective elements.   

### Key Features  
- **Custom Validators**: Ensure date values are valid, not set in the future, and not expired. 
- **Shapes and Constraints**: Detailed specifications for instances of `sdc:SecurityScanResult` and `sdc:Vulnerability`, defining their expected structure and properties. 
- **Class and Property Definitions**: Establish the semantic foundation for representing security scan results and related concepts, providing clear documentation through labels and comments

___

## How to Contribute

We deeply appreciate your interest in contributing to the Ontology of the Swiss Data Custodian Project. Here’s a guide to get started:

### Contributor License Agreement Guidance

Before making contributions, ensure you have signed the appropriate Contributor License Agreement (CLA).

- **Individual Contributors (CLAI):** If you're contributing independently (not on behalf of a company), sign the [Individual Contributor License Agreement (CLAI)](https://gitlab.com/data-custodian/custodian/-/blob/main/CLAI.txt).
- **Corporate Contributors (CLAC):** If contributions are made by employees on behalf of a company, the company should sign the [Corporate Contributor License Agreement (CLAC)](https://gitlab.com/data-custodian/custodian/-/blob/main/CLAC.txt).

### Contribution Steps

For minor improvements, consider opening a new issue. For larger contributions or if you want to discuss privately before contributing, contact our coordinators at [contact@datascience.ch](mailto:contact@datascience.ch).

### Follow Contribution Guidelines

For more details on contributing, please visit our [Contribution Guide](https://sdsc-ord.github.io/Custodian_documentation/starting/contributing/) on the project's website.
  
Thank you for your interest and support! Happy contributing!

## License Information

Copyright © 2019-2023 Swiss Data Science Center, [www.datascience.ch](https://www.datascience.ch). All rights reserved.

The Ontology Component is distributed under the Creative Commons Attribution Share Alike 4.0 International (CC-BY-SA-4.0) license. Detailed information regarding this license can be found in the `LICENSE.CC-BY-SA` file included within this repository.

Under the CC BY-SA license, you are free to:
- **Share** — copy and redistribute the material in any medium or format
- **Adapt** — remix, transform, and build upon the material for any purpose, even commercially.

However, you must provide appropriate attribution, and if you modify the component, you must distribute your contributions under the same license.

If the CC BY-SA license does not accommodate your project or business needs, alternative licensing options are available to meet specific requirements. These arrangements can be facilitated through the EPFL Technology Transfer Office. For more information, kindly visit their official website at [tto.epfl.ch](https://tto.epfl.ch/) or direct your inquiries via email to [info.tto@epfl.ch](mailto:info.tto@epfl.ch).

Please note that the Ontology Component should be used responsibly and ethically. Users and developers must adhere to legal standards and ethical guidelines during use.
