# Helper Stuff for Working with Nix

## Find a Command in the `nixpkgs`

```shell
nix repl .
```

```shell
nix-repl> builtins.unsafeGetAttrPos "runCommand" pkgs
{ column = 3; file = "/nix/store/sfycwi72zfjsspidinx56ajaiffpyh17-source/pkgs/build-support/trivial-builders/default.nix"; line = 14; }
```

or to open it:

```shell
nix-repl> :e pkgs.runCommand
```
