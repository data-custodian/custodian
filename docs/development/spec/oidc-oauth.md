# Access Tokens

- **ID Token**:

  - Issued: by an OpenID provider.
  - Introduced by [OpenID Connect](https://openid.net/connect), proves that the
    user is **authenticated**.
  - Format: JSON Web Token (JWT), use the claims part to get custom information
    about the user.
  - Spec: OpenID Connect

- **Access Token**:
  - Issued: by an authorization server. In the
    [OAuth2](https://auth0.com/docs/protocols/protocol-oauth2) context, allows a
    client application to access a specific resource to perform scoped actions
    on behalf of a user. A secret to prove that you are authorized to access a
    resource.
  - Specs: OAuth2
  - Format: Not specified by
    [OAuth2 core specifications](https://datatracker.ietf.org/doc/html/rfc6749).
    Common format is a JSON Web Token (JWT). Treat it as opaque string.

An ID Token should never be used to make authorization decision, like accessing
a resource over an API. For that the Access Token should be used.

## Overview

<p align="center">
  <img width="600" src="assets/id-token-vs-access-token.jpg">
</p>

# Gateway Flow

```mermaid
sequenceDiagram
    actor U as User<br>(Browser)
    participant G as Gateway<br>
    participant K as OIDC Provider<br>(Keycloak)
    participant F as Frontend<br>(Playground)

    U ->> G: 1. GET http://<domain>/a/b/c (no cookie)
    G ->> G : 2. Check if ID token cookie present.
    G -->> U: 3. Resp: Redirect to login page.
    U ->> G: 4. GET http://<domain>/auth/login.
    G -->> U: 5. Resp: Redirect to OpenID Provider<br>http://<oidc-provider>/realms/data-custodian/protocol/openid-connect/auth
    U ->> K: 6. GET http://<oidc-provider>/realms/data-custodian/protocol/openid-connect/auth
    K -->> U: Resp: Login page.
    U ->> K: User logs in. Login successful.
    K -->> U: 7. Resp: Redirect to <br>http://<domain>/auth/callback.
    U ->> G: 8. GET http://<domain>/auth/callback.
    G ->> K: 9. Handle OIDC code exchange.
    K -->> G: Resp: ID token and access token.
    G -->> U: 10. Resp: Redirect to SWISSDATACUSTODIAN_REDIRECT in cookie.<br>Set encrypted cookie for ID token and access token.
    U ->> G: 11. GET http://<domain>/a/b/c
    G ->> F: 12. Forward request to `playground`.
    F -->> U: Resp: UI Frontend.
```

1. The user accesses the Custodian from the browser on **`http://<domain>`**.

2. The `gateway` service checks if the ID token cookie
   `SWISSDATACUSTODIAN_IDTOKEN` (JWT) is present and checks the signature. The
   `gateway` on startup has an OIDC relay proxy created which inspects the
   issuer API with
   `http://<oidc-provider>/realms/data-custodian/.well-known/openid-configuration`,
   also the public key is (?) requested at this point.

3. In the `gateway`: If the cookie

   - is not existing or
   - the signature cannot be verified
   - or anything is wrong with `rp.VerifyIDToken()` (`zitadel/oidc`)

   a redirect response is sent to **`http://<domain>/auth/login`** and a cookie
   `SWISSDATACUSTODIAN_REDIRECT=http://<domain>/a/b/c` is set.

4. The browser accesses to **`http://<domain>/auth/login`**.

5. The `gateway` redirects (redirect response) to the OIDC provider on
   **`http://<oidc-provider>/realms/data-custodian/protocol/openid-connect/auth`**
   with parameters:

   ```yaml
   client_id: custodian
   redirect_uri: http://<domain>/auth/callback
   scope: openid+profile+roles
   state: 192e4942-fe3e-4c3e-b286-9f440950a87d
   ```

   and sets a cookie `state`.

6. The browsers accesses the OIDC provider and gets the login page. The user
   logs in.

7. The OIDC backend redirects to `http://<domain>/auth/callback`.

8. The browser accesses `http://<domain>/auth/callback`.

9. The `gateway` handles `http://<domain>/auth/callback` by doing the OIDC code
   exchange in `rp.CodeExachangeHandler` to get the **ID token** and **access
   token**.

10. Then `gateway` then sends a redirect to the URL in cookie
    `SWISSDATACUSTODIAN_REDIRECT` and sets cookies `SWISSDATACUSTODIAN_IDTOKEN`
    and `SWISSDATACUSTODIAN_ACCESSTOKEN` from the code exchange.

11. The browser accesses `http://<domain>/a/b/c`.

12. The `gateway` forwards the request to the `playground`.

## Realm Roles

- Data Owner (Submitted Stuff, Login -> Submit)
- Data Processor/Requestor (Login -> Request)

Contract Content

```yaml
contract-a:
  dataset-id:
  data-processor: Gabriel (created the contract)
  data-owner: Luana

signatures:
  contract: contract-a
  name: Gabriel (requestor)
  signed: true

signatures:
  contract: contract-a
  name: Luana (owner)
  signed: false
```

Login as User: Luana

- See Data Request of Contract `A`.
- Operation: Accept/Deny :
  - Check on the `contract-a`: are you allowed, yes because `data-owner: Luana`

Login as User: Luana

- Submit new data/meta-data `D`. You want check if

Keycloak: Maybe we should distinguish:

- default role `requester`: can I request

  - read all contracts (you are part of)
  - see event log for all you are part of.

- optional role `submitter`

  - can submit new data.

- role `admin-read`:
  - read every contract,
  - see event log for all contracts.
  - no write operations on anything.

## Service `contract-manager` -> `key-manager`:

In `contract-manager` you have an access token: gives access to
`contract-manager`? Should it give access to `key-manager`?
