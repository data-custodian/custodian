## Graph Databases

### Schema

The following schema shows two graph databases (`jena`)

```mermaid
graph TD
    Users[Users]
    Proxy[Proxy]
    KnowledgeBase[Knowledge Base<br>Database]
    CustodianDB[Custodian<br>Database]
    ContractsDataset[Contracts<br>Dataset]
    OntologyDataset[Ontology<br>Dataset]
    ValidationDataset[Validation<br>Dataset]

    Users --> Proxy
    Proxy --> KnowledgeBase
    Proxy --> CustodianDB
    CustodianDB --> ContractsDataset
    CustodianDB --> OntologyDataset
    CustodianDB --> ValidationDataset
```

There are 2 databases behind a proxy (`haproxy`).

- **Knowledge Base**: Stores meta data related to a user or a dataset.
- **Contract**: Split in three datasets: one to store the contracts and the
  signatures and one only used to validate the shapes of the contract when
  uploaded. This dataset is temporary and must not be used to store anything
  permanent. Last dataset stores the ontology.
