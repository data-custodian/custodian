# Custodian

<details>
<summary>Contents</summary>

<!--toc:start-->

- [Custodian](#custodian)
  - [General Architecture](#general-architecture)
  - [Contract-Manager](#contract-manager)
    - [Nomenclature:](#nomenclature)
    - [Create a Contract](#create-a-contract)
      - [Client](#client)
      - [Backend](#backend)
    - [Retrieve a Contract](#retrieve-a-contract)
      - [Client](#client)
      - [Backend](#backend)
    - [List Contracts](#list-contracts)
      - [Client](#client)
      - [Backend](#backend)
    - [Archive a Contract](#archive-a-contract)
      - [Client](#client)
      - [Backend](#backend)
    - [Give & Revoke Consent on a Contract](#give-revoke-consent-on-a-contract)
      - [Client](#client)
      - [Backend](#backend)
    - [Contract Data](#contract-data)
      - [Hash](#hash)
    - [Consent Decision Data](#consent-decision-data)
  - [Policy Decision Point](#policy-decision-point) -
  [Validation Policy](#validation-policy)
  <!--toc:end-->

</details>

## General Architecture

<center>
    <img src="./assets/architecture.drawio.svg" width="100%"/>
</center>

## Contract-Manager

The contract manager is a service with the following basic functionalities:

**Contract Management:**

- [Create contracts.](#create-a-contract)
- [Retrieve a specific contract.](#retrieve-a-contract)
- [List contracts.](#list-contracts)
- [Archive a contract.](#archive-a-contract)

**Consents Management:**

- [Give & revoke a consent for a contract.](#give-revoke-consent-on-a-contract)
- [Retrieve consents for a contract.](#retrieve-consents-for-a-contract)

### Nomenclature:

- ↦ : Abbreviation for "maps to", "leads to".
- `User`: any user accessing the endpoint (potentially through UI).
- `DP`: The data processor (-> group of user IDs).
- `DC`: The data controller (-> group of user IDs) is aborted and the end-point
  is not successful .
- `DP_id`, `User_id`: Identity Provider IDs (UUID) of the interacting
  data-processor or general user.
- `C`: A contract.
- `D`: A dataset.
- `P` : A policy.
- `CC`: A consent.
- `CM`: The contract manager backend.
- `C.x`: Simplified access to property on data `C`.
- `Assert:` denotes and early return assert statement, if its `false` the logic
  aborts.

Assumptions:

- A `User_id` might be in the group of a `DP_id` or `DC_id`, but currently that
  is not modeled below. Its just an indirection more which we might need in the
  future.

- Access tokens contains these informaton:

  - ID: `User_id`: the user.
  - ID: `DP_id` : potential data processor group id.
  - ID: `DC_id` : potential data controller group id.
  - Role: `admin-role` (potential).

### Create a Contract

- Endpoint: _POST_ `/contracts`
- Content-Type: `application/json`

#### Client

- A user `User_id` (belonging to `DP_id`) creates contract `C` on data-set `D`.

  ```go
    C = NewContractRequest(
        data_processor=[DP_id, ... ],
        // dataController= set by backend,
        data=DataSet(id=D_id),
        startDate=S,
        endData=E,
        ...
    )
  ```

  Note: How does a client easily generate this: We use OpenAPI Schema to
  generate the client API (`CreateContract`, `PostContract`)

- Send `C` to `CM` with access token `t` (bound to `User_id`, `DP_id`).

#### Backend

- `CM` receives `C`, token `t`:

  - Access token `t` ↦ contains user-info `User_id` and `DP_id`.

- **Pre-Condition Checks:**

  1. Assert: `C` validates with `minimal.json.schema`. no signatures is implied
     by this.
  1. Assert: Is access token `t` allowed to create contract (write access?)?
  1. Assert: Is `DP_id` matching `C.DP_id` (did you: `DP_id` create the
     contract?) (Go)
  1. Assert: `now <= C.start_date < C.end_date` (only Go).

  Custom Checks (Extension Point, Feature-Toggles)

  1. ...

- Query all data sets `D_i` on the Dataset Query API endpoint.

- Query policy `P_i` on `D_i.policy.id` on the Policy API endpoint.

- Set missing data on `C`:

  - Set data controller from data-set `D_i` to contract `C`, e.g.
    `C.DC_id += D_i.DC_id`.

  - Compute all hashes of the policies and store them in `C.data.i.policy.hash`.

- **Pre-Condition Checks**

  - Assert: `C` validates with `full.json.schema` (use-case specific aligns with
    `ontology.ttl`).

- Store `C` in DB.

### Retrieve a Contract

A contract can be retrieved for certain users.

- Endpoint: _GET_ `/contracts/{contractID}`
- Content-Type: `application/json`

#### Client

- `User_id` sends `contractID` to `CM` with access token `t` to get it.

#### Backend

- `CM` receives `contractID`, access token `t`:

  - Access token `t` ↦ `User_id`.

- **Pre-Condition Checks:**

  1. Assert: Is access token `t` allowed to _retrieve_ a contract? (Go)
  1. Assert: Is `User_id` belonging to `C.DP_id` or `C.DC_id`.

- Query DB and get contract `C` for `contractID`.

- Return `C` in JSON.

### List Contracts

A list of contracts can be retrieved by

- Endpoint: _GET_ `/contracts`
- Parameters:
  - `userId`: Only select contracts where the `userID` has access to (either
    `dataProcessor` or `dataController`).
  - `dataProcessors`: Only get contracts with `dataProcessors` of these UUIDs.
  - `dataControllers`: Only get contracts with `dataControllers` of these UUIDs.
- Content-Type: `application/json`

TODO

#### Client

- Access the endpoint with access token `t` to get all contracts which are
  scoped to `t`.

#### Backend

TODO:

### Archive a Contract

A contract can never be deleted, it can only be archived and is then read-only
from that point on.

Endpoint: _POST_ `/contracts/{contractID}/archive`

#### Client

- `User_id` sends `contractID` to `CM` with access token `t` to archive it.

#### Backend

- `CM` receives `contractID`, access token `t`:

  - Access token `t` ↦ `User_id`.

- **Pre-Condition Checks 1:**

  1. Assert: Is access token `t` allowed to _archive_ contract? (Go)

- Query DB and get contract `C` and signatures `S` for `contractID`. Signature
  have status `none|revokedd|signed`

- **Pre-Condition Checks 2:**

  1. Assert: Is `User_id` belonging to `C.DP_id` or `C.DC_id` or has admin role
     `admin`.
  1. Assert: Strategy A (early returns)

  - If time span `[C.start_date, C.end-data]` is in the past ↦ contract no more
    valid ↦ `true`

  - If `User_id` (from `t`) matches `C.DP_id` ↦ `true` (owner).

  - **Not for now:** If `User_id` matches `C.DC_id` and all signatures of
    `DC_id`s in `S` have status `revokedd` (or potentially
    `non-signed|revokedd`) ↦ `true`.

  <!-- **Note:** Data processor can archive because he/she is the owner and the data -->
  <!-- controller only if all other signatures of data-controllers are `revoked` (or -->
  <!-- potentially also `not-signed|revokedd`). -->
  <!---->

- Delete contract in DB (delete).

### Give & Revoke Consent on a Contract

Consent can be given to a contract by any party which is either a data
processor, data controller, (or later data subject). The consent object is
created by the user and sent to `CM` which will check its legitimacy before
accepting it.

- Endpoint: _POST_ `/contracts/{contractID}/consent`
- Content-Type: `application/json`

##### Client

- `User_id` (`8d138581-4aa7-4104-a78c-2af1f2e686bc`) gets the contract `C` on
  the _GET_ `/contracts/{contractID}` endpoint.

- [Computes the hash](#hash) of the contract `contractHash` which contains

  ```yaml
  # Digest of the subject (contract hash).
  digest: 3aac07671d136afbade9ef701b7c56cd634dbc8924c63cc4be0178df8f42093b
  #  Digest method used.
  digestMethod: sha256
  ```

  **Note:** The client should check what ever it needs to check on the contract
  `C` before it computes blindly the hash. (Custodian should not be trusted at
  this point.)

- Client creates the consent decision `CC` and fills in the needed fields. The
  following constructors will create the following structure:

  ```yaml
  # @type: sealed<T: ConsentDecision>:
  version: 1

  seal:
    # The method used for the `signature` below.
    method: Ed25519

    # Timestamp in ISO-8601: `YYYY-MM-DDTHH:mm:ss.SSSZ`
    timestamp: 2025-02-06T10:15:30.123Z

    # The user who created this seal. Either the `creator` of the consent
    # or `custodian`.
    issuer: 8d138581-4aa7-4104-a78c-2af1f2e686bc

    # The public key ID (uuid) of the key the user signed with.
    # (Changeable by user)
    publicKeyID: 7c3bac62-2303-48fe-aa36-ce71ebe07a0c

    # The cryptographic signature.
    signature: "..."

  data: # @type: ConsentDecision
    # The decision this consent describes (either `given` or `revoked`).
    decision: "given"

    # The user UUID who created this consent.
    # Note: It belongs to the groups in `onBehalfOf`.
    creator: 8d138581-4aa7-4104-a78c-2af1f2e686bc

    # The data controller group UUIDs this decision is taken on behalf of.
    onBehalfOf:
      - ca2f12fc-f65d-48bc-a732-71f2ceefe995
      - ...

    subject:
      # Whats the subject of this consent decision (for now only `contract`).
      type: contract
      id: c169ac00-2f7a-4c15-b3dc-28d455e9ba4d

      hash:
        # Digest of the subject (contract hash).
        digest: 3aac07671d136afbade9ef701b7c56cd634dbc8924c63cc4be0178df8f42093b
        # Digest method used.
        digestMethod: sha256
  ```

  The client can easily create such a signature with:

  ```go
  consent := CreateConsentDecision({
    version: 1,
    subject: Contract {
      id: "c169ac00-2f7a-4c15-b3dc-28d455e9ba4d",
      hash: contractHash,
    },
    consent: Consent {
        method: "Ed25519",
        action: "given",
        author: "8d138581-4aa7-4104-a78c-2af1f2e686bc",
        onBehalfOf: "...",
    },
  })

  // Either the client signs the contract.
  // Sets the `timestamp` and computes the `signature`.
  consent.seal(
      publicKeyID: "...",
      privateKey: "...",
      issuer: "8d138581-4aa7-4104-a78c-2af1f2e686bc")

  // or set it to be sealed by the backend.
  // which sets  the fields in `signature.consent:` to the Custodian backend one.
  signature.sealByCustodian()
  ```

- Sends the signature `CC` to the backend `CM` with token `t`.

##### Backend

- Receives the consent decision `CC` and access token `t`.

  - Access token `t` ↦ `User_id`.

- **Pre-Condition Check**

  1. Assert: Is access token `t` allowed to create _consents_ on contract?

  1. Assert: Validate the consent `CC` with JSON schema.

  1. Assert: Is timestamp `CC.seal.timestamp` no older than 2 minutes.

  1. If `CC.seal.issuer != custodian`, check that the provided signature
     `CC.signature` is correct by fetching the public key from the key store and
     verifying the signature.

  1. If `CC.seal.issuer == custodian` -> ok.

- Retrieve contract `C` with `CC.data.subject.id`.

- **Pre-Condition Checks**:

  1. Assert: Is contract `C`'s hash equal to `CC.subject.hash.digest`.

  1. Assert: Is `User_id == CC.data.consent.author`.
  1. Assert: Is `User_id` belonging to any `C.DP_id` or `C.DC_id` or has role
     `admin`.

- Accept the signature `CC`

  - If `CC.data.consent.givenBy == custodian`: Add `CC.seal.timestamp`,
    `CC.seal.publicKeyID` from Custodian and compute the `CC.seal.signature`

  - Compute the seal: Add `CC.seal.{timestamp,method,...}`, then hash
    `CC.data + CC.seal` and sign the hash and add it back to
    `CC.seal.signature`.

- Store the consent `CC` in the DB.

### Retrieve Consents on a Contract

- Endpoint: _GET_ `/contracts/{contractID}/consents`
- Content-Type: `application/json`: List of `sealed<T = ConsentDecision>`.

### Contract Data

**Assumptions**:

- A `process` is always attached to one policy id. That means you can only list
  datasets in `datasets` with the same policy ids. Create multiple processes if
  you need to list datasets for different policies.

```yaml
contract:
  version: 1

  id: 737b4563-c4ba-43cc-8c1a-670c7c9967df

  # Start/end date in ISO-8601.
  startDate: 2025-02-06T10:15:30Z
  endDate: 2025-03-06T10:15:30Z

  metadata:
    # The user UUID who created this contract.
    author: 42a31003-56d4-4cc0-80af-66e4d7ed78ab
    # Timestamp in ISO-8601: `YYYY-MM-DDTHH:mm:ss.SSSZ`
    timestamp: 2025-02-06T10:15:30.123Z

  process:
    - dataProcessor:
        - f8d754a0-0a60-4374-a92a-78160d30a2bd
        - a06af010-f2b8-4eb3-aadb-81b8934bbac1
        - ...

      dataController:
        - b0c30a7c-b13c-46bd-b678-269c5966abbd
        - 5b4321b4-0aa7-43bc-9adc-6349a97307e1
        - ...

      # A list of possible types `"oneOf: ["#/$defs/Dataset"`, "#$defs/???", ... ]`
      datasets:
        - id: 3b8f14d0-b402-4728-acd3-999c364cfddb
          url: s3://bla.com/dataset-3b8f14d0-b402-4728-acd3-999c364cfddb.ttl
        - ... # multiple possible but need to follow the same policy below.

      # The policy this process follows.
      policy:
        id: 9a0110cf-d6e2-4a4e-87ae-2e4ba2ffa378
        hash: ...

      # A subset of purposes from the `policy.id`.
      purposes:
        - academic
        - non-academic
        - commercial

      # A subset of processing from the `policy.id`.
      processings:
        - secure-storage
        - local-download
        - store

      # All measures from the `policy.id`.
      measures:
        - encryption
        - security-training

    # If there is another dataset with another policy than e.g.
    # `9a0110cf-d6e2-4a4e-87ae-2e4ba2ffa378`, one needs to add
    # another process.
    - dataProcessor: ...
```

#### Create Contract

A partial contract schema is required for the request on the
[create contract endpoint](#create-a-contract):

```yaml
contract:
  version: 1

  # Start/end date in ISO-8601.
  startDate: 2025-02-06T10:15:30Z
  endDate: 2025-03-06T10:15:30Z

  process:
    - dataProcessor:
        - f8d754a0-0a60-4374-a92a-78160d30a2bd
        - a06af010-f2b8-4eb3-aadb-81b8934bbac1
        - ...

      # A list of possible types `"oneOf: ["#/$defs/Dataset"`, "#$defs/???", ... ]`
      datasets:
        - id: 3b8f14d0-b402-4728-acd3-999c364cfddb
          url: s3://bla.com/dataset-3b8f14d0-b402-4728-acd3-999c364cfddb.ttl
        - ... # multiple possible but need to follow the same policy below.

      # The policy this process follows.
      policy:
        id: 9a0110cf-d6e2-4a4e-87ae-2e4ba2ffa378

      # A subset of purposes from the `policy.id`.
      purposes:
        - academic
        - non-academic
        - commercial

      # A subset of processing from the `policy.id`.
      processings:
        - secure-storage
        - local-download
        - store

      # All measures from the `policy.id`.
      measures:
        - encryption
        - security-training

    # If there is another dataset with another policy than e.g.
    # `9a0110cf-d6e2-4a4e-87ae-2e4ba2ffa378`, one needs to add
    # another process.
    - dataProcessor: ...
```

### Dataset Meta-Data

The contract manager needs a way to query information on the data set. The data
set contains a policy ID.

```yaml
# @type: DatasetMetadata
dataset:
  id: 3b8f14d0-b402-4728-acd3-999c364cfddb
  policy: 9e350cc8-2f4d-408c-8422-3e094cabdf2a
```

### Policy Data

A [policy](#policy-data) defines a list of purposes, processings and measures
for a certain data controller.

```yaml
# @type: Policy
id: 9e350cc8-2f4d-408c-8422-3e094cabdf2a

# The one who governs a dataset under this policy given by
# `purpose`, `processing` and `measures`.
# Currently we only want one data controller.
dataController: db407343-a8b1-4dc0-acef-5829d1cc17e3

purpose:
  - academic
  - non-academic
  - commercial
processing:
  - secure-storage
  - local-download
  - store
measure:
  - encryption
  - security-training
```

#### Hash

The contract hash or the policy hash is computed by computing the
[ASN.1](https://en.wikipedia.org/wiki/ASN.1) with the method `DER` and then
hashing it with `sha512`. Distinguished Encoding Rules (`DER`) guarantees
canonical (deterministic) encoding and ensures that sets and maps (unordered
collections) are sorted by their encoded byte values (lexicographically). Widely
used in cryptographic applications (like X.509 certificates) where deterministic
encoding is essential.

### Consent Decision Data

A consent decision allows a user to give consent or revoked consent a contract.
By giving consent (or signing this contract), the user confirms that they have
read and agreed to all the terms outlined in the contract.

A signature is stored next to the contract as JSON and is immutable once it is
created. Multiple signatures can exist, they are implicitly sorted on the
time-axis by the `timestamp` which the backend controls.

```yaml
## @type: sealed<T = ConsentDecision>
seal:
  timestamp: 2025-02-06T10:15:30.123Z
  method: Ed25519
  issuer: b85645ce-e42e-4771-a024-a19fcd98ad77
  publicKeyID: 20c9926e-7577-4729-8935-b1ada7de96dd
  signature: "..."

data: # @type: ConsentDecision
  version: 1

  # The decision this consent describes (either `give` or `revoke`).
  decision: "give"

  # The user UUID who created this consent.
  # Note: It belongs to the groups in `onBehalfOf`.
  creator: 8d138581-4aa7-4104-a78c-2af1f2e686bc

  # The data controller group UUIDs this decision is taken on behalf of.
  onBehalfOf:
    - ca2f12fc-f65d-48bc-a732-71f2ceefe995
    - ...

  subject:
    # Whats the subject of this signature (for now only `contract`).
    type: contract
    id: c169ac00-2f7a-4c15-b3dc-28d455e9ba4d

    hash:
      # Digest of the subject (contract hash).
      digest: 3aac07671d136afbade9ef701b7c56cd634dbc8924c63cc4be0178df8f42093b
      #  Digest method used.
      digestMethod: sha256
```

## Policy Decision Point

TODO

### Validation Policy

- Every `groupID` in `dataController` and `dataProcessor` needs to have at least
  one [`consent`](#contract-consent-data) to make the contract ``.
