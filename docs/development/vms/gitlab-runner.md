# Gitlab-Runner

The general deployment is described in the [general chapter](./general.md). This
document gives some more information about the maintenance of the Gitlab Runner.

## Maintenance

All recipes here refer to the
[`justfile`](../../../tools/nix/hosts/gitlab-runner/justfile).

### Visualizing Performance

On the VM do:

```shell
btop
```

### VM's Nix Store Maintenance

gf the NixOS `/nix/store` of the VM can be simply garbage-collected with

```shell
just gc
```

### Podman Normal Maintenance

This will just garbage collect stuff:

```shell
just podman-gc
```

### Prune Podman Images

The CI might add more images than the `local/nix-daemon` and `local/alpine` (or
`local/nix`), you can clean them with stopping all containers (**note: this
stops jobs**)

```shell
just podman-remove-images
```

It will only delete images which do not have `no-prune` label (e.g it will not
prune `local/nix-daemon`, `local/alpine` or `local/nix`).

### Prune Podman Volumes

The VM runs a container `nix-daemon-container` which runs the `nix daemon` which
manages the `/nix/store` and it has 3 volumes attached:

- `nix-daemon-store`: \*\*Contains the shared `/nix/store` which each job will
  mount `read-only`.

  :::tip

  This volume can grow quite significantly.l :::

- `nix-daemon-socket`: Contains the Nix socket which each job will mount, to
  communicate to the Nix daemon and be able to store stuff into the `/nix/store`
  (it is only **read-only**).

- `nix-daemon-db`: The Nix database which is accompanied with the store.

#### Cleaning the `nix-daemon-volume` Volume

Run

```shell
just podman-recreate-nix-daemon-store
```

## Full VM Restart

To clean all volumes and space:

1. Stop the Runner

```shell
systemctl stop gitlab-runner.service
```

This will gracefully shutdown. If you want to kill all jobs directly run step 2
before!

1. Stop all containers

```shell
just podman-remove-containers
```

1. Remove all containers/images and volumes.

```shell
podman prune system --force --all --volumes
```

1. Redeploy the VM with

```shell
just deploy --force
```

1. Clean the `/nix/store` of the VM

```shell
just gc
```
