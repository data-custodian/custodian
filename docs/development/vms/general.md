## Deploy NixOS VM

To deploy a NixOS we use `nixos-anywhere`. The ingredients you need to deploy a
NixOS to a VM (e.g. [engines.switch.ch](engines.switch.ch)) are:

- Access to [engines.switch.ch](https://engines.switch.ch).
- An SSH key pair called `switch-engines-init_rsa`, we use that for the initial
  connection to the VM. You can get this private key from the owner of the
  repository or create your own one and add it to the VM on creation.
- [Nix installed](#installing-nix).
- A `flake.nix` which defines a `nixosConfiguration` output. You can use a
  [default example here](https://github.com/nix-community/nixos-anywhere-examples/blob/main/flake.nix).
  **For this guide we use our
  [`gitlab-runner`](../../tools/nix/hosts/gitlab-runner/default.nix) in
  [`flake.nix`](../../tools/nix/flake.nix) which defines a NixOS configuration.
  The modules which define this machine can be
  [seen here](../../tools/nix/nixos).**

### Step: Create a VM Instance

The first thing you need is to create a VM. On
[engines.switch.ch](https://engine.switch.ch) you can create an instance and
attach the SSH key to it. The instance's Linux image is not important. Choose
ideally: Debian Bookworm as it is the smallest.

Check your SSH access by adding the following in `~/.ssh/config`:

```shell
Host gitlab-runner-nixos
    HostName 1.1.1.1
    IdentityFile ~/.ssh/switch-engines-init_rsa
    IdentitiesOnly no
```

and executing

```shell
ssh -T gitlab-runner-nixos
```

**Note:** Do not uncomment `IdentitiesOnly yes` since it will block the
`nixos-anywhere` SSH key which is generated on the fly. You can enable it
afterwards again.

### Step: Install NixOS onto the VM

The NixOS install for [`flake.nix`](../../tools/nix/flake.nix) is executed by
doing

```shell
nix run github:nix-community/nixos-anywhere -- --flake "./tools/nix#gitlab-runner" gitlab-runner-nixos
```

**Note:** This is only done once. After installing check the connection by login
in with `ssh <user>@gitlab-runner-nixos` (the user in this example is `ci`, also
`root` is possible)

**Troubleshooting**: There is weird bug in `nixos-anywhere` about some store
paths with `.` in the filename or similar. **Uncomment the `packages` in
`flake.nix` output.**

### Updating the SSH Configs

Update the `User` setting in `~/.ssh/config` to the user you setup in the VM.
Check that the connection works with `ssh -T gitlab-runner-nixos`.

### Update the VM's SSH Key

Run `ssh-keyscan <ip>` and extract the `ssh-rsa ....` key and replace
`vmPublicKey` in
[`hosts/gitlab-runner/secrets/secrets.nix`](../../tools/nix/hosts/gitlab-runner/secrets/secrets.nix).

This is needed to properly deploy the secrets with `agenix`. Now you need to
re-encrypt the secrets with

```shell
cd tools/nix/hosts/gitlab-runner
just reencrypt-secrets ~/.ssh/your-private-key
git add .
```

**And now re-deploy the VM with the below.**

### Updating the VM

Once you can access the NixOS VM, you can also update the NixOS with
[`deploy`](https://github.com/serokell/deploy-rs) which works with the setting
in `deploy` output in the [`flake.nix`](../../tools/nix/flake.nix):

```nix
deploy = {
  nodes.gitlab-runner = {
    hostname = "gitlab-runner-nixos";
    fastConnection = true;
    profiles = {
      system = {
        user = "root";
        sshUser = "root";
        path = inputs.deploy-rs.lib.x86_64-linux.activate.nixos nixosConfigurations.gitlab-runner;
      };
    };
  };
};
```

Run the following:

```shell
cd tools/nix/hosts/gitlab-runner
just deploy --force
```
