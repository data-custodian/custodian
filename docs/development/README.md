# Development Docs

<!--toc:start-->

- [Development Docs](#development-docs)

  - [Development Setup](#development-setup)
    - [Nix Development Environment](#nix-development-environment)
      - [Installing Nix](#installing-nix)
      - [Entering the Development Shell](#entering-the-development-shell)
  - [Component Development](#component-development)
  - [Nix Packaging](#nix-packaging)
    - [Debugging `flake.nix`](#debugging-flakenix)
  - [Repository Structure](#repository-structure)
  - [Git Branching Model](#git-branching-model)
  - [Deployment](#deployment)
    - [Development Environment](#development-environment)
      - [Container Images](#container-images)
    - [Production Environment](#production-environment)
      - [Semantic Versioning](#semantic-versioning)
  - [CI](#ci)
  - [Deploy NixOS VM](#deploy-nixos-vm)

  <!--toc:end-->

## Development Setup

In this section you'll find instructions on how to install the tools we'll use
to build the custodian ecosystem.

All of these tools are available for Linux, macOS.

There is currently the following development setups we support:

- **1. Nix Development Environment**: A stable development environment with the
  Nix package manager.

- **2. Manual Setup**: Install the tools yourself on the system of your choice.
  This is strongly not recommended as it does not align with a streamlined
  approach which aligns with the tooling used in CI.

- **3. [DevContainer](../.devcontainer)**: Not yet provided.

### Nix Development Environment

#### Installing Nix

If you have not installed `nix` on your Linux or macOS use
[`nix-installer`](https://github.com/DeterminateSystems/nix-installer):

```shell
curl --proto '=https' --tlsv1.2 -sSf -L https://install.determinate.systems/nix | sh -s -- install
```

#### Entering the Development Shell

If you have the package manager
[`nix`](https://github.com/DeterminateSystems/nix-installer) installed you can
enter a development setup easily with

```shell
nix develop './tools/nix#default'
```

or `just develop`.

**It is strongly suggested to install [`direnv`](https://direnv.net) and
[setup for your shell](https://direnv.net/docs/hook.html)**. This enables you to
just enter `cd custodian-repo` which will auto-source `.envrc` and enable the
default shell by default. To disable too much verbose output in `direnv` use
[these settings](https://github.com/direnv/direnv/issues/68#issuecomment-2054033048).

## Component Development

Run the following inside the development shell **to build the components** with
pattern `<component-pattern>` (glob):

```shell
just build "<component-pattern>"
```

To build all components use `*`.

To **lint** or **test** or built the image of components use:

```shell
just lint "<component-pattern>"
just test "<component-pattern>"
just image "<component-pattern>"
```

**Note: The command above will run over [`quitsh`](../../tools/quitsh/README.md)
which is our Go scripting utility to provide a better experience than writing
Bash/Python scripts etc.**

## Nix Packaging

To build a package you can use e.g.

```nix
just package-nix policy-decision-point-service
```

:::tip

List all packages with `just package-nix-show`

:::

#### Debugging `flake.nix`

Its handy to inspect what the [`flake.nix`](../../tools/nix/flake.nix) provides,
use

```shell
just nix-repl
```

which loads all `outputs` of the Flake file (e.g
`packages.x86_64-linux.custodian.quitsh` etc.).

You can also use `nix repl -f <nixpkgs>` with an additional `:lf ./tools/nix`
which will load also the `inputs` and `outputs` from the Flake file.

## Repository Structure

This mono-repository is structured into different **components** which mostly
live inside [`components`](../../components) folder. Each component contains a
`.component.yaml` which specifies some properties and build tooling steps to
`lint`,`build`, `test` itself.

To see all components run:

```shell
just list
```

#### Update Fixed-Output-Derivation Hashes

Because some derivations (`just package-nix-show`) use fixed-output-derivation
(dependencies of Go modules specified in `go.mod` files.), the hard-coded hash
of this output can deviate from what is actually needed.

The command `just quitsh nix fix-hash` will check all hashes of the packages and
update these values in `*.nix` files. CI will also use the same command to check
for any deviation.

## Git Branching Model

We use a stable mainline branching model where `main` is the stable branch. All
feature branches:

- `feat/*`
- `fix/*`

will branch of from `main` and merge back by pull-request to `main`.

## Deployment

### Development Environment

Tracking:
[![Static Badge](https://img.shields.io/badge/issue-missing-red)](missing-link)

The `main` branch will be continuously deployed to a development environment.

#### Container Images

The container images will use the following naming scheme:

```xml
<repository-name>/custodian/service-<service-name>:<git-hash>
```

where

- **`<repository>`** : Points to the container registry.
- **`<service-name>`** : The name of the micro-service.
- **`<git-hash>`** : The 11-digit Git hash where this image was build.

### Production Environment

TODO: Once we are on the monorepo, this will contain information how we version
and name out outputs from each component.

Tracking:
[![Static Badge](https://img.shields.io/badge/issue-missing-red)](missing-link)

#### Semantic Versioning

Tagging of a complete mono-repository release with all its components will be
done on the mainline branch `main` by using a _Git version tag_ in the form:
`v<version>` where `<version>` is a [semantic version](https://semver.org/),
e.g. `v1.2.3-rc1+343123aef`

Pre-release versioning (e.g. `<prefix>v1.2.3-<pre-release-tag>` is best avoided
as it complicates matter and is not really necessary.

All build output (container images etc.) will use the semantic versions in their
image tag including the digest:

```xml
<repository-name>/custodian/service-<service-name>:<version>@sha265:<digest>
```

where

- Others [see here](#development-environment).
- **`<version>`** : The semantic version of this release.
- **`<digest>`** : The image digest for more security. See
  `podman inspect --format "{}" <image>`.

## CI

### CI Attributes

You can pass attributes to the CI to steer certain behavior. This happens in the
form of a YAML Markdown code block one adds to either a merge-request
description, commit or annotated tag in the commit body. The YAML markdown block
must use the language `yaml {ci}`, e.g.

````markdown
```yaml {ci}
build:
  lazy: false
image:
  push: false
```
````

**The default attributes are described in the
[merge-request template here](../../.gitlab/merge_request_templates).**

### Change-Detection

The CI uses change-detection between two Git refs (`git diff <before> HEAD`) to
get all changed paths and then `quitsh`
[will decide when computing](../../tools/quitsh-custodian/pkg/custodian/pipeline/generate.go)
the execution order what is considered changed and only build the targets which
need to be built. This feature is opt-in with `lazy: true` (see
[CI attributes](#ci-attributes)). The change computation currently does not
incorporate unsuccessful builds on the CI. \_This is crucial when the CI failed
multiple times (e.g. on `main`). If the change computation only takes the diff
`HEAD~1..HEAD` on a next commit, this would resort to building only incomplete
stuff in regards to the last successful commit.

**TODO**: The change computation should compute the diff revisions `<before>`
and `<after>`, where `<before>` is the last successful build on the following
condition (see below visualization or
[see online](https://mermaid.live/edit#pako:eNqNUr1OwzAQfhXLc1MaRm-0VdtIVELQCWW5xpfEIrYjc0aqqo5sPAK8HE-CEwRNkSNhyYPvu-_nrDvywkrkgleK1g7aWuSGhVNYrRUxJQXL-Us6nU1n7PP9NedxOO3gj7cofDNKnA85ewemqEO1RKCrvSeyJspapaNWq-sLrxqLJ-uJaVAm0r0Qo0KLLNlBJVg_WppzRocWxSZbb27D3UUYywutH-fhLD2i0VXYB_ojeTbe3id3qsVGGfz3JFky779vwIxHjgbjEx5yBW0ZFuHYdeacatRBpVOXWIJvqAtzCq3gyT4cTMEFOY8T7qyvai5KaJ7Dy7cSCJcKKgf6t9qCebT2_EapyLrt9-r1G3j6Apa509Q)):

- On a _branch_ pipeline (not a merge-request pipeline): The last successful
  build on a commit on this branch must be taken. The CI on commit
  `CI-Branch-Pipeline` should use `<before> = A` which is the last successful
  build.

- On a Merge-Request pipeline: The last successful build is not helpful. The CI
  on commit `CI-MR-Pipeline` (which synced with the source branch: `main`) can
  just use `<before> = HEAD~1`.

- On a _tag_ pipeline: the last successful build of the last Git version tag
  must be taken. The CI on commit `CI-Tag` should take `<before> = v1.0.0`.

```mermaid
gitGraph:
    commit id: "v1.0.0 ✅"
    commit id: "v1.1.0 ❌"
    commit id: "A ✅"
    commit id: "B ❌"
    branch "feat/button"
    commit id: "F1 ❌"
    commit id: "F2 ✅"
    checkout main
    commit id: "C: ❌"
    commit id: "CI-Tag: v1.1.1" type:HIGHLIGHT
    commit id: "D ❌"
    checkout feat/button
    merge main type:HIGHLIGHT id: "CI-MR-Pipeline"
    checkout main
    commit id: "CI-Branch-Pipeline" type:HIGHLIGHT
    checkout feat/button
```

### Local Gitlab Runners

There are two scripts which help in setting up Gitlab runners. You can run them
locally or in a VM to startup a Gitlab runner in a **container**. This is only
for local use on your workstation and not the same as the
[Gitlab Runner](#deploy-nixos-vm) which we use.

- [start-gitlab-runner-podman.sh](../../tools/ci/scripts/start-gitlab-runner-podman.sh):
  Starting a sophisticated Gitlab runner in a Podman container which is
  isolated! The container which is started by this script runs its own `podman`
  engine! and thus the jobs run containers over the `podman` engine of the
  started container.

- [start-gitlab-runner-docker.sh](../../tools/ci/scripts/start-gitlab-runner-docker.sh):)
  Starting a normal Gitlab runner in a Docker container. The jobs run containers
  over the `docker` engine on the host (where you started this script -> mounted
  socket). **This a very insecure way of running the Gitlab Runner. You should
  not run this locally on your laptop.**

## Deploy NixOS VM

Read more in

- [this general section](./vms/general.md)
- [this section about the gitlab-runner VM](./vms/gitlab-runner.md)
