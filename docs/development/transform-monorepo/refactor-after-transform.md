# Refactoring after Transformation

- [ ] convention naming also apply to const in go - in this case camelCase
- [ ] put URI / URL and so on in variable
      (https://google.github.io/styleguide/go/decisions.html#initialisms)
- [x] remove all `/config` path variables and replace with `GetConfigPath` in
      `lib-common`.
- [ ] one module to rule them all: think of what does into
      `lib-common/pkgs/???`.
- [ ] all YAML files `config.yml`, `secrets.yml` -> need `camelCase` content.
- [ ] make all sparql templates embedded in the binary executable with
      `//go:generate go run script.go --output embedded-sparql-queries.go` to
      compile it into the binary.
- [ ] agree on error handling / format / lower or upper case (+ log only when
      error is handled)

- [ ] Move `ontolgy` directly to this repo in maybe `api/ontology`?
- [ ] Make all files in `ontology` camelCase`
- [ ] Provide Codeownerfile.
- [ ] English sentence for logs + errors
- [ ] Graph pkg : be more specific to narrow the "graph" to the graph we
      actually talk about. This means introducing types on which we can code
      methods etc. and run tests. The underlying type, e.g. UserGraph would
      referenc a private g.Graph, but would also have methods to interact with
      that stuff. Typesafety is key!
- [x] jena functions better documentation
- [ ] kb functions better documentation
- [ ] take out mongodb connection stuff to
      `lib-common/mongodb/{connection, helpers, queries,...}.go`
- [ ] graph pkg functions better documentation
- [ ] use `jwt.GetUserID()` instead of `jwt.GetClaimFromJWT(..., "sub")`
- [ ] make `jwt.GetClaimFromJWT()` private
- [ ] error messages sent to the user are super generic and do not say much
      about the issue

## Tooling

- [ ] Make Gopls work with all components which use Go -> look into go.work file
      and https://go.dev/ref/mod#workspaces
