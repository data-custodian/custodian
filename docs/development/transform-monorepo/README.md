# Restructuring into a Mono-Repo

After the discussion with Martin, we came to the conclusion it makes sense to
make `custodian` a proper mono-repo because it will profit from the advantages
of keeping the code/configs at a same place.

## Structure

The overall structure should be converted into:

<details>

```shell
custodian
├── .githooks/
├── .gitlab/
├── .editorconfig
├── .gitignore
├── justfile
├── README.md
├── CONTRIBUTION.md
├── CLAI.md
├── CLAC.md
├── LICENSE.md
├── CHANGELOG.md
│
├── api/
│   └── swagger
├── components/
│   ├── service-acs/ # each component acts as one independent thin
│   │   ├── .component.yaml # custom yaml for tooling,
│   │   │                   # e.g. contains path regexes for change-detection to trigger Ci builds etc.
│   │   ├── images/
│   │   │   ├── service/Containerfile
│   │   │   ├── db-migration/Containerfile # Each folder here builds a OCI container, which is somehow referenced
│   │   │                                  # in the manifests at the end. Note there might be more containers
│   │   │                                  # to build for one service.
│   │   ├── go.mod # will pull in `service-cms`
│   │   ├── src/
│   │   ├── internal/
│   │   ├── cmd/
│   │   ├── pkgs/
│   │   └── ...
│   ├── service-frontend/ # A possible frontend written in typescript/flutter,dart or what ever.
│   ├── service-gateway/
│   ├── service-kms/
│   ├── service-audit-trail/
│   ├── service-cms/
│   └── lib-common/
│       ├── internal   # functions/models/methods we do **not** wish to share with the community
│       │   ├── db
│       │   ├── graph
│       │   ├── mq
│       │   ├── crypto
│       │   ├── events
│       │   ├── util
│       │   └── ...
│       └── pkg   # functions/models/methods we want to share with the community (not yes soo important for now)
│           └── ...
├── deploy
│   └── manifests
│       ├── service-acs
│       │   └── templates
│       ├── service-audit-trail
│       │   └── templates
│       ├── service-cms
│       │   └── templates
│       ├── service-gateway
│       │   └── templates
│       └── service-db-jena
│           └── templates
├── docs/
├── external
│   └── ontology # The external ontology, if the tooling does not depend on this, it can live outside of this repo
└── tools/
    ├── nix/
    ├── ci/
    │   ├── go run scripts with common CI library for tooling in CI.
    └── secrets-generator
        ├── src/
        └── scripts/
```

</details>

## Reasoning & Vision

- The tooling makes it easier to view from toplevel each component as separate,
  such that we can add language independent stuff, like the `service-frontend`
  which might be written in typescript etc. Its no problem to have dependencies
  among components (attention: architecture decisions =)). With go we just
  include the relevant parts in `lib-common` or other components, rather go over
  `lib-common` if general stuff is needed.
- The `deploy` folder contains all manifests and stuff to deploy the components
  (the one which need deployments e.g. in the future `.component.yaml` can be
  used for tooling).
- The `tools` folder contains all tooling related aspects for CI and local
  development. Tooling for CI will be mainly written in Go, no `python` and
  minimal `bash`. The `tools` folder will contain other needed handy stuff (e.g.
  maybe also some Go tools etc.)
- CI in `.gitlab` will be written with Nix in mind to fix the toolchain. Nix can
  be in a later stage be used to also build directly the docker files (no
  `buildah`, no `docker` needed, Nix is the better OCI image builder =))
- Submodules will be avoided, maybe only `external/ontology` can be outsourced
  if its not used on the build. If `ontology` belongs to the API (exposed
  application prog. interface, dont think of endpoints, thing of it as a spec)
  it might be better to put it into `api/ontology` and have `api/endpoints`.
- The `api` folder on toplevel can stay for now, maybe it makes more sense to
  make it component dependent. But having it globally together might also be
  good.

## Transformation Procedure

The transformation is not about replacing tools, but only to make the structure
right. CI stays largely the same for now.

1.  Put the folder structure in place

1.  For every component (e.g. `components/banana`) write a `justfile` with the
    basic steps `build *args:` (more to come: `lint`, `test`, `package`,
    `deploy`).

    **Method 1:** (simpler)

    <details>

    - The `build` target will excute the **build script** `make.go`:

      ```shell
      build:
         cd /components/banana && \
            go run ${root_dir}/tools/ci/make.go build --component-dir ./
      ```

      This will read the `.component.yaml` inside the component.

    - The CI runs a command `ci-build` in the `justfile` because it will do the
      following:

      ```shell
      develop *args:
         cd /components/banana && \
             nix develop ./tools/nix#ci --command "$@"

      build *args:
         go run make.go build "$@"

      ci-build *args:
         just develop just build "$@"

      test *args:
         go run make.go test "$@"

      ci-test *args:
         just develop just build "$@"
      ```

    - The `nix-package` target in `components/banana/justfile` will call the
      components specific `./tools/nix/flake.nix` file with:

      ```shell
      nix-package:
        cd components/banana
        nix build ./tools/nix#service --out-dir ./build/package/service
        nix build ./tools/nix#images.service --out-dir ./build/images/service
      ```

      We could think of using either builders from `nixpkgs` or write our own
      Nix function which reuses `go run ./tools/ci/make.go build ...` in the
      `build` step. However the builders from `nixpkgs` have some additional
      considerations about hardening etc. and security related stuff which we
      would need to also use later on.

    - Each component has its own targets `ci-<what>` where
      `<what> := `build|test|lint|package|deploy`.

    - The `${root_dir}/tools/ci` should be build by Nix as well into
      `build-tool` derivation such that we can hand this over to the different
      Nix component build functions as **input**.

    </details>

    **Method 2:**

    This will dispatch not over `build.go` script but a built `quitsh` binary
    (Go). Its more complicated which is probably not necessary.

    <details>

    - The `build` target will execute
      `nix develop ./tools/nix#build-envs.<language> --command quitsh build --component-dir .`
      which will enter a language-specfic `<language>` dev. shell and execute
      the `./tools/ci` Go tool `quitsh` and **run** it with parameters
      `build --component-dir .`.

    - The `quitsh` will read the `.component.yaml` (spec to be properly
      defined).

    - The **`quitsh`** will dispatch the build for different components which
      define their builder in `.component.yaml`: something like

      ```yaml
      name: banana # name of the component
      version: "1.0.0" # version of the component (also needed for Nix)

      dependencies: # decides when this component is considered changed and needs building.
        patterns:
          - ${comp:flower}/docs/**
          - ${dir:root}/docs/**
        use: # also look into these files and determine the dependency to other components from it.
          - ./go.mod

      build: &s
        builder: go

      lint: *s
      test: *s
      package:
        builder: nix-go
      deploy:
        builder: nix-docker
      ```

    For the `go` builder, the `quitsh build` by default will call
    `./tools/ci/builders/go/build.go::build(...)` which will orchestrate some
    `go generate`, `go build`, to the correct `./components/banana/build` output
    folder.

    </details

1.  Make the CI run with the new structure. Setup some `tools/ci/go.mod` ...
    module with common CI libs/scripts to be used in CI already.

1.  Make the helm deployment run properly, make some CI test for rendering the
    manifests (all of them).

1.  Merge everything to `main`.

## Further Work

- [x] Everybody rebases its work onto the `main` branch.
- [x] Branches remote unused branches get cleaned up or renamed to `feat/.*` or
      `fix/.*`. **[done]**

- [ ] CI gets improved:
  - [x] Format everything (global treeformat)
  - [x] Lint
  - [ ] Deploy
  - [x] Test
  - [x] Improve Caching (improve CI containers, investigate some Caching
        solutions)
  - [ ] Deploy: This means something different depending on the branch or tag.
    - On any branch: Build OCI images with either `buildah` or `Nix` (needs each
      component to be built with Nix [done])
    - On any branch: Test `kubectl apply --dry-run` works (needs a cluster
      connection).
    - [later]: On any PR branch: Run fast integration tests by pushing images to
      a test registry and running a integration test (see how Renku team does
      it.)
    - On main branch: Push images and apply manifests to `dev` environment
      (cluster).
    - [later]: On Release Tag: Push images and apply manifests to `staging`
      environment etc. etc.
    - [future]: If a proper build tool is needed we might emply `buck2` or a
      full Nix solution.
