For the RDF aspect of this use case, it's important to understand how FEGA/SIB
see a data request. From a Custodian (and DPV) point of view, we want to model
certain types of data requests as sort of templates. Depending on the purpose of
the research (to be specified using DUO codes and dpv:Process enumerations), the
processing that is to be done, we can imagine different types of measures that
should be abided by and shown proof for by the data requester.

For instance a data controller may state that dataset X may be described with
the following:

|            | Dataset X requirements                                         |
| ---------- | -------------------------------------------------------------- |
| Purpose    | Disease Specific                                               |
| Processing | Store, Analyze                                                 |
| Measures   | NDA, Ethics Board Approval, secure storage, encryption of data |

Then, a researcher wanting access to that dataset must similarly describe his
research (by providing an RDF data request):

|              | Dataset X requirements                                         | Researcher project Y data request                                                                                                                           |
| ------------ | -------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Purpose      | Disease Specific                                               | Lung cancer research                                                                                                                                        |
| Processing   | Store, Analyze                                                 | Store, Analyze                                                                                                                                              |
| Measures     | NDA, Ethics Board Approval, secure storage, encryption of data | Signed NDA, Signed Ethics board approval, Document outlining conformance to secure storage protocol, Document outlining conformance to encryption protocols |
| Instance URL |                                                                | Instance URL is filled                                                                                                                                      |
| ID on CEGA   |                                                                | ID is filled (automatically?)                                                                                                                               |

In the case above, the data custodian would check whether the researcher's data
request is well structured, and does not immediately violate some basic premises
for the use of the data (types of processing to be done, having all fields of a
request filled).

Then, it is (for now) up to the DAC to analyze whether the provided documents
are correct, and sign the data request using the custodian playground.

Open questions: How is the instance URL and CEGA ID filled? Do DUO codes
compeltely cover the use-case of FEGA, or do we need to supplement with DPV
purpose codes also? What do people fill in there now, are they already using DUO
codes for the purpose? How are the measures like "secure storage" or
"encryption" evaluated? What are some typical use cases/examples of data
controllers, data handlers in this use case?
