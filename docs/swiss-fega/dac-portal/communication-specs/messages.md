# Communication Messages from CEGA to SEGA

## RabbitMQ Messages

All RabbitMQ messages are
[described here](https://github.com/EGA-archive/LocalEGA/blob/master/src/handler/Messages%20format.md#dac-dataset-mapping-cega--fega)

The important ones for the Swiss DAC Portal are probably:

- [DAC Dataset Mapping](https://github.com/EGA-archive/LocalEGA/blob/master/src/handler/Messages%20format.md#dac-dataset-mapping-cega--fega)
- [DAC Members](https://github.com/EGA-archive/LocalEGA/blob/master/src/handler/Messages%20format.md#dac-members-cega--fega)
- [DAC Information](https://github.com/EGA-archive/LocalEGA/blob/master/src/handler/Messages%20format.md#dac-members-cega--fega)

### Questions

- Why are there no accession ids for the datasets (`EGAD\d{11}`) on messages
  like:
  [Dataset Mapping](https://github.com/EGA-archive/LocalEGA/blob/master/src/handler/Messages%20format.md#dac-members-cega--fega)
  How do you know to which DAC it was assigned?

  **Answer:** You can query the information on the archive with:

  ```shell
  curl https://metadata.ega-archive.org/datasets/EGAD00010001972/dacs | jq
  ```
