************************
Audit Trail
************************

.. image:: ../_static/images/audit_trail.png
  :width: 900
  :alt: Swiss Data Custodian Architecture: Audit Trail

Purpose
=========

The Audit Trail (AT) has a dual task:

- it collects and stores all relevant events that happen to resources that are under the guard of the Swiss Data Custodian
- it provides access for resource owners to monitor these events

Monitoring
===========

The Audit Trail provides an API endpoint for authenticated users, that can be accessed via the Reverse Proxy Gateway.
It allows resource owners to monitor all access attempts that happen on the resources that they own.

Gathering of Events
====================

Events are gathered in Custodian via a :term:`Rabbit Message Queue <Rabbit Message Queue>`. The other components of the Custodian such as :doc:`Contract Management System </architecture/cms>` and :doc:`Access Control System </architecture/acs>` report their events to this message queue. The Audit-Trail picks the events up from the message queue and inserts them into a :term:`Mongo Database <Mongo Database>`. The Mongo Dababase stores the events long term and makes them available for the Monitoring.

Events
========

The following events that are stored by the Audit Trail:
- contract creation and contract signing events are reported by the :doc:`Contract Management System </architecture/cms>`
- access requests are reported by the :doc:`Access Control System </architecture/acs>`

The events have the following structure:

.. code-block:: go
   :caption: Events as they are reported in Go

    CreatedAt:    time.Now(),
        OwnerUserIds: []string{"https://user/bob", "https://user/walter"},
        ActionUserId: "https://user/anna",
        ContractUri:  "https://contract2",
        ResourceId:   "https://resource3",
        ActionId:     "https://action/service",
        Success:      true,
        Message:      "The resource has been accessed",

.. code-block:: javascript
    :caption: Events as they are stored in the Mongodb

    {
      _id: ObjectId('65f13b22f4e0d3be1ef70a8e'),
      created_at: ISODate('2024-03-13T05:35:30.960Z'),
      owner_user_uris: [ 'https://user/walter' ],
      action_user_uri: 'https://user/susan',
      contract_uri: 'https://contract5',
      resource_uri: 'https://resource3',
      action_uri: 'https://action/access',
      success: true,
      message: 'The resource has been accessed'
    }

.. list-table::
    :widths: 10 30 10
    :header-rows: 1

    * - Field
      - Description
      - Datatype
    * - `_id`
      - internal id in the MongoDB
      - Mongodb ObjectID
    * - `created_at`
      - Timestamp of the event
      - Unix timestamp (`uint64`)
    * - `owner_user_uris`
      - List of URIs to the Resource Owner in the Knowledge Base
      - User URI
    * - `action_user_uri`
      - URI to the Resource in the Knowledge Base
      - User URI
    * - `contract_uri`
      - URI to the Resource in the Contract Knowledge Graph
      - Contract URI
    * - `resource_uri`
      - Resource accessed by the actor
      - URI to the Resource in the Knowledge Base
    * - `success`
      - Was the action denied or accepted?
      - `boolean`
    * - `message`
      - Optional message provind details on the event
      - `string`
