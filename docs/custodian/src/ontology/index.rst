*******************************
Ontology
*******************************

The `Custodian Ontology <https://gitlab.com/data-custodian/custodian-ontology>`__ aims to model contracts related to sharing research data. In practice, it employs the `Resource Description Framework (RDF) <https://en.wikipedia.org/wiki/Resource_Description_Framework>`__, `Web Ontology Language (OWL) <https://www.w3.org/OWL/>`__, and integrates the `Data Privacy Vocabulary (DPV) <https://w3c.github.io/dpv/dpv/>`__ in order to offer interoperable contracts. The model tries to be broad enough to be adapted to any real contract, but also flexible enough to adapt to any use case. This approach allows to include the wide-variety of existing contracts together with institution-specific clauses. Given the delicate balance of this process, the Custodian Ontology is a constant work in progress. 

.. image:: ../_static/images/custodian_ontology.png
  :width: 500
  :alt: Contract Ontology

In the current model, at the core of each contract is one or more instances of “Data Handling”, defining the entities involved, the content of the data, its purpose and the measures/restrictions applied to it. To formalize the model, the Custodian Ontology re-uses concepts from the `DPV Ontology <https://w3c.github.io/dpv/dpv/>`__.

There are three main benefits in using semantic contracts defined with the Custodian Ontology:

1. **Consistency**: The ontology offers a uniform approach to detailing contracts across diverse domains or initiatives, facilitating the sharing and reuse of contract information.
2. **Accuracy**: Built upon RDF and OWL standards, the ontology ensures the descriptions of contracts are accurate and unambiguous.
3. **Extensibility**: With its extensible design, the contract ontology allows the incorporation of new contract entities and relationships as necessary, making it adaptable to a wide range of contracts.

Note that the Custodian Ontology will come with a specification of its own, but that’s currently under construction.
