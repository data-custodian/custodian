*******************************
Interface
*******************************

For a more user-friendly interaction with the Custodian Ontology, we developed the Custodian Contract Creation Interface: a drag-and-drop playground to create, export and import contracts.
`Try the interface <https://dev.swisscustodian.ch/>`__

.. image:: ../_static/images/custodian_interface.png
  :width: 900
  :alt: Swiss Data Custodian Contract Creation Interface

In short, you can drag all the elements from the left column to the main canvas on the right. Elements have white dots on either side called “connectors”, which are used to connect elements. To do so, click on one connector and drag it to another one. On the top right you can find a toolbar where you can: 

.. |maximize_button| image:: ../_static/images/enter_full_screen_icon.png
  :width: 30
  :alt: enter full screen mode icon

.. |minimize_button| image:: ../_static/images/exit_full_screen_icon.png
  :width: 30
  :alt: exit full screen mode icon

.. |load_button| image:: ../_static/images/open_folder_icon.png
  :width: 30
  :alt: open folder icon

.. |save_button| image:: ../_static/images/save_icon.png
  :width: 30
  :alt: save icon   

.. |delete_button| image:: ../_static/images/delete_icon.png
  :width: 30
  :alt: delete icon   

.. |export_button| image:: ../_static/images/download_icon.png
  :width: 30
  :alt: download icon  

- Minimize or maximize nodes |minimize_button| |maximize_button| : use this option if your contract interface is getting crowded or you’d like to see more details about each node
- Load |load_button| or save |save_button| a diagram: in order to share progress with a collaborator or simply saving a template, this will turn a diagram into a JSON file and vice versa
- Use the delete button |delete_button| to remove all elements and start a new clean canvas
- Export the contract in JSON-LD |export_button| to have an interoperable and machine readable format. This is the format in which the Swiss Data Custodian interacts with contracts.

The code for the Web UI interface can be found `here <https://gitlab.com/data-custodian/custodian-contract-interface>`__. In case of bugs, feature requests or questions, don’t hesitate to open an issue.   

How does the Custodian Contract Creation Interface interact with the Swiss Data Custodian?
==========================================================================================

The Custodian Contract Creation Interface can interact with the Swiss Data Custodian as an external application to interactively create contracts. In such cases the drag and drop blocks available in the interface will reflect the content of the host Knowledge Base. Additionally, once contracts have been created, they are sent as requests to the :term:`Contract Management System <Contract Management System (CMS)>`, where they are stored. 

In practice the Interface interacts with the Swiss Data Custodian via the Gateway and connects to the http API endpoint of the :term:`Contract Management System <Contract Management System (CMS)>`

What does a skeleton contract look like?
==========================================

.. image:: ../_static/images/contract_in_ui.png
  :width: 900
  :alt: Swiss Data Custodian Architecture
