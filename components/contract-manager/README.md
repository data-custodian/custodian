# Consent Management Engine (cms)

This folder contains the services handling consents.

- contract-signature-endpoint: REST API to upload, delete, verify, revoke or
  display a document [contract - ontology - signature]

# How to Run the Service With Docker Compose

Before running docker-compose, you must configure the environment variables in
the `docker-compose.yml` file.

Specify the following environment variables for the contract-signature-endpoint:

- `SERVER_HOSTNAME`: the hostname to be used by the contract-endpoint
- `SERVER_PORT`: the port the contract-endpoint listens to

Create a folder with the secret files and reference them in the
`docker-compose.yml` file.

Then, you can run the following command in a terminal, from the repository
containing the `docker-compose.yml` file:

```
docker compose up
```

## API

#### Upload the ontology

```
curl -X POST http://172.23.0.3:8004/cms/ontology -H "Content-Type:application/ld+json" -T '/path/to/ontology/ontology.jsonld'
```

#### Delete the ontology

```
curl -X DELETE http://172.23.0.3:8004/cms/ontology -H "Content-Type:application/ld+json"
```

#### Post a contract

File (JSON-LD):

```
curl -X POST http://172.23.0.3:8004/cms/contract -H "Content-Type:application/ld+json" -T '/path/to/file.jsonld'
```

String:

```
curl -X POST http://172.23.0.3:8004/cms/contract/upload -H "Content-Type: application/ld+json" --data-raw '{
  "@context": {
    "dcat1": "https://www.w3.org/TR/vocab-dcat-2/#",
    "dpv": "https://w3id.org/dpv#",
    "euthemes": "http://publications.europa.eu/resource/authority/data-theme/",
    "sdc": "http://swisscustodian.ch/vocab#"
  },
  "@graph": [
    {
      "@id": "sdc:dataHandling2",
      "@type": "dpv:DataHandling",
      "sdc:projectID": "Alice Big Project",
      "sdc:usage": "Service",
      "dpv:hasDataController": {
        "@id": "sdc:bob%40example.com"
      },
      "dpv:hasDataProcessor": {
        "@id": "sdc:alice%40example.com"
      },
      "dpv:hasPersonalData": {
        "@id": "sdc:PersonalData"
      },
      "dpv:purpose": "Research"
    },
    {
      "@id": "sdc:PersonalData",
      "@type": "dcat1:Dataset",
      "dcat1:downloadURL": {
        "@id": "https://opendata.swiss/en/dataset/covid-19-fallzahlen-kanton-thurgau-nach-alter-und-geschlecht"
      },
      "dcat1:theme": {
        "@id": "euthemes:heal"
      }
    },
    {
      "@id": "sdc:alice%40example.com",
      "@type": "dpv:DataProcessor",
      "sdc:email": "alice@example.com",
      "dpv:hasIdentifier": "791659e7-ff7d-4b40-abbb-952eb0d82b33"
    },
    {
      "@id": "sdc:bob%40example.com",
      "@type": "dpv:DataController",
      "sdc:email": "bob@example.com",
      "dpv:hasIdentifier": "6053c57d-17e6-476c-92e9-77736db38982"
    }
  ]
}
'
```

#### Get a document

```
curl -X GET http://172.23.0.3:8004/cms/contract/{contract-uuid} -H "Content-Type: application/ld+json"
```

#### Delete a document

Possible only if not already signed

```
curl -X DELETE http://172.23.0.3:8004/cms/contract/{contract-uuid} -H "Content-Type: application/ld+json"
```

## Security known issues

- Currently, and for test purpose, the Keycloak token is used but not verified.
- Communication with the Jena DBs are through HTTP.

# Signature Webservice

This REST API can be used to sign or revoke an existing contract.

The webservice receives requests and passes them in the messages queue.

## Installation

### The Dependencies

The code has been built with `Go 1.19.2`, but should work with any version that
supports Go modules.

To install the dependencies, open a terminal in the `code` folder and type

```
go get
```

The signature endpoint uses either Pulsar or Kafka as message queue. Make sure
that the message queue you configured the code for (default is Pulsar) is
currently running before running the code.

### Configuration

Create a `config.yml` file with the following content using the template
`config.docker.yml` or `config.local.yml.dist`

```sh
cp config.local.yml.dist config.yml
```

### Build and Run

To build and run the code, open a terminal in the `custodian/contract-manager`
folder and run the following command:

```
go build -o contract-signature-endpoint cmd/main.go
./contract-signature-endpoint
```

### Secrets

Create a `secrets.yaml` file with the following content using the template
`secrets.dist.yml`

```sh
cp secrets.dist.yml secrets.yml
```

Fill in the secrets according to your OIDC configuration.

## Usage

The list of endpoints and their description can be found in `api/swagger.yml`

## In-memory storage

In order to test the logic of the CMS alone - without all other components and
dependency to Jena -, it is possible to make it run as a standalone. There is an
in-memory storage implemented.

To make it run, you need to modify the `cmd/main.go` to use the proper storage

```
storage := cms.JenaStorage{Jena: jena.New("config")}
//storage := cms.InMemoryStorage{DBContract: map[id.ContractID]g.Graph{}, DBOntology: map[id.OntologyID]g.Graph{}}
//storage.Create()

```

Comment the first line and uncomment the two others. Then you can make it run as
explained above.

The in-memory storage implements the same methods as the jena storage.

Here is the token of Bob (Data controller) - for tests purposes

`eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJXRGdUdXc4dnFscEdLUkU0aWFNSEZfOGRwM0tkMXlrcU1JT00wZVMzOU44In0.eyJleHAiOjE3MTA0MjYxNTAsImlhdCI6MTcxMDQyNTg1MCwiYXV0aF90aW1lIjoxNzEwNDI1ODUwLCJqdGkiOiIxN2QyYTNkZS0xZmEwLTRhNDktOWUxZi05NDEzNTVmNzVmYmQiLCJpc3MiOiJodHRwczovL2F1dGguZGV2LnN3aXNzY3VzdG9kaWFuLmNoL2F1dGgvcmVhbG1zL2N1c3RvZGlhbi1wbGF5Z3JvdW5kIiwiYXVkIjoiY3VzdG9kaWFuIiwic3ViIjoiNjA1M2M1N2QtMTdlNi00NzZjLTkyZTktNzc3MzZkYjM4OTgyIiwidHlwIjoiSUQiLCJhenAiOiJjdXN0b2RpYW4iLCJzZXNzaW9uX3N0YXRlIjoiZmVhOWUxOTQtZTM4My00ZThjLThiYjAtMjA1MjYzYzkxYzhjIiwiYXRfaGFzaCI6IkVGMUJ1NWg4aEV1MEtkOFl1YjhJeHciLCJhY3IiOiIxIiwic2lkIjoiZmVhOWUxOTQtZTM4My00ZThjLThiYjAtMjA1MjYzYzkxYzhjIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsIm5hbWUiOiJCb2IgRG9lIiwiZ3JvdXBzIjpbImRhdGEtb3duZXIiLCJkZWZhdWx0LXJvbGVzLWN1c3RvZGlhbi1wbGF5Z3JvdW5kIiwib2ZmbGluZV9hY2Nlc3MiLCJ1bWFfYXV0aG9yaXphdGlvbiJdLCJ1c2VyX3VyaSI6Imh0dHBzOi8vc3dpc3NkYXRhY3VzdG9kaWFuLmNoL2JvYiIsInByZWZlcnJlZF91c2VybmFtZSI6ImJvYiIsImdpdmVuX25hbWUiOiJCb2IiLCJmYW1pbHlfbmFtZSI6IkRvZSIsImVtYWlsIjoiYm9iQGV4YW1wbGUuY29tIn0.qC8zVrz89MOWzJJdxFwyvtemHh4z3VS3cVbTdd1xCfLq3sYCRLdVWpPydOeUWnxH6Tpn_kHSJGEN4GcOF985aUInk0VgdJXH1qiOYc6llOEqqlYq70WLORr8KFWjEjWI3yJMqhL_pNcs6YJyzDnrmDRT5l3xs9x9hEYuRGXfo1zuBn6NLokAipR3TiieOYlmCTsR_YnVNZ0FfNFlufiNd5JHKE8PM0x3cEhvot4g4Fw31SaGXrFpBjXWcQnItzI8Qdc0v4w8KSuTmpaTm6YgT1T2WjIRoQpMPBqmz0JMsitl7wk5nXfM9nwE3XAIBcMnfwJ_B0qqWsuLod1UsMelHg`
