{
  lib,
  pkgs,
  service,
}:
let
  fs = lib.fileset;
  cnLib = lib.custodian;

  rootDir = cnLib.filesets.rootDir;
  compPath = cnLib.components.getRootPath service.pname;

  # Add the sparql files to the image.
  sparqlFiles = fs.fromSource (compPath + "/sparql");
  ontologyFiles = fs.fromSource (rootDir + "/external/ontology/contract-ontology-dpv2-shapes.ttl");

  auxSrc = fs.toSource {
    # Set the root in the generated `/nix/store/...` path to this folder.
    root = rootDir;
    fileset = fs.unions [
      sparqlFiles
      ontologyFiles
    ];
  };
in
pkgs.dockerTools.buildLayeredImage {
  name = "custodian/${service.pname}-service";
  tag = service.version;

  contents = [ service ];

  fakeRootCommands = ''
    ${pkgs.dockerTools.shadowSetup}
    mkdir -p /workspace/ontology
    ln -s "${auxSrc}/sparql" "/workspace/sparql"
    ln -s "${auxSrc}/external/contract-ontology-dpv2-shapes.ttl" "/workspace/ontology/contract-ontology-dpv2-shapes.ttl"

    groupadd -r non-root
    useradd -r -g non-root non-root
    chown non-root:non-root /workspace
  '';
  enableFakechroot = true;

  config = {
    Entrypoint = [ "${service}/bin/${service.pname}" ];
    WorkingDir = "/workspace";
    Volumes = {
      "/workspace/config" = { };
    };
    Labels = {
      "org.opencontainers.image.source" = "https://gitlab.com/data-custodian/custodian";
      "org.opencontainers.image.description" = service.meta.description;
      "org.opencontainers.image.license" = service.meta.license.shortName;
      "org.opencontainers.image.version" = service.version;
    };
    User = "non-root";
  };
}
