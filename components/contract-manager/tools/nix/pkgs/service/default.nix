{ lib, pkgs }:
let
  cnPkgs = pkgs.custodian;
  cnLib = lib.custodian;

  compName = "contract-manager";
in
cnPkgs.buildGoModule {
  inherit compName;
  pname = compName;
  version = cnLib.components.readVersion compName;

  src = cnLib.filesets.toSource [
    compName
    "lib-common"
    "ontology"
  ];

  target = "service";
  vendorHash = "sha256-HlfJybOtITpCHy1aZjU//jGDs3Y28MTPLUAaZKBFlFY=";

  meta = {
    description = compName;
    homepage = "https://gitlab.com/data-custodian/custodian";
    license = lib.licenses.agpl3Plus;
    maintainers = [ "sdcs-ordes" ];
    mainProgram = compName;
  };
}
