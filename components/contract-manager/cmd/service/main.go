package main

import (
	cms "custodian/components/contract-manager/internal"
	"custodian/components/contract-manager/internal/config"
	cmc "custodian/components/lib-common/pkg/config"
	"custodian/components/lib-common/pkg/jena"
	"custodian/components/lib-common/pkg/log"
	"custodian/components/lib-common/pkg/rabbitmq"
	"net/http"
	"time"
)

const (
	readTimeOut  = 5 * time.Minute
	writeTimeOut = 10 * time.Second
)

// @Version 0.1.0
// @Title Custodian CMS API
// @Description We offer some API that provides custodian services
// @ContactName Custodian Team
// @ContactEmail sdsc@epfl.ch
// @ContactURL https://gitlab.com/data-custodian/custodian
// @TermsOfServiceUrl https://gitlab.com/data-custodian/custodian
// @LicenseName GNU AGPLv3
// @LicenseURL https://gitlab.com/data-custodian/custodian/-/blob/develop/LICENSE.AGPL?ref_type=heads
// @Server 172.23.0.3:8004/cms
// @Security AuthorizationHeader read write
// @SecurityScheme AuthorizationHeader http bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9...

func main() {
	log.Setup()

	conf, err := cmc.LoadConfigs[config.Config]()
	if err != nil {
		log.ErrorE(err, "Failed loading config files.")

		return
	}

	storage := &cms.JenaStorage{Jena: jena.New(
		conf.Storage.Jena.Host,
		conf.Storage.Jena.Scheme,
		conf.Storage.Jena.CredentialFile,
		conf.Storage.Jena.CaTest)}
	// storage := &cms.InMemoryStorage{DBContract: map[id.ContractID]g.Graph{}, DBOntology: map[id.OntologyID]g.Graph{}}
	// storage.Create()

	log.Info("Storage", "storage", storage)

	// RabbitMR connection
	queue, err := rabbitmq.NewRabbitMQConnection(
		conf.RabbitMQ.URL,
		&conf.RabbitMQ.Credentials.Username,
		&conf.RabbitMQ.Credentials.Token,
	)
	if err != nil {
		log.ErrorE(err, "Connection to RabbitMQ failed.")
	}

	// Creates a handler that contains contextual information -> jena storage and some config information.
	handler := cms.NewHandler(
		storage,
		queue,
		conf.ECDSAKeys.CMSPrivateKey,
		conf.ECDSAKeys.CMSPublicKey,
	)
	router := cms.NewRouter(handler, conf.Server.ComponentURLPath)

	// TODO: add SSL authentication (client AND server).

	err = handler.UploadOntology(conf.Ontology.Shapes, conf.Ontology.Enums)
	if err != nil {
		log.Error("error while uploading ontology", err)

		return
	}
	host := conf.Server.Hostname + ":" + conf.Server.Port
	server := &http.Server{
		Addr:         host,
		Handler:      router,
		ReadTimeout:  readTimeOut,
		WriteTimeout: writeTimeOut,
	}
	log.Info("Server is running at " + conf.Server.Port + " port.")
	err = server.ListenAndServe()
	if err != nil {
		return
	}
}
