package cms

import (
	"custodian/components/lib-common/pkg/crypto"
	"custodian/components/lib-common/pkg/errors"
	g "custodian/components/lib-common/pkg/graph"
	utils "custodian/components/lib-common/pkg/http"
	"custodian/components/lib-common/pkg/id"
	"custodian/components/lib-common/pkg/owl"
	pred "custodian/components/lib-common/pkg/predicates"
	"encoding/hex"
)

type InMemoryStorage struct {
	DBContract map[id.ContractID]*g.Graph
	DBOntology map[id.OntologyID]*g.Graph
}

// ======================================================
// CMS methods

func (m *InMemoryStorage) Create() {
	m.DBContract = make(map[id.ContractID]*g.Graph)
	m.DBOntology = make(map[id.OntologyID]*g.Graph)
}

func (m *InMemoryStorage) GetGraphHash(contract id.ContractID) (string, error) {
	graph, ok := m.DBContract[contract]
	if !ok {
		return "", errors.New("contract %s does not exist", contract.URI())
	}

	graphFormatStr := g.GraphToFormatString(graph)
	hash, err := crypto.Hash([]byte(graphFormatStr))
	if err != nil {
		return "", err
	}
	hexHash := hex.EncodeToString(hash)

	return hexHash, nil
}

func (m *InMemoryStorage) RemoveContract(contract id.ContractID) error {
	// Check if signature exists for the contract.
	status, err := m.askIfSignatureExists(contract)
	if err != nil {
		return err
	}
	// If the contract was not signed, we can delete it.
	if !status {
		delete(m.DBContract, contract)

		return nil
		// If the signature exists for a contract, then it cannot be deleted.
	} else {
		return errors.New("the contract %s has been signed, therefore you cannot delete it: revoke it instead",
			contract.URI())
	}
}

// GetContract retrieve a contract.
func (m *InMemoryStorage) GetContract(contract id.ContractID) (*g.Graph, error) {
	if value, ok := m.DBContract[contract]; ok {
		return value, nil
	}

	return nil, errors.New("contract not found")
}

func (m *InMemoryStorage) UploadContract(contractG *g.Graph, contract id.ContractID, _ontologyEnum *g.Graph) error {
	// check that the contract is unique, not already signed, has no node orphan and no new custodian object is created.
	isValid := owl.CheckOWL(contractG)
	if !isValid {
		return errors.New(
			"the contract is not unique, contains a signature, " +
				"has orphan nodes or an illegal object has been created",
		)
	}
	// TODO : check shacl shape without Jena :)
	if _, ok := m.DBContract[contract]; ok {
		return errors.New("contract already exists")
	}
	m.DBContract[contract] = contractG

	return nil
}

func (m *InMemoryStorage) GetContractStatus(
	contract id.ContractID,
	_user id.GeneralID,
) (signaturesStatus map[string]string, err error) {
	graph, ok := m.DBContract[contract]
	if !ok {
		return nil, errors.New("contract not found")
	}
	signaturesStatus, err = g.GetSignaturesStatus(graph)
	if err != nil {
		return
	}

	return
}

func (m *InMemoryStorage) GetOntology() (*g.Graph, error) {
	if value, ok := m.DBOntology[id.GetOntologyIDCustodian()]; ok {
		return value, nil
	}

	return nil, errors.New("ontology not found")
}

func (m *InMemoryStorage) UploadOntology(ontology *g.Graph) error {
	if _, ok := m.DBOntology[id.GetOntologyIDCustodian()]; ok {
		return errors.New("ontology already exists")
	}
	m.DBOntology[id.GetOntologyIDCustodian()] = ontology

	return nil
}

// GetContractByUserUUID get all the contracts associated to a user.
func (m *InMemoryStorage) GetContractByUserUUID(
	user id.GeneralID,
) (ids []id.ContractID, err error) {
	var keys []id.ContractID
	for _, val := range m.DBContract {
		for _, item := range val.Graphs[g.DefaultName] {
			if item.Predicate.GetValue() == g.DataController ||
				item.Predicate.GetValue() == g.DataProcessor {
				if item.Object.GetValue() == user.URI() {
					k, _ := id.NewContractIDFromURI(item.Subject.GetValue())
					keys = append(keys, k)

					break
				}
			}
		}
	}

	return keys, nil
}

// SignContract creates a signature and appends it to the contract.
func (m *InMemoryStorage) SignContract(
	contract id.ContractID,
	_signature id.GeneralID,
	signatureTriples string,
) error {
	value, ok := m.DBContract[contract]
	if !ok {
		return errors.New("contract not found")
	}
	signaturesG, err := g.SerializeToGraph([]byte(signatureTriples), utils.Triples)
	if err != nil {
		return errors.New("could not serialize signatures")
	}
	quads := (value).Graphs[g.DefaultName]
	quads = append(quads, signaturesG.Graphs[g.DefaultName]...)
	value.Graphs[g.DefaultName] = quads

	return nil
}

// Find the signature document according to contract id.
// TODO : take source field as the contract id -> is that correct?
func (m *InMemoryStorage) askIfSignatureExists(contract id.ContractID) (bool, error) {
	graph, ok := m.DBContract[contract]
	if !ok {
		return false, errors.New("contract not found")
	}
	allSignaturesURI := g.GetObjectsByPredicate(
		graph,
		pred.SwissCustodian+"hasSignature",
	)

	return len(allSignaturesURI) > 0, nil
}

// GetContractPolicy TODO.
/*
 * For now, policies have not been implemented yet so it will always return the "default policy"
 * Policies define criteria to allow or reject an access
 * For instance, data controller must have signed, user A must have completed security training ...
 */
func (m *InMemoryStorage) GetContractPolicy(_graph *g.Graph) string {
	return defaultVal
}
