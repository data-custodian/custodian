package cms

import (
	g "custodian/components/lib-common/pkg/graph"
	"custodian/components/lib-common/pkg/id"
)

type Policy interface {
	IsContractSigned(contract *g.Graph) (bool, error)
	CanSign(contract *g.Graph, signatureRequest string, user id.GeneralID) (bool, error)
}

type DefaultPolicy struct {
	// empty.
	// In this policy, we assume all users must have signed the contract to make it valid.
}

// validateStatusOfContract returns true only if all users have signed.
func (d DefaultPolicy) validateStatusOfContract(strings map[string]string) bool {
	for _, s := range strings {
		if s != valid && (s == "" || s == revoke) {
			return false
		}
	}

	return true
}

// IsContractSigned checks if the contract is signed by all data processor and data owner.
func (d DefaultPolicy) IsContractSigned(contract *g.Graph) (bool, error) {
	signaturesStatus, err := g.GetSignaturesStatus(contract)
	if err != nil {
		return false, err
	}
	contractValid := d.validateStatusOfContract(signaturesStatus)

	return contractValid, nil
}

// CanSign checks if the contract can be signed or revoked.
func (d DefaultPolicy) CanSign(contract *g.Graph, signatureRequest string, user id.GeneralID) (bool, error) {
	signaturesStatus, err := g.GetSignaturesStatus(contract)
	if err != nil {
		return false, err
	}
	userSign := signaturesStatus[user.URI()]
	// If already signed and user want to resign it, quit.
	if userSign == valid && signatureRequest == valid {
		return false, nil
	}
	// If already revoked and user want to re-revoke it, quit.
	if userSign == revoke && signatureRequest == revoke {
		return false, nil
	}

	return true, nil
}
