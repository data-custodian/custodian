package cms

import (
	"custodian/components/lib-common/pkg/crypto"
	g "custodian/components/lib-common/pkg/graph"
	"custodian/components/lib-common/pkg/id"
	"custodian/components/lib-common/pkg/log"
	"encoding/hex"

	"github.com/google/uuid"
)

func (h Handler) newSignatureRequest(
	contract id.ContractID,
	user id.GeneralID,
	signatureStatus string) (*g.Graph, error) {
	// TODO : is it relevant to check if the signature that is in the contract is valid ? [valid status, timestamp].
	contractHash, err := h.Storage.GetGraphHash(contract)
	if err != nil {
		return nil, err
	}
	log.Info("Get contract hash.", "contractHash", contractHash)
	signature := id.NewGeneralID(uuid.New())
	signatureG := g.CreateSignature(signature, contract, user, contractHash, signatureStatus)

	return signatureG, nil
}

func (h Handler) updateSignature(contract id.ContractID, _user id.GeneralID, signature *g.Graph) error {
	// TODO : is it relevant to check if the signature that is in the contract is valid ? [valid status, timestamp].
	// TODO : verify that the json received does not have more / custom fields added.
	// TODO : check that the fields are correct.
	/*
	   signee            # check it exists // check it exists and validates signature field with it
	   createdAt         # check time is in the past?
	   status            # check it is the right one (how?)
	   signatureMethod   # check it is the same
	   contractHash      # recompute?
	   signedAt          # check it is between now and createdAt
	   signatureOf       # check it is the uri of the contract
	   signedBy          # if empty, put custodian
	   signedFor         # if empty, put user's uri, otherwise, check uri the user's
	   signature         # verify hash
	*/
	triplesFormat := g.GraphToFormatString(signature)
	privateKey, err := crypto.ReadPrivateKeyFromFile(h.CMSPrivKey)
	if err != nil {
		return err
	}
	signatureHsigned, err := crypto.Sign([]byte(triplesFormat), privateKey)
	if err != nil {
		return err
	}
	signatureHsignedStr := hex.EncodeToString(signatureHsigned)
	signature = g.AddSignedSignature(signature, signatureHsignedStr)
	triplesStr := g.GraphToFormatString(signature)
	id := g.GetID(signature)
	err = h.Storage.SignContract(contract, id, triplesStr)
	if err != nil {
		return err
	}

	return nil
}
