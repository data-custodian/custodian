package cms

import (
	"custodian/components/lib-common/pkg/errors"
	g "custodian/components/lib-common/pkg/graph"
	"custodian/components/lib-common/pkg/id"
	"custodian/components/lib-common/pkg/jena"
	"custodian/components/lib-common/pkg/log"
	pred "custodian/components/lib-common/pkg/predicates"
	tmpl "custodian/components/lib-common/pkg/template"
)

func (s *JenaStorage) getContextualKnowledge(contractG *g.Graph) (*g.Graph, error) {
	// This graph will contain the contract and all the triples in the knowledgebase needed to validate the contract.
	contextualGraph, err := g.CopyGraph(contractG)
	if err != nil {
		return nil, err
	}
	users := g.GetObjectsByPredicate(contractG, g.DataController)
	dp := g.GetObjectsByPredicate(contractG, g.DataProcessor)
	ds := g.GetObjectsByPredicate(contractG, g.Dataset)
	users = append(users, dp...)
	// Get the information for the datasets,
	for _, d := range ds {
		data1 := map[string]interface{}{
			"SwissCustodian": pred.SwissCustodian,
			"Dcat":           pred.Dcat,
			"Data":           d,
		}
		query, e := tmpl.ReplaceInTemplate("sparql/query-get-dataset-by-uri.sparql.tmpl", data1)
		if e != nil {
			return nil, e
		}
		log.Info("Post query.", "query", string(query))
		dataset, e := s.Jena.FetchGraph(jena.KnowledgeBase, string(query))
		if e != nil {
			return nil, e
		}
		g.AppendGraph(contextualGraph, dataset)
	}
	// Get all information for each user.
	for _, u := range users {
		data1 := map[string]interface{}{
			"SwissCustodian": pred.SwissCustodian,
			"Data":           u,
		}
		query, e := tmpl.ReplaceInTemplate("sparql/query-get-user-by-uri.sparql.tmpl", data1)
		if e != nil {
			return nil, e
		}
		log.Info("Post query.", "query", string(query))
		userG, e := s.Jena.FetchGraph(jena.KnowledgeBase, string(query))
		if e != nil {
			return nil, e
		}
		g.AppendGraph(contextualGraph, userG)
	}

	return contextualGraph, nil
}

func (s *JenaStorage) prepareGraph(contractG *g.Graph) (*g.Graph, error) {
	ontologyEnums, err := s.Jena.GetGraph(jena.Ontology, id.GetOntologyEnumsIDCustodian())
	if err != nil {
		return nil, errors.AddContext(err, "error while getting ontology enums")
	}
	contextalGraph, err := s.getContextualKnowledge(contractG)
	if err != nil {
		return nil, errors.AddContext(err, "error while getting contextual knowledge")
	}
	g.AppendGraph(contextalGraph, ontologyEnums)
	log.Info("CGRAPH", contextalGraph)

	return contextalGraph, nil
}
