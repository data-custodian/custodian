package cms

import (
	"custodian/components/lib-common/pkg/crypto"
	"custodian/components/lib-common/pkg/errors"
	g "custodian/components/lib-common/pkg/graph"
	"custodian/components/lib-common/pkg/id"
	"custodian/components/lib-common/pkg/jena"
	"custodian/components/lib-common/pkg/log"
	"custodian/components/lib-common/pkg/owl"
	pred "custodian/components/lib-common/pkg/predicates"
	tmpl "custodian/components/lib-common/pkg/template"
	"encoding/hex"
	"strconv"
)

type JenaStorage struct {
	Jena jena.Jena
}

func (s *JenaStorage) GetGraphHash(contract id.ContractID) (string, error) {
	graph, err := s.Jena.GetGraph(jena.Contracts, contract)
	if err != nil {
		return "", err
	}

	graphFormatStr := g.GraphToFormatString(graph)
	hash, err := crypto.Hash([]byte(graphFormatStr))
	if err != nil {
		return "", err
	}
	hexHash := hex.EncodeToString(hash)

	return hexHash, nil
}

func (s *JenaStorage) RemoveContract(contract id.ContractID) error {
	// Check if signature exists for the contract.
	status, err := s.askIfSignatureExists(contract)
	if err != nil {
		return err
	}

	// If the contract was not signed, we can delete it.
	if !status {
		err = s.Jena.DeleteGraph(jena.Contracts, contract)
		// TODO check the code if it's 200 or 204.
		// Contract was removed.
		if err != nil {
			return err
		}

		return nil
		// If the signature exists for a contract, then it cannot be deleted.
	} else {
		return errors.New("the contract %s has been signed, therefore you cannot delete it: "+
			"revoke it instead", contract.URI())
	}
}

func (s *JenaStorage) RemoveTmpContract(contract id.ContractID) error {
	err := s.Jena.DeleteGraph(jena.Validation, contract)
	if err != nil {
		return err
	}

	return nil
}

// GetContract retrieve a contract.
func (s *JenaStorage) GetContract(contract id.ContractID) (*g.Graph, error) {
	graph, err := s.Jena.GetGraph(jena.Contracts, contract)
	if err != nil {
		return nil, err
	}

	return graph, nil
}

func (s *JenaStorage) uploadPreCondition(contractG *g.Graph, contract id.ContractID) error {
	// check that the contract is unique, not already signed, has no node orphan and no new custodian object is created.
	isValid := owl.CheckOWL(contractG)
	if !isValid {
		return errors.New("the contract is not unique, contains a signature, " +
			"has orphan nodes or an illegal object has been created")
	}

	graph, err := s.prepareGraph(contractG)
	if err != nil {
		return errors.AddContext(err, "error while getting contextual knowledge")
	}
	deleteCallback, err := s.Jena.PostTmpGraph(graph, contract)
	cleanUp := func() {
		err = deleteCallback()
		if err != nil {
			log.PanicE(err, "failed to clean")
		}
	}
	if err != nil {
		return err
	}
	validationReport, err := s.Jena.ValidateShaclShape(contract, id.GetOntologyIDCustodian())
	if err != nil {
		log.Error("Something wrong happened while validating shapes.", "error message", err)

		return errors.AddContext(err, "something wrong happened while validating shapes")
	}
	if !validationReport.Valid() {
		return errors.New("contract is not valid and has been removed : %s", validationReport.Report())
	}
	defer cleanUp()

	return nil
}

func (s *JenaStorage) UploadContract(contractG *g.Graph, contract id.ContractID) error {
	err := s.uploadPreCondition(contractG, contract)
	if err != nil {
		return err
	}

	err = s.Jena.PostGraph(contractG, contract, jena.Contracts)
	if err != nil {
		// The contract should not have been added but out of safety we try to delete it anyway.
		return err
	}

	return nil
}

// GetContractStatus check if a contract is valid or not.
func (s *JenaStorage) GetContractStatus(
	contract id.ContractID,
	_user id.GeneralID) (signaturesStatus map[string]string, err error) {
	graph, err := s.Jena.GetGraph(jena.Contracts, contract)
	if err != nil {
		return
	}
	signaturesStatus, err = g.GetSignaturesStatus(graph)
	if err != nil {
		return
	}

	return
}

func (s *JenaStorage) GetOntology() (*g.Graph, error) {
	graph, err := s.Jena.GetGraph(jena.Ontology, id.GetOntologyIDCustodian())
	if err != nil {
		return nil, errors.AddContext(err, "error while getting ontology")
	}

	return graph, nil
}

func (s *JenaStorage) UploadOntology(shapes *g.Graph, enums *g.Graph) error {
	data := map[string]interface{}{
		"Owl": pred.Owl,
	}
	query, err := tmpl.ReplaceInTemplate("sparql/query-check-if-ontology-exists.sparql.tmpl", data)
	if err != nil {
		return err
	}
	log.Info("Posting query.", "query", string(query))
	resp, err := s.Jena.FetchFilteredTriples(jena.Ontology, string(query))
	if err != nil {
		return errors.AddContext(err, "could not know if ontology exists")
	}
	val, err := strconv.ParseBool(resp[0])
	if err != nil {
		err = errors.AddContext(err, "data could not be parsed to bool")

		return err
	}
	if val {
		// No error returned if ontology already uploaded.
		// This means you can reboot the cms without an error.
		log.Info("Ontology already exists. Remove it first if you wish to upload a new one")

		return nil
	}
	log.Info("Upload ontology shapes.", "uri", id.GetOntologyIDCustodian().URI())
	err = s.Jena.PostGraph(shapes, id.GetOntologyIDCustodian(), jena.Ontology)
	if err != nil {
		return err
	}
	log.Info("Upload ontology enums.", "uri", id.GetOntologyEnumsIDCustodian().URI())
	err = s.Jena.PostGraph(enums, id.GetOntologyEnumsIDCustodian(), jena.Ontology)
	if err != nil {
		return err
	}

	return nil
}

// GetContractByUserUUID get all the contracts associated to a user.
func (s *JenaStorage) GetContractByUserUUID(user id.GeneralID) (ids []id.ContractID, err error) {
	data := map[string]interface{}{
		"Dpv":     pred.Dpv,
		"UserURI": user.URI(),
	}
	query, err := tmpl.ReplaceInTemplate("sparql/query-graph-uri-by-user.sparql.tmpl", data)
	if err != nil {
		return
	}
	response, err := s.Jena.FetchFilteredTriples(jena.Contracts, string(query))
	if err != nil || response == nil {
		return
	}
	for _, r := range response {
		cID, e := id.NewContractIDFromURI(r)
		if e != nil {
			log.Error("Could not get contract uri", "error message", err)
		}
		ids = append(ids, cID)
	}

	return
}

// SignContract creates a signature and appends it to the contract.
func (s *JenaStorage) SignContract(contract id.ContractID, signature id.GeneralID, signatureTriples string) error {
	data := map[string]interface{}{
		"ContractUri":      contract.URI(),
		"SignatureUri":     signature.URI(),
		"SignatureTriples": signatureTriples,
	}
	query, err := tmpl.ReplaceInTemplate("sparql/query-sign-contract.sparql.tmpl", data)
	if err != nil {
		log.Error("error while getting query", err)

		return err
	}
	log.Info("Posting query.", "query", string(query))
	err = s.Jena.UpdateGraph(jena.Contracts, string(query))
	if err != nil {
		return errors.AddContext(err, "could not post the signature")
	}

	return nil
}

// Find the signature document according to contract id,
// TODO : take source field as the contract id -> is that correct?
func (s *JenaStorage) askIfSignatureExists(contract id.ContractID) (bool, error) {
	graph, err := s.GetContract(contract)
	if err != nil {
		return false, errors.AddContext(err, "could not retrieve contract")
	}
	allSignaturesURI := g.GetObjectsByPredicate(graph, "https://swisscustodian.ch/doc/ontology#hasSignature")

	return len(allSignaturesURI) > 0, nil
}

// GetContractPolicy returns the policy associated to the contract.
/*
 * For now, policies have not been implemented yet so it will always return the "default policy"
 * Policies define criteria to allow or reject an access
 * For instance:
 " - data controller must have signed
 " - user A must-have completed security training ...
*/
func (s *JenaStorage) GetContractPolicy(_graph *g.Graph) string {
	// TODO.
	return defaultVal
}
