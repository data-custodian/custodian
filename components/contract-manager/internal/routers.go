package cms

import (
	"net/http"

	"github.com/gorilla/mux"
)

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

func NewRouter(h *Handler, componentURLPath string) *mux.Router {
	r := mux.NewRouter().StrictSlash(true)
	router := r.PathPrefix(componentURLPath).Subrouter()

	router.
		Methods("GET").
		Path("/ontology").
		Name("GetOntology").
		HandlerFunc(h.GetOntology)

	router.
		Methods("POST").
		Path("/contracts/{id}/sign").
		Name("SignContract").
		HandlerFunc(h.SignContract)

	router.
		Methods("POST").
		Path("/contracts/{id}/revoke").
		Name("RevokeContract").
		HandlerFunc(h.RevokeContract)

	router.
		Methods("PATCH").
		Path("/contracts/{id}/sign").
		Name("PatchSignContract").
		HandlerFunc(h.PatchSignContract)

	router.
		Methods("PATCH").
		Path("/contracts/{id}/revoke").
		Name("PatchRevokeContract").
		HandlerFunc(h.PatchRevokeContract)

	router.
		Methods("POST").
		Path("/contracts").
		Name("UploadContract").
		HandlerFunc(h.UploadContract)

	router.
		Methods("GET").
		Path("/contracts/{id}").
		Name("GetContract").
		HandlerFunc(h.GetContract)

	router.
		Methods("DELETE").
		Path("/contracts/{id}").
		Name("DeleteContract").
		HandlerFunc(h.DeleteContract)

	router.
		Methods("GET").
		Path("/contracts").
		Name("GetAllContractsForCurrentUser").
		HandlerFunc(h.GetAllContractsForCurrentUser)

	router.
		Methods("GET").
		Path("/contracts/{id}/status").
		Name("GetContractStatus").
		HandlerFunc(h.GetContractStatus)

	return router
}
