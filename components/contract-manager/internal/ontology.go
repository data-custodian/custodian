package cms

import (
	"custodian/components/lib-common/pkg/errors"
	g "custodian/components/lib-common/pkg/graph"
	utils "custodian/components/lib-common/pkg/http"
	"os"
)

// UploadOntology uploads the shapes to the database and returns the enums in a graph.
func (h Handler) UploadOntology(shapesPath string, enumsPath string) error {
	data, err := os.ReadFile(shapesPath)
	if err != nil {
		return errors.AddContext(err, "failed to read ontology shapes")
	}

	ontologyShapes, err := g.SerializeToGraph(data, utils.Turtle)
	if err != nil {
		return errors.AddContext(err, "ontology shapes could not be serialized")
	}

	data, err = os.ReadFile(enumsPath)
	if err != nil {
		return errors.AddContext(err, "failed to read ontology shapes")
	}

	ontologyEnums, err := g.SerializeToGraph(data, utils.Turtle)
	if err != nil {
		return errors.AddContext(err, "ontology shapes could not be serialized")
	}

	err = h.Storage.UploadOntology(ontologyShapes, ontologyEnums)
	if err != nil {
		return errors.AddContext(err, "ontology shapes could not be uploaded")
	}

	return nil
}
