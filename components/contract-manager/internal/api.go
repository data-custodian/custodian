package cms

import (
	auth "custodian/components/lib-common/pkg/authorization"
	g "custodian/components/lib-common/pkg/graph"
	utils "custodian/components/lib-common/pkg/http"
	"custodian/components/lib-common/pkg/id"
	"custodian/components/lib-common/pkg/jwt"
	"custodian/components/lib-common/pkg/log"
	mq "custodian/components/lib-common/pkg/rabbitmq"
	"net/http"

	"github.com/google/uuid"
)

// The signature can validate a contract or revoke it.
// These are the two only options.
const (
	revoke string = "REVOKE"
	valid  string = "VALID"

	defaultVal string = "default"
)

// SignContract sign a contract
// @Summary		Sign a contract
// @Description	Sign a contract. Once a contract is signed, it cannot be resigned by the same person.
// @Resource	contracts
// @Param		id		path		string	true	"Contract UUID" 	"123e4567-e89b-12d3-a456-426614174000"
// @Success		200			object	utils.ResponseStruct	"OK"
// @Failure		400			object	utils.ErrorResponse	"Bad request"
// @Failure		401			object	utils.ErrorResponse	"Unauthorized"
// @Failure		403			object	utils.ErrorResponse	"Forbidden"
// @Router		/contracts/{id}/sign [post]
//
//nolint:funlen
func (h Handler) SignContract(w http.ResponseWriter, r *http.Request) {
	if !jwt.CheckAuthTokenExists(r) {
		utils.EncodeError(w, "No auth token provided.", http.StatusUnauthorized)

		return
	}
	// Get contract id.
	contractUUID, err := utils.GetPathFromURL(r, "id")
	if err != nil {
		log.Error("error with contract id", "error message", err)
		utils.EncodeError(w, "Contract ID is missing or wrongly defined.", http.StatusBadRequest)

		return
	}
	// input sanitized.
	c, err := uuid.Parse(contractUUID)
	if err != nil {
		log.Error("error while parsing contract", "error message", err)
		utils.EncodeError(w, "Contract ID is missing or wrongly defined.", http.StatusForbidden)

		return
	}
	user, err := jwt.GetUserID(r)
	if err != nil {
		log.ErrorE(err, "Could not extract user ID.")
		utils.EncodeError(w, "Could not extract user ID.", http.StatusForbidden)

		return
	}
	contract := id.NewContractID(c)
	// Retrieve contract as graph object.
	contractGraph, err := h.Storage.GetContract(contract)
	if err != nil {
		log.Error("error with contract", err)
		utils.EncodeError(w, "Could not retrieve the contract.", http.StatusNotFound)

		return
	}
	// Check if the user can see this contract.
	hasAccess := auth.HasContractAccess(user, contractGraph)
	if !hasAccess {
		utils.EncodeError(w, "You cannot access this contract.", http.StatusForbidden)

		return
	}
	// Get contract policy.
	// For now, policy is always "default".
	contractPolicy := h.Storage.GetContractPolicy(contractGraph)
	var policy Policy
	if contractPolicy == defaultVal {
		policy = DefaultPolicy{}
	} else {
		utils.EncodeError(w, "Policy error", http.StatusBadRequest)

		return
	}
	canSign, err := policy.CanSign(contractGraph, valid, user)
	if err != nil || !canSign {
		log.Error("error with signature", "error message", err)
		utils.EncodeError(w, "An error occurred when signing the contract.", http.StatusBadRequest)

		return
	}
	signature, err := h.newSignatureRequest(contract, user, valid)
	if err != nil {
		log.Error("error with signature", "error message", err)
		utils.EncodeError(w, "An error occurred when signing the contract.", http.StatusBadRequest)

		return
	}
	signatureJsonld, err := g.GraphToJsonld(signature)
	if err != nil {
		log.Error("error with signature", "error message", err)
		utils.EncodeError(w, "An error occurred when signing the contract.", http.StatusBadRequest)

		return
	}
	// Event to MQ.
	// Retrieve the URI of the data controllers and current user.
	dataControllerURI := g.GetDataControllerURI(contractGraph)
	err = mq.SendEvent(h.Rabbitmq, user, contract,
		dataControllerURI, id.EmptyGeneralID(), "SIGN", true,
		"The resource has been signed.")
	if err != nil {
		log.Error("error with mq event", "error message", err)
		utils.EncodeError(w, "An error occurred when sending the event.", http.StatusBadRequest)

		return
	}
	utils.EncodeJSONLDStruct(w, signatureJsonld)
}

// RevokeContract revoke a contract
// @Summary		Revoke a contract
// @Description	Revoke a contract
// @Resource	contracts
// @Param		id		path		string	true	"Contract UUID"			Format(uuid)
// Example(123e4567-e89b-12d3-a456-426614174000)
// @Success		200			object	utils.ResponseStruct	"OK"
// @Failure		400			object	utils.ErrorResponse	"Bad request"
// @Failure		401			object	utils.ErrorResponse	"Unauthorized"
// @Failure		403			object	utils.ErrorResponse	"Forbidden"
// @Router		/contracts/{id}/revoke [post].
//
//nolint:funlen
func (h Handler) RevokeContract(w http.ResponseWriter, r *http.Request) {
	if !jwt.CheckAuthTokenExists(r) {
		utils.EncodeError(w, "No auth token provided.", http.StatusUnauthorized)

		return
	}
	// Get contract id.
	contractUUID, err := utils.GetPathFromURL(r, "id")
	if err != nil {
		log.Error("contract uuid is not valid", "error message", err, "contractUUID", contractUUID)
		utils.EncodeError(w, "Contract ID is missing or wrongly defined.", http.StatusBadRequest)

		return
	}
	// input sanitized.
	c, err := uuid.Parse(contractUUID)
	if err != nil {
		log.Error("error while parsing contract", "error message", err)
		utils.EncodeError(w, "Contract ID is missing or wrongly defined.", http.StatusForbidden)

		return
	}
	user, err := jwt.GetUserID(r)
	if err != nil {
		log.ErrorE(err, "Could not extract user ID.")
		utils.EncodeError(w, "Could not extract user ID.", http.StatusForbidden)

		return
	}
	contract := id.NewContractID(c)
	// Retrieve contract as graph object.
	contractGraph, err := h.Storage.GetContract(contract)
	if err != nil {
		log.Error("error with contract", err)
		utils.EncodeError(w, "Could not retrieve the contract.", http.StatusNotFound)

		return
	}
	// Check if the user can see this contract.
	hasAccess := auth.HasContractAccess(user, contractGraph)
	if !hasAccess {
		utils.EncodeError(w, "You cannot access this contract.", http.StatusForbidden)

		return
	}
	// Get contract policy.
	// For now, policy is always "default".
	contractPolicy := h.Storage.GetContractPolicy(contractGraph)
	var policy Policy
	if contractPolicy == defaultVal {
		policy = DefaultPolicy{}
	} else {
		utils.EncodeError(w, "Policy error", http.StatusBadRequest)

		return
	}
	canSign, err := policy.CanSign(contractGraph, revoke, user)
	if err != nil || !canSign {
		log.Error("error with signature", "error message", err)
		utils.EncodeError(w, "An error occurred when signing the contract.", http.StatusBadRequest)

		return
	}
	signatures, err := h.newSignatureRequest(contract, user, revoke)
	if err != nil {
		log.Error("error with signature", "error message", err)
		utils.EncodeError(w, "An error occurred when revoking the contract.", http.StatusTeapot)

		return
	}
	signatureJsonld, err := g.GraphToJsonld(signatures)
	if err != nil {
		log.Error("error with signature", "error message", err)
		utils.EncodeError(w, "An error occurred when signing the contract.", http.StatusBadRequest)

		return
	}
	// Event to MQ.
	// Retrieve the URI of the data controllers and current user.
	dataControllerURI := g.GetDataControllerURI(contractGraph)
	err = mq.SendEvent(h.Rabbitmq, user, contract,
		dataControllerURI, id.EmptyGeneralID(), "REVOKE", true,
		"Revoke signature created.")
	if err != nil {
		log.Error("error with mq event", "error message", err)
		utils.EncodeError(w, "An error occurred when sending the event.", http.StatusTeapot)

		return
	}
	utils.EncodeJSONLDStruct(w, signatureJsonld)
}

// PatchSignContract sign a contract
// @Summary		Sign a contract
// @Description	Sign a contract. Once a contract is signed, it cannot be resigned by the same person.
// @Resource	contracts
// @Param		id		path		string	true	"Contract UUID" 	"123e4567-e89b-12d3-a456-426614174000"
// @Success		200			object	utils.ResponseStruct	"OK"
// @Failure		400			object	utils.ErrorResponse	"Bad request"
// @Failure		401			object	utils.ErrorResponse	"Unauthorized"
// @Failure		403			object	utils.ErrorResponse	"Forbidden"
// @Router		/contracts/{id}/sign [patch].
//
//nolint:funlen
func (h Handler) PatchSignContract(w http.ResponseWriter, r *http.Request) {
	if !jwt.CheckAuthTokenExists(r) {
		utils.EncodeError(w, "No auth token provided.", http.StatusUnauthorized)

		return
	}
	// Get contract id.
	contractUUID, err := utils.GetPathFromURL(r, "id")
	if err != nil {
		log.Error("error with contract id", "error message", err)
		utils.EncodeError(w, "Contract ID is missing or wrongly defined.", http.StatusBadRequest)

		return
	}
	// input sanitized.
	c, err := uuid.Parse(contractUUID)
	if err != nil {
		log.Error("error while parsing contract", "error message", err)
		utils.EncodeError(w, "Contract ID is missing or wrongly defined.", http.StatusForbidden)

		return
	}
	user, err := jwt.GetUserID(r)
	if err != nil {
		log.ErrorE(err, "Could not extract user ID.")
		utils.EncodeError(w, "Could not extract user ID.", http.StatusForbidden)

		return
	}
	contract := id.NewContractID(c)
	// Retrieve contract as graph object.
	contractGraph, err := h.Storage.GetContract(contract)
	if err != nil {
		log.Error("error with contract", err)
		utils.EncodeError(w, "Could not retrieve the contract.", http.StatusNotFound)

		return
	}
	// Check if the user can see this contract.
	hasAccess := auth.HasContractAccess(user, contractGraph)
	if !hasAccess {
		utils.EncodeError(w, "You cannot access this contract.", http.StatusForbidden)

		return
	}
	// Get contract policy.
	// For now, policy is always "default".
	contractPolicy := h.Storage.GetContractPolicy(contractGraph)
	var policy Policy
	if contractPolicy == defaultVal {
		policy = DefaultPolicy{}
	} else {
		utils.EncodeError(w, "Policy error", http.StatusBadRequest)

		return
	}
	canSign, err := policy.CanSign(contractGraph, valid, user)
	if err != nil || !canSign {
		log.Error("error with signature", "error message", err)
		utils.EncodeError(w, "An error occurred when signing the contract.", http.StatusBadRequest)

		return
	}
	// Get the signature through the body.
	signatureBody := utils.DecodeBody(r)
	contentType := r.Header.Get("Content-Type")
	if ok := utils.CheckContentType(contentType); !ok {
		log.Debug("info", "content-type", contentType)
		utils.EncodeError(w, "Unsupported media type. Only '"+utils.Jsonld+"' and '"+utils.Turtle+"' "+
			"are accepted.", http.StatusUnsupportedMediaType)

		return
	}
	signatures, err := g.SerializeToGraph([]byte(signatureBody), contentType)
	if err != nil {
		log.ErrorE(err, "Could not serialize signature.")
		utils.EncodeError(w, "Could not serialize signature to graph.", http.StatusInternalServerError)

		return
	}
	err = h.updateSignature(contract, user, signatures)
	if err != nil {
		log.ErrorE(err, "Could not update signature.")
		utils.EncodeError(w, "An error occurred when signing the contract.", http.StatusInternalServerError)

		return
	}
	// Event to MQ.
	// Retrieve the URI of the data controllers and current user.
	dataControllerURI := g.GetDataControllerURI(contractGraph)
	err = mq.SendEvent(h.Rabbitmq, user, contract,
		dataControllerURI, id.EmptyGeneralID(), "SIGN", true,
		"The resource has been signed.")
	if err != nil {
		log.Error("error with mq event", "error message", err)
		utils.EncodeError(w, "An error occurred when sending the event.", http.StatusBadRequest)

		return
	}
	utils.EncodeResponse(w, "Contract has been signed")
}

// PatchRevokeContract revoke a contract
// @Summary		Revoke a contract
// @Description	Revoke a contract
// @Resource	contracts
// @Param		id		path		string	true	"Contract UUID"			Format(uuid)
// Example(123e4567-e89b-12d3-a456-426614174000)
// @Success		200			object	utils.ResponseStruct	"OK"
// @Failure		400			object	utils.ErrorResponse	"Bad request"
// @Failure		401			object	utils.ErrorResponse	"Unauthorized"
// @Failure		403			object	utils.ErrorResponse	"Forbidden"
// @Router		/contracts/{id}/revoke [patch].
//
//nolint:funlen
func (h Handler) PatchRevokeContract(w http.ResponseWriter, r *http.Request) {
	if !jwt.CheckAuthTokenExists(r) {
		utils.EncodeError(w, "No auth token provided.", http.StatusUnauthorized)

		return
	}
	// Get contract id.
	contractUUID, err := utils.GetPathFromURL(r, "id")
	if err != nil {
		log.Error("contract uuid is not valid", "error message", err, "contractUUID", contractUUID)
		utils.EncodeError(w, "Contract ID is missing or wrongly defined.", http.StatusBadRequest)

		return
	}
	// input sanitized.
	c, err := uuid.Parse(contractUUID)
	if err != nil {
		log.Error("error while parsing contract", "error message", err)
		utils.EncodeError(w, "Contract ID is missing or wrongly defined.", http.StatusForbidden)

		return
	}
	user, err := jwt.GetUserID(r)
	if err != nil {
		log.ErrorE(err, "Could not extract user ID.")
		utils.EncodeError(w, "Could not extract user ID.", http.StatusForbidden)

		return
	}
	contract := id.NewContractID(c)
	// Retrieve contract as graph object
	contractGraph, err := h.Storage.GetContract(contract)
	if err != nil {
		log.Error("error with contract", err)
		utils.EncodeError(w, "Could not retrieve the contract.", http.StatusNotFound)

		return
	}
	// Check if the user can see this contract.
	hasAccess := auth.HasContractAccess(user, contractGraph)
	if !hasAccess {
		utils.EncodeError(w, "You cannot access this contract.", http.StatusForbidden)

		return
	}
	// Get contract policy.
	// For now, policy is always "default".
	contractPolicy := h.Storage.GetContractPolicy(contractGraph)
	var policy Policy
	if contractPolicy == defaultVal {
		policy = DefaultPolicy{}
	} else {
		utils.EncodeError(w, "Policy error", http.StatusBadRequest)

		return
	}
	canSign, err := policy.CanSign(contractGraph, revoke, user)
	if err != nil || !canSign {
		log.Error("error with signature", "error message", err)
		utils.EncodeError(w, "An error occurred when signing the contract.", http.StatusBadRequest)

		return
	}
	// Get the signature through the body.
	signatureBody := utils.DecodeBody(r)
	contentType := r.Header.Get("Content-Type")
	if ok := utils.CheckContentType(contentType); !ok {
		log.Debug("info", "content-type", contentType)
		utils.EncodeError(w, "Unsupported media type. Only '"+utils.Jsonld+"' and '"+utils.Turtle+"' "+
			"are accepted.", http.StatusUnsupportedMediaType)

		return
	}
	signatures, err := g.SerializeToGraph([]byte(signatureBody), contentType)
	if err != nil {
		log.ErrorE(err, "Could not serialize signature.")
		utils.EncodeError(w, "Could not serialize signature to graph.", http.StatusInternalServerError)

		return
	}
	err = h.updateSignature(contract, user, signatures)
	if err != nil {
		log.Error("error with signature", "error message", err)
		utils.EncodeError(w, "An error occurred when revoking the contract.", http.StatusTeapot)

		return
	}
	// Event to MQ.
	// Retrieve the URI of the data controllers and current user.
	dataControllerURI := g.GetDataControllerURI(contractGraph)
	err = mq.SendEvent(h.Rabbitmq, user, contract,
		dataControllerURI, id.EmptyGeneralID(), "REVOKE", true,
		"The resource has been revoked.")
	if err != nil {
		log.Error("error with mq event", "error message", err)
		utils.EncodeError(w, "An error occurred when sending the event.", http.StatusTeapot)

		return
	}
	utils.EncodeResponse(w, "Contract has been revoked.")
}

// UploadContract upload a contract to the database
// @Summary		Upload a contract
// @Description	Upload a contract. Is automatically deleted if the shapes are not valid.
// @Resource	contracts
// @Accept		application/ld+json
// @Accept		text/turtle
// @Param		requestBody	body		utils.JSONLDResponseStruct	true "contract (json-ld or turtle)"
// @Success		200			object	utils.ResponseStruct	"OK"
// @Failure		400			object	utils.ErrorResponse	"Bad request"
// @Failure		401			object	utils.ErrorResponse	"Unauthorized"
// @Failure		403			object	utils.ErrorResponse	"Forbidden"
// @Router		/contracts [post].
func (h Handler) UploadContract(w http.ResponseWriter, r *http.Request) {
	// TODO check if users in contract exist
	if !jwt.CheckAuthTokenExists(r) {
		utils.EncodeError(w, "No auth token provided.", http.StatusUnauthorized)

		return
	}
	// Get the contract through the body
	contractBody := utils.DecodeBody(r)
	contentType := r.Header.Get("Content-Type")
	if ok := utils.CheckContentType(contentType); !ok {
		log.Debug("info", "content-type", contentType)
		utils.EncodeError(w, "Unsupported media type. Only '"+utils.Jsonld+"' and '"+utils.Turtle+"' "+
			"are accepted.", http.StatusUnsupportedMediaType)

		return
	}
	// Create a new contract ID,
	contractID := uuid.New()
	contract := id.NewContractID(contractID)
	log.Info("Upload contract.", "id", contract.URI())
	contractBodyGraph, err := g.SerializeToGraph([]byte(contractBody), contentType)
	if err != nil {
		log.Error("Contract cannot be parsed in graph object with contract.", "error message", err)
		utils.EncodeError(w, "An error occurred when uploading the contract.", http.StatusInternalServerError)

		return
	}
	err = h.Storage.UploadContract(contractBodyGraph, contract)
	if err != nil {
		log.Error("Could not upload the contract.", "error message", err)
		utils.EncodeError(w, "An error occurred when uploading the contract.", http.StatusInternalServerError)

		return
	}
	userURI, err := jwt.GetUserID(r)
	if err != nil {
		log.ErrorE(err, "Could not extract user ID.")
		utils.EncodeError(w, "Could not extract user ID.", http.StatusForbidden)

		return
	}
	// Event to MQ.
	// Retrieve the URI of the data controllers and current user.
	dataControllerURI := g.GetDataControllerURI(contractBodyGraph)
	err = mq.SendEvent(h.Rabbitmq, userURI, contract,
		dataControllerURI, id.EmptyGeneralID(), "UPLOAD REQUEST", true,
		"The resource has been uploaded.")
	if err != nil {
		log.Error("error with mq event", "error message", err)
		utils.EncodeError(w, "An error occurred when sending the event.", http.StatusInternalServerError)

		return
	}
	utils.EncodeResponse(w, contract.URI())
}

// GetContract retrieve a contract
// Pass the contract UUID by parameters
// @Title		Get a contract
// @Description	Get a contract.
// @Resource	contracts
// @Produce		application/ld+json
// @Produce		text/turtle
// @Param		id	path		string	true	"Contract UUID"	"123e4567-e89b-12d3-a456-426614174000"
// @Success		200			object	utils.JSONLDResponseStruct "Success (contract in JSON-LD)"
// @Failure		400			object	utils.ErrorResponse	"Bad request"
// @Failure		401			object	utils.ErrorResponse	"Unauthorized"
// @Failure		403			object	utils.ErrorResponse	"Forbidden"
// @Router		/contracts/{id} [get].
func (h Handler) GetContract(w http.ResponseWriter, r *http.Request) {
	if !jwt.CheckAuthTokenExists(r) {
		utils.EncodeError(w, "No auth token provided", http.StatusUnauthorized)

		return
	}
	acceptType := r.Header.Get("Accept")
	// Getting ID from URL.
	contractUUID, err := utils.GetPathFromURL(r, "id")
	if err != nil {
		log.Error("contract uuid is not valid", "error message", err, "contractUUID", contractUUID)
		utils.EncodeError(w, "Contract ID is missing or wrongly defined.", http.StatusBadRequest)

		return
	}
	// input sanitized.
	c, err := uuid.Parse(contractUUID)
	if err != nil {
		log.Error("error while parsing contract", "error message", err)
		utils.EncodeError(w, "Contract ID is missing or wrongly defined.", http.StatusForbidden)

		return
	}
	contract := id.NewContractID(c)
	log.Debug("Get document", "contract id", contract.URI())
	graph, err := h.Storage.GetContract(contract)
	if err != nil {
		log.Error("error with contract", "error message", err)
		utils.EncodeError(w, "Could not retrieve the contract.", http.StatusNotFound)

		return
	}

	user, err := jwt.GetUserID(r)
	if err != nil {
		log.ErrorE(err, "Could not extract user ID.")
		utils.EncodeError(w, "Could not extract user ID.", http.StatusForbidden)

		return
	}
	// Check if the user can see this contract
	hasAccess := auth.HasContractAccess(user, graph)
	if !hasAccess {
		utils.EncodeError(w, "You cannot access this contract.", http.StatusForbidden)

		return
	}
	// if accept header was specified, return turtle.
	if acceptType == utils.Turtle {
		ttl, e := g.GraphToTurtle(graph)
		if e != nil {
			log.Error("error with graph", "error message", err)
			utils.EncodeError(w, "An error occurred during the graph serialization.", http.StatusBadRequest)

			return
		}
		utils.EncodeTurtleStruct(w, ttl)

		return
	}
	// default return jsonld.
	jsonld, err := g.GraphToJsonld(graph)
	if err != nil {
		log.Error("error with graph", "error message", err)
		utils.EncodeError(w, "An error occurred during the graph serialization.", http.StatusBadRequest)

		return
	}
	utils.EncodeJSONLDStruct(w, jsonld)
}

// DeleteContract remove a contract from the DB if it was not signed
// Can be removed only if it was not signed
// @Summary		Delete a contract
// @Description	Delete a contract. A contract can be deleted if and only if it is not signed.
// @Resource	contracts
// @Param		id	path		string	true	"Contract UUID"	"123e4567-e89b-12d3-a456-426614174000"
// @Success		200			object	utils.ResponseStruct	"OK"
// @Failure		400			object	utils.ErrorResponse	"Bad request"
// @Failure		401			object	utils.ErrorResponse	"Unauthorized"
// @Failure		403			object	utils.ErrorResponse	"Forbidden"
// @Router		/contracts/{id} [delete].
func (h Handler) DeleteContract(w http.ResponseWriter, r *http.Request) {
	if !jwt.CheckAuthTokenExists(r) {
		utils.EncodeError(w, "No auth token provided", http.StatusUnauthorized)

		return
	}
	// Get contract ID.
	contractUUID, err := utils.GetPathFromURL(r, "id")
	if err != nil {
		log.Error("contract uuid is not valid", "error message", err, "contractUUID", contractUUID)
		utils.EncodeError(w, "Contract ID is missing or wrongly defined.", http.StatusBadRequest)

		return
	}
	// input sanitized.
	c, err := uuid.Parse(contractUUID)
	if err != nil {
		log.Error("error while parsing contract", "error message", err)
		utils.EncodeError(w, "Contract ID is missing or wrongly defined.", http.StatusForbidden)

		return
	}
	contract := id.NewContractID(c)
	graph, err := h.Storage.GetContract(contract)
	if err != nil {
		log.Error("error with contract", "error message", err)
		utils.EncodeError(w, "Could not retrieve the contract.", http.StatusNotFound)

		return
	}
	user, err := jwt.GetUserID(r)
	if err != nil {
		log.ErrorE(err, "Could not extract user ID.")
		utils.EncodeError(w, "Could not extract user ID.", http.StatusForbidden)

		return
	}
	// Check if the user can see this contract.
	hasAccess := auth.HasContractAccess(user, graph)
	if !hasAccess {
		utils.EncodeError(w, "You cannot access this contract.", http.StatusForbidden)

		return
	}
	err = h.Storage.RemoveContract(contract)
	if err != nil {
		log.Error("error while removing contract", "error message", err)
		utils.EncodeError(w, "The contract could not be deleted.", http.StatusTeapot)

		return
	}
	utils.EncodeResponse(w, "The contract was deleted.")
}

// GetOntology retrieve the ontology
// @Summary		Get the ontology
// @Description	This endpoint is for testing only. For production, the ontology will be uploaded during the
// Custodian's installation. Get the ontology
// @Resource	ontology
// @Produce		application/ld+json
// @Produce		text/turtle
// @Success		200			object	utils.JSONLDResponseStruct	"ontology in JSON-LD"
// @Failure		400			object	utils.ErrorResponse	"Bad request"
// @Failure		401			object	utils.ErrorResponse	"Unauthorized"
// @Failure		403			object	utils.ErrorResponse	"Forbidden"
// @Router			/ontology [get].
func (h Handler) GetOntology(w http.ResponseWriter, r *http.Request) {
	if !jwt.CheckAuthTokenExists(r) {
		utils.EncodeError(w, "No auth token provided.", http.StatusUnauthorized)

		return
	}
	acceptType := r.Header.Get("Accept")
	graph, err := h.Storage.GetOntology()
	if err != nil {
		log.Error("error while getting ontology", "error message", err)
		utils.EncodeError(w, "Ontology could not be retrieved.", http.StatusTeapot)

		return
	}
	// if accept header was specified, return turtle.
	if acceptType == utils.Turtle {
		ttl, e := g.GraphToTurtle(graph)
		if e != nil {
			log.Error("error with graph", "error message", e)
			utils.EncodeError(w, "Could not get ontology.", http.StatusBadRequest)

			return
		}
		utils.EncodeTurtleStruct(w, ttl)

		return
	}
	// default return jsonld.
	jsonld, err := g.GraphToJsonld(graph)
	if err != nil {
		log.Error("error with graph", "error message", err)
		utils.EncodeError(w, "Could not get ontology.", http.StatusBadRequest)

		return
	}
	utils.EncodeJSONLDStruct(w, jsonld)
}

// GetAllContractsForCurrentUser get the list of the contracts for the current user.
// @Summary		Retrieve a list of UUIDs representing all contracts belonging to the user currently logged in.
// @Description	Retrieve a list of UUIDs representing all contracts belonging to the user currently logged in.
// @Resource	contracts
// @Produce		json
// @Param		uri		query		string	true	"User uri"			Format(uri)	Example(http://example.com/1234)
// @Success		200			object	utils.UUIDResponseStruct	"Success"
// @Failure		400			object	utils.ErrorResponse	"Bad request"
// @Failure		401			object	utils.ErrorResponse	"Unauthorized"
// @Failure		403			object	utils.ErrorResponse	"Forbidden"
// @Router			/contracts [get].
func (h Handler) GetAllContractsForCurrentUser(w http.ResponseWriter, r *http.Request) {
	if !jwt.CheckAuthTokenExists(r) {
		utils.EncodeError(w, "No auth token provided.", http.StatusUnauthorized)

		return
	}
	user, err := jwt.GetUserID(r)
	if err != nil {
		log.ErrorE(err, "Could not extract user ID.")
		utils.EncodeError(w, "Could not extract user ID.", http.StatusForbidden)

		return
	}
	if user.URI() == "" {
		utils.EncodeError(w, "User uri is missing or wrongly defined.", http.StatusForbidden)

		return
	}
	contractUUIDStrArray, err := h.Storage.GetContractByUserUUID(user)
	if err != nil {
		log.Error("error while getting contract", "error message", err)
		utils.EncodeError(w, "Could not retrieve the contract.", http.StatusNotFound)

		return
	}
	array := []string{}
	for _, c := range contractUUIDStrArray {
		array = append(array, c.URI())
	}

	utils.EncodeUUIDResponse(w, array)
}

// GetContractStatus get the status of a contract
// @Summary		Check if the contract is valid or not
// @Description	Get the status of the contract [valid, not valid] and if the status of each user's signature
// @Resource	contracts
// @Produce		json
// @Param		id	path		string	true	"Contract UUID"	"123e4567-e89b-12d3-a456-426614174000"
// @Success		200	object	utils.ContractStatusResponse ""
// @Failure		400	object	utils.ErrorResponse	"Bad request"
// @Failure		401			object	utils.ErrorResponse	"Unauthorized"
// @Failure		403			object	utils.ErrorResponse	"Forbidden"
// @Router		/contracts/{id}/status [get].
func (h Handler) GetContractStatus(w http.ResponseWriter, r *http.Request) {
	if !jwt.CheckAuthTokenExists(r) {
		utils.EncodeError(w, "No auth token provided.", http.StatusUnauthorized)

		return
	}
	// Get user ID
	contractUUID, err := utils.GetPathFromURL(r, "id")
	if err != nil {
		log.Error("contract uuid is not valid", "error message", err, "contractUUID", contractUUID)
		utils.EncodeError(w, "Contract ID is missing or wrongly defined.", http.StatusBadRequest)

		return
	}
	// input sanitized
	c, err := uuid.Parse(contractUUID)
	if err != nil {
		log.ErrorE(err, "Could not parse contract UUID.")
		utils.EncodeError(w, "Could not parse contract UUID.", http.StatusBadRequest)

		return
	}
	contract := id.NewContractID(c)
	user, err := jwt.GetUserID(r)
	if err != nil {
		log.ErrorE(err, "Could not extract user ID.")
		utils.EncodeError(w, "Could not extract user ID.", http.StatusForbidden)

		return
	}
	graph, err := h.Storage.GetContract(contract)
	if err != nil {
		log.Error("error with contract", "error message", err)
		utils.EncodeError(w, "Could not retrieve the contract.", http.StatusNotFound)

		return
	}
	// Check if the user can see this contract
	hasAccess := auth.HasContractAccess(user, graph)
	if !hasAccess {
		utils.EncodeError(w, "You cannot access this contract.", http.StatusForbidden)

		return
	}
	// Get contract policy
	// For now, policy is always "default"
	contractPolicy := h.Storage.GetContractPolicy(graph)
	var policy Policy
	if contractPolicy == "default" {
		policy = DefaultPolicy{}
	} else {
		utils.EncodeError(w, "Policy error", http.StatusBadRequest)

		return
	}
	// Check if contract is signed.
	isContractSigned, err := policy.IsContractSigned(graph)
	if err != nil {
		log.ErrorE(err, "Could not check if contract is signed.")
		utils.EncodeError(w, "Contract validation process failed.", http.StatusInternalServerError)

		return
	}
	userStatus, err := h.Storage.GetContractStatus(contract, user)
	if err != nil {
		log.Error("error while getting contract status", "error message", err)
		utils.EncodeError(w, "Contract is not valid.", http.StatusTeapot)

		return
	}
	utils.EncodeContractStatusResponse(w, isContractSigned, userStatus)
}
