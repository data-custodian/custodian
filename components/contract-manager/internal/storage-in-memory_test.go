package cms

import (
	g "custodian/components/lib-common/pkg/graph"
	utils "custodian/components/lib-common/pkg/http"
	"custodian/components/lib-common/pkg/id"
	"testing"

	"github.com/stretchr/testify/require"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func TestGetGraphHash(t *testing.T) {
	db := InMemoryStorage{DBContract: map[id.ContractID]*g.Graph{}}
	contractStr := `@prefix ns0: <https://w3id.org/dpv#> .
                    <https://swisscustodian.ch/inst#Contract123>
                    ns0:hasProcess <https://swisscustodian.ch/inst#Process201229> .`
	expectedHash := "6d85503bcd072438e464c20b1d2048f12cde9c8ea7f75dbe99105ccfb28ba673"
	graph, _ := g.SerializeToGraph([]byte(contractStr), utils.Turtle)
	u, _ := uuid.NewUUID()
	newID := id.NewContractID(u)
	db.DBContract[newID] = graph
	hash, _ := db.GetGraphHash(newID)
	assert.Equal(t, expectedHash, hash)
}

func TestRemoveContract(_t *testing.T) {
	// TODO
}

// Retrieve a contract.
func TestGetContract(t *testing.T) {
	db := InMemoryStorage{DBContract: map[id.ContractID]*g.Graph{}}
	contractStr := `@prefix ns0: <http://w3id.org/dpv#> .
                    <https://swisscustodian.ch/inst#Contract123>
                    ns0:hasProcess <https://swisscustodian.ch/inst#Process201229> .`
	expectedGraph, _ := g.SerializeToGraph([]byte(contractStr), utils.Turtle)
	u, _ := uuid.NewUUID()
	newID := id.NewContractID(u)
	db.DBContract[newID] = expectedGraph
	graph, err := db.GetContract(newID)
	require.NoError(t, err)
	assert.Equal(t, expectedGraph, graph)
}

func TestUploadContract(_t *testing.T) {
	// TODO.
}

func TestGetContractStatus(_t *testing.T) {
	// TODO
}

func TestGetOntology(_t *testing.T) {
	// TODO.
}

func TestUploadOntology(_t *testing.T) {
	// TODO.
}

// Get all the contracts associated to a user.
func TestGetContractByUserUUID(_t *testing.T) {
	// TODO.
}

// SignContract creates a signature and appends it to the contract.
func TestSignContract(_t *testing.T) {
	// TODO.
}

// Find the signature document according to contract id.
// TODO : take source field as the contract id -> is that correct?
func TestAskIfSignatureExists(_t *testing.T) {
	db := InMemoryStorage{DBContract: map[id.ContractID]*g.Graph{}}
	contractStr := `@prefix ns0: <http://w3id.org/dpv#> .
                    @prefix ns1: <https://swisscustodian.ch/vocab#> .
                    @prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
                    <https://swisscustodian.ch/inst#Process201229>
                    a <https://w3id.org/dpv#Process> ;
                    ns0:hasData <https://swisscustodian.ch/inst#d4156e8a-3a78-4087-9597-23a54016b615> ;
                    ns0:hasDataController <https://swisscustodian.ch/inst#791659e7-ff7d-4b40-abbb-952eb0d82b33> ;
                    ns0:hasDataProcessor <https://swisscustodian.ch/inst#6053c57d-17e6-476c-92e9-77736db38982> ;
                    ns0:hasHumanInvolvement <https://swisscustodian.ch/inst#HumanInvolvementForVerification802391> ;
                    ns0:hasProcessing ns0:Download ;
                    ns0:hasPurpose ns0:AcademicResearch ;
                    ns0:hasTechnicalMeasure <https://swisscustodian.ch/inst#Deidentification607597>,
                    <https://swisscustodian.ch/inst#EndToEndEncryption757763> ;
                    ns1:hasDataAnalysisPlan "Plan 1"^^xsd:string ;
                    ns1:projectName "Project 1"^^xsd:string .
                    <https://swisscustodian.ch/inst#Contract123>
                    a ns0:Contract ;
                    ns0:hasProcess <https://swisscustodian.ch/inst#Process201229> .`

	graph, _ := g.SerializeToGraph([]byte(contractStr), utils.Triples)
	u, _ := uuid.NewUUID()
	newID := id.NewContractID(u)
	db.DBContract[newID] = graph
	// assert.Equal(t, true, db.askIfSignatureExists())
}
