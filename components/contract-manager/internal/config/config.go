package config

type Config struct {
	// Config struct for webapp config
	Server    server    `yaml:"server"`
	ECDSAKeys ecdsaKeys `yaml:"ecdsaKeys"`
	// OpenID Connect struct
	OIDC     oidc     `yaml:"oidc"`
	Ontology ontology `yaml:"ontology"`

	RabbitMQ rabbitmq `yaml:"rabbitmq"`
	Storage  storage  `yaml:"storage"`
}

type storage struct {
	Jena jena `yaml:"jena"`
}

type ontology struct {
	Enums  string `yaml:"enums"`
	Shapes string `yaml:"shapes"`
}

type ecdsaKeys struct {
	CMSPrivateKey string `yaml:"cmsPrivateKey"`
	CMSPublicKey  string `yaml:"cmsPublicKey"`
}

type server struct {
	//	hostname string `yaml:"hostname"`
	Hostname string `yaml:"hostname"`
	//	host string `yaml:"port"`
	Port string `yaml:"port"`
	// URL path containing the component name (e.g., "/cme/signing")
	ComponentURLPath string `yaml:"componentURLPath"`
}

type oidc struct {
	// OpenID Connect issuer
	Issuer string `yaml:"issuer"`
	// OpenID Connect client ID
	ClientID string `yaml:"clientID"`
}

type rabbitmq struct {
	URL       string `yaml:"url"`
	QueueName string `yaml:"queueName"`

	Credentials rabbitMQCredentials `yaml:"credentials"`
}

type rabbitMQCredentials struct {
	Username string `yaml:"username"`
	Token    string `yaml:"token"`
}

type jena struct {
	Host           string `yaml:"host"`
	Scheme         string `yaml:"scheme"`
	CredentialFile string `yaml:"credentialFile"`
	CaTest         string `yaml:"caTest"`
}
