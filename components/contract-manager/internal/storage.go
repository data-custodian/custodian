package cms

import (
	g "custodian/components/lib-common/pkg/graph"
	"custodian/components/lib-common/pkg/id"
)

type Storage interface {
	UploadOntology(shapes *g.Graph, enums *g.Graph) error
	GetOntology() (*g.Graph, error)

	GetGraphHash(contract id.ContractID) (string, error)
	RemoveContract(contract id.ContractID) error
	RemoveTmpContract(contract id.ContractID) error
	GetContract(contract id.ContractID) (*g.Graph, error)
	UploadContract(contractGraph *g.Graph, contract id.ContractID) error

	GetContractByUserUUID(user id.GeneralID) ([]id.ContractID, error)

	GetContractStatus(contract id.ContractID, user id.GeneralID) (map[string]string, error)

	SignContract(contract id.ContractID, signature id.GeneralID, signatureTriples string) error
	askIfSignatureExists(contract id.ContractID) (bool, error)

	GetContractPolicy(*g.Graph) string
}
