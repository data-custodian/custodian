package cms

import (
	rmq "custodian/components/lib-common/pkg/rabbitmq"
)

type Handler struct {
	Storage    Storage
	Rabbitmq   *rmq.RabbitMQ
	CMSPrivKey string
	CMSPubKey  string
}

func NewHandler(storage Storage, rabbitmq *rmq.RabbitMQ, cmsPrivKey, cmsPubKey string) *Handler {
	return &Handler{Storage: storage, Rabbitmq: rabbitmq, CMSPrivKey: cmsPrivKey, CMSPubKey: cmsPubKey}
}
