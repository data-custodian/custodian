/**
 * Swiss Data Custodian provides mechanisms for contract management and access control.
 * Copyright (C) 2019 - Swiss Data Science Center
 * A partnership between École Polytechnique Fédérale de Lausanne (EPFL) and
 * Eidgenössische Technische Hochschule Zürich (ETHZ).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package main

import (
	cmc "custodian/components/lib-common/pkg/config"
	"custodian/components/lib-common/pkg/log"
	config "custodian/gateway/internal/config"
	"custodian/gateway/internal/endpoints"
	"custodian/gateway/internal/oidc"
	"custodian/gateway/pkg/proxy"

	"context"
	"os"
	"time"

	"github.com/google/uuid"
	slogecho "github.com/samber/slog-echo"
	oidcrelay "github.com/zitadel/oidc/v3/pkg/client/rp"
	oidchttp "github.com/zitadel/oidc/v3/pkg/http"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"

	"github.com/gorilla/securecookie"
)

const (
	verifierIssueOffset = 5 * time.Second

	hashKeyLength  = 64
	blockKeyLength = 32
)

func main() {
	log.Setup()

	conf, err := cmc.LoadConfigs[config.Config]()
	log.Info("Config", "config", conf)
	if err != nil {
		log.PanicE(err, "Error reading config.")
	}

	ctx := context.Background()
	oidcRelay := createOIDCRelay(ctx, conf)
	handler := createHTTPHandler(oidcRelay, &conf)

	err = handler.Start(conf.Server.Hostname + ":" + conf.Server.Port)

	if err != nil {
		log.PanicE(err, "HTTP handler failed.")
	}
}

func createHTTPHandler(oidcRelay *oidc.OIDCRelay, conf *config.Config) *echo.Echo {
	// Generate some state (representing the state of the user in your application,
	// e.g. the page where he was before sending him to login
	state := func() string {
		return uuid.New().String()
	}

	httpHandler := echo.New()

	// Create new group for `/auth` prefix
	auth := httpHandler.Group(endpoints.AuthPathPrefix)
	log.Info("Install log/recover middleware.", "group", endpoints.AuthPathPrefix)
	auth.Use(slogecho.New(log.Get()))
	auth.Use(middleware.Recover())

	log.Info("Install login/logout/callback endpoints.", "group", endpoints.AuthPathPrefix)
	auth.GET(endpoints.LoginRelPath, oidcRelay.LoginHandler(state))
	auth.GET(endpoints.LogoutRelPath, oidcRelay.LogoutHandler(conf.OIDC.DefaultPostLogoutRedirect))
	auth.GET(endpoints.CallbackRelPath, oidcRelay.CallbackHandler())

	for _, redirection := range conf.Redirections {
		log.Info("Install redirection.", "redirection", redirection)

		proxy.SetProxyPrefix(
			httpHandler,
			redirection.Path,
			redirection.Scheme,
			redirection.Host,
			slogecho.New(log.Get()),
			middleware.Recover(),
			oidcRelay.Authenticate(conf.OIDC.IDTokenHeader),
			oidcRelay.Authorize(),
		)
	}

	return httpHandler
}

func createOIDCRelay(ctx context.Context, conf config.Config) *oidc.OIDCRelay {
	hashKey := securecookie.GenerateRandomKey(hashKeyLength)
	blockKey := securecookie.GenerateRandomKey(blockKeyLength)

	secureCookie := securecookie.New(hashKey, blockKey)

	cookieHandler := oidchttp.NewCookieHandler(hashKey, blockKey, oidchttp.WithUnsecure())

	options := []oidcrelay.Option{
		oidcrelay.WithCookieHandler(cookieHandler),
		oidcrelay.WithVerifierOpts(oidcrelay.WithIssuedAtOffset(verifierIssueOffset)),
	}

	if conf.OIDC.ClientSecret == "" {
		options = append(options, oidcrelay.WithPKCE(cookieHandler))
	}

	if keyPath := os.Getenv("KEY_PATH"); keyPath != "" {
		options = append(options, oidcrelay.WithJWTProfile(oidcrelay.SignerFromKeyPath(keyPath)))
	}

	oidcRelay, err := oidc.NewOIDCRelay(
		ctx,
		secureCookie,
		conf.OIDC.Issuer,
		conf.OIDC.ClientID,
		conf.OIDC.ClientSecret,
		conf.OIDC.Callback,
		conf.OIDC.Scopes,
		options...)

	if err != nil {
		log.PanicE(err, "Error creating OIDC provider.")
	}

	return oidcRelay
}
