package proxy

import (
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
)

// testMiddleware is a middleware for testing.
// It adds a X-Test header to the response.
func testMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		c.Response().Header().Set("X-Test", "test")

		return next(c)
	}
}

func TestSetProxyPrefix(t *testing.T) {
	testPrefix := "/test"
	testPrefixWithMiddleware := "/test-middleware"

	//nolint:revive // The `r` parameter is not used here, but must be present in the function signature
	svr1 := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		_, err := w.Write([]byte("destination 1"))
		if err != nil {
			t.Fatal(err)
		}
	}))

	//nolint:revive // The `r` parameter is not used here, but must be present in the function signature
	svr2 := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		_, err := w.Write([]byte("destination 2"))
		if err != nil {
			t.Fatal(err)
		}
	}))

	defer svr1.Close()

	// Parse srv1 URL
	u1, err := url.Parse(svr1.URL)
	if err != nil {
		t.Fatal(err)
	}

	// Parse srv2 URL
	u2, err := url.Parse(svr2.URL)
	if err != nil {
		t.Fatal(err)
	}

	// New echo router
	e := echo.New()

	// Set reverse proxy for srv1 and srv2
	SetProxyPrefix(e, testPrefix, u1.Scheme, u1.Host)
	SetProxyPrefix(e, testPrefixWithMiddleware, u2.Scheme, u2.Host, testMiddleware)

	// Test the /prefix route
	req := httptest.NewRequest(http.MethodGet, testPrefix, nil)
	w := httptest.NewRecorder()

	e.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code, "wrong status code")
	assert.Equal(t, "destination 1", w.Body.String(), "wrong response body")

	// Test the /prefix-middleware route
	req = httptest.NewRequest(http.MethodGet, testPrefixWithMiddleware, nil)
	w = httptest.NewRecorder()

	e.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code, "wrong status code")
	assert.Equal(t, "destination 2", w.Body.String(), "wrong response body")
	assert.Equal(t, "test", w.Header().Get("X-Test"), "wrong header value")
}
