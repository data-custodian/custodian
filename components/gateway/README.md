# Swiss Data Custodian Gateway - Reverse Proxy

## About

The Swiss Data Custodian Gateway authenticates users with OpenID Connect and
redirect them to the right service.

## Technologies

The Gateway uses
[OpenID Connect](https://openid.net/developers/how-connect-works/) to
authenticate users. More specifically, when a users visits an endpoint, the
gateway does

1. Verify if the user owns a cookie with an ID token
   1. If not, redirect the user to the OIDC issuer login page
   2. Store the new ID token in an encrypted cookie
2. Validate the ID token
3. Pass the user's request to the right service, with the ID token in the
   `Authorization` header.

## Setup

Configure the OIDC and redirection options in `config.yml`.

### OIDC Config

You can specify what scopes you want to request from the OIDC issuer, and the
header in which you want to store the ID token.

```yaml
OIDC:
  Scopes:
    - "openid"
    - "profile"
  IdTokenHeader: "X-Id-Token"
```

### Redirection Config

Enter the host and scheme of the services you want to redirect the users to, and
bind them to a endpoint prefix.

Here is an example for a service running on port 8080 on localhost.

```yaml
Redirections:
  - Path: "/my-service"
    Scheme: "http"
    Host: "localhost:8080"
```

When users connect to an endpoint with prefix `/my-service` (e.g.,
`/my-service/my-query`), their request is transmitted to the service running on
localhost:8080 using the same endpoint
(`http://localhost:8080/my-service/my-query`).

### Secrets

Create a `secrets.yaml` file with the following content using the template
`secrets.dist.yml`

```sh
cp secrets.dist.yml secrets.yml
```

Fill in the secrets according to your OIDC configuration.
