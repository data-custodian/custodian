package oidc

import (
	"custodian/components/lib-common/pkg/log"
	"custodian/gateway/internal/endpoints"
	"errors"
	"net/http"
	"time"

	"github.com/labstack/echo/v4"

	"github.com/zitadel/oidc/v3/pkg/client/rp"
	"github.com/zitadel/oidc/v3/pkg/oidc"
)

const (
	CookieNameRedirection = "SWISSDATACUSTODIAN_REDIRECT"
)

// Store the IDToken and Access Token in separate cookies.
//
//nolint:revive // The parameters r, state and rp are required by the rp.CodeExchangeCallback signature.
func (o OIDCRelay) storeTokens(
	w http.ResponseWriter,
	r *http.Request,
	tokens *oidc.Tokens[*oidc.IDTokenClaims],
	state string,
	rp rp.RelyingParty,
) {
	log.Info("Store id token and access token in cookie.")

	err := o.storeCookie(
		w,
		CookieNameIDToken,
		tokens.IDToken,
		tokens.IDTokenClaims.Expiration.AsTime(),
	)
	if err != nil {
		log.ErrorE(err, "Failed to store ID token in cookie")
	}
	err = o.storeCookie(w, CookieNameAccessToken, tokens.AccessToken, tokens.Token.Expiry)
	if err != nil {
		log.ErrorE(err, "Failed to store access token in cookie")
	}
}

// Store the Access Token in a cookie.
func (o OIDCRelay) storeCookie(
	w http.ResponseWriter,
	name string,
	value string,
	expiration time.Time,
) error {
	encodedCookie, err := o.secureCookie.Encode(name, value)
	if err != nil {
		return err
	}
	cookie := new(http.Cookie)
	cookie.Name = name
	cookie.Value = encodedCookie
	cookie.Expires = expiration
	cookie.HttpOnly = true
	cookie.Path = "/"
	cookie.Secure = true

	http.SetCookie(w, cookie)

	return nil
}

// CallbackHandler is the handler for the callback URL.
func (o OIDCRelay) CallbackHandler() func(c echo.Context) error {
	return func(c echo.Context) error {
		handlerFunc := rp.CodeExchangeHandler(o.storeTokens, o.rp)
		handlerFunc(c.Response(), c.Request())

		// Get the redirect URL from the cookie.
		cookie, err := c.Cookie(CookieNameRedirection)
		if err != nil {
			if errors.Is(err, http.ErrNoCookie) {
				// If the cookie is not set, redirect to the index page.
				return c.Redirect(http.StatusFound, "/")
			} else {
				return err
			}
		}

		url := cookie.Value

		// Delete the cookie.
		cookie.MaxAge = -1
		cookie.Path = endpoints.CallbackPath()
		c.SetCookie(cookie)

		return c.Redirect(http.StatusFound, url)
	}
}

// LoginHandler is the handler for the login URL.
func (o OIDCRelay) LoginHandler(state func() string) func(c echo.Context) error {
	return echo.WrapHandler(rp.AuthURLHandler(state, o.rp, rp.WithPromptURLParam("Welcome back!")))
}

// LogoutHandler is the handler for the logout URL.
func (o OIDCRelay) LogoutHandler(defaultPostLogoutRedirect string) func(c echo.Context) error {
	return func(c echo.Context) error {
		// get url parameters
		postLogoutRedirectURI := c.QueryParam("post_logout_redirect_uri")
		if postLogoutRedirectURI == "" {
			postLogoutRedirectURI = defaultPostLogoutRedirect
		}

		// extract the IDToken from the cookie
		cookie, err := c.Cookie(CookieNameIDToken)
		if err != nil {
			if errors.Is(err, http.ErrNoCookie) {
				// If the cookie is not set, redirect to the index page.
				return c.Redirect(http.StatusFound, "/")
			} else {
				return err
			}
		}
		// get the IDToken from the cookie
		idToken, err := o.readEncryptedCookie(*cookie)
		if err != nil {
			return err
		}

		// Delete the cookie.
		cookie.MaxAge = -1
		cookie.Path = "/"
		c.SetCookie(cookie)

		// Delete the Access Token cookie
		cookie, err = c.Cookie(CookieNameAccessToken)
		if err == nil {
			cookie.MaxAge = -1
			cookie.Path = "/"
			c.SetCookie(cookie)
		}

		redirectURI, err := rp.EndSession(
			c.Request().Context(),
			o.rp,
			idToken,
			postLogoutRedirectURI,
			"",
		)
		if err != nil {
			return err
		}

		return c.Redirect(http.StatusFound, redirectURI.String())
	}
}
