package oidc

import (
	"context"
	"custodian/components/lib-common/pkg/log"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gorilla/securecookie"
)

// setupMockOIDCServer sets up a mock OIDC server for testing.
func setupMockOIDCServer() *httptest.Server {
	server := httptest.NewUnstartedServer(nil)
	server.Config.Handler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path == "/.well-known/openid-configuration" {
			w.WriteHeader(http.StatusOK)
			_, err := w.Write([]byte(fmt.Sprintf(`{
				"issuer": "%s",
				"authorization_endpoint": "https://issuer.com/authorize",
				"token_endpoint": "https://issuer.com/token",
				"userinfo_endpoint": "https://issuer.com/userinfo",
				"jwks_uri": "https://issuer.com/jwks",
				"response_types_supported": ["code", "id_token", "token id_token"],
				"subject_types_supported": ["public"],
				"id_token_signing_alg_values_supported": ["RS256"],
				"scopes_supported": ["openid", "profile", "email"],
				"token_endpoint_auth_methods_supported": ["client_secret_basic"],
				"claims_supported": ["sub", "iss", "auth_time", "name", "email"]
			}`, server.URL)))
			log.PanicE(err, "Failed to write response")
		} else {
			w.WriteHeader(http.StatusNotFound)
		}
	})
	server.Start()

	return server
}

func TestNewOIDCProxy(t *testing.T) {
	mockOIDCServer := setupMockOIDCServer()
	ctx := context.Background()
	secureCookie := securecookie.New([]byte("test"), nil)
	issuer := mockOIDCServer.URL
	clientID := "clientID"
	clientSecret := "clientSecret"
	redirectURI := "https://redirect.com"
	scopes := []string{"openid", "profile", "email"}

	proxy, err := NewOIDCRelay(
		ctx,
		secureCookie,
		issuer,
		clientID,
		clientSecret,
		redirectURI,
		scopes,
	)
	if err != nil {
		t.Errorf("NewOIDCProxy() got error: %v", err)
	}
	if proxy == nil {
		t.Errorf("NewOIDCProxy() got nil")
	}
}
