/**
 * Swiss Data Custodian provides mechanisms for contract management and access control.
 * Copyright (C) 2019 - Swiss Data Science Center
 * A partnership between École Polytechnique Fédérale de Lausanne (EPFL) and
 * Eidgenössische Technische Hochschule Zürich (ETHZ).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package oidc

import (
	"context"
	"custodian/components/lib-common/pkg/log"
	"custodian/gateway/internal/endpoints"
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/zitadel/oidc/v3/pkg/client/rp"
	"github.com/zitadel/oidc/v3/pkg/oidc"

	"github.com/labstack/echo/v4"
)

const (
	CookieNameIDToken     = "SWISSDATACUSTODIAN_IDTOKEN"
	CookieNameAccessToken = "SWISSDATACUSTODIAN_ACCESSTOKEN"
)

// checkAuthorizationHeader checks if the request contains a Authorization Bearer token. It does not validate the token.
func checkAuthorizationHeader(h http.Header) bool {
	auth := h.Get("Authorization")
	if auth == "" {
		return false
	}
	if !strings.HasPrefix(auth, oidc.PrefixBearer) {
		return false
	}

	return true
}

// redirectToLoginPage redirects the user to the login page.
// Beforehand, the current URL is stored in a cookie.
func redirectToLoginPage(c echo.Context) error {
	cookie := new(http.Cookie)
	cookie.Name = CookieNameRedirection
	cookie.Value = c.Request().URL.String()
	cookie.Path = endpoints.CallbackPath()
	cookie.Expires = time.Now().Add(1 * time.Minute)
	cookie.HttpOnly = true
	c.SetCookie(cookie)

	return c.Redirect(http.StatusSeeOther, endpoints.LoginPath())
}

func (o OIDCRelay) readEncryptedCookie(cookie http.Cookie) (string, error) {
	var value string
	err := o.secureCookie.Decode(cookie.Name, cookie.Value, &value)
	if err != nil {
		return "", err
	}

	return value, nil
}

func (o OIDCRelay) Authorize() echo.MiddlewareFunc {
	authorize := func(c echo.Context) (bool, error) {
		var authTokenHeader string
		request := c.Request()

		//nolint:nestif,nolintlint
		if checkAuthorizationHeader(request.Header) {
			log.Info("Checking access token.")
			// Get the access token from the header
			// when accessing it through pure API call not through browser.
			authTokenHeader = request.Header.Get("Authorization")
		} else {
			log.Info("Checking access token in cookie.")

			cookie, err := request.Cookie(CookieNameAccessToken)
			if err != nil {
				if errors.Is(err, http.ErrNoCookie) {
					return false, redirectToLoginPage(c)
				}

				return false, err
			}
			accessToken, err := o.readEncryptedCookie(*cookie)
			if err != nil {
				cookie.MaxAge = -1
				c.SetCookie(cookie)

				return false, redirectToLoginPage(c)
			}

			authTokenHeader = fmt.Sprintf("%s%s", oidc.PrefixBearer, accessToken)
		}

		log.Info("Access validation from cookie successful.")

		c.Request().Header.Del("Authorization")
		c.Request().Header.Add("Authorization", authTokenHeader)

		return true, nil
	}

	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			callNext, err := authorize(c)
			if err != nil {
				return err
			}

			if callNext {
				return next(c)
			}

			return nil
		}
	}
}

func (o OIDCRelay) validateIDToken(ctx context.Context, idToken string) bool {
	if o.rp == nil {
		log.Error("Relying party is not initialized")

		return false
	}
	_, err := rp.VerifyIDToken[*oidc.IDTokenClaims](ctx, idToken, o.rp.IDTokenVerifier())
	log.ErrorE(err, "Failed to verify id token.")

	return err == nil
}

// Authenticate is a middleware that verifies that the user is authenticated with a valid cookie.
// If not, it redirects the user to the login page.
//
//nolint:gocognit // This function is not too complex.
func (o OIDCRelay) Authenticate(idTokenHeader string) echo.MiddlewareFunc {
	authenticate := func(c echo.Context) (bool, error) {
		request := c.Request()
		var idToken string

		ok := checkAuthorizationHeader(request.Header)
		//nolint:nestif,nolintlint
		if ok {
			log.Info("Checking ID token and validate it.")
			// Get the ID token from the header
			// when accessing it through pure API call not through browser.
			idToken = strings.TrimPrefix(request.Header.Get(idTokenHeader), oidc.PrefixBearer)
			ok = o.validateIDToken(request.Context(), idToken)

			if !ok {
				return false, echo.NewHTTPError(http.StatusUnauthorized, "Unauthorized")
			}
		} else {
			log.Info("Checking ID token in cookie and validate it.")
			cookie, err := c.Request().Cookie(CookieNameIDToken)
			if err != nil {
				log.ErrorE(err, "Failed to retrieve cookie.")
				if errors.Is(err, http.ErrNoCookie) {
					return false, redirectToLoginPage(c)
				}

				return false, err
			}

			idToken, err = o.readEncryptedCookie(*cookie)
			if err != nil {
				cookie.MaxAge = -1
				c.SetCookie(cookie)

				return false, redirectToLoginPage(c)
			}

			ok = o.validateIDToken(c.Request().Context(), idToken)
			if !ok {
				return false, redirectToLoginPage(c)
			}
		}

		log.Info("ID Token validation from cookie successful.")

		request.Header.Del(idTokenHeader)
		request.Header.Add(idTokenHeader, fmt.Sprintf("%s%s", oidc.PrefixBearer, idToken))

		return true, nil
	}

	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			callNext, err := authenticate(c)
			if err != nil {
				return err
			}

			if callNext {
				return next(c)
			}

			return nil
		}
	}
}
