package oidc

import (
	"custodian/gateway/internal/endpoints"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

const (
	//nolint:gosec,lll // This is a test token
	idToken  = `eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImFhYSI6NjkyfQ.eyJhY3IiOiJzb21ldGhpbmciLCJhbXIiOlsiZm9vIiwiYmFyIl0sImF0X2hhc2giOiIyZHpibV92SXh5LTdlUnRxVUlHUFB3IiwiYXVkIjpbInVuaXQiLCJ0ZXN0IiwiNTU1NjY2Il0sImF1dGhfdGltZSI6MTY3ODEwMDk2MSwiYXpwIjoiNTU1NjY2IiwiYmFyIjp7ImNvdW50IjoyMiwidGFncyI6WyJzb21lIiwidGFncyJdfSwiY2xpZW50X2lkIjoiNTU1NjY2IiwiZXhwIjo0ODAyMjM4NjgyLCJmb28iOiJIZWxsbywgV29ybGQhIiwiaWF0IjoxNjc4MTAxMDIxLCJpc3MiOiJsb2NhbGhvc3Q6NDQ0NCIsImp0aSI6Ijk4NzYiLCJuYmYiOjE2NzgxMDEwMjEsIm5vbmNlIjoiMTIzNDUiLCJzdWIiOiJ0aW1AbG9jYWwuY29tIn0.EF0eMYwPq0ItKw07Q2y4iIbRosJEfaVCgQme2uHxv7IqWJydtoPh_qvSu8FXQaUjFzTI1UzltVG3FIUC6xTKb1g29xzgTq2knZLxUZzZox3VD4U2-3npFu4ZqIEeM8tr1ijMakmWkqy4cge2RypUgRWOhjr642kInGbOqs0G6nREjNvON71sh2wovrEjC2naIvP9naKzGQ3U77qRrUnJzVFUBRjXW0-5kPH6j7-SamS-AjHXkA2rqi9f_Jmr2VNskPL4ehYzySdhASbslfLNXNicP8921CWLhdjAUu3jQ8PkS_CDFlKH2HAUuVlGeP1KR07i79ISjFfkyXei1beQPg`
	testPath = "/test"
)

func setTestEnvVars(t *testing.T) {
	t.Setenv("CLIENT_ID", "test-client-id")
	t.Setenv("CLIENT_SECRET", "test-client-secret")
	t.Setenv("KEY_PATH", "testdata/key.pem")
	t.Setenv("ISSUER", "https://localhost:8080")
	t.Setenv("PORT", "8080")
	t.Setenv("SCOPES", "openid profile email")
}

// parseCookie parses a raw cookie string and returns a *http.Cookie.
func parseCookie(rawCookie string) *http.Cookie {
	parsedCookie := strings.Split(rawCookie, "; ")

	cookieFieldsMap := make(map[string]string)
	cookieFieldsMap["Name"] = strings.Split(parsedCookie[0], "=")[0]
	cookieFieldsMap["Value"] = strings.Split(parsedCookie[0], "=")[1]
	for _, cookieField := range parsedCookie[1:] {
		cookieFieldSplit := strings.Split(cookieField, "=")
		if len(cookieFieldSplit) != 2 {
			continue
		}
		cookieFieldsMap[cookieFieldSplit[0]] = cookieFieldSplit[1]
	}

	cookie := new(http.Cookie)
	cookie.Name = cookieFieldsMap["Name"]
	cookie.Value = cookieFieldsMap["Value"]
	cookie.Path = cookieFieldsMap["Path"]
	cookie.Expires, _ = time.Parse(time.RFC1123, cookieFieldsMap["Expires"])
	cookie.HttpOnly = cookieFieldsMap["HttpOnly"] == "true"

	return cookie
}

func TestRedirectToLoginPage(t *testing.T) {
	setTestEnvVars(t)

	e := echo.New()

	req := httptest.NewRequest(http.MethodGet, testPath, nil)

	w := httptest.NewRecorder()
	c := e.NewContext(req, w)

	require.NoError(
		t,
		redirectToLoginPage(c),
		"redirectToLoginPage should not return an error",
	)
	assert.Equal(t,
		endpoints.LoginPath(),
		w.Header().Get("Location"),
		"wrong location header set by redirectToLoginPage")

	response := w.Result()
	defer response.Body.Close()
	cookie := parseCookie(response.Header["Set-Cookie"][0])

	assert.Equal(t, testPath, cookie.Value, "wrong cookie value set by redirectToLoginPage")
	assert.Equal(t,
		endpoints.CallbackPath(),
		cookie.Path,
		"wrong cookie path set by redirectToLoginPage")

	if cookie.Expires.IsZero() {
		t.Errorf("redirectToLoginPage should set a cookie with a non-zero expiration date")
	}

	if cookie.Expires.Sub(time.Now().Add(1*time.Minute)) > 0 {
		t.Errorf(
			"redirectToLoginPage should set a cookie with an expiration date of at most 1 minute",
		)
	}
}

func TestCheckAuthorizationHeader(t *testing.T) {
	setTestEnvVars(t)

	e := echo.New()

	req := httptest.NewRequest(http.MethodGet, testPath, nil)
	req.Header.Add("Authorization", "Bearer "+idToken)

	w := httptest.NewRecorder()
	c := e.NewContext(req, w)

	if !checkAuthorizationHeader(c.Request().Header) {
		t.Errorf("checkAuthorizationHeader should return true")
	}

	req.Header.Del("Authorization")

	if checkAuthorizationHeader(c.Request().Header) {
		t.Errorf(
			"checkAuthorizationHeader should return false because the Authorization header is missing",
		)
	}

	req.Header.Add("Authorization", "some other value")

	if checkAuthorizationHeader(c.Request().Header) {
		t.Errorf(
			"checkAuthorizationHeader should return false because the Authorization header does not start with 'Bearer'",
		)
	}
}
