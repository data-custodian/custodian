package oidc

import (
	"context"

	"github.com/gorilla/securecookie"
	"github.com/zitadel/oidc/v3/pkg/client/rp"
)

type OIDCRelay struct {
	rp           rp.RelyingParty
	secureCookie *securecookie.SecureCookie
}

func NewOIDCRelay(
	ctx context.Context,
	secureCookie *securecookie.SecureCookie,
	issuer,
	clientID,
	clientSecret,
	redirectURI string,
	scopes []string,
	options ...rp.Option,
) (*OIDCRelay, error) {
	rp, err := rp.NewRelyingPartyOIDC(
		ctx,
		issuer,
		clientID,
		clientSecret,
		redirectURI,
		scopes,
		options...)
	if err != nil {
		return nil, err
	}

	return &OIDCRelay{
		rp:           rp,
		secureCookie: secureCookie,
	}, nil
}
