package endpoints

import "path"

const (
	AuthPathPrefix  = "/auth"
	LoginRelPath    = "/login"
	CallbackRelPath = "/callback"
	LogoutRelPath   = "/logout"
)

// LoginPath returns the login path.
func LoginPath() string {
	return path.Join(AuthPathPrefix, LoginRelPath)
}

// CallbackPath returns the callback path.
func CallbackPath() string {
	return path.Join(AuthPathPrefix, CallbackRelPath)
}

// LogoutPath returns the callback path.
func LogoutPath() string {
	return path.Join(AuthPathPrefix, LogoutRelPath)
}
