/*
 * Swiss Data Custodian provides mechanisms for contract management and access control.
 * Copyright (C) 2019 - Swiss Data Science Center
 * A partnership between École Polytechnique Fédérale de Lausanne (EPFL) and
 * Eidgenössische Technische Hochschule Zürich (ETHZ).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package config

// Config contains the reverse proxy redirections.
type Config struct {
	Redirections []Redirection `yaml:"redirections"`
	OIDC         OIDCConfig    `yaml:"oidc"`
	Server       ServerConfig  `yaml:"server"`
}

// Redirection contains settings for the redirection.
type Redirection struct {
	Path   string `yaml:"path"`
	Scheme string `yaml:"scheme"`
	Host   string `yaml:"host"`
}

// ServerConfig are settings for the HTTP handler.
type ServerConfig struct {
	Hostname string `yaml:"hostname"`
	Port     string `yaml:"port"`
}

// OIDCConfig contains the OIDC settings.
type OIDCConfig struct {
	ClientID                  string   `yaml:"clientID"`
	ClientSecret              string   `yaml:"clientSecret"`
	Issuer                    string   `yaml:"issuer"`
	Callback                  string   `yaml:"callback"`
	DefaultPostLogoutRedirect string   `yaml:"defaultPostLogoutRedirect"`
	Scopes                    []string `yaml:"scopes"`
	IDTokenHeader             string   `yaml:"idTokenHeader"`
}
