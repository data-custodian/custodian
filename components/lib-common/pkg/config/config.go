package config

import (
	fs "custodian/components/lib-common/pkg/filesystem"
	"custodian/components/lib-common/pkg/log"
	"io"
	"os"
	"strings"

	"github.com/spf13/viper"
)

// LoadConfigs reads the config files at location `path` and merges it into
// the existing config in `vIn` (if given).
func loadConfigs[Config any](configs ...io.Reader) (conf Config, err error) {
	v := viper.New()

	replacer := strings.NewReplacer(".", "_")
	v.SetConfigType("yaml")
	v.SetEnvKeyReplacer(replacer)
	v.AutomaticEnv()

	for _, f := range configs {
		err = v.MergeConfig(f)
		if err != nil {
			log.ErrorE(err, "Failed to read and merge config file.")

			return
		}
	}

	err = v.Unmarshal(&conf)
	if err != nil {
		log.ErrorE(err, "Failed to marshal config file.")

		return
	}

	return conf, nil
}

// LoadConfigWithSecrets will load a general `config/config.yml`
// and a secrets config in `config/secrets.yml` (merged with the first one)
// in the current working directory.
func LoadConfigs[Config any]() (conf Config, err error) {
	log.Info("Loading config files.")

	var files []io.Reader

	secretConfigPath, err := fs.GetSecretConfigPath()
	if err != nil {
		return
	}

	generalConfigPath, err := fs.GetGeneralConfigPath()
	if err != nil {
		return
	}

	for _, p := range []string{secretConfigPath, generalConfigPath} {
		var f *os.File

		f, err = os.Open(p)
		if err != nil {
			log.Error("Failed to read file.", "path", p)

			return
		}
		defer f.Close()

		files = append(files, f)
	}

	return loadConfigs[Config](files...)
}
