package jwt

import (
	"custodian/components/lib-common/pkg/id"
	"errors"
	"net/http"

	"github.com/golang-jwt/jwt/v5"
	"github.com/google/uuid"
)

func CheckAuthTokenExists(r *http.Request) bool {
	// Check if the token is in the header.

	// TODO: Check that access token is valid!
	// - just read it -> check signature (public keycloak, config.yaml -> keycloak url `gocloak`) -> check signature
	// (as we do for ID Token JWT)
	// - or use Keycloak ->
	//   API endpoint token/introspect
	// -> use to query if this opaque access token is valid.
	//
	// - You get the public from issuer: keycloak
	//   Avoid asking the the issuer to many times to GET the public key
	//   Cache for the public keys.
	auth := r.Header.Get("Authorization")

	return auth != ""
}

// GetClaimFromJWT verifies that the JWT in the header is valid and returns the value for the claim.
// TODO : assumption: the user is a UUID and it's the same as defined in the contract.
// Otherwise, need to implement a mapping function to retrieve the user UUID as defined in the contract.
// TODO : We assume that the full token verification is made in the gateway (otherwise we need to know the secret here).
// If the token is already verified and approved, we can only get the part that we want.
func GetClaimFromJWT(r *http.Request, claim string) (string, error) {
	// Check if the token is in the header.
	auth := r.Header.Get("Authorization")

	if auth == "" {
		return "", errors.New("missing authorization header")
	}
	auth = auth[len("Bearer "):]

	// Parse the token without verifying the signature.
	token, _, err := new(jwt.Parser).ParseUnverified(auth, jwt.MapClaims{})

	if err != nil {
		return "", err
	}
	/*

		Check token is valid
			 	ctx := context.Background()
				provider, err := oidc.NewProvider(ctx, config.C.OIDC.Issuer)
				if err != nil {
					log.Println("Failed to create provider: %v", err)
					return "", err
				}

				verifier := provider.Verifier(&oidc.Config{ClientID: config.C.OIDC.ClientID})

				// Parse and verify ID Token payload.
				tokenString := r.Header.Get("Authorization")
				idToken, err := verifier.Verify(tokenString)
				if err != nil {
					// handle error
					log.Println("Failed to verify ID Token: %v", err)
					return "", err
				}

				log.Println("Verified ID Token: %v", idToken)*/

	// Retrieve claim.
	if claims, ok := token.Claims.(jwt.MapClaims); ok {
		if c, exists := claims[claim].(string); exists {
			return c, nil
		} else {
			return "", errors.New("claim not found")
		}
	} else {
		return "", errors.New("claims in token are not valid")
	}
}

func GetUserID(r *http.Request) (id.GeneralID, error) {
	userID, err := GetClaimFromJWT(r, "sub")
	if err != nil {
		return id.GeneralID{}, err
	}

	userUUID, err := uuid.Parse(userID)
	if err != nil {
		return id.GeneralID{}, err
	}

	return id.NewGeneralID(userUUID), nil
}
