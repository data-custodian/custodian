package crypto

import (
	"crypto/ecdsa"
	"crypto/rand"
	"crypto/sha256"
	"crypto/x509"
	"custodian/components/lib-common/pkg/errors"
	"encoding/pem"
	"os"

	"golang.org/x/crypto/sha3"
)

func Sign(message []byte, privateKey *ecdsa.PrivateKey) ([]byte, error) {
	signature, err := sign(message, privateKey)
	if err != nil {
		return nil, err
	}

	return signature, nil
}

func sign(message []byte, privateKey *ecdsa.PrivateKey) ([]byte, error) {
	hashed := sha256.Sum256(message)
	signature, err := ecdsa.SignASN1(rand.Reader, privateKey, hashed[:])
	if err != nil {
		return nil, err
	}

	return signature, nil
}

func Verify(signatureMetaData []byte, signature []byte, keypath string) error {
	bytes, err := os.ReadFile(keypath)
	if err != nil {
		return err
	}

	block, _ := pem.Decode(bytes)
	pKey, _ := x509.ParsePKIXPublicKey(block.Bytes)
	publicKey, ok := pKey.(*ecdsa.PublicKey)
	if !ok {
		return errors.New("not an ecdsa public key")
	}
	err = verify(signatureMetaData, signature, publicKey)

	return err
}

func verify(signatureMetaData []byte, signature []byte, publicKey *ecdsa.PublicKey) error {
	hash := sha256.Sum256(signatureMetaData)
	valid := ecdsa.VerifyASN1(publicKey, hash[:], signature)
	if !valid {
		return errors.New("signature does not match")
	}

	return nil
}

func Hash(message []byte) ([]byte, error) {
	hash := sha3.New256()
	_, err := hash.Write(message)
	if err != nil {
		return nil, err
	}
	sha3 := hash.Sum(nil)

	return sha3, nil
}

func VerifyPEM(pemStr string) (*ecdsa.PublicKey, error) {
	block, _ := pem.Decode([]byte(pemStr))
	if block == nil {
		return nil, errors.New("failed to decode PEM block")
	}

	pubKey, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		return nil, errors.AddContext(err, "failed to parse ECDSA public key")
	}

	key, ok := pubKey.(*ecdsa.PublicKey)
	if !ok {
		return nil, errors.New("the PEM does not contain an ECDSA public key")
	}

	return key, nil
}

func ReadPrivateKeyFromFile(keyPath string) (*ecdsa.PrivateKey, error) {
	keyBytes, err := os.ReadFile(keyPath)
	if err != nil {
		return nil, errors.AddContext(err, "failed to read key file")
	}

	block, _ := pem.Decode(keyBytes)
	if block == nil {
		return nil, errors.New("failed to decode PEM block containing the key")
	}

	if block.Type != "EC PRIVATE KEY" {
		return nil, errors.New("not a EC Key : %s", block.Type)
	}

	key, err := x509.ParseECPrivateKey(block.Bytes)
	if err != nil {
		return nil, errors.AddContext(err, "failed to parse ECDSA private key")
	}

	return key, nil
}
