package crypto

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/x509"
	"custodian/components/lib-common/pkg/errors"
	"encoding/hex"
	"encoding/pem"
	"testing"

	"github.com/stretchr/testify/require"

	"github.com/stretchr/testify/assert"
)

func TestVerify(t *testing.T) {
	signatureMetaData := []byte(
		`<https://swisscustodian.ch/inst#6b2659d2-cf9a-4ad6-8be9-65ea87867927> <http://schema.org/contractHash> 
"ceee77a4f38cf03badefa9b67ced522b1dd0211f640c64651468b024e74c806b" .
<https://swisscustodian.ch/inst#6b2659d2-cf9a-4ad6-8be9-65ea87867927> <http://schema.org/createdAt> 
"2024-08-08 13:15:49.358868516  0000 UTC m= 5006.739798437" .
<https://swisscustodian.ch/inst#6b2659d2-cf9a-4ad6-8be9-65ea87867927> <http://schema.org/signature> 
"0c3c43286bfe9ae93273612559900471dd57f35bcf11d52b4a6767a849f6647c" .
<https://swisscustodian.ch/inst#6b2659d2-cf9a-4ad6-8be9-65ea87867927> <http://schema.org/signatureMethod> "ECDSA" .
<https://swisscustodian.ch/inst#6b2659d2-cf9a-4ad6-8be9-65ea87867927> <http://schema.org/signee> 
"https://swisscustodian.ch/inst#6053c57d-17e6-476c-92e9-77736db38982" .
<https://swisscustodian.ch/inst#6b2659d2-cf9a-4ad6-8be9-65ea87867927> <http://schema.org/status> "VALID" .
`,
	)
	hash, err := Hash(signatureMetaData)
	require.NoError(t, err)
	// Generate random key.
	privateKey, _ := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	t.Log(hash)
	signature, err := sign(hash, privateKey)
	require.NoError(t, err)
	err = verify(hash, signature, &privateKey.PublicKey)
	require.NoError(t, err)
}

func TestHash(t *testing.T) {
	toHash := "custodian"
	// SHA3-256.
	expectedHash := "9fb1a67db4a6d4142ec54373914d5322c27b48076b48ce8edf28cd266654ec8c"
	actualHash, _ := Hash([]byte(toHash))
	hexHash := hex.EncodeToString(actualHash)
	assert.Equal(t, expectedHash, hexHash)
}

func TestVerifyPEM(t *testing.T) {
	pemStr1 := `-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEt/UpBZuqAhMmQwp3pmbhWyvaIq2d
04gOWePrWNnSZPLbBKdDjO/8bgeACjrlqDEm4/9+WW9g6hqHQfyGIdw8ww==
-----END PUBLIC KEY-----
`
	pemStr2 := `-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgEt/UpBZuqAhMmQwp3pmbhWyvaIq2d
04gOWePrWNnSZPLbBKdDjO/8bgeACjrlqDEm4/9+WW9g6hqHQfyGIdw8ww==
-----END PUBLIC KEY-----`
	// Generate random key.
	privateKey, _ := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	publicKeyDER, _ := x509.MarshalPKIXPublicKey(&privateKey.PublicKey)
	publicKeyPEM := pem.EncodeToMemory(&pem.Block{
		Type:  "PUBLIC KEY",
		Bytes: publicKeyDER,
	})
	pemStr3 := string(publicKeyPEM)
	// Verify the PEM.
	_, err1 := VerifyPEM(pemStr1)
	_, err2 := VerifyPEM(pemStr2)
	key, err3 := VerifyPEM(pemStr3)

	require.NoError(t, err1)
	assert.Equal(t, errors.New("failed to decode PEM block"), err2)
	require.NoError(t, err3)
	assert.Equal(t, privateKey.PublicKey.X, key.X)
}
