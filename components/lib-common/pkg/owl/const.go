package owl

import (
	pred "custodian/components/lib-common/pkg/predicates"
)

const (
	rdfType            string = pred.Rdf + "type"
	contractObject     string = pred.Dpv + "Contract"
	signaturePredicate string = pred.SwissCustodian + "hasSignature"
)
