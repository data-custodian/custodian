package owl

import (
	"custodian/components/lib-common/pkg/graph"
	utils "custodian/components/lib-common/pkg/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCheckOWL(t *testing.T) {
	contractUnique := "" +
		"<https://sdc.ch/inst#Contract123> <https://w3id.org/dpv#hasProcess> " +
		"<https://swisscustodian.ch/inst#Process201229> .\n" +
		"<https://sdc.ch/inst#Contract123> <https://w3id.org/dpv#hasProcess> " +
		"<https://swisscustodian.ch/inst#Process907720> .\n" +
		"<https://sdc.ch/inst#Contract123> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> " +
		"<https://w3id.org/dpv#Contract> .\n" +
		"<https://swisscustodian.ch/inst#Process201229> <https://w3id.org/dpv#hasDataController> " +
		"<https://swisscustodian.ch/inst#791659e7-ff7d-4b40-abbb-952eb0d82b33> ."
	graphUnique, _ := graph.SerializeToGraph([]byte(contractUnique), utils.Triples)

	assert.True(t, CheckOWL(graphUnique))
}

func TestIsUnique(t *testing.T) {
	contractUnique := `
		<https://sdc.ch/inst#C123> <https://w3id.org/dpv#hasProcess> <https://sdc.ch/inst#Process201229> . 
		<https://sdc.ch/inst#C123> <https://w3id.org/dpv#hasProcess> <https://sdc.ch/inst#Process907720> . 
		<https://sdc.ch/inst#C123> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <https://w3id.org/dpv#Contract> . 
		<https://sdc.ch/inst#CA209327> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <https://w3id.org/dpv#CA> .`
	contractNotUnique := `
		<https://sdc.ch/inst#C123> <https://w3id.org/dpv#hasProcess> <https://sdc.ch/inst#Process201229> . 
		<https://sdc.ch/inst#C123> <https://w3id.org/dpv#hasProcess> <https://sdc.ch/inst#Process907720> . 
		<https://sdc.ch/inst#C123> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <https://w3id.org/dpv#Contract> . 
		<https://sdc.ch/inst#C124> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <https://w3id.org/dpv#Contract> . 
		<https://sdc.ch/inst#CA209327> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <https://w3id.org/dpv#CA> .`
	graphUnique, _ := graph.SerializeToGraph([]byte(contractUnique), utils.Triples)
	graphNonUnique, _ := graph.SerializeToGraph([]byte(contractNotUnique), utils.Triples)

	assert.True(t, hasSingleContract(graphUnique))
	assert.False(t, hasSingleContract(graphNonUnique))
}

func TestHasSignature(t *testing.T) {
	contractNoSign := "" +
		"<https://sdc.ch/inst#Contract123> <https://w3id.org/dpv#hasProcess> " +
		"<https://swisscustodian.ch/inst#Process201229> .\n" +
		"<https://sdc.ch/inst#Contract123> <https://w3id.org/dpv#hasProcess> " +
		"<https://swisscustodian.ch/inst#Process907720> .\n" +
		"<https://sdc.ch/inst#Contract123> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> " +
		"<https://w3id.org/dpv#Contract> .\n" +
		"<https://sdc.ch/inst#ConfidentialityAgreement209327> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> " +
		"<https://w3id.org/dpv#ConfidentialityAgreement> ."
	contractSign := "" +
		"<https://sdc.ch/inst#Contract123> <https://w3id.org/dpv#hasProcess> " +
		"<https://swisscustodian.ch/inst#Process201229> .\n" +
		"<https://sdc.ch/inst#Contract123> <https://w3id.org/dpv#hasProcess> " +
		"<https://swisscustodian.ch/inst#Process907720> .\n" +
		"<https://sdc.ch/inst#Contract123> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> " +
		"<https://w3id.org/dpv#Contract> .\n" +
		"<https://sdc.ch/inst#Contract123> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> " +
		"<https://w3id.org/dpv#Contract> .\n" +
		"<https://sdc.ch/inst#Contract123> <https://swisscustodian.ch/doc/ontology#hasSignature> " +
		"<https://sdc.ch/inst#Sign123> ."
	graphNoSign, _ := graph.SerializeToGraph([]byte(contractNoSign), utils.Triples)
	graphSign, _ := graph.SerializeToGraph([]byte(contractSign), utils.Triples)

	assert.False(t, hasSignatures(graphNoSign))
	assert.True(t, hasSignatures(graphSign))
}

func TestCheckCustodianURI(t *testing.T) {
	// TODO.
	contract := "" +
		"<https://sdc.ch/inst#Contract123> <https://w3id.org/dpv#hasProcess> " +
		"<https://swisscustodian.ch/inst#Process201229> .\n" +
		"<https://sdc.ch/inst#Contract123> <https://w3id.org/dpv#hasProcess> " +
		"<https://swisscustodian.ch/inst#Process907720> .\n" +
		"<https://sdc.ch/inst#Contract123> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> " +
		"<https://w3id.org/dpv#Contract> .\n"
	graph, _ := graph.SerializeToGraph([]byte(contract), utils.Triples)

	assert.True(t, checkCustodianURI(graph))
}

func TestHasOrphanNode(t *testing.T) {
	contractNoOrphan := `
        <https://sdc.ch/inst#C123> <https://w3id.org/dpv#hasProcess> <https://swisscustodian.ch/inst#Process201229> .
        <https://sdc.ch/inst#C123> <https://w3id.org/dpv#hasProcess> <https://swisscustodian.ch/inst#Process907720> .
        <https://sdc.ch/inst#C123> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <https://w3id.org/dpv#Contract> .
        <https://sdc.ch/inst#C123> <hhttps://w3id.org/dpv#CA> <https://sdc.ch/inst#CA209327> .
        <https://sdc.ch/inst#CA209327> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <https://w3id.org/dpv#CA> .`
	contractOrphan := `
<https://sdc.ch/inst#C123> <https://w3id.org/dpv#hasProcess> <https://swisscustodian.ch/inst#Process201229> .
        <https://sdc.ch/inst#C123> <https://w3id.org/dpv#hasProcess> <https://swisscustodian.ch/inst#Process907720> .
        <https://sdc.ch/inst#C123> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <https://w3id.org/dpv#Contract> .
        <https://sdc.ch/inst#CA209327> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <https://w3id.org/dpv#CA> .`
	hasOrphan, _ := graph.SerializeToGraph([]byte(contractOrphan), utils.Triples)
	noOrphan, _ := graph.SerializeToGraph([]byte(contractNoOrphan), utils.Triples)

	assert.True(t, hasOrphanNode(hasOrphan))
	assert.False(t, hasOrphanNode(noOrphan))
}
