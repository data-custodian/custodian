package owl

import (
	"custodian/components/lib-common/pkg/common"
	g "custodian/components/lib-common/pkg/graph"
	"custodian/components/lib-common/pkg/log"

	"github.com/piprate/json-gold/ld"
)

// CheckOWL checks if a graph is compliant with the ontology.
func CheckOWL(graph *g.Graph) bool {
	hasSingleContract := hasSingleContract(graph)
	hasSignatures := hasSignatures(graph)
	uri := checkCustodianURI(graph)
	orphan := hasOrphanNode(graph)
	valid := hasSingleContract && !hasSignatures && uri && !orphan
	if !valid {
		log.Error(
			"Contract is not valid.",
			"single-contract",
			hasSingleContract,
			"has-signature",
			hasSignatures,
			"uriCheck",
			uri,
			"contains-other-graphs",
			orphan,
		)
	}

	return valid
}

// hasSingleContract checks that there is only one instance of contract in the graph.
func hasSingleContract(graph *g.Graph) bool {
	count := 0
	for _, item := range graph.Graphs[g.DefaultName] {
		if item.Predicate.GetValue() == rdfType && item.Object.GetValue() == contractObject {
			count += 1
		}
	}

	return count == 1
}

// hasSignatures returns `true` if signatures are present in the contract.
func hasSignatures(graph *g.Graph) bool {
	for _, item := range graph.Graphs[g.DefaultName] {
		if item.Predicate.GetValue() == signaturePredicate {
			return true
		}
	}

	return false
}

// checkCustodianURI checks if the URI in a graph belongs to the custodian and if it exists.
func checkCustodianURI(_graph *g.Graph) bool {
	// TODO.
	// Checks that URI exists.
	// If they don't exist but are in custodian domain -> deny.
	// How do we check that it exists indeed ?
	// check ontology file for predicates.
	// ask DB for resources?
	return true
}

// hasOrphanNode checks if a graph contains orphan nodes.
func hasOrphanNode(graph *g.Graph) bool {
	defaultG := graph.Graphs[g.DefaultName]
	var rootNode ld.Node
	for _, item := range defaultG {
		if item.Object.GetValue() == contractObject {
			rootNode = item.Subject

			break
		}
	}
	if rootNode == nil {
		return false
	}
	// All children (objects) to a certain subject node.
	childMap := make(map[string][]ld.Node, len(defaultG))
	for _, edge := range defaultG {
		if ld.IsIRI(edge.Object) {
			key := edge.Subject.GetValue()
			childMap[key] = append(childMap[key], edge.Object)
		}
	}
	visitedCount := len(visit(rootNode, childMap))
	allCount := countAllNodes(graph)
	hasOrphans := visitedCount != allCount

	if hasOrphans {
		log.Warn("Graph contains orphans.",
			"visited", visitedCount,
			"all", allCount, "orphans", allCount-visitedCount)
	}

	return hasOrphans
}

type empty struct{}

// visit is an implementation of bfs algorithm.
func visit(rootNode ld.Node, childMap map[string][]ld.Node) map[string]empty {
	visited := map[string]empty{}
	bfsStack := common.NewStack[ld.Node]()
	bfsStack.Push(rootNode)
	for bfsStack.Len() != 0 {
		currNode := bfsStack.PopFront()
		iri := currNode.GetValue()
		if _, exists := visited[iri]; exists {
			continue
		}
		// Add all children, set node visited and continue.
		bfsStack.Push(childMap[iri]...)
		visited[iri] = empty{}
	}

	return visited
}

// countAllNodes counts the number of unique nodes (URI) in a graph.
func countAllNodes(graph *g.Graph) int {
	nodeSet := make(map[string]empty)
	for _, item := range graph.Graphs[g.DefaultName] {
		nodeSet[item.Subject.GetValue()] = empty{}
		if ld.IsIRI(item.Object) {
			nodeSet[item.Object.GetValue()] = empty{}
		}
	}

	return len(nodeSet)
}
