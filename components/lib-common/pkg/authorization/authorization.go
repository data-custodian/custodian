package authorization

import (
	g "custodian/components/lib-common/pkg/graph"
	"custodian/components/lib-common/pkg/id"
)

// HasContractAccess checks that the user is in the contract and can see it.
func HasContractAccess(user id.GeneralID, contract *g.Graph) bool {
	// TODO : check roles in token when they exist.
	graph := contract.Graphs[g.DefaultName]
	for _, data := range graph {
		if data.Object.GetValue() == user.URI() {
			return true
		}
	}

	return false
}

// HasUserAccess checks that the user can see another user.
func HasUserAccess(_user id.GeneralID, _contract *g.Graph) bool {
	// TODO
	// TODO : check roles in token when they exist
	// check if current user and user share a contract together ?
	// TODO : by default returns only the uri ? or the name ?

	return true
}

// HasDatasetAccess checks that the user can see a dataset.
func HasDatasetAccess(_user id.GeneralID, _contract *g.Graph) bool {
	// TODO
	// TODO : check roles in token when they exist
	// TODO : by default, can see it. If has a flag (i.e. sensitive ? private ? ) then needs granular check

	return true
}
