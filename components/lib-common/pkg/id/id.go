package id

import (
	pred "custodian/components/lib-common/pkg/predicates"
	"fmt"
	"strings"

	"github.com/google/uuid"
)

const (
	custodianOntologyID      string = "http://ontology.swiss-custodian.ch/"
	custodianOntologyEnumsID string = "http://ontology.swiss-custodian.ch/enums"
	uriPrefixContract        string = "http://contracts.swiss-custodian.ch/"
	uriPrefixGeneral         string = pred.CustodianInstance
)

type ID interface {
	URI() string
}

func splitUUID(prefix string, uri string) (id uuid.UUID, err error) {
	idS := strings.TrimPrefix(uri, prefix)
	if idS == uri {
		err = fmt.Errorf("could not split prefix '%v' from '%v'", prefix, uri)

		return
	}
	id, err = uuid.Parse(idS)
	if err != nil {
		err = fmt.Errorf("cannot parse UUID '%v'", idS)
	}

	return
}

func GetOntologyEnumsIDCustodian() OntologyID {
	return OntologyID{custodianOntologyEnumsID}
}

func GetOntologyIDCustodian() OntologyID {
	return OntologyID{custodianOntologyID}
}

// NewContractID returns a unique ID for the contract.
func NewContractID(uuid uuid.UUID) ContractID {
	return ContractID{uuid}
}

// NewContractIDFromURI returns a contract ID from a URI.
func NewContractIDFromURI(uri string) (id ContractID, err error) {
	i, err := splitUUID(uriPrefixContract, uri)
	if err != nil {
		return
	}

	return ContractID{i}, nil
}

// NewGeneralID returns a general unique ID for other data.
func NewGeneralID(uuid uuid.UUID) GeneralID {
	return GeneralID{uuid}
}

// NewGeneralIDFromURI returns a general ID from a URI.
func NewGeneralIDFromURI(uri string) (id GeneralID, err error) {
	i, err := splitUUID(uriPrefixGeneral, uri)

	if err != nil {
		return
	}

	return GeneralID{i}, nil
}

func EmptyGeneralID() GeneralID {
	return GeneralID{uuid.UUID{}}
}
