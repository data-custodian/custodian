package id

import "github.com/google/uuid"

// ContractID represents an ID of a contract.
type ContractID struct {
	id uuid.UUID
}

func (i ContractID) ID() uuid.UUID {
	return i.id
}

func (i ContractID) URI() string {
	return uriPrefixContract + i.id.String()
}
