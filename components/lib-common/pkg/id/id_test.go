package id

import (
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func TestID(t *testing.T) {
	uuid := uuid.New()
	assert.Equal(t, "http://ontology.swiss-custodian.ch/", GetOntologyIDCustodian().URI())
	assert.Equal(t, "http://contracts.swiss-custodian.ch/"+uuid.String(), NewContractID(uuid).URI())
	assert.Equal(t, "https://swisscustodian.ch/inst#"+uuid.String(), NewGeneralID(uuid).URI())
}

func TestSplitUUID(t *testing.T) {
	uuid := uuid.New()
	uri := NewContractID(uuid)
	split, _ := splitUUID(uriPrefixContract, uri.URI())
	assert.Equal(t, uuid, split)
}
