package id

import "github.com/google/uuid"

// GeneralID represents a general id.
type GeneralID struct {
	id uuid.UUID
}

func (i GeneralID) ID() uuid.UUID {
	return i.id
}

func (i GeneralID) URI() string {
	return uriPrefixGeneral + i.id.String()
}
