package id

// OntologyID represents the id for an ontology.
type OntologyID struct {
	uri string
}

func (i OntologyID) ID() string {
	return i.uri
}

func (i OntologyID) URI() string {
	return i.uri
}
