package db

import (
	"context"

	"go.mongodb.org/mongo-driver/bson"
)

type DBConnection interface {
	Store(ctx context.Context, entry any) error
	Count(ctx context.Context, filter bson.M) (int64, error)
	Disconnect(context.Context) error
}
