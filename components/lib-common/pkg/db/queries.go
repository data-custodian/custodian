package db

import (
	"context"
	mongodb "custodian/components/lib-common/pkg/db/mongo"

	"go.mongodb.org/mongo-driver/bson"
)

// FindEntries finds all entries given a
// filter, limit, skip and sort options.
func FindEntries[T any](
	ctx context.Context,
	conn DBConnection,
	filter bson.M,
	limit int64,
	skip int64,
	sort bson.D) (events []T, err error) {
	if db, ok := conn.(mongodb.MongoDBConnection); ok {
		return mongodb.FindEntries[T](ctx, db, filter, limit, skip, sort)
	}

	panic("Not implemented")
}
