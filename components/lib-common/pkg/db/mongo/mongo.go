package mongodb

import (
	"context"
	dbg "custodian/components/lib-common/pkg/debug"
	log "custodian/components/lib-common/pkg/log"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type MongoDBConnection struct {
	Client     *mongo.Client
	Collection *mongo.Collection
}

func (m MongoDBConnection) Store(ctx context.Context, entry any) (err error) {
	// Perform InsertOne operation & validate against the error.
	_, err = m.Collection.InsertOne(ctx, entry)
	if err != nil {
		log.ErrorE(err, "Insert into MongoDB failed.", "entry", entry)

		return
	}

	return
}

func (m MongoDBConnection) Disconnect(ctx context.Context) (err error) {
	err = m.Client.Disconnect(ctx)
	if err != nil {
		log.ErrorE(err, "Disconnect from Mongodb failed", "client", m.Client)

		return
	}

	return
}

// NewMongoDBConnection connects to the MongoDB server and returns the connected database struct
// Make sure to call Disconnect() on the returned struct when done.
func NewMongoDBConnection(ctx context.Context, conf *MongoDBConfig) (MongoDBConnection, error) {
	serverAPI := options.ServerAPI(options.ServerAPIVersion1)

	credential := options.Credential{
		AuthSource: conf.AuthenticationDB,
		Username:   conf.Credentials.Username,
		Password:   conf.Credentials.Token,
	}

	opts := options.Client().
		ApplyURI(conf.URL).
		SetServerAPIOptions(serverAPI).
		SetAuth(credential)

	client, err := mongo.Connect(ctx, opts)
	if err != nil {
		log.ErrorE(err, "Connect to MongoDB failed.", "options", opts)

		return MongoDBConnection{}, err
	}
	log.Info("Mongodb Server started.")

	log.Info("Settings MongoDB collection.", "collection", conf.Collection)
	collection := client.Database(conf.DB).Collection(conf.Collection)
	dbg.Assert(collection != nil, "DB collection is not available.")

	return MongoDBConnection{
		Client:     client,
		Collection: collection,
	}, nil
}

func (m MongoDBConnection) Count(
	ctx context.Context,
	filter bson.M,
) (int64, error) {
	count, err := m.Collection.CountDocuments(ctx, filter)
	if err != nil {
		return 0, err
	}

	return count, nil
}

// FindEntries finds all entries given a
// filter, limit, skip and sort options.
func FindEntries[T any](
	ctx context.Context,
	conn MongoDBConnection,
	filter bson.M,
	limit int64,
	skip int64,
	sort bson.D) (events []T, err error) {
	options := options.Find()

	options.SetSort(sort)
	options.SetLimit(limit)
	options.SetSkip(skip)

	cur, err := conn.Collection.Find(ctx, filter, options)
	if err != nil {
		return
	}
	defer func() {
		e := cur.Close(ctx)
		if e != nil {
			log.ErrorE(e, "Closing MongoDB cursor failed.")
		}
	}()

	// Map result to slice.
	for cur.Next(ctx) {
		var t T

		err = cur.Decode(&t)

		if err != nil {
			return
		}

		events = append(events, t)
	}

	return events, nil
}
