package mongodb

type MongoDBConfig struct {
	AuthenticationDB string      `yaml:"authenticationDB"`
	Collection       string      `yaml:"collection"`
	Credentials      Credentials `yaml:"credentials"`
	DB               string      `yaml:"db"`
	URL              string      `yaml:"url"`
}

type Credentials struct {
	Username string `yaml:"username"`
	Token    string `yaml:"token"`
}
