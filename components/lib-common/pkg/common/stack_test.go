//nolint:dupl // Allowed in this tests.
package common

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestStack(t *testing.T) {
	stack := Stack[string]{}

	stack.Push("a")
	assert.Equal(t, 1, stack.Len())

	stack.Push("b")
	assert.Equal(t, 2, stack.Len())

	assert.Equal(t, "b", stack.Pop())
	assert.Equal(t, 1, stack.Len())

	stack.Push("c")
	assert.Equal(t, 2, stack.Len())

	assert.Equal(t, "c", stack.Pop())
	assert.Equal(t, 1, stack.Len())

	assert.Equal(t, "a", stack.Pop())
	assert.Equal(t, 0, stack.Len())

	assert.Panics(t, func() { _ = stack.Pop() })
}

func TestStackFrontPop(t *testing.T) {
	stack := NewStackWithCap[string](10)

	stack.Push("a")
	assert.Equal(t, 1, stack.Len())

	stack.Push("b")
	assert.Equal(t, 2, stack.Len())

	assert.Equal(t, "a", stack.PopFront())
	assert.Equal(t, 1, stack.Len())

	stack.Push("c")
	assert.Equal(t, 2, stack.Len())

	assert.Equal(t, "b", stack.PopFront())
	assert.Equal(t, 1, stack.Len())

	assert.Equal(t, "c", stack.Pop())
	assert.Equal(t, 0, stack.Len())

	assert.Panics(t, func() { _ = stack.PopFront() })
}
