package log

import (
	"context"
	"custodian/components/lib-common/pkg/build"
	"log/slog"
	"os"
	"runtime"
	"time"

	"github.com/golang-cz/devslog"
)

// Our global default logger. Yes singletons are code-smell,
// but we allow it for the logging functionality.
var logger *slog.Logger = slog.Default()

// Get returns the logger, either setup
// by `Setup` or the default `slog` one.
func Get() *slog.Logger {
	return logger
}

// Setup sets up the default loggers for any component.
// In debug mode we use `devslog` package.
// In release mode we use the JSON handler to emit log messages.
// TODO: Having the source location in the log does not work yet, since
// the source location will point always to this location.
// Wrap it with https://github.com/samber/slog-multi.
func Setup() {
	if build.DebugEnabled {
		slogOpts := &slog.HandlerOptions{
			AddSource: true,
			Level:     slog.LevelDebug,
		}

		opts := &devslog.Options{
			HandlerOptions:    slogOpts,
			MaxSlicePrintSize: 4,
			SortKeys:          true,
			NewLineAfterLog:   false,
		}

		logger = slog.New(devslog.NewHandler(os.Stdout, opts))
	} else {
		logger = slog.New(slog.NewJSONHandler(os.Stdout,
			&slog.HandlerOptions{
				Level:     slog.LevelInfo,
				AddSource: true,
			}))
	}

	slog.SetDefault(logger)
}

// createRecord creates a record with the proper source level.
func createRecord(skipFns int, level slog.Level, msg string, args ...any) slog.Record {
	var pcs [1]uintptr
	runtime.Callers(3+skipFns, pcs[:]) // skip [Callers, createRecord, callers function]
	r := slog.NewRecord(time.Now(), level, msg, pcs[0])
	r.Add(args...)

	return r
}

// Debug will log an info if debug is enabled.
func Debug(msg string, args ...any) {
	if build.DebugEnabled {
		ctx := context.Background()
		const level = slog.LevelDebug

		if !logger.Enabled(ctx, level) {
			return
		}
		r := createRecord(0, level, msg, args...)
		_ = logger.Handler().Handle(ctx, r)
	}
}

// Info will log an info.
func Info(msg string, args ...any) {
	ctx := context.Background()
	const level = slog.LevelInfo

	if !logger.Enabled(ctx, level) {
		return
	}
	r := createRecord(0, level, msg, args...)
	_ = logger.Handler().Handle(ctx, r)
}

// Warn will log an info.
func Warn(msg string, args ...any) {
	warnS(1, msg, args...)
}

// Warn will log a warning for an error `err`.
func WarnE(err error, msg string, args ...any) {
	a := make([]interface{}, 0, 2+len(args))
	a = append(a, "error", err)
	a = append(a, args...)
	warnS(1, msg, a...)
}

// Error will log an error.
func Error(msg string, args ...any) {
	errorS(1, msg, args...)
}

// Error will log an error for `err`.
func ErrorE(err error, msg string, args ...any) {
	errorES(1, err, msg, args...)
}

// Panic will log and panic if `condition` is `true`.
func Panic(condition bool, msg string, args ...any) {
	if condition {
		errorS(1, msg, args...)
		panic("Condition not met, see log.")
	}
}

// PanicE will log and panic if `err` is not `nil`.
func PanicE(err error, msg string, args ...any) {
	if err != nil {
		errorES(1, err, msg, args...)
		panic(err)
	}
}

func warnS(skipFns int, msg string, args ...any) {
	ctx := context.Background()
	const level = slog.LevelWarn

	if !logger.Enabled(ctx, level) {
		return
	}
	r := createRecord(skipFns, level, msg, args...)
	_ = logger.Handler().Handle(ctx, r)
}

func errorS(skipFns int, msg string, args ...any) {
	ctx := context.Background()
	const level = slog.LevelError

	if !logger.Enabled(ctx, level) {
		return
	}
	r := createRecord(skipFns, level, msg, args...)
	_ = logger.Handler().Handle(ctx, r)
}

func errorES(skipFns int, err error, msg string, args ...any) {
	a := make([]interface{}, 0, 2+len(args))
	a = append(a, args...)
	a = append(a, "error", err)
	errorS(skipFns+1, msg, a...)
}
