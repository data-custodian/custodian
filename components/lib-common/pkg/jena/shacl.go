package jena

import (
	"bytes"
	"context"
	"custodian/components/lib-common/pkg/errors"
	g "custodian/components/lib-common/pkg/graph"
	utils "custodian/components/lib-common/pkg/http"
	"custodian/components/lib-common/pkg/id"
	"custodian/components/lib-common/pkg/log"
	"custodian/components/lib-common/pkg/shacl"
	"net/http"
	u "net/url"
	"strconv"
)

// ValidateShaclShape validates a graph according to its ontology.
//
//nolint:nakedret // TODO change this in another PR
func (j Jena) ValidateShaclShape(graph, shape id.ID) (report shacl.ValidationReport, err error) {
	// Get ontology.
	path, err := u.JoinPath(Ontology, endPointData)
	if err != nil {
		err = errors.AddContext(err, "url join path did not work")

		return
	}
	url := &u.URL{
		Scheme:   j.Scheme,
		Host:     j.Host,
		Path:     path,
		RawQuery: u.Values{"graph": {shape.URI()}}.Encode(),
	}

	req, err := http.NewRequestWithContext(context.Background(), http.MethodGet, url.String(), nil)
	if err != nil {
		return
	}

	req.Header.Add("Accept", utils.Jsonld)

	status, resp := j.sendQuery(req)
	if status != http.StatusOK {
		err = errors.New("jena query to retrieve ontology failed with status %d : %s", status, resp)

		return
	}

	// Post the shape file with the data to validate as a URL parameter.
	path, err = u.JoinPath(Validation, endPointValidation)
	if err != nil {
		return
	}

	url = &u.URL{
		Scheme:   j.Scheme,
		Host:     j.Host,
		Path:     path,
		RawQuery: u.Values{"graph": {graph.URI()}}.Encode(),
	}

	req2, err := http.NewRequestWithContext(context.Background(), http.MethodPost, url.String(), bytes.NewBuffer(resp))
	if err != nil {
		return
	}

	req2.Header.Set("Content-Type", utils.Jsonld)
	req2.Header.Add("Accept", utils.Jsonld)

	status, resp = j.sendQuery(req2)
	if status != http.StatusOK {
		err = errors.New("jena query to validate shacl shape failed with status %d : %s", status, resp)

		return
	}

	// Analyze the result
	validationReport, err := g.SerializeToGraph(resp, utils.Jsonld)
	if err != nil {
		log.ErrorE(err, "Failed to serialize to graph.")

		return
	}

	// Get conforms value.
	getConformsObj := g.GetObjectsByPredicate(validationReport, shaclConforms)

	if len(getConformsObj) != 1 {
		log.Error("could not find conforms value", "error message", err)

		return
	}

	isValid, err := strconv.ParseBool(getConformsObj[0])

	if err != nil {
		log.Error("could not parse conforms value", "error message", err)

		return
	}

	// Gets error message if not conforms.
	// getResultMessage := g.GetObjectsByPredicate(validationReport, shaclReportMessage).
	// result := strings.Join(getResultMessage, "\n").

	result := g.GraphToFormatString(validationReport)
	report = shacl.NewValidationReport(isValid, result)

	return
}
