package jena

import (
	"bytes"
	"context"
	"custodian/components/lib-common/pkg/errors"
	g "custodian/components/lib-common/pkg/graph"
	utils "custodian/components/lib-common/pkg/http"
	"custodian/components/lib-common/pkg/id"
	"custodian/components/lib-common/pkg/log"
	"encoding/json"
	"net/http"
	u "net/url"
)

// GetGraph retrieves a graph from DB.
func (j Jena) GetGraph(database string, graphID id.ID) (graph *g.Graph, err error) {
	path, err := u.JoinPath(database, endPointData)
	if err != nil {
		err = errors.AddContext(err, "url join path did not work")

		return
	}
	url := &u.URL{
		Scheme:   j.Scheme,
		Host:     j.Host,
		Path:     path,
		RawQuery: u.Values{"graph": {graphID.URI()}}.Encode(),
	}
	// Post graphID to graph in Jena DB.
	log.Info("Get graphID.", "url", url.String())
	req, err := http.NewRequestWithContext(context.Background(), http.MethodGet, url.String(), nil)
	if err != nil {
		return
	}
	// Default format is RDF/XML and may return errors if the graph contains special character - like #.
	req.Header.Set("Accept", utils.Triples)

	status, resp := j.sendQuery(req)
	if status != http.StatusOK {
		err = errors.New("get graphID failed with status %d : %s", status, resp)

		return
	}
	log.Info("Get graphID response.", "response", string(resp))
	graph, err = g.SerializeToGraph(resp, utils.Triples)

	return
}

// DeleteGraph deletes a graph in the DB.
func (j Jena) DeleteGraph(database string, graphID id.ID) (err error) {
	path, err := u.JoinPath(database, endPointData)
	if err != nil {
		err = errors.AddContext(err, "url join path did not work")

		return
	}
	url := &u.URL{
		Scheme:   j.Scheme,
		Host:     j.Host,
		Path:     path,
		RawQuery: u.Values{"graph": {graphID.URI()}}.Encode(),
	}

	// Post graphID to graph in Jena DB.
	log.Info("Delete graphID.", "url", url.String())
	req, err := http.NewRequestWithContext(context.Background(), http.MethodDelete, url.String(), nil)
	if err != nil {
		return
	}
	status, resp := j.sendQuery(req)
	if status != http.StatusNoContent {
		err = errors.New("delete graphID failed with status %d : %s", status, string(resp))

		return
	}

	return
}

// PostGraph posts a graph in the DB.
// temporary is set to true if the graph is meant to be tested (i.e. shacl shape validations).
func (j Jena) PostGraph(graph *g.Graph, graphID id.ID, database string) (err error) {
	err = j.postGraph(graph, graphID, database)

	return
}

// PostTmpGraph posts a graph in the temporary dataset of the DB.
// temporary is set to true if the graph is meant to be tested (i.e. shacl shape validations).
func (j Jena) PostTmpGraph(graph *g.Graph, graphID id.ID) (deleteCallback func() error, err error) {
	err = j.postGraph(graph, graphID, Validation)
	deleteCallback = func() error { return j.DeleteGraph(Validation, graphID) }

	return
}

// postGraph posts a graph in the DB.
// temporary is set to true if the graph is meant to be tested (i.e. shacl shape validations).
func (j Jena) postGraph(graph *g.Graph, graphID id.ID, database string) (err error) {
	path, err := u.JoinPath(database, endPointData)
	if err != nil {
		err = errors.AddContext(err, "url join path did not work")

		return
	}
	url := &u.URL{
		Scheme: j.Scheme,
		Host:   j.Host,
		Path:   path,
	}
	// In the KB, everything lives in the same graphID.
	if database != KnowledgeBase {
		url.RawQuery = u.Values{"graph": {graphID.URI()}}.Encode()
	}

	log.Info("Upload graph.", "url", url.String())
	dataLD, err := g.GraphToJsonld(graph)
	if err != nil {
		return
	}
	dat, err := json.Marshal(dataLD)
	if err != nil {
		return
	}
	req, err := http.NewRequestWithContext(context.Background(), http.MethodPost, url.String(), bytes.NewBuffer(dat))
	if err != nil {
		return
	}
	req.Header.Set("Content-Type", utils.Jsonld)
	status, resp := j.sendQuery(req)
	if status != http.StatusCreated && status != http.StatusOK {
		err = errors.New("post graph failed with status %d : %s", status, resp)

		return
	}

	return
}
