package jena

const (

	// Databases.
	custodianDB     string = "custodian"
	knowledgeBaseDB string = "knowledgebase"

	// Datasets.
	contracts  string = "contracts"
	validation string = "validation"
	ontology   string = "ontology"
	dataset    string = "dataset"

	// Endpoints.
	endPointQuery      string = "sparql"
	endPointData       string = "data"
	endPointGet        string = "get"
	endPointValidation string = "shacl"
	endPointUpdate     string = "update"

	KnowledgeBase = knowledgeBaseDB + "/" + dataset
	Contracts     = custodianDB + "/" + contracts
	Validation    = custodianDB + "/" + validation
	Ontology      = custodianDB + "/" + ontology
)
