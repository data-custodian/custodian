package jena

import (
	"bytes"
	"context"
	"custodian/components/lib-common/pkg/errors"
	g "custodian/components/lib-common/pkg/graph"
	utils "custodian/components/lib-common/pkg/http"
	"custodian/components/lib-common/pkg/log"
	"net/http"
	u "net/url"
)

// FetchFilteredTriples posts a SPARQL query and returns selected triples from a graph.
func (j Jena) FetchFilteredTriples(endpoint string, sparql string) (output []string, err error) {
	path, err := u.JoinPath(endpoint, endPointQuery)
	if err != nil {
		return nil, errors.AddContext(err, "url join path did not work")
	}

	url := &u.URL{
		Scheme:   j.Scheme,
		Host:     j.Host,
		Path:     path,
		RawQuery: u.Values{"output": {"csv"}}.Encode(),
	}

	// Convert query to byte and to endpoint format.
	query := []byte("query=" + sparql)

	// Post query
	log.Info("Post Query.", "url", url.String())
	req, err := http.NewRequestWithContext(context.Background(), http.MethodPost, url.String(), bytes.NewBuffer(query))
	if err != nil {
		log.Error("error with post", err)

		return
	}

	req.Header.Set("Content-Type", utils.Tsv)

	status, resp := j.sendQuery(req)

	response := string(resp)

	if status != http.StatusOK {
		err = errors.New("post query returned bad status %d : %s", status, response)

		return
	}

	output = destructureResponse(response)

	return
}

// FetchGraph posts a SPARQL query and returns a complete graph.
func (j Jena) FetchGraph(endpoint string, sparql string) (*g.Graph, error) {
	path, err := u.JoinPath(endpoint, endPointQuery)
	if err != nil {
		return nil, errors.AddContext(err, "url join path did not work")
	}

	url := &u.URL{
		Scheme:   j.Scheme,
		Host:     j.Host,
		Path:     path,
		RawQuery: u.Values{"output": {"tsv"}}.Encode(),
	}

	// Convert query to byte and to endpoint format.
	query := []byte("query=" + sparql)

	// Post query.
	log.Info("Post Query.", "url", url.String())
	req, err := http.NewRequestWithContext(context.Background(), http.MethodPost, url.String(), bytes.NewBuffer(query))
	if err != nil {
		return nil, errors.AddContext(err, "could not send update request")
	}

	req.Header.Set("Content-Type", utils.Tsv)

	status, resp := j.sendQuery(req)

	log.Info("Query response.", "resp", string(resp))

	if status != http.StatusOK {
		return nil, errors.New("get data failed with status %d : %s", status, resp)
	}

	graph, err := g.SerializeToGraph(resp, utils.Tsv)

	if err != nil {
		return nil, errors.AddContext(err, "tsv could not be transformed to graph")
	}

	return graph, nil
}

// UpdateGraph updates a graph by adding some triples through a SPARQL query.
func (j Jena) UpdateGraph(endpoint string, sparql string) error {
	path, err := u.JoinPath(endpoint, endPointUpdate)
	if err != nil {
		return errors.AddContext(err, "url join path did not work")
	}

	url := &u.URL{
		Scheme: j.Scheme,
		Host:   j.Host,
		Path:   path,
	}

	// Convert query to byte and to endpoint format.
	query := []byte("update=" + sparql)

	// Post query.
	log.Info("Post Query.", "url", url.String())
	req, err := http.NewRequestWithContext(context.Background(), http.MethodPost, url.String(), bytes.NewBuffer(query))
	if err != nil {
		return errors.AddContext(err, "could not send update request")
	}

	req.Header.Set("Content-Type", utils.Tsv)

	status, b := j.sendQuery(req)

	if status != http.StatusOK {
		return errors.New("update data failed with status %d : %s", status, b)
	}

	return nil
}
