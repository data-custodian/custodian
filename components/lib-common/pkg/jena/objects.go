package jena

import pred "custodian/components/lib-common/pkg/predicates"

const (
	shaclConforms      string = pred.Shacl + "conforms"
	shaclReportMessage string = pred.Shacl + "resultMessage"
)
