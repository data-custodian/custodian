package jena

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDestructureResponse(t *testing.T) {
	test := "test"
	toDestruct := "will be removed\n" + test
	resp := destructureResponse(toDestruct)
	assert.Equal(t, test, resp[0])
}
