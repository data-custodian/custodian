package jena

import (
	"custodian/components/lib-common/pkg/log"
	"io"
	"net/http"
	"os"
	"strings"

	"crypto/tls"
	"crypto/x509"
)

const (
	credentialNumber = 2
)

type Jena struct {
	CredentialFile string
	Username       string
	Password       string
	// Haproxy conf
	Host        string
	Scheme      string
	CaTest      string
	HTTPClient  *http.Client
	HTTPSClient *http.Client
}

func New(host string, scheme string, cred string, certificateAuthorityPath string) Jena {
	var jena Jena

	jena.Host = host
	jena.Scheme = scheme
	jena.CredentialFile = cred
	jena.CaTest = certificateAuthorityPath

	// Read credentials for basic auth.
	fuseki, err := os.ReadFile(jena.CredentialFile)
	if err != nil {
		log.Error("error reading conf", err)
		os.Exit(1)
	}

	fusekiCred := string(fuseki)

	lines := strings.SplitN(fusekiCred, " ", credentialNumber)
	jena.Username = lines[0]
	jena.Password = strings.TrimSuffix(lines[1], "\n")

	// Create TLS client.
	// ca-template is a copy of the one in the folder platform jena haproxy ssl.
	caCert, err := os.ReadFile(jena.CaTest)
	if err != nil {
		log.Error("error reading conf", err)
		os.Exit(1)
	}
	caCertPool := x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(caCert)

	jena.HTTPSClient = &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				RootCAs:    caCertPool,
				MinVersion: tls.VersionTLS12,
			},
		},
	}
	jena.HTTPClient = &http.Client{}

	return jena
}

/*
 * Internal method to send a query.
 */
func (j Jena) sendQuery(req *http.Request) (int, []byte) {
	req.SetBasicAuth(j.Username, j.Password)

	resp, err := func() (*http.Response, error) {
		if req.TLS == nil {
			return j.HTTPClient.Do(req)
		}

		return j.HTTPSClient.Do(req)
	}()

	if err != nil {
		log.Error("error with client", err)

		return http.StatusInternalServerError, nil
	}

	defer func() {
		if resp.Body != nil {
			if e := resp.Body.Close(); err != nil {
				log.Error("error closing body", "error message", e)
			}
		}
	}()

	bodyBytes, err := io.ReadAll(resp.Body)

	if err != nil {
		log.Error("error with response", err)

		return http.StatusInternalServerError, nil
	}

	return resp.StatusCode, bodyBytes
}

func destructureResponse(response string) (output []string) {
	if response == "" {
		log.Info("Empty response.")

		return
	}
	response = strings.TrimSpace(response)
	response = strings.ReplaceAll(response, "\r", "")

	log.Info("Query response.", "resp", response)

	lines := strings.Split(response, "\n")

	// Remove the first line because it's the name of the variable for the jena query.
	output = lines[1:]

	if len(output) == 0 {
		log.Info("Empty response.")
	}

	return
}
