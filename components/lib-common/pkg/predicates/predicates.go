package predicates

const (
	SwissCustodian string = "https://swisscustodian.ch/doc/ontology#"
	Dpv            string = "https://w3id.org/dpv#"
	Dcat           string = "https://www.w3.org/ns/dcat#"
	Owl            string = "http://www.w3.org/2002/07/owl#"
	Rdf            string = "http://www.w3.org/1999/02/22-rdf-syntax-ns#"
	DataType       string = "http://www.w3.org/2001/XMLSchema#"
	Schema         string = "http://schema.org/"
	Did            string = "https://www.w3.org/ns/did/v1#"
	Shacl          string = "http://www.w3.org/ns/shacl#"
	SecVocab       string = "https://w3c-ccg.github.io/security-vocab/#"

	CustodianInstance string = "https://swisscustodian.ch/inst#"
)
