package shacl

// ValidationReport contains a SHACL validation report.
type ValidationReport struct {
	valid  bool
	report string
}

// NewValidationReport creates a ValidationReport according to the parameters in input.
func NewValidationReport(valid bool, report string) ValidationReport {
	return ValidationReport{valid, report}
}

// Valid returns `true` if the validation succeeded without errors.
func (v *ValidationReport) Valid() bool {
	return v.valid
}

// Report returns the report.
func (v *ValidationReport) Report() string {
	return v.report
}
