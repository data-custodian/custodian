package http

import (
	"custodian/components/lib-common/pkg/log"
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"net/url"
	"regexp"
	"strings"

	"github.com/gorilla/mux"

	"gopkg.in/validator.v2"
)

func CheckContentType(contentType string) bool {
	return !(contentType != Jsonld && contentType != Turtle)
}

func GetPathFromURL(r *http.Request, parameter string) (string, error) {
	vars := mux.Vars(r)
	param, ok := vars[parameter]
	if !ok {
		return "", errors.New("no param found")
	}
	param = strings.TrimSpace(param)
	uuidValidator := UUID{ID: param}
	if errs := validator.Validate(uuidValidator); errs != nil {
		return "", errs
	}

	return param, nil
}

func GetParameterFromURL(r *http.Request, parameter string) (string, error) {
	paramQuery, ok := r.URL.Query()[parameter]
	if !ok {
		return "", errors.New("Missing '" + parameter + "' parameter")
	}
	decodedParam, err := url.QueryUnescape(paramQuery[0])
	if err != nil {
		return "", err
	}
	decodedParam = strings.TrimSpace(decodedParam)

	return decodedParam, nil
}

// DecodeBody retrieve the body of a request.
func DecodeBody(r *http.Request) string {
	b, err := io.ReadAll(r.Body)
	if err != nil {
		return ""
	}
	body := string(b)

	return body
}

// DecodeBodyCheck retrieve the body of a request and returns an error if the content is too big.
func DecodeBodyCheck(r *http.Request, maxSizeContent int64) (string, error) {
	r.Body = http.MaxBytesReader(nil, r.Body, maxSizeContent)
	contentBytes, err := io.ReadAll(r.Body)
	if err != nil {
		return "", err
	}
	content := string(contentBytes)

	return content, nil
}

// CheckUUID check if the parameter is an uuid.
func CheckUUID(param string) bool {
	re := regexp.MustCompile(`^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}$`)

	return re.MatchString(param)
}

// CheckWord check that the parameter has only letters in it.
func CheckWord(param string) bool {
	re := regexp.MustCompile(`^[A-Z][a-z]{1,15}$`)

	return re.MatchString(param)
}

// DecodeParameter decode a URL parameter.
func DecodeParameter(r *http.Request, param string) string {
	parameterQuery, ok := r.URL.Query()[param]
	if !ok {
		return ""
	}
	decodedParameter, err := url.QueryUnescape(parameterQuery[0])
	if err != nil {
		return ""
	}

	return decodedParameter
}

func EncodeUUIDResponse(w http.ResponseWriter, uuid []string) {
	w.WriteHeader(http.StatusOK)
	data := UUIDResponseStruct{uuid}
	e := json.NewEncoder(w)
	err := e.Encode(data)
	if err != nil {
		log.Error("Failed to encode response", "error message", err)

		return
	}
}

func EncodeJSONLDStruct(w http.ResponseWriter, jsonld []interface{}) {
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/ld+json")
	data := JSONLDResponseStruct{jsonld}
	e := json.NewEncoder(w)
	err := e.Encode(data)
	if err != nil {
		log.Error("Failed to encode response", "error message", err)

		return
	}
}

func EncodeTurtleStruct(w http.ResponseWriter, ttl string) {
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "text/turtle")
	data := TurtleResponseStruct{ttl}
	e := json.NewEncoder(w)
	err := e.Encode(data)
	if err != nil {
		log.Error("Failed to encode response", "error message", err)

		return
	}
}

func EncodeContractStatusResponse(w http.ResponseWriter, contractStatus bool, userStatus map[string]string) {
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	cs := ContractStatus{ContractStatus: contractStatus, UserStatus: userStatus}
	data := ContractStatusResponse{Data: cs}
	e := json.NewEncoder(w)
	err := e.Encode(data)
	if err != nil {
		log.Error("Failed to encode response", "error message", err)

		return
	}
}

// EncodeResponse encodes `text` in json, and sends it to the response writer with the status code `statusCode`.
func EncodeResponse(w http.ResponseWriter, text string) {
	w.WriteHeader(http.StatusOK)
	data := ResponseStruct{text}
	e := json.NewEncoder(w)
	err := e.Encode(data)
	if err != nil {
		log.Error("Failed to encode response", "error message", err)

		return
	}
}

func EncodeError(w http.ResponseWriter, text string, statusCode int) {
	w.WriteHeader(statusCode)
	err := Error{StatusCode: statusCode, Message: text}
	data := ErrorResponse{err}
	e := json.NewEncoder(w)
	er := e.Encode(data)
	if er != nil {
		log.Error("Failed to encode response", "error message", err)

		return
	}
}
