package http

type ResponseStruct struct {
	Resp string `json:"data"`
}

type ErrorResponse struct {
	Error Error `json:"error"`
}

type Error struct {
	StatusCode int    `json:"statusCode"`
	Message    string `json:"message"`
}

type ContractStatus struct {
	ContractStatus bool              `json:"contractIsValid"`
	UserStatus     map[string]string `json:"usersStatus"`
}

type ContractStatusResponse struct {
	Data ContractStatus `json:"data"`
}

type UUIDResponseStruct struct {
	Data []string `json:"data" example:"['123e4567-e89b-12d3-a456-426614174000','123e4567-e89b-12d3-a456-426614174001']"`
}

type JSONLDResponseStruct struct {
	Data []interface{} `json:"data"`
}

type TurtleResponseStruct struct {
	Data string `json:"data"`
}

type UUID struct {
	ID string `validate:"regexp=^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$"`
}
