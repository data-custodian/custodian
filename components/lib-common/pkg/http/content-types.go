package http

const (
	Jsonld  string = "application/ld+json"
	Turtle  string = "text/turtle"
	Triples string = "application/n-triples"
	Tsv     string = "application/x-www-form-urlencoded"
	Pem     string = "application/x-pem-file"
)
