package graph

import (
	"custodian/components/lib-common/pkg/errors"
	"custodian/components/lib-common/pkg/id"
	"custodian/components/lib-common/pkg/log"
	pred "custodian/components/lib-common/pkg/predicates"
	"time"

	"github.com/piprate/json-gold/ld"
)

const (
	two = 2
)

// GetDataControllerURI returns a list of the DataControllers in a graph.
func GetDataControllerURI(graph *Graph) []string {
	dc := GetObjectsByPredicate(graph, DataController)

	return dc
}

// getID returns the URI.
func getID(graph *Graph) string {
	return graph.Graphs[DefaultName][0].Subject.GetValue()
}

// GetID returns the URI.
func GetID(graph *Graph) id.GeneralID {
	i := graph.Graphs["@default"][0].Subject.GetValue()
	newID, err := id.NewGeneralIDFromURI(i)
	if err != nil {
		return id.EmptyGeneralID()
	}

	return newID
}

// GetObjectsByPredicate returns a list of objects that match the predicate.
func GetObjectsByPredicate(graph *Graph, predicate string) []string {
	var values []string
	for _, item := range graph.Graphs[DefaultName] {
		// TODO : not hardcode
		if item.Predicate.GetValue() == predicate {
			values = append(values, item.Object.GetValue())
		}
	}

	return values
}

func GetSubGraphBySubject(graph *Graph, subject string) *Graph {
	var values []*ld.Quad
	for _, item := range graph.Graphs[DefaultName] {
		// TODO : not hardcode
		if item.Subject.GetValue() == subject {
			values = append(values, item)
		}
	}
	newDataset := ld.NewRDFDataset()
	newDataset.Graphs[DefaultName] = values

	return newDataset
}

func GetSubjectsByPredicate(graph *Graph, predicate string, object string) []string {
	var values []string
	for _, item := range graph.Graphs[DefaultName] {
		if item.Predicate.GetValue() == predicate && item.Object.GetValue() == object {
			values = append(values, item.Subject.GetValue())
		}
	}

	return values
}

func GetObjectsBySubject(graph *Graph, subject string, predicate string) []string {
	var values []string
	for _, item := range graph.Graphs[DefaultName] {
		if item.Predicate.GetValue() == predicate && item.Subject.GetValue() == subject {
			values = append(values, item.Object.GetValue())
		}
	}

	return values
}

// CreateSignature returns a new signature metadata that must be signed.
func CreateSignature(
	signatureID id.GeneralID,
	contract id.ContractID,
	user id.GeneralID,
	contractHash string,
	signatureStatus string,
) *Graph {
	dataset := ld.NewRDFDataset()
	var quads []*ld.Quad

	subject := ld.NewLiteral(signatureID.URI(), "", "")

	quad := ld.NewQuad(
		subject,
		ld.NewIRI(RdfType),
		ld.NewIRI(DigitalSignature),
		"",
	)
	quads = append(quads, quad)

	quad = ld.NewQuad(subject, ld.NewIRI(SignatureOf), ld.NewIRI(contract.URI()), "")
	quads = append(quads, quad)

	// TODO : not harcoded custodian key
	quad = ld.NewQuad(subject, ld.NewIRI(SignedBy), ld.NewIRI(pred.CustodianInstance+"custodian-key"), "")
	quads = append(quads, quad)

	quad = ld.NewQuad(subject, ld.NewIRI(SignedFor), ld.NewIRI(user.URI()), "")
	quads = append(quads, quad)

	quad = ld.NewQuad(subject, ld.NewIRI(Signee), ld.NewIRI(user.URI()), "")
	quads = append(quads, quad)
	quad = ld.NewQuad(
		subject,
		ld.NewIRI(CreatedAt),
		ld.NewLiteral(time.Now().Format(time.RFC850), DataTypeString, ""),
		"",
	)
	quads = append(quads, quad)
	quad = ld.NewQuad(
		subject,
		ld.NewIRI(ContractStatus),
		ld.NewLiteral(signatureStatus, DataTypeString, ""),
		"",
	)
	quads = append(quads, quad)
	quad = ld.NewQuad(
		subject,
		ld.NewIRI(SignatureMethod),
		ld.NewLiteral("ECDSA", DataTypeString, ""),
		"",
	)
	quads = append(quads, quad)
	quad = ld.NewQuad(
		subject,
		ld.NewIRI(ContractHash),
		ld.NewLiteral(contractHash, DataTypeString, ""),
		"",
	)
	quads = append(quads, quad)

	(dataset).Graphs[DefaultName] = quads

	return dataset
}

// AddSignedSignature appends to the signature metadata the signature triples signed with a private key.
func AddSignedSignature(dataset *Graph, signatureH string) *Graph {
	quads := (dataset).Graphs[DefaultName]
	subject := ld.NewIRI(getID(dataset))
	quad := ld.NewQuad(
		subject,
		ld.NewIRI(Signature),
		ld.NewLiteral(signatureH, DataTypeString, ""),
		"",
	)
	quads = append(quads, quad)
	dataset.Graphs[DefaultName] = quads

	return dataset
}

//nolint:gocognit
func GetSignaturesStatus(dataset *Graph) (map[string]string, error) {
	// Retrieve all users that must sign the contract.
	users := GetObjectsByPredicate(dataset, DataController)
	dp := GetObjectsByPredicate(dataset, DataProcessor)

	if len(users) < 1 || len(dp) < 1 {
		return nil, errors.New(
			"missing data processor (len = %d) or data controller (len = %d)",
			len(dp),
			len(users),
		)
	}

	users = append(users, dp...)
	signatureStatus := make(map[string]string)
	var subject string

	for _, userURI := range users {
		uURI, err := id.NewGeneralIDFromURI(userURI)
		if err != nil {
			return nil, err
		}
		signaturesSubj := GetSubjectsByPredicate(dataset, Signee, uURI.URI())
		// No signature.
		switch {
		case signatureStatus == nil || len(signaturesSubj) < 1:
			// No signature found.
			signatureStatus[uURI.URI()] = ""
		case len(signaturesSubj) > two:
			// Error
			return nil, errors.New("user has more than 2 signatures : %d", len(signaturesSubj))
		default:
			subject = signaturesSubj[0]

			// If multiple signatures, get latest one.
			if len(signaturesSubj) == two {
				time1 := GetObjectsBySubject(dataset, signaturesSubj[0], CreatedAt)
				time2 := GetObjectsBySubject(dataset, signaturesSubj[1], CreatedAt)

				layout := time.RFC850
				// Parsing the date-time strings into time.Time objects.
				dateTime1, e := time.Parse(layout, time1[0])
				if e != nil {
					return nil, errors.New("could not parse time date: '%s'", time1[0])
				}
				dateTime2, e := time.Parse(layout, time2[0])
				if e != nil {
					return nil, errors.New("could not parse time date: '%s'", time1[0])
				}
				if dateTime2.Unix() > dateTime1.Unix() {
					subject = signaturesSubj[1]
				}
			}
			signatureGraph := GetSubGraphBySubject(dataset, subject)
			log.Info("s", "s", signatureGraph)
			value := GetObjectsByPredicate(signatureGraph, ContractStatus)
			signatureStatus[uURI.URI()] = value[0]
		}
	}

	return signatureStatus, nil
}
