package graph

import (
	pred "custodian/components/lib-common/pkg/predicates"
)

const (

	// Objects.
	DataController string = pred.Dpv + "hasDataController"
	DataProcessor  string = pred.Dpv + "hasDataProcessor"
	Dataset        string = pred.Dpv + "hasData"

	// Signature.
	DataTypeString string = pred.DataType + "string"
	RdfType        string = pred.Rdf + "type"

	ContractStatus  string = pred.SwissCustodian + "status"
	Signee          string = pred.SwissCustodian + "signee"
	Signature       string = pred.SwissCustodian + "signature"
	ContractHash    string = pred.SwissCustodian + "contractHash"
	SignatureMethod string = pred.SwissCustodian + "signatureMethod"

	SignatureOf string = pred.SwissCustodian + "signatureOf"
	SignedBy    string = pred.SwissCustodian + "signedBy"
	SignedFor   string = pred.SwissCustodian + "signedFor"

	CreatedAt        = pred.Schema + "createdAt"
	DigitalSignature = pred.Did + "DigitalSignature"
)
