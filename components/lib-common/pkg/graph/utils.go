package graph

import (
	"github.com/piprate/json-gold/ld"
)

// ChangeInternalID modifies the subject of a graph.
func ChangeInternalID(dataset *Graph, id string) (*Graph, error) {
	graph := dataset.Graphs[DefaultName]
	subject := ld.NewLiteral(id, "", "")
	newDataset := ld.NewRDFDataset()
	var quads []*ld.Quad
	for _, data := range graph {
		q := ld.NewQuad(subject, data.Predicate, data.Object, "")
		quads = append(quads, q)
	}
	sanitisedTriples := make([]*ld.Quad, 0, len(quads))
	for _, t := range quads {
		if t.Valid() {
			sanitisedTriples = append(sanitisedTriples, t)
		}
	}
	newDataset.Graphs[DefaultName] = sanitisedTriples

	return newDataset, nil
}

// CopyGraph copies a graph to a new one.
func CopyGraph(dataset *Graph) (*Graph, error) {
	graph := dataset.Graphs[DefaultName]
	newDataset := ld.NewRDFDataset()
	var quads []*ld.Quad
	for _, data := range graph {
		q := ld.NewQuad(data.Subject, data.Predicate, data.Object, "")
		quads = append(quads, q)
	}
	sanitisedTriples := make([]*ld.Quad, 0, len(quads))
	for _, t := range quads {
		if t.Valid() {
			sanitisedTriples = append(sanitisedTriples, t)
		}
	}
	newDataset.Graphs[DefaultName] = sanitisedTriples

	return newDataset, nil
}

// AppendGraph copies a graph to an existing one.
func AppendGraph(target *Graph, toCopy *Graph) {
	graph := target.Graphs[DefaultName]

	var quads []*ld.Quad
	for _, data := range graph {
		q := ld.NewQuad(data.Subject, data.Predicate, data.Object, "")
		quads = append(quads, q)
	}
	graph2 := toCopy.Graphs[DefaultName]
	for _, data := range graph2 {
		q := ld.NewQuad(data.Subject, data.Predicate, data.Object, "")
		quads = append(quads, q)
	}
	sanitisedTriples := make([]*ld.Quad, 0, len(quads))
	for _, t := range quads {
		if t.Valid() {
			sanitisedTriples = append(sanitisedTriples, t)
		}
	}
	target.Graphs[DefaultName] = sanitisedTriples
}
