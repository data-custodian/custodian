package graph

import (
	"bytes"
	"custodian/components/lib-common/pkg/errors"
	utils "custodian/components/lib-common/pkg/http"
	"encoding/json"
	"fmt"
	"regexp"
	"sort"
	"strings"

	"github.com/knakk/rdf"
	"github.com/piprate/json-gold/ld"
)

type Quad struct {
	Subject   string
	Predicate string
	Object    string
	Graph     string
}

const DefaultName = "@default"

type Graph = ld.RDFDataset
type Quads []*ld.Quad

// GraphToJsonld takes a graph object as an input and returns a jsonld object.
func GraphToJsonld(graph *Graph) ([]interface{}, error) {
	api := ld.NewJsonLdApi()
	options := ld.NewJsonLdOptions("")
	doc, err := api.FromRDF(graph, options)
	if err != nil {
		return nil, err
	}

	return doc, nil
}

// SerializeToGraph takes bytes and a content type and returns a graph object.
func SerializeToGraph(data []byte, contentType string) (*Graph, error) {
	switch contentType {
	case utils.Jsonld:
		return jsonldToGraph(data)
	case utils.Turtle:
		return turtleToGraph(data)
	case utils.Triples:
		return triplesToGraph(data)
	case utils.Tsv:
		return tsvToGraph(data)
	}

	return nil, errors.New("unknown content type")
}

// tsvToGraph transforms a tsv string to a graph object.
func tsvToGraph(resp []byte) (*Graph, error) {
	// Format the string with the correct syntax.
	tsv := string(resp)
	tsv = strings.ReplaceAll(tsv, "	", " ")
	tsv = strings.ReplaceAll(tsv, "\r", " ")
	tsv = strings.ReplaceAll(tsv, "?subject ?predicate ?object\n", "")
	tsv = strings.ReplaceAll(tsv, "\n", " . \n")
	dataset, _ := ld.ParseNQuadsFrom(tsv)

	return dataset, nil
}

// turtleToGraph transforms a turtle string to a graph object.
func turtleToGraph(resp []byte) (*Graph, error) {
	decoder := rdf.NewTripleDecoder(bytes.NewReader(resp), rdf.Turtle)
	triples, err := decoder.DecodeAll()
	if err != nil {
		return nil, err
	}
	// Put them in a graph according to jsonld.
	dataset := ld.NewRDFDataset()
	var quads []*ld.Quad
	for _, triple := range triples {
		subject := ld.NewIRI(triple.Subj.String())
		predicate := ld.NewIRI(triple.Pred.String())

		var object ld.Node

		switch obj := triple.Obj.(type) {
		case rdf.IRI:
			object = ld.NewIRI(triple.Obj.String())
		case rdf.Literal:
			object = ld.NewLiteral(triple.Obj.String(), obj.DataType.String(), "")
		case rdf.Blank:
			object = ld.NewBlankNode(triple.Obj.String())
		}
		q := ld.NewQuad(subject, predicate, object, "")
		quads = append(quads, q)
	}
	sanitisedTriples := make([]*ld.Quad, 0, len(quads))
	for _, t := range quads {
		if t.Valid() {
			sanitisedTriples = append(sanitisedTriples, t)
		}
	}
	dataset.Graphs[DefaultName] = sanitisedTriples

	return dataset, nil
}

// GraphToTurtle takes a dataset as input and returns a turtle string.
func GraphToTurtle(dataset *Graph) (string, error) {
	triples := dataset.Graphs[DefaultName]

	var triplesSlice []rdf.Triple
	for _, triple := range triples {
		var t rdf.Triple
		subjIRI, err := rdf.NewIRI(triple.Subject.GetValue())
		if err != nil {
			return "", err
		}
		predIRI, err := rdf.NewIRI(triple.Predicate.GetValue())
		if err != nil {
			return "", err
		}
		if ld.IsIRI(triple.Object) {
			objIRI, e := rdf.NewIRI(triple.Object.GetValue())
			if e != nil {
				return "", e
			}
			t = rdf.Triple{
				Subj: subjIRI,
				Pred: predIRI,
				Obj:  objIRI,
			}
		} else {
			objLit, e := rdf.NewLiteral(triple.Object.GetValue())
			if e != nil {
				return "", e
			}
			t = rdf.Triple{
				Subj: subjIRI,
				Pred: predIRI,
				Obj:  objLit,
			}
		}
		triplesSlice = append(triplesSlice, t)
	}

	var buf bytes.Buffer
	enc := rdf.NewTripleEncoder(&buf, rdf.Turtle)
	err := enc.EncodeAll(triplesSlice)

	if err != nil {
		return "", err
	}
	err = enc.Close()
	if err != nil {
		return "", err
	}

	ttl := buf.String()

	ttl = strings.ReplaceAll(ttl, "\t", " ")
	ttl = strings.TrimSpace(ttl)

	// prefix may not be at the beginning of the file
	prefixRegex := regexp.MustCompile(`(?m)^@prefix[^\n]*\n`)
	prefixes := prefixRegex.FindAllString(ttl, -1)
	withoutPrefixes := prefixRegex.ReplaceAllString(ttl, "")
	result := strings.Join(prefixes, "") + "\n" + strings.TrimSpace(withoutPrefixes)

	return result, nil
}

// jsonldToGraph transforms a jsonld string to a graph object.
func jsonldToGraph(data []byte) (*Graph, error) {
	// To json.
	var input interface{}
	err := json.Unmarshal(data, &input)
	if err != nil {
		return nil, err
	}

	// Format it to flatten json ld.
	// Could be another format but if the data has "@graph" and "@context" -> does not work.
	// Because ToRDF is looking for []interface{} and the format mentioned above does not have that.
	proc := ld.NewJsonLdProcessor()

	options := ld.NewJsonLdOptions("")

	flattenedDoc, err := proc.Flatten(input, nil, options)
	if err != nil {
		return nil, err
	}

	api := ld.NewJsonLdApi()

	dataset, err := api.ToRDF(flattenedDoc, options)
	if err != nil {
		return nil, err
	}

	return dataset, nil
}

// triplesToGraph transforms a triple string to a graph object.
func triplesToGraph(resp []byte) (*Graph, error) {
	triples := string(resp)
	dataset, err := ld.ParseNQuadsFrom(triples)
	if err != nil {
		return nil, err
	}

	return dataset, nil
}

// GraphToFormatString takes a graph as input, formats is to a string of triples and sort it alphabetically.
func GraphToFormatString(dataset *Graph) string {
	triples := dataset.Graphs[DefaultName]
	var triplesSlice []string
	for _, triple := range triples {
		tmp := fmt.Sprintf(
			"<%s> <%s> \"%s\" .",
			triple.Subject.GetValue(),
			triple.Predicate.GetValue(),
			triple.Object.GetValue(),
		)
		triplesSlice = append(triplesSlice, tmp)
	}
	sort.Strings(triplesSlice)
	triplesStr := strings.Join(triplesSlice, "\n")

	return triplesStr
}
