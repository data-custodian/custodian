package graph

import (
	utils "custodian/components/lib-common/pkg/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestSerializeToGraph(t *testing.T) {
	jsonldStr := `{
	  "@context": {
	    "name": "http://schema.org/name",
	    "homepage": {
	      "@id": "http://schema.org/url",
	      "@type": "@id"
	    }
	  },
	  "@id": "http://example.org/person/123",
	  "name": "Jane Doe",
	  "homepage": "http://example.org/"
	}`
	turtleStr := `
		@prefix schema: <http://schema.org/> .
		@prefix ex: <http://example.org/person/> .
		ex:123 schema:name "Jane Doe" .
		ex:123 schema:url <http://example.org/> .`
	newGraphJ, _ := SerializeToGraph([]byte(jsonldStr), utils.Jsonld)
	newGraphT, _ := SerializeToGraph([]byte(turtleStr), utils.Turtle)

	assert.Equal(
		t,
		"http://example.org/person/123",
		newGraphJ.Graphs[DefaultName][0].Subject.GetValue(),
	)
	assert.Equal(
		t,
		"http://example.org/person/123",
		newGraphT.Graphs[DefaultName][0].Subject.GetValue(),
	)
}

func TestChangeInternalId(t *testing.T) {
	turtleStr := `
		@prefix schema: <http://schema.org/> .
		@prefix ex: <http://example.org/person/> .
		ex:123 schema:name "John Doe" .
		ex:123 schema:url <http://example.org/> .`

	newGraphT, _ := SerializeToGraph([]byte(turtleStr), utils.Turtle)
	newID := "http://this-is-a-new-id/123"
	newGraphT, _ = ChangeInternalID(newGraphT, newID)

	assert.Equal(t, newID, newGraphT.Graphs[DefaultName][0].Subject.GetValue())
}

func TestGraphToFormatString(t *testing.T) {
	turtleStr := `
		@prefix schema: <http://schema.org/> .
		@prefix ex: <http://example.org/person/> .
		ex:123 schema:name "Charlie Doe" .
		ex:123 schema:url <http://example.org/> .`
	newGraphT, _ := SerializeToGraph([]byte(turtleStr), utils.Turtle)
	formatStr := `<http://example.org/person/123> <http://schema.org/name> "Charlie Doe" .
<http://example.org/person/123> <http://schema.org/url> "http://example.org/" .`

	assert.Equal(t, formatStr, GraphToFormatString(newGraphT))
}

func TestGraphToTurtle(t *testing.T) {
	turtleStr := `@prefix ns0: <http://schema.org/> .
@prefix ns1: <http://example.org/person/> .
ns1:123 ns0:name "Jane Doe" .
ns1:124 ns0:name "Bob Doe" .
ns1:123 ns0:city "Somewhere" .
ns1:123 ns0:friend ns1:1234 .
`
	newGraphT, _ := SerializeToGraph([]byte(turtleStr), utils.Turtle)
	_, err := GraphToTurtle(newGraphT)

	require.NoError(t, err)
}
