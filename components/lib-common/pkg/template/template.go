package template

import (
	"bytes"
	"text/template"
)

// ReplaceInTemplate replaces variables in sparql template files to create a query.
func ReplaceInTemplate(path string, data map[string]interface{}) ([]byte, error) {
	tmpl, err := template.ParseFiles(path)
	if err != nil {
		return nil, err
	}

	var output bytes.Buffer

	err = tmpl.Execute(&output, data)
	if err != nil {
		return nil, err
	}

	return output.Bytes(), nil
}
