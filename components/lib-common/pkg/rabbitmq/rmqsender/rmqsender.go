package rmqsender

import (
	"context"
	"custodian/components/lib-common/pkg/log"
	"encoding/json"
	"time"

	amqp "github.com/rabbitmq/amqp091-go"
)

const (
	timeout = 5 * time.Second
)

type Event struct {
	CreatedAt    time.Time `bson:"created_at"`
	OwnerUserIDs []string  `bson:"owner_user_uris,omitempty"`
	ActionUserID string    `bson:"action_user_uri,omitempty"`
	ContractURI  string    `bson:"contract_uri,omitempty"`
	ResourceID   string    `bson:"resource_uri,omitempty"`
	ActionID     string    `bson:"action_uri,omitempty"`
	Success      bool      `bson:"success"`
	Message      string    `bson:"message,omitempty"`
}

func SendEvent(ch *amqp.Channel, event Event) (err error) {
	json, err := json.Marshal(event)
	if err != nil {
		return
	}

	q, err := ch.QueueDeclare(
		"Event",
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	body := json
	err = ch.PublishWithContext(ctx,
		"",
		q.Name,
		false,
		false,
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        body,
		})
	if err != nil {
		return
	}
	log.Info(" [x] Sent:", "message", body)

	return
}
