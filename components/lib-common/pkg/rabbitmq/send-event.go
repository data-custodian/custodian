package rabbitmq

import (
	"custodian/components/lib-common/pkg/errors"
	"custodian/components/lib-common/pkg/id"
	"custodian/components/lib-common/pkg/rabbitmq/rmqsender"
	"time"
)

func SendEvent(
	queue *RabbitMQ,
	user id.GeneralID,
	contract id.ContractID,
	dataControllerURI []string,
	resourceID id.GeneralID,
	actionID string,
	success bool,
	message string) error {
	if queue == nil {
		return errors.New("cannot send event, queue is nil")
	}

	e := rmqsender.Event{
		CreatedAt:    time.Now(),
		OwnerUserIDs: dataControllerURI,
		ActionUserID: user.URI(),
		ContractURI:  contract.URI(),
		ResourceID:   resourceID.URI(),
		ActionID:     actionID,
		Success:      success,
		Message:      message,
	}

	return rmqsender.SendEvent(queue.Channel, e)
}
