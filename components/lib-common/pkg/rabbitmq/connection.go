package rabbitmq

import (
	"custodian/components/lib-common/pkg/errors"
	"custodian/components/lib-common/pkg/log"
	"net/url"

	amqp "github.com/rabbitmq/amqp091-go"
)

type RabbitMQ struct {
	Conn    *amqp.Connection
	Channel *amqp.Channel
}

// buildURL creates a URL with credentials to connect to the MQ.
func buildURL(rawURL string, username, password *string) (u *url.URL, err error) {
	u, err = url.Parse(rawURL)
	if err != nil {
		return
	}
	userInfo := url.UserPassword(*username, *password)
	u.User = userInfo

	return
}

// NewRabbitMQConnection creates the RabbitMQ connection.
func NewRabbitMQConnection(rawURL string, username, password *string) (*RabbitMQ, error) {
	log.Info("Connecting to RabbitMQ server.")

	url, err := buildURL(rawURL, username, password)
	if err != nil {
		return nil, err
	}

	conn, err := amqp.Dial(url.String())
	if err != nil {
		log.ErrorE(err, "Failed to connect to RabbitMQ server.", "url", rawURL)

		return nil, err
	}

	log.Info("Open a RabbitMQ channel.")
	ch, err := conn.Channel()
	if err != nil {
		log.ErrorE(err, "Failed to create channel.")

		return nil, errors.Combine(err, conn.Close())
	}

	return &RabbitMQ{
		Conn:    conn,
		Channel: ch,
	}, nil
}
