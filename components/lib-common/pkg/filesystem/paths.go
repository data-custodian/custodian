package fs

import (
	"custodian/components/lib-common/pkg/log"
	"os"
	"path"
)

// Exists checks if a path exists. Follows symlinks.
func Exists(path string) (exists bool) {
	exists, _ = ExistsE(path)

	return
}

// ExistsE checks if a path exists. Follows symlinks.
func ExistsE(path string) (bool, error) {
	_, err := os.Stat(path)
	if err != nil {
		return false, err
	}

	return true, nil
}

// ExistsL checks if a path exists. Does not follow symlink.
func ExistsL(path string) (exists bool) {
	exists, _ = ExistsLE(path)

	return
}

// ExistsLE checks if a path exists. Does not follow symlinks.
func ExistsLE(path string) (bool, error) {
	_, err := os.Lstat(path)
	if err != nil {
		return false, err
	}

	return true, nil
}

// MakeAbsolute makes a path absolute to the
// current working directory.
func MakeAbsolute(p string) string {
	if !path.IsAbs(p) {
		cwd, err := os.Getwd()
		if err != nil {
			log.PanicE(err, "Could not evaluate cwd.")
		}

		p = path.Join(cwd, p)
	}

	return p
}

// GetGeneralConfigPath gets the general config path in the current working directory.
func GetGeneralConfigPath() (string, error) {
	configPath, err := GetConfigPath()
	if err != nil {
		return "", err
	}

	return path.Join(configPath, "config.yml"), nil
}

// GetSecretConfigPath gets the secret config path in the current working directory.
func GetSecretConfigPath() (string, error) {
	configPath, err := GetConfigPath()
	if err != nil {
		return "", err
	}

	return path.Join(configPath, "secrets.yml"), nil
}

// GetConfigPath returns the directory to the config files.
func GetConfigPath() (configPath string, err error) {
	currentDir, err := os.Getwd()
	if err != nil {
		return
	}

	configPath = path.Join(currentDir, "config")

	return
}

// getExecutablePath gets the current executable path.
func getExecutablePath() string {
	return MakeAbsolute(os.Args[0])
}

// isPathError returns `true` if the error is a `os.PathError`.
func isPathError(err error) bool {
	return err != nil && err.(*os.PathError) != nil
}
