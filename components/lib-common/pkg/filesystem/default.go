package fs

import "os"

const (
	DefaultPermissionsDir  = os.FileMode(0775) //nolint:mnd
	DefaultPermissionsFile = os.FileMode(0664) //nolint:mnd
)
