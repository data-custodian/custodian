# playground-user-interfaces

## GUI CUSTODIAN

**The playground aims to be an example of a front-end for the Swiss Data
Custodian (a data governance tool): interacting with its complex back-end
through API calls.**

- [APIs](playground_user_interfaces/gui_custodian/custodian_api.py): required to
  connect front-end to Custodian back-end
- [Parsers](playground_user_interfaces/gui_custodian/rdf): required to connect
  front-end to Custodian back-end

Sequence diagram for a Data Controller

```mermaid
%%{init: {"flowchart": {"htmlLabels": false}} }%%
sequenceDiagram
    autonumber
    participant DC as DataController<br/>(Owner of the Dataset)
    participant G as Gateway
    participant PG as Playground<br/>(Web UI)
    participant KB as KB<br/>(Knowledge Base - Jena DB)
    participant CMS as CMS<br/>(Contract Management System)
    participant PDP as PDP<br/>(Policy Decision Point)
    participant CDB as CDB<br/>(Contract Data Base - Jena DB)


    Note over DC,PG: Login
    DC->>G: Login
    G->>G: Authenticate with OpenID Connect
    G->>PG: Redirect User to homepage

    Note over DC,PG: Register User to KB
    PG->>KB: Register User upon Login: POST user metadata

    Note over DC,PG: Register Dataset to KB
    DC->>G: Register Dataset (provide dataset's metadata)
    G->>PG: Register Dataset (provide dataset's metadata)
    PG->>KB: POST dataset metadata

    Note over DC,PG: Review Contract
    DC->>G: View Contract
    G->>PG: View Contract
    PG->>CMS: GET Contract
    CMS->>CDB: Find Contract
    CMS-->>PG: RETURN Contract

    Note over DC, PG: Sign or Revoke Contract
    PG->>CMS: GET Signature Status
    CMS->>CDB: Find Signature Status in Contract
    CMS-->>PG: RETURN Signature Status
    DC->>G: Sign or Revoke Contract
    G->>PG: Sign or Revoke Contract
    PG->>CMS: POST change to signature
    CMS->>CDB: Update Signature in Contract


```

Sequence diagram for a Data Processor

```mermaid
%%{init: {"flowchart": {"htmlLabels": false}} }%%
sequenceDiagram
    autonumber
    participant DP as DataProcessor<br/>(User of the Dataset)
    participant G as Gateway
    participant PG as Playground<br/>(Web UI)
    participant KB as KB<br/>(Knowledge Base - Jena DB)
    participant CMS as CMS<br/>(Contract Management System)
    participant PDP as PDP<br/>(Policy Decision Point)
    participant CDB as CDB<br/>(Contract Data Base - Jena DB)

    Note over DP, PG: Login
    DP->>G: Login
    G->>G: Authenticate with OpenID Connect
    G->>PG: Redirect User to homepage

    Note over DP,PG: Register User to KB
    PG->>KB: Register User upon Login: POST user metadata

    Note over DP, PG: Request a Dataset & Create Contract
    DP->>G: Request Dataset (fill out request form)
    G->>PG: Request Dataset (fill out request form)
    PG->>PG: Generate RDF Contract from form
    PG->>KB: GET dataset & user URI to reference in contract
    KB-->>PG: RETURN data & user URI to reference in contract
    PG->>CMS: POST Contract
    CMS->>CMS: SHACL validation of RDF
    CMS-->>PG: RETURN SHACL validation
    CMS->>CDB: Store Contract

    Note over DP,PG: Review Contract
    DP->>G: View Contract
    G->>PG: View Contract
    PG->>CMS: GET Contract
    CMS->>CDB: Find Contract
    CMS-->>PG: RETURN Contract

    Note over DP, PG: Sign or Revoke Contract
    PG->>CMS: GET Signature Status
    CMS->>CDB: Find Signature Status in Contract
    CMS-->>PG: RETURN Signature Status
    DP->>G: Sign or Revoke Contract
    G->>PG: Sign or Revoke Contract
    PG->>CMS: POST change to signature
    CMS->>CDB: Update Signature in Contract

    Note over DP, PG: Access Dataset
    DP->>PG: Access Dataset
    G->>PG: Access Dataset
    PG->>PDP: GET Validation status
    PDP->>CDB: Check contract exists and signature status
    PDP-->>PG: Return Permission or Refusal
    PG-->>DP: Return Access to Dataset


```

### Use-Case specific: Open Data Swiss

The current playground runs on a _fake_ use-case: governing access to Open Data
Swiss.

Sensitive data is defined as a dataset with an eutheme of society, health or
justice. Non sensitive data is defined as a dataset without those themes. The
themes are collected directly from the dataset RDF metadata, through the API of
Open Data Swiss for the dataset URL.

_Fake_ Sensitive Data Policy for Open Data Swiss

```mermaid
%%{init: {"flowchart": {"htmlLabels": false}} }%%
flowchart TD
    sensitive["`The data is **sensitive**`"]
    purpose["`Purpose
    can only be
    **Academic Research**`"]
    sensitive --> purpose

    processing_download["`Processing
    on the data is
    **Download**`"]
    processing_access["`Processing
    on the data is
    **Access**`"]
    purpose --> processing_download
    purpose --> processing_access

```

_Fake_ Non Sensitive Data Policy for Open Data Swiss

```mermaid
%%{init: {"flowchart": {"htmlLabels": false}} }%%
flowchart TD
    nonsensitive["`The data is **non sensitive**`"]
    processing_download["`Processing
    on the data can only be
    **Download**`"]
    nonsensitive --> processing_download


    purpose_academic["`Purpose is
    **Academic Research**`"]
    purpose_commercial["`Purpose is
    **Commercial Purpose**`"]
    purpose_quality["`Purpose is
    **Quality Assessment**`"]
    processing_download --> purpose_academic
    processing_download --> purpose_commercial
    processing_download --> purpose_quality



```

### Adapt to YOUR Use-Case

[Inside](playground_user_interfaces/gui_custodian/use_case_specific)
`use_case_specific`:

---

#### Connecting to your Data:

1. **Data Source :**

**IMPORTANT: keep the name `data_source()` for the function.**

In `data_source.py` you must define the link to your data (it can be an API call
as defined in `opendataswiss_api.py` or any access to a Data Catalog). Keep the
creation of the `metadata_col` variable at the beginning of the function (see
code comments). The end of your data source needs to end with the storage of an
`api_result`, an `api_button` and a call to the `register_data` function as
such:

> save_to_storage("api_result", {"METADATA1": VALUE, "METADATA2": VALUE, etc.})

> save_to_storage("api_button", {"value": True})

> register_data(metadata_col)

(This code comes from `opendataswiss_api.py`)

2. **Add your custom metadata :**

**IMPORTANT: keep the name and arguments
`add_extra_data_metadata(graph, data_object, datametadata)` for the function.**

In `data_extra_metadata.py` you must define the RDF definitions of the metadata
for your data. (For OpenDataSwiss, we define extra metadata which are the themes
and a description of the data.).

All the metadata terms you wish to capture for your data need to be defined in
the variable `controlled_values` in `data_extra_metadata.py`.

3. **Define your data types (& extract metadata):**

**IMPORTANT: keep the structure and logic of the functions in the
`utils_use_case` script.**

In `utils_use_case.py` define the `extract_type(json_datametadata)` function
which will define the types of your different data (sensitive vs non-sensitive
for example. This is then what triggers different policies as explained in the
next section.)

In order to show the metadata for your registered data, there should be 1
'extract()' function to match 1 custom metadata field (same as those specified
in `controlled_values` in step 2). All the extract functions are called in the
`extract_custom_metadata()` (keep name, arguments and structure of these
functions).

4. **Adapt the Data object to your metadata:**

**IMPORTANT: keep the structure and logic of Data class defined in the
`objects/data.py` script.**

In the `Data` object, you will have to define the attributes relevant to your
data metadata (it may not be label and downloadURL as in the current state). You
will also have to edit the function that adds Data objects to a Process object
by modifying the `add_data` function in the `process.py` script.

---

#### Policies/Rules to govern your Data:

For adapting to your use-case, the main files that need to be taylored are in
the `policies` folder.

The current system starts in the `policies_generator` script where non-sensitive
and sensitive policies are defined in their respective functions. The
`policies_generator/get_policy_elements` function is called when clicking the
`button_contract_generate` (in `functionalities/request_user_input.py`). It
retrieves all the policy elements based on the selected datasets' types and
shows these policies `policies_show/show_policies`. Before showing them
`policy_elements_collider` aggregates identifical policies (in our case,
datasets who are of the same type and will be processed with the same purpose
and processing/usage).

Although we recommend you keep the logic in place here, if your data metadata
and rules to access the data are too different from what is implemented, you
will have to rewrite it.

Key aspects of how policies are handled:

- Input: keep a `get_policy_elements` as an entry point to your policy system.
- Content: many policy elements are defined and can be reused in
  `policies_elements.py`. Process objects are the key to manipulating policy
  elements, and each process has a policy_elements attribute (process creation
  happens in `policy_elements_collider`).
- Output: You need to include the `add_submit_button()` _at the end of each
  policy/rule_ because it launches writing up the form to RDF and uploading it
  to the Custodian.

---

#### Accessing the Data with Custodian Validation:

In our use-case, access to the data means download (i.e. a pop-up page to Open
Data Swiss). Here, a click on the download button triggers an API call to the
Policy Decision Point (PDP) of the Custodian, which checks valid contract and
conditions for the user trying to access the data.

The current implementation revolves around the access to the data via URL, and
the possibility to download this data (using its URL): if this URL download is
not of interest for your use-case, remove the
[script](playground_user_interfaces/gui_custodian/use_case_specific/download_data.py)
`download_data.py` and remove the `downloader` call from the
[script](playground_user_interfaces/gui_custodian/functionalities/contracts_list.py)
`contracts_list.py`, or rewrite it.

---

### Launch Custodian Playground

#### 1. Install requirements

`poetry install`

#### 2. Launch Back-end

##### Dockerized version

(recommended order)

- Keycloak:
  [installation detailed here](https://github.com/sdsc-ordes/ordes-kubernetes/tree/main)

You will also need to add a `mapper` to include user URIs in the token: you can
follow
[this tutorial](https://medium.com/@ramanamuttana/custom-attribute-in-keycloak-access-token-831b4be7384a)

- Gateway:
  [installation detailed here](https://gitlab.com/data-custodian/custodian/-/tree/main/components/gateway/reverse-proxy)

For gateway local set-up, you would have to change the config.yaml to have a
redirection to your localhost as such:

> oidc: scopes:

    - "openid"
    - "profile"
    - "roles"

idTokenHeader: "X-Id-Token"

> redirections:

- path: "/" scheme: "http" host: "localhost:5050"

- Jena Platform:
  [installation detailed here](https://gitlab.com/data-custodian/custodian/-/tree/main/components/db-jena)

- Audit Trail: installation detailed here:
  [part 1](https://gitlab.com/data-custodian/custodian/-/tree/main/components/audit-trail)
  &
  [part 2](https://gitlab.com/data-custodian/custodian/-/tree/main/components/audit-trail-consumer)

- Contract Management System (CMS):
  [installation detailed here](https://gitlab.com/data-custodian/custodian/-/tree/main/components/contract-manager)

- Policy Decision Point (PDP):
  [installation detailed here](https://gitlab.com/data-custodian/custodian/-/tree/main/components/policy-decision-point)

- Knowledge Base:
  [installation detailed here](https://gitlab.com/data-custodian/custodian/-/tree/main/components/knowledgebase)

##### Kubernetes Launch

All helm charts for the mentioned back-end components can be found
[here](https://gitlab.com/data-custodian/custodian/-/tree/main/helm-chart).

#### 3. Launch Playground

A. Build Dockerfile

`docker build .`

OR

B. Run the script

` poetry run python playground_user_interfaces/gui_custodian/main.py`

OR

C. Kubernetes deploy

Use [the helm cart for the playground](helm-chart/playground-user-interfaces)

## LICENSE

AGPL License
