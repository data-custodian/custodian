This part of the project documentation focuses on the **FIELDS of the Request Form**.

# POLICY ELEMENTS of the Request Form

Take these field/policy elements components and mix and match them to create your own request form matching your data governance policies.

::: playground.policies.policies_elements
