set fallback := true
set positional-arguments
set shell := ["bash", "-cue"]
comp_dir := justfile_directory()
root_dir := `git rev-parse --show-toplevel`
flake_dir := root_dir / "./tools/nix"

default: list

list:
    just --list

# Enter a Nix development shell.
develop:
    #!/usr/bin/env bash
    set -eu
    cd "{{comp_dir}}"
    args=("$@") && [ "${#args[@]}" != 0 ] || args="$SHELL"
    nix develop --accept-flake-config "{{flake_dir}}#python-playground" --command "${args[@]}"

# Serve the playground.
serve:
    #!/usr/bin/env bash
    cd "{{comp_dir}}"
    if [ ! -d ".venv" ]; then
        uv venv && uv pip install .
    fi
    uv run serve
