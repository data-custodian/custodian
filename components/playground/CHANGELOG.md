# Change Log

All notable changes to the playground interfaces will be documented in this
file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this
project adheres to [Semantic Versioning](http://semver.org/).

## [3.0.0] - 2024-09

#### Added

- The data request form now supports selecting multiple datasets to generate a
  Contract made of multiple Processes.
- Objects exists to represent Processes, Contracts, Data and Persons.
- The RDF definitions used have been updated to dpv2 elements as much as
  possible.

#### Changed

- Major refactor of code organisation and functions in :
  - policies
  - rdf
  - functionalities
  - name of API calls

## [2.0.0] - 2024-06-26

### GUI CUSTODIAN

#### Added

- Knowledge Base integration:
  - Registering users and data: scripts in `register` folder
  - Displaying registered data in `functionalities/data_registered_show.py`
  - New API calls in `custodian_api.py`

#### Changed

- Repository folder organization
- Request form format in `functionalities/request_form.py`
- Changes to parsers for calling the knowledge base in the `parsers` folder and
  its corresponding `utils/utils_rdf_write.py` and `utils/utils_rdf_read.py`

#### Fixed

- Simpler data request format generated in RDF
- Audit trail using URIs to access events for a data controller

## [1.2.0] - 2024-04-10

### GUI CUSTODIAN

#### Added

NA

#### Changed

- `main.py` connection to the `use_case_specific` folder
- `README.md` for clearer instructions on how to reuse the playground

#### Fixed

- PDP API download condition

## [1.1.0] - 2024-04-08

### GUI CUSTODIAN

#### Added

- Audit trail pagination

#### Changed

- PDP API result interpretation

#### Fixed

NA
