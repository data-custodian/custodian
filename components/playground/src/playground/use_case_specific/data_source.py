from nicegui import ui
from playground.use_case_specific.opendataswiss_api import (
    opendataswiss_api,
)


def data_source():
    """This function creates a UI to submit a dataset from Open Data Swiss.
    It uses a URL Checker class to check if the URL being submitted is valid.
    The submit button calls the API of Open Data Swiss to get the dataset metadata.
    """
    # prepare error message dialog
    with ui.dialog() as dialog_opendataswiss, ui.card():
        ui.button(
            "URL Error: Make sure you are submitting a dataset from Open Data Swiss.",
            on_click=lambda: ui.navigate.to("/"),
        )

    # initialize a url checker
    url_checker = URLChecker()
    expansion_card = ui.card()
    ######## MANDATORY TO KEEP in all use-cases
    # create a column where the metadata will be displayed before registering
    metadata_col = ui.column()
    ########
    with expansion_card.style("width: 1300px"):
        with ui.expansion("Submit Dataset from Open Data Swiss!", icon="expand"):
            with ui.card().classes("no-shadow border-[1px]"):
                ui.link(
                    "Here are 10,000+ Datasets from Open Data Swiss",
                    "https://opendata.swiss/en/dataset",
                    new_tab=True,
                )
                url_input = (
                    ui.input(
                        "Insert Dataset URL",
                        value=" ",
                        on_change=url_checker.on_change,
                        validation={"Needs to be a url!": url_checker.url_check},
                    )
                    .props("size=150")
                    .props("clearable")
                )
            submit = ui.button(
                "submit",
                on_click=lambda: opendataswiss_api(
                    url_input.value, [], dialog_opendataswiss, metadata_col
                ),
                color="white",
            ).classes("text-grey")
            url_input.value = ""
            submit.bind_enabled_from(url_checker, "no_errors")


class URLChecker:
    """This class is used to check if the URL is valid. It is used to enable the submit button only if the URL is valid.
    A valid URL means the http string can be found at the beginning of the URL."""

    def __init__(self) -> None:
        """Initializes the URLChecker class."""
        self.inputs = {}
        self.no_errors = True

    def url_check(self, u):
        """This function checks if the URL is valid.
        Args:
            u (str): The URL to be checked."""
        return u and u.startswith("http")

    def on_change(self, e):
        """This function is called when the URL input is changed.
        Args:
            e (nicegui.ui.Event): The event object."""
        valid = self.url_check(e.value)
        self.inputs[e.sender.id] = valid
        self.update()

    def update(self):
        """This function updates the state of the URLChecker."""
        for i in self.inputs.values():
            if i is not True:
                self.no_errors = False
                break
        else:
            self.no_errors = True
