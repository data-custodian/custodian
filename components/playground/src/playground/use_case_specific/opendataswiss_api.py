import requests
from nicegui import ui
from playground.ui_setup.storage import save_to_storage
from playground.knowledgebase.register_data import (
    register_data,
)


def opendataswiss_api(base_url, themes, dialog, metadata_col):
    """This function calls the Open Data Swiss API to get the metadata of the dataset and extrat the euthemes defined for a specific dataset.

    Args:
        base_url (str): The URL of the Opendataswiss dataset.
        themes (list): The empty list for storing the themes from the API.
        dialog (nicegui.ui.dialog): The dialog to be opened in case of an error.
        metadata_col (nicegui.ui.column): The column where the metadata will be displayed before registering.
    """
    try:
        # Extract the name of the dataset from the base_url
        api_title_for_slug = base_url.split("/")[-1]
        # Call the opendataswiss api for metadata
        response = requests.get(
            "https://opendata.swiss/api/3/action/package_search?fq=title_for_slug:"
            + api_title_for_slug
        )
        json_metadata = response.json()
        # Extract the themes from the json response
        themes.extend(
            i["name"] for i in json_metadata["result"]["results"][0]["groups"]
        )
        description = json_metadata["result"]["results"][0]["description"]["de"]
        # Save themes and the button clicked to the storage
        save_to_storage(
            "api_result",
            {"themes": themes, "downloadURL": base_url, "description": description},
        )
        save_to_storage("api_button", {"value": True})
        register_data(metadata_col)
    except Exception as e:
        print("URL ERROR")
        dialog.open()
