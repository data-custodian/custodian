from nicegui import ui
import urllib.parse
from playground.custodian_api import (
    api_acs_pdp_access,
    api_cms_sign_status,
)
from playground.ui_setup.storage import get_from_storage
from playground.utils.utils_strings import remove_hashtag
from playground.rdf.utils_rdf_read import extract_id
from playground.knowledgebase.utils_knowledgebase import (
    get_user_id,
)


def downloader(uri_contract, contract):
    """The downloader creates a download button and blocks this button if the data request status is revoked.

    Args:
        uri_contract (str): The URI of the contract.
        contract (Contract): The contract object.
    """
    header = get_from_storage("header")["value"]
    resp_status = api_cms_sign_status(uri_contract, header)
    user_uid = get_user_id()
    user_status = resp_status["usersStatus"][user_uid]
    processes = contract.get_attribute("processes")
    processings = contract.get_attribute_all_processes("hasProcessing")
    unique_processings = list(set(processings))
    with ui.column():
        for unique_processing in unique_processings:
            create_action_button(user_status, unique_processing, processes)


def create_action_button(user_status, processing, processes):
    """Creates a button to execute the processing of data if the Custodian confirms access is allowed, through PDP API.
    Args:
        user_status (str): The status of the user.
        processing (str): The processing to be executed.
        processes (list): The list of processes.
    """
    processing = remove_hashtag(processing)
    action_button = ui.button(
        processing,
        icon="play_arrow",
        on_click=lambda e: call_pdp(processes, processing),
        color="#92202A",
    ).classes("text-white")
    if user_status == "REVOKE":
        action_button.disable()


def call_pdp(processes, processing):
    """For all processes, for specific processing, calls the PDP API to check if the user is allowed to do that processing on that data.
    If for all data and for that processing the access is allowed then the processing is performed.
    Args:
        processes (list): The list of processes.
        processing (str): The processing to be executed.
    """
    header = get_from_storage("header")["value"]
    processing_processes = []
    processing_data_uris = []
    for process in processes:
        if process.get_attribute("hasProcessing") == processing:
            data_uris = process.get_attribute("data_uris")
            processing_data_uris.extend(data_uris)
            processing_processes.append(process)
    all_access_responses = []
    for data_uri in processing_data_uris:
        data_uri = extract_id(data_uri)
        access_response = api_acs_pdp_access(processing, data_uri, header)
        all_access_responses.append(access_response)

    download_authorization = (
        False if any("not valid" in s for s in all_access_responses) else True
    )

    if download_authorization:
        perform_processing(processing_processes, processing)
    else:
        ui.label(
            f"You are not allowed to {processing.lower()} the data because the Contract has not been signed by everyone or has been revoked."
        )


def perform_processing(processing_processes, processing):
    """Performs the processing of the data from each process.
    Args:
        processing_processes (list): The list of processes to get the data from each one.
        processing (str): The processing to be executed.
    """
    urls = []
    for process in processing_processes:
        urls.extend(process.get_attribute_all_data("downloadURL"))
    if processing == "Download":
        ui.label("Download granted.")
        for url in urls:
            print(url)
            if ("<" in url) or (">" in url):
                url = url.replace("<", "")
                url = url.replace(">", "")
            ui.download(url)
            ui.label(url)
    elif processing == "Access":
        ui.label("Access granted to: " + " & ".join(urls))
