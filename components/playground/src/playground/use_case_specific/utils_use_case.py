from playground.rdf.utils_rdf_read import (
    search_in_jsonld,
)


def extract_custom_metadata(json_datametadata, datametadata_minimal):
    """Extracts the minimal metadata from the data metadata (JSON-LD format).
    The data type is extracted based on the themes of the dataset (sensitive or non-sensitive).
    The label is extracted (i.e. the description of the dataset).
    The download URL is extracted.

    Args:
        json_datametadata (dict): The data metadata in JSON-LD format.
        datametadata_minimal (dict): The dictionary where the minimal metadata will be stored.

    Returns:
        datametadata_minimal (dict): The dictionary with the minimal metadata.
    """
    datametadata_minimal["type"] = extract_type(json_datametadata)
    datametadata_minimal["label"] = extract_label(json_datametadata)
    datametadata_minimal["downloadURL"] = extract_url(json_datametadata)
    return datametadata_minimal


def extract_themes(themes):
    """Extracts the themes from the dataset metadata.

    Args:
        themes (list): The themes of the dataset. It is a list of dictionaries with one item @id which is the URL of the theme.

    Returns:
        list: The list of themes extracted from the dataset metadata.
    """
    themes_list = []
    for theme_dict in themes:
        theme_uid = theme_dict["@id"]
        theme = theme_uid.split("/")[-1]
        themes_list.append(theme)
    return themes_list


def extract_type(json_datametadata):
    """Extracts the data type based on the themes of the dataset (sensitive or non-sensitive).
    For Open Data Swiss, euthemes corresponding to:

     - health (heal), society (soci), and justice (just) were arbitrarily considered sensitive

     - others are arbitrarily considered non-sensitive.

    Args:
        json_datametadata (dict): The data metadata in JSON-LD format.

    Returns:
        str: The data type (sensitive or non-sensitive).
    """
    sensitive = ["heal", "soci", "just"]
    sensitive = set(sensitive)
    themes = search_in_jsonld(json_datametadata, "theme")
    themes_list = extract_themes(themes)
    themes = set(themes_list)
    if sensitive & themes:
        return "sensitive"
    else:
        return "non-sensitive"


def extract_label(json_datametadata):
    """Extracts the label from the data metadata (i.e. the description of the dataset).

    Args:
        json_datametadata (dict): The data metadata in JSON-LD format.

    Returns:
        str: The label/description of the dataset.
    """
    data_label = search_in_jsonld(json_datametadata, "label")[0]["@value"]
    return data_label


def extract_url(json_datametadata):
    """Extracts the download URL from the data metadata.

    Args:
        json_datametadata (dict): The data metadata in JSON-LD format.

    Returns:
        str: The download URL of the dataset.
    """
    data_url = search_in_jsonld(json_datametadata, "downloadURL")[0]["@id"]
    return data_url


def extract_name(data):
    """Extracts the name of the dataset from the download URL in the metadata.

    Args:
        data (list): The metadata of the dataset.

    Returns:
        data_name (str): The name of the dataset.
    """
    data_name = (
        search_in_jsonld(data, "downloadURL")[0]["@id"].split("/")[-1].split("-")
    )
    data_name = " ".join(data_name).title()
    return data_name
