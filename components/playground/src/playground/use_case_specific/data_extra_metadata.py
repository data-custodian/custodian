from rdflib import Namespace
from rdflib import URIRef, Literal

controlled_values = ["themes", "description"]


def add_extra_data_metadata(graph, data_object, datametadata):
    """Add extra metadata to the data object in the graph.
    How to use:

    1. retrieve the metadata specific to your data from datametadata where you should have stored it in a dictionary form

    2. define the prefixes needed to save the metadata as RDF

    3. then add them to the graph one by one

    Args:
        graph (rdflib.graph): The RDF graph.
        data_object (rdflib.term.URIRef): The data object URI.
        datametadata (dict): The data metadata.

    Returns:
        rdflib.graph: The RDF graph with the extra metadata.
    """
    # themes
    themes = datametadata["themes"]
    euthemes = Namespace("http://publications.europa.eu/resource/authority/data-theme/")
    dcat = Namespace("https://www.w3.org/TR/vocab-dcat-2/#")
    for theme in themes:
        graph.add((data_object, dcat.theme, URIRef(euthemes + theme)))

    # label
    description = datametadata["description"]
    rdfs = Namespace("http://www.w3.org/2000/01/rdf-schema#")
    graph.add((data_object, rdfs.label, Literal(description)))
    return graph
