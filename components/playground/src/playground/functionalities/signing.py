from nicegui import ui
from playground.custodian_api import (
    api_cms_sign,
    api_cms_revoke,
    api_cms_sign_status,
)
from playground.ui_setup.storage import get_from_storage
from playground.objects.contract import ContractStatus
from playground.knowledgebase.utils_knowledgebase import (
    get_user_id,
)


def process_api_cms_sign(uri_contract, neutral_value, positive_value, negative_value):
    """Process the API sign status and return the status and value based on the response.

    Args:
        uri_contract (str): The unique identifier of the data request.
        neutral_value (str): The neutral value of the status.
        positive_value (str): The positive value of the status.
        negative_value (str): The negative value of the status.

    Returns:
        status (str): The status of the data request.
        value (bool): The value of the data request status.
    """
    header = get_from_storage("header")["value"]
    resp_status = api_cms_sign_status(uri_contract, header)
    user_uid = get_user_id()
    user_status = resp_status["usersStatus"][user_uid]
    if user_status == "":
        status = neutral_value
        value = False
    elif user_status == "VALID":
        status = positive_value
        value = True
    elif user_status == "REVOKE":
        status = negative_value
        value = False
    else:
        status = neutral_value
        value = False
    return status, value


def make_toggle(uri_contract):
    """Create a toggle button to display a positive or negative status defined in a model.

    Args:
        uri_contract (str): The unique identifier of the data request.
    """
    neutral_value = "Not Signed"
    positive_value = "Signed"
    negative_value = "Revoked"
    status, start_value = process_api_cms_sign(
        uri_contract, neutral_value, positive_value, negative_value
    )
    model = {"status": status}
    stat_label = ContractStatus(positive_value).bind_text_from(model, "status")
    sign_switch = ui.switch(
        value=start_value,
        on_change=lambda e: sign_revoke(
            uri_contract,
            model,
            e.value,
            positive_value,
            negative_value,
            neutral_value,
        ),
    )
    if status == negative_value:
        sign_switch.disable()


def sign_revoke(
    uri_contract, model, value, positive_value, negative_value, neutral_value
):
    """Sign or revoke the data request based on the value of the toggle button.

    Args:
        uri_contract (str): The unique identifier of the data request.
        model (dict): The model that contains the status of the data request.
        value (bool): The value of the toggle button.
        positive_value (str): The positive value of the status.
        negative_value (str): The negative value of the status.
        neutral_value (str): The neutral value of the status.
    """
    header = get_from_storage("header")["value"]
    if (model["status"] == neutral_value) and (value == True):
        api_cms_sign(uri_contract, header)
        model.update(status=positive_value)
    elif (model["status"] == positive_value) and (value == False):
        api_cms_revoke(uri_contract, header)
        model.update(status=negative_value)
