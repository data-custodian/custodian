import random
from nicegui import ui

from playground.policies.policies_generator import (
    get_policy_elements,
)
from playground.policies.purpose_processing_selector import (
    select_purpose_processing,
)


def form_generator(form_col):
    """This function generates the policy form based on the themes extracted from the dataset.
    For Open Data Swiss, euthemes corresponding to:

     - health (heal), society (soci), and justice (just) were arbitrarily considered sensitive

     - others are arbitrarily considered non-sensitive

    Args:
        form_col (nicegui.ui.column): The column where the form subsections will be displayed (subsections: purpose and processing selection and the form itself).
    """
    with form_col:
        with ui.row().classes("w-full border"):
            with ui.column():
                select_col = ui.column()
                col_contract_generate = ui.column()
            display_col = ui.column()
        with col_contract_generate:
            button_contract_generate = ui.button(
                "Generate Data Request Form",
                on_click=lambda t: get_policy_elements(display_col),
                color="#92202A",
            ).classes("text-white")
        with select_col:
            ui.space()
            select_purpose_processing(select_col, display_col, button_contract_generate)
