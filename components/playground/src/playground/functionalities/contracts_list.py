from nicegui import ui
import urllib.parse
from playground.custodian_api import (
    api_cms_get_datarequests_for_user,
    api_cms_get_datarequest,
)
from playground.knowledgebase.utils_knowledgebase import (
    get_user_id,
)
from playground.ui_setup.storage import get_from_storage
from playground.rdf.parser_rdf_read import (
    get_request_content,
)
from playground.functionalities.signing import make_toggle
from playground.use_case_specific.download_data import (
    downloader,
)

from playground.objects.contract import ShowContract


def contracts_show(contracts):
    """Display the data request button for details and the data request signing status.
    For data requestors, a button to download the data is also created.

    Args:
        contracts (dict): The data requests to display (key: the uid of the data request, value: the content).
    """

    async def show(contract):
        result = await ShowContract(contract)

    for uri_contract, contract in contracts.items():
        ui.label("Data Request | Contract: " + uri_contract)
        with ui.row():
            ui.button(
                "Show data request details",
                on_click=lambda contract=contract: show(contract),
            )
            # the toggle to show the signing status and allow user actions
            make_toggle(uri_contract)
            # for data requestors, the button to download the data is shown
            if get_from_storage("user_role")["value"] == "data-processor":
                downloader(uri_contract, contract)
        ui.separator()


def show_contracts(dialog):
    """Retrieve the data requests made by the user and show its details and status.
    From the user identifier, the user uid is retrieved.
    From the user uid, all his/her/their data requests are retrieved.
    Each data request is processed individually and added to a dictionary of data requests to display.

    Args:
        dialog (ui.dialog): The dialog to display in case of an error.
    """
    # try:
    header = get_from_storage("header")["value"]
    uid_user = get_user_id()
    requests_ids = api_cms_get_datarequests_for_user(
        urllib.parse.quote(uid_user), header
    )
    contracts = {}
    for id in requests_ids:
        if len(id) > 1:
            id = id.split("/")[-1]
            rep_json = api_cms_get_datarequest(id, header)
            contract = get_request_content(rep_json)
            contracts[id] = contract
    contracts_show(contracts)
    # except Exception as e:
    #     print("Custodian Error")
    #     dialog.open()
