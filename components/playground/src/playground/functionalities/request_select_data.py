from nicegui import ui

from playground.ui_setup.storage import (
    save_to_storage,
    get_from_storage,
)

from playground.knowledgebase.utils_knowledgebase import (
    find_dataset,
    create_information_dialog,
)

from playground.ui_setup.layout import (
    writing_style,
)

from playground.functionalities.request_user_input import (
    form_generator,
)


def set_icons(data_type):
    """This function sets the icon and color based on the data type.

    Args:
        data_type (str): The type of data (sensitive or non-sensitive).

    Returns:
        icon (str): The icon to be displayed.
        color (str): The color of the icon.
    """
    if data_type == "sensitive":
        icon = "report"
        color = "#ffe6cc"
    elif data_type == "non-sensitive":
        icon = "radio_button_unchecked"
        color = "#e7fae1"
    return icon, color


def next_button_trigger(two, tabs_panel):
    """This function is triggered when a dataset has been selected and the next button is clicked.
    It enables the form tab and sets it as the active tab.

    Args:
        two (nicegui.ui.tab): The form tab.
        tabs_panel (nicegui.ui.tab_panels): The tab panels where the tabs are.
    """
    two.enable()
    tabs_panel.value = two


def represent_dataset(
    datametadata, icon, color, selected_data_row, label_dialog, form_col, next_button
):
    """This function represents a dataset.

    Args:
        datametadata (dict): The metadata of the dataset.
        icon (str): The information icon to access the dataset's metadata.
        color (str): The color of the dataset, corresponding to the dataset type.
        selected_data_row (nicegui.ui.chat_message): The label at the top of the page that displays the selected dataset.
        label_dialog (nicegui.ui.dialog): The dialog to be opened in case of an error.
        form_col (nicegui.ui.column): The column where the form will be displayed.
        next_button (nicegui.ui.button): The button to move to the form tab.
    """
    ui.button(icon="info", color=color, on_click=lambda d: label_dialog.open()).props(
        "fab"
    )
    ui.button(
        datametadata["name"],
        icon=icon,
        on_click=lambda clicked: chose_dataset(
            clicked, datametadata, selected_data_row, form_col, next_button
        ),
        color=color,
    )


def create_finder(
    keyword,
    minimal_datametadata,
    datasets_col,
    selected_data_row,
    form_col,
    next_button,
):
    """This function creates a finder to search through the datasets.

    Args:
        keyword (str): The keyword to search for in the datasets name.
        minimal_datametadata (list): The list of datasets with their minimal metadata to search through.
        datasets_col (nicegui.ui.column): The column where the datasets will be displayed.
        form_col (nicegui.ui.column): The column where the form will be displayed.
        next_button (nicegui.ui.button): The button to move to the form tab.
    """
    # sort the buttons being created if based on a filtering keyword
    searched_datametadata = find_dataset(minimal_datametadata, keyword)
    display_datasets(
        searched_datametadata, datasets_col, selected_data_row, form_col, next_button
    )


def create_select_space():
    """This function creates a space to display the selected datasets.

    Returns:
        nicegui.ui.row: The row that displays the selected datasets through a ui.chat_message widget.
    """
    ui.separator()
    ui.space()
    save_to_storage("selected_datametadata", {}, override=True)
    with ui.row() as selected_data_row:
        ui.chat_message(
            "No Dataset Selected. Please click on all the datasets you want to request."
        ).props('bg-color="grey-6"').style(writing_style)
    ui.space()
    ui.separator()
    return selected_data_row


def display_datasets(
    minimal_datametadata, datasets_col, selected_data_row, form_col, next_button
):
    """This function displays the datasets available in the knowledgebase for which the requestor can request access.

    Args:
        minimal_datametadata (list): The list of datasets with their minimal metadata to display.
        datasets_col (nicegui.ui.column): The column where the datasets will be displayed.
        selected_data_row (nicegui.ui.row): The row where the selected datasets will be displayed.
        form_col (nicegui.ui.column): The column where the form for requesting will be displayed.
        next_button (nicegui.ui.button): The button to move to the form tab.
    """
    datasets_col.clear()
    with datasets_col:
        for datametadata in minimal_datametadata:
            # set the icon for sensitive or non-sensitive
            icon, color = set_icons(datametadata["type"])
            label_dialog = create_information_dialog(datametadata)
            # create a column for each dataset
            with ui.row():
                represent_dataset(
                    datametadata,
                    icon,
                    color,
                    selected_data_row,
                    label_dialog,
                    form_col,
                    next_button,
                )


def chose_dataset(
    clicked, selected_datametadata, selected_data_row, form_col, next_button
):
    """This function is triggered when a dataset is selected.
    It clears the form column and highlights the selected dataset.
    It prepares the form column in the next tab and enables the next button to move to the form tab.
    It calls policy generator on the selected datasets to infer which policy elements will have to be shown to the user to make his request.

    Args:
        clicked (bool): The click event.
        datametadata (dict): The metadata of the selected dataset.
        selected_data_row (nicegui.ui.chat_message): The label at the top of the page that displays the selected dataset.
        form_col (nicegui.ui.column): The column where the form will be displayed.
        next_button (nicegui.ui.button): The button to move to the form tab.
    """
    if clicked:
        form_col.clear()
        stored_selected_datametadata = get_from_storage("selected_datametadata")
        if stored_selected_datametadata is None:
            stored_selected_datametadata = {
                selected_datametadata["name"]: selected_datametadata
            }
        else:
            if selected_datametadata["name"] in stored_selected_datametadata.keys():
                del stored_selected_datametadata[selected_datametadata["name"]]
            else:
                stored_selected_datametadata[selected_datametadata["name"]] = (
                    selected_datametadata
                )
        save_to_storage(
            "selected_datametadata", stored_selected_datametadata, override=True
        )
        data_names = list(stored_selected_datametadata.keys())
        selected_data_row.clear()
        form_generator(form_col)
        if not stored_selected_datametadata:
            with selected_data_row:
                ui.chat_message("No Data Selected").props('bg-color="grey-6"').style(
                    writing_style
                )
            next_button.disable()
        else:
            with selected_data_row:
                ui.chat_message(
                    "Data Selected for this Data Request: \n \n"
                    + "\n\n".join(data_names).upper()
                ).props('bg-color="grey-4"').style(writing_style)
            next_button.enable()
