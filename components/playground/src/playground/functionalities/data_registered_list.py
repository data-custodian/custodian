from nicegui import ui

from playground.ui_setup.storage import get_from_storage
from playground.knowledgebase.utils_knowledgebase import (
    prep_datametadata,
    find_dataset,
    create_information_dialog,
)
from playground.custodian_api import (
    api_knowledgebase_get_datametadata_for_user,
)

from playground.functionalities.request_select_data import (
    set_icons,
)


def create_finder_controller(keyword, minimal_datametadata, datasets_col):
    """Calls a find function to find a dataset based on a keyword. Then displays the output of the search.

    Args:
        keyword (str): The keyword to search for in the dataset name.
        minimal_datametadata (list of dictionaries): The list of all the datasets in the knowledgebase.
        datasets_col (ui.column): The column where the datasets will be displayed.
    """
    selected_datametadata = find_dataset(minimal_datametadata, keyword)
    display_datasets_controller(selected_datametadata, datasets_col)


def represent_dataset_controller(datametadata, icon, color, label_dialog):
    """Creates a button for each dataset with an icon and color.

    Args:
        datametadata (dictionary): The metadata of the dataset.
        icon (str): The icon to display on the button.
        color (str): The color of the button.
        label_dialog (ui.dialog): The dialog box with the metadata of the dataset.
    """
    ui.button(icon="info", color=color, on_click=lambda d: label_dialog.open()).props(
        "fab"
    )
    ui.button(datametadata["name"], icon=icon, color=color)


def display_datasets_controller(minimal_datametadata, datasets_col):
    """Displays the datasets in the specific nicegui column. For each datametadata an icon, color is defined (use-case specific) and metadata is prepared to be displayed.

    Args:
        minimal_datametadata (list of dictionaries): The list of all the datasets in the knowledgebase.
        datasets_col (ui.column): The column where the datasets will be displayed.
    """
    datasets_col.clear()
    with datasets_col:
        for datametadata in minimal_datametadata:
            # set the icon for sensitive or non-sensitive
            icon, color = set_icons(datametadata["type"])
            # extract metadata to display for the data
            label_dialog = create_information_dialog(datametadata)
            # create a column for each dataset
            with ui.row():
                represent_dataset_controller(datametadata, icon, color, label_dialog)


def show_data_registered():
    """Displays the datasets registered by the user. The user can search for a dataset by keywords."""
    header = get_from_storage("header")["value"]
    all_datasets_ids = api_knowledgebase_get_datametadata_for_user(header)
    if all_datasets_ids:
        # Minimal information for display is extracted from each dataset
        minimal_datametadata = prep_datametadata(all_datasets_ids)
        # A search engine is created
        finder_row = ui.row()
        datasets_col = ui.column()
        with finder_row:
            ui.input(
                "Find a dataset.",
                on_change=lambda e: create_finder_controller(
                    e.value, minimal_datametadata, datasets_col
                ),
            ).props("clearable")
        display_datasets_controller(minimal_datametadata, datasets_col)
    else:
        ui.label("You have not uploaded any datasets yet.")
