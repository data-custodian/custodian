from nicegui import ui

from playground.ui_setup.storage import (
    get_from_storage,
)
from playground.custodian_api import (
    api_knowledgebase_get_all_datametadata,
)
from playground.knowledgebase.utils_knowledgebase import (
    prep_datametadata,
)

from playground.ui_setup.layout import (
    title_style,
)

from playground.functionalities.request_select_data import (
    create_select_space,
    display_datasets,
    next_button_trigger,
    create_finder,
)


def show_request_form():
    """This function creates two tabs for the process of requesting data.
    The first tab is for choosing datasets, with a finder.
    The selected datasets are shown in a the selected box (a nicegui chat_message).
    Once a dataset has been selected, the next button gets enabled to move to the form tab.
    """
    header = get_from_storage("header")["value"]
    all_datasets = api_knowledgebase_get_all_datametadata(header)
    with ui.tabs().classes("w-full") as tabs:
        one = ui.tab("Datasets")
        two = ui.tab("Request Form")
    two.disable()
    tabs_panel = ui.tab_panels(tabs, value=one).classes("w-full")
    with tabs_panel:
        with ui.tab_panel(two):
            form_col = ui.column()
        with ui.tab_panel(one):
            if all_datasets:
                minimal_datametadata = prep_datametadata(all_datasets)
                ui.label("1. Select amongst the available datasets.").style(title_style)
                finder_row = ui.row()
                datasets_col = ui.column()
                selected_data_row = create_select_space()
                ui.label("2. Click next to make your request.").style(title_style)
                next_button = ui.button(
                    "Next: Request this data",
                    on_click=lambda t: next_button_trigger(two, tabs_panel),
                    color="#92202A",
                ).classes("text-white")
                next_button.disable()
                with finder_row:
                    ui.input(
                        "Find a dataset.",
                        on_change=lambda e: create_finder(
                            e.value,
                            minimal_datametadata,
                            datasets_col,
                            selected_data_row,
                            form_col,
                            next_button,
                        ),
                    ).props("clearable")
                display_datasets(
                    minimal_datametadata,
                    datasets_col,
                    selected_data_row,
                    form_col,
                    next_button,
                )
            else:
                ui.label("No datasets available.")
