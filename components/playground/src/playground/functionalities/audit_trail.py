from nicegui import ui
from datetime import datetime
import urllib.parse
from playground.ui_setup.storage import get_from_storage
from playground.custodian_api import (
    api_audit_trail_get,
    api_knowledgebase_get_user,
)
from playground.rdf.utils_rdf_read import (
    get_person_details,
)


def format_events_for_display(events):
    """Formats the events to be displayed in a table.
    Args:
        events (list): The list of events to format.

    Returns:
        list: The list of formatted events.
    """
    formatted_events = []
    for event in events:
        for key, val in event.items():
            # Convert the string format of the date into a datetime object and split time and date
            if key == "CreatedAt":
                date_object = datetime.strptime(val, "%Y-%m-%dT%H:%M:%S.%fZ")
                event["CreatedAt"] = date_object.ctime()
            elif key == "OwnerUserIds" or key == "ActionUserId":
                header = get_from_storage("header")["value"]
                if type(val) is list:
                    users = []
                    for v in val:
                        uri = urllib.parse.quote(v)
                        user_jsonld = api_knowledgebase_get_user("uri", uri, header)
                        user = get_person_details(user_jsonld)
                        users.append(user)
                    users = " | ".join(users)
                else:
                    uri = urllib.parse.quote(val)
                    user_jsonld = api_knowledgebase_get_user("uri", uri, header)
                    users = get_person_details(user_jsonld)
                event[key] = users
        formatted_events.append(event)
    return formatted_events


def format_audit_table(formatted_events):
    """Defines the format of the audit trail table.

    Args:
        formatted_events (list): The list of formatted events. The keys of the first event are used to determine the columns of the table

    Returns:
        list: The list of columns of the table.
        list: The list of formatted events.
    """
    keys = formatted_events[0].keys()
    columns = []
    for col in keys:
        table_col = {"name": col, "label": col, "field": col, "sortable": True}
        columns.append(table_col)
    return columns, formatted_events


def create_audit_table(table_row, num_events, filter_input):
    """Creates the audit trail table. Calls the Audit trail API to retrieve events then formats the events before formatting the table to display them.

    Args:
        table_row (ui.row): The row where the table will be displayed.
        num_events (int): The number of events to retrieve.
        filter_input (ui.input): The input field to filter the events.
    """
    table_row.clear()
    header = get_from_storage("header")["value"]
    events = api_audit_trail_get(header, num_events)
    formatted_events = format_events_for_display(events)
    columns, formatted_events = format_audit_table(formatted_events)
    ui.table(
        columns=columns, rows=formatted_events, row_key="name", pagination=10
    ).bind_filter_from(filter_input, "value")


def show_audit_trail():
    """Shows the audit trail of all events occuring in Custodian."""
    ui.label("Here are all the events happening for your datasets.")
    try:
        filter_input = ui.input(placeholder="Find an event...")
        buttons_row = ui.row()
        table_row = ui.row()
        with buttons_row:
            ui.button(
                "100 most recent events",
                on_click=lambda: create_audit_table(table_row, 100, filter_input),
            )
            ui.button(
                "All events",
                on_click=lambda: create_audit_table(table_row, 1000, filter_input),
            )
        with table_row:
            create_audit_table(table_row, 10, filter_input)
    except:
        ui.label("No Events To Show.")
