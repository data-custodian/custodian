import os
import random
from os.path import dirname, join

from dotenv import load_dotenv
from playground.custodian_api import api_knowledgebase_get_datametadata
from playground.objects.process import Process
from playground.rdf.parser_rdf_write import write_contract_rdf
from playground.rdf.utils_rdf_read import search_in_jsonld
from playground.ui_setup.home import home
from playground.ui_setup.storage import get_from_storage
from playground.utils.utils_process import convert_to_dict

dotenv_path = join(dirname(__file__), ".env")
load_dotenv(dotenv_path)
REQUEST_PATH = os.environ.get("REQUEST_PATH", "data/requests")


def check_dataprocessors(process):
    """Check if the data processors are empty and add the user as a data processor.

    Args:
        inputs (dict): The inputs to check for data processors.

    Returns:
        inputs (dict): The inputs with the data processors added if needed.
    """
    inputs = process.get_attribute("body")
    if (
        (not ("hasDataProcessor" in inputs.keys()))
        or (inputs["hasDataProcessor"] is None)
        or (inputs["hasDataProcessor"] == [])
    ):
        user = {}
        user["email"] = get_from_storage("login_user_email")["value"]
        process.add_inputs(key="hasDataProcessor", val=[user])


def add_project_name(process):
    """Add the data controller to the inputs

    Args:
        inputs (dict): The inputs.

    Returns:
        inputs (dict): The inputs with the data controller added.
    """
    # TO DO - QUICK FIX: this should be validated by the Custodian SHACL shapes
    inputs = process.get_attribute("body")
    if (
        (not ("projectName" in inputs.keys()))
        or (inputs["projectName"] is None)
        or (inputs["projectName"] == [])
    ):
        process.add_inputs(key="projectName", val="NA")


def extract_data_metadata(process):
    """For a process, go through all the selected datasets to find its respective datasets. For these datasets, retrieve their RDF metadata by calling the knowledgebase API.
    Args:
        process (dict): The process for which we want to retrieve the datasets'metadata
    """
    data_uris = []
    datacontrollers_uri = []
    data_names = process.get_attribute("data_names")
    header = get_from_storage("header")["value"]
    stored_selected_datametadata = get_from_storage("selected_datametadata")
    for _, stored_datametada in stored_selected_datametadata.items():
        for name in data_names:
            if stored_datametada["name"] == name:
                datametada_url = stored_datametada["downloadURL"]
                datametadata = api_knowledgebase_get_datametadata(
                    "url", datametada_url, header
                )
                data_uri = datametadata[0]["@id"]
                data_uris.append(data_uri)
                data_controller = search_in_jsonld(datametadata, "hasDataController")
                datacontrollers_uri.append(data_controller[0]["@id"])
    process.add_data_uri(data_uris)
    process.add_persons_uri(datacontrollers_uri, "DataController")


def submit_request(dialog):
    """Submit the request.
    For each process, check if the data processors are empty and if so add the user as a data processor.
    For each process, add a stand-in project name if it is missing.
    For each process, extract add the enriched metadata for the process's datasets.

    Convert all the processes into an RDF contract and send it to the Custodian API.

    Args:
        dialog (ui.dialog): The dialog to display in case of an error.
    """
    try:
        contract = get_from_storage("contract")
        processes = get_from_storage("processes")
        for process_id, process_dict in processes.copy().items():
            process = Process(init_dict=process_dict)
            # Some forms don't require to add Data Handlers but we still need to tie the request to a requestor
            check_dataprocessors(process)
            # ProjectName is required for display and download
            add_project_name(process)
            extract_data_metadata(process)
            process_dict = convert_to_dict(process)
            processes[process_id] = process_dict

        ######
        id = random.randint(0, 100000000)
        data_file_path = REQUEST_PATH + "/data_file_" + str(id) + ".json"
        header = get_from_storage("header")["value"]
        write_contract_rdf(data_file_path, contract, processes, header)
        # # #TO DO: validate with SHACL shapes
        home()
    except Exception as e:
        dialog.open()
