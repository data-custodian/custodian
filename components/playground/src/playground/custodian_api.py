import os
import urllib.parse
from os.path import dirname, join

import requests
from dotenv import load_dotenv

dotenv_path = join(dirname(__file__), ".env")
load_dotenv(dotenv_path)
CM_URL = os.environ.get("CM_URL")
AUDIT_URL = os.environ.get("AUDIT_URL")
PDP_URL = os.environ.get("PDP_URL")
KB_URL = os.environ.get("KB_URL")

from playground.rdf.utils_rdf_read import get_signature

############################################################################################################

# FOR Contract Management System (CMS): updloading, downloading, and all signing processes


def api_cms_post_datarequest(datarequest_file_path, header):
    """POST the RDF file of a data request to the CMS.

    Args:
        datarequest_file_path (str): The path to the data request RDF file.
        header (str): The authentification header of the user.
    """
    headers = {"Content-Type": "application/ld+json", "Authorization": header}

    with open(datarequest_file_path, "rb") as f:
        data = f.read()
    requests.post(CM_URL + "/contracts", headers=headers, data=data)


def api_cms_get_datarequest(uid_datarequest, header):
    """GET the RDF file of a data request from the CMS.

    Args:
        uid_datarequest (str): The unique ID of the data request.
        header (str): The authentification header of the user.

    Returns:
        response: The data request as a dictionary in JSON-LD format.
    """
    headers = {"Authorization": header}
    response = requests.get(CM_URL + "/contracts/" + uid_datarequest, headers=headers)
    return response.json()["data"]


def api_cms_get_datarequests_for_user(user_uri, header):
    """GET all data requests for a data processor from the CMS.

    Args:
        header (str): The authentification header of the user.

    Returns:
        response: A list of dictionaries in JSON-LD where each list item corresponds to one data request.
    """
    headers = {"Authorization": header}
    response = requests.get(CM_URL + "/contracts", headers=headers)
    return response.json()["data"]


def api_cms_sign(uid_datarequest, header):
    """Sign a data request through the CMS

    Args:
        uid_datarequest (str): The unique ID of the data request.
        header (str): The authentification header of the user.
    """
    headers = {"Content-Type": "application/ld+json", "Authorization": header}
    response = requests.post(
        CM_URL + "/contracts/" + uid_datarequest + "/sign", headers=headers
    )

    signature_jsonld = get_signature(response.json()["data"])

    requests.patch(
        CM_URL + "/contracts/" + uid_datarequest + "/sign",
        headers=headers,
        data=signature_jsonld,
    )


def api_cms_revoke(uid_datarequest, header):
    """Revoke a data request through the CMS.

    Args:
        uid_datarequest (str): The unique ID of the data request.
        header (str): The authentification header of the user.
    """
    headers = {"Content-Type": "application/ld+json", "Authorization": header}
    response = requests.post(
        CM_URL + "/contracts/" + uid_datarequest + "/revoke", headers=headers
    )

    signature_jsonld = get_signature(response.json()["data"])

    requests.patch(
        CM_URL + "/contracts/" + uid_datarequest + "/revoke",
        headers=headers,
        data=signature_jsonld,
    )


def api_cms_sign_status(uid_datarequest, header):
    """GET the signature status of a data request through the CMS.

    Args:
        uid_datarequest (str): The unique ID of the data request.
        header (str): The authentification header of the user.

    Returns:
        response: The signature status of the data request.
    """
    headers = {"Authorization": header}
    response = requests.get(
        CM_URL + "/contracts/" + uid_datarequest + "/status", headers=headers
    )
    return response.json()["data"]


############################################################################################################

# FOR Policy Decision Point (PDP) : get validation status of a data request in order to download data


def api_acs_pdp_access(processing, uid_data, header):
    """GET the validation that a data request exists and has been signed by all parties from the PDP. Process the response to make it immediately usable.

    Args:
        uid_user (str): The unique ID of the user.
        projectName (str): The name of the project.
        uid_data (str): The unique ID of the dataset.
        header (str): The authentification header of the user.

    Returns:
        response: The validation status of the data request.
    """
    headers = {"Authorization": header}
    uid_data = urllib.parse.quote(uid_data)
    api_call = PDP_URL + "/token?processing=" + processing + "&dataset=" + uid_data
    response = requests.get(api_call, headers=headers)
    return response.json()["data"]


############################################################################################################

# FOR AUDIT TRAIL:  get all events ongoing in the custodian


def api_audit_trail_get(header, num_events=10):
    """GET the audit trail of all events occuring in the custodian.

    Args:
        middleware_header (str): The authentification header of the user reformatted for the audit trail.

    Returns:
        response: The response of the GET request."""
    headers = {"Authorization": header}
    response = requests.get(
        AUDIT_URL + "/events?perPage=" + str(num_events), headers=headers
    )
    return response.json()["events"]


############################################################################################################

# FOR KNOWLEDGE BASE: register data metadata

# POST


def api_knowledgebase_post_data(datametadata_file_path, header):
    """POST the RDF file of the data to the Knowledgebase.

    Args:
        datametadata_file_path (str): The path to the data RDF file.
        header (str): The authentification header of the user.
    """
    headers = {"Content-Type": "application/ld+json", "Authorization": header}
    with open(datametadata_file_path, "rb") as f:
        data = f.read()
    resp = requests.post(KB_URL + "/datasets-metadata", headers=headers, data=data)


def api_knowledgebase_post_user(user_file_path, header):
    """POST the RDF file of a user to the Knowledgebase.

    Args:
        user_file_path (str): The path to the user RDF file.
        header (str): The authentification header of the user.
    """
    headers = {"Content-Type": "application/ld+json", "Authorization": header}
    with open(user_file_path, "rb") as f:
        data = f.read()
    requests.post(KB_URL + "/users", headers=headers, data=data)


# GET


def api_knowledgebase_get_all_datametadata(header):
    """GET all data metadata from the Knowledgebase.

    Args:
        header (str): The authentification header of the user.

    Returns:
        response (list of dictionaries): A list of all the datametada in the Knowledgebase (json-ld format).
    """
    headers = {"Authorization": header}
    response = requests.get(KB_URL + "/datasets-metadata", headers=headers)
    print(response.text)
    return response.json()["data"]


def api_knowledgebase_get_datametadata_for_user(header):
    """GET all data metadata from the Knowledgebase for the user logged in.

    Args:
       header (str): The authentification header of the user

    Returns:
       response (list of dictionaries): A list of all the datametada in the Knowledgebase (json-ld format) for that user
    """
    headers = {"Authorization": header}
    response = requests.get(KB_URL + "/datasets-metadata/user", headers=headers)
    print(response.text)
    return response.json()["data"]


def api_knowledgebase_get_datametadata(id_manner, id, header):
    """GET the RDF file of data metadata from the Knowledgebase.

    Args:
        id_manner (str): The manner in which the data is identified (e.g. uri, url).
        id (str): The value to identify the data (uri, url).
        header (str): The authentification header of the user.

    Returns:
        response (list of one dictionary): The datametadata requested.
    """
    headers = {"Authorization": header}
    response = requests.get(
        KB_URL + "/datasets-metadata?" + id_manner + "=" + id, headers=headers
    )
    print("api_knowledgebase_get_datametadata")
    print(response.text)
    return response.json()["data"]


def api_knowledgebase_get_user(id_manner, id, header):
    """GET the RDF file of a user from the Knowledgebase.

    Args:
        id_manner (str): The manner in which the user is identified (e.g. uri, email).
        id (str): The value to identify the user (email, uri).
        header (str): The authentification header of the user.

    Returns:
        response (list of one dictionary): The user requested.
    """
    headers = {"Authorization": header}
    print("api_knowledgebase_get_user")
    response = requests.get(KB_URL + "/users?" + id_manner + "=" + id, headers=headers)
    print(response.text)
    return response.json()["data"]
