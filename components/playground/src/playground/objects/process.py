from typing import List, Dict, Any
from playground.objects.data import Data
from playground.objects.person import Person


class Process:
    """Process object to store the data, data type, data names, purpose, processing, policy elements, and user inputs"""

    def __init__(
        self,
        data: List[Data] = [],
        data_type: str = None,
        data_names: List[str] = [],
        hasPurpose: str = None,
        hasProcessing: str = None,
        policy_elements: List[str] = [],
        init_dict: Dict[str, Any] = None,
        body: Dict[str, Any] = {},
        datacontrollers_uri: List[Dict] = [],
        dataprocessors_uri: List[str] = [],
        datacontrollers: List[Person] = [],
        dataprocessors: List[Person] = [],
        data_uris: List[str] = [],
    ):
        """Initialize the Process object either from a dictionary or from the attributes.
        Args:
            data (List[Data]): The data objects.
            data_type (str): The type of data.
            data_names (List[str]): The names of the data.
            hasPurpose (str): The purpose of the data processing.
            hasProcessing (str): The processing of the data.
            policy_elements (List[str]): The policy elements.
            init_dict (Dict[str, Any]): The dictionary to initialize the object.
            body (Dict[str, Any]): The user inputs.
            datacontrollers_uri (List[Dict]): The data controllers URIs.
            dataprocessors_uri (List[str]): The data processors URIs.
            datacontrollers (List[Person]): The data controllers.
            dataprocessors (List[Person]): The data processors.
            data_uris (List[str]): The data URIs.
        """
        if init_dict:
            self.data_uris = init_dict["data"]
            self.data_names = init_dict["data_names"]
            self.data_type = init_dict["data_type"]
            self.hasPurpose = init_dict["hasPurpose"]
            self.hasProcessing = init_dict["hasProcessing"]
            self.policy_elements = init_dict["policy_elements"]
            self.body = init_dict["inputs"] if "inputs" in init_dict.keys() else {}
            self.datacontrollers_uris = (
                init_dict["datacontrollers_uri"]
                if "datacontrollers_uri" in init_dict.keys()
                else None
            )
        else:
            self.data_uris = data_uris
            self.data_names = data_names
            self.data_type = data_type
            self.hasPurpose = hasPurpose
            self.hasProcessing = hasProcessing
            self.policy_elements = policy_elements
            self.body = body
            self.datacontrollers_uris = datacontrollers_uri

        self.data = data

        self.dataprocessors_uris = dataprocessors_uri
        self.datacontrollers = datacontrollers
        self.dataprocessors = dataprocessors
        self.process_dict = {}

    ##### PROCESS DATA

    def add_data_uri(self, uris: List[str]):
        """Add the data URIs.
        Args:
            uris (List[str]): The data URIs.
        """
        self.data_uris = uris

    def add_data_name(self, data_name: str):
        """Add a data name to the datanames.
        Args:
            data_name (str): The data name to add.
        """
        self.data_names.append(data_name)

    def add_data(self, data_list_dict: List[Dict]):
        """Add the data. The data comes in as a dictionary, which is converted to a Data object.
        Args:
            data_list_dict (List[Dict]): The data as a list of dictionaries.
        """
        for data_dict in data_list_dict:
            self.data.append(
                Data(label=data_dict["label"], downloadURL=data_dict["downloadURL"])
            )

    ##### PROCESS PERSONS

    def add_persons_uri(self, uris: List[str], role: str):
        """Add the data controllers or data processors URIs.
        Args:
            uris (List[str]): The data controllers or data processors URIs.
            role (str): The role of the persons, to know to which attribute the uris should be added.
        """
        if role == "DataController":
            self.datacontrollers_uris = uris
        elif role == "DataProcessor":
            self.dataprocessors_uris = uris

    def add_persons(self, persons_list_dict: List[Dict], role: str):
        """Add the data controllers or data processors. The persons come in as a dictionary, which is converted to a Person object.
        Args:
            persons_list_dict (List[Dict]): The data controllers or data processors as a list of dictionaries.
            role (str): The role of the persons, to know to which attribute the persons should be added.
        """
        for person_dict in persons_list_dict:
            if role == "DataController":
                self.datacontrollers.append(
                    Person(
                        firstName=person_dict["firstName"],
                        lastName=person_dict["lastName"],
                        email=person_dict["email"],
                        role=role,
                    )
                )
            elif role == "DataProcessor":
                self.dataprocessors.append(
                    Person(
                        firstName=person_dict["firstName"],
                        lastName=person_dict["lastName"],
                        email=person_dict["email"],
                        role=role,
                    )
                )

    ##### PROCESS INPUTS & POLICY ELEMENTS

    def reset_inputs(self):
        """Reset the user inputs."""
        self.body = {}

    def reset_policy_elements(self):
        """Reset the policy elements."""
        self.policy_elements = []

    def add_inputs(self, key: str, val: Any):
        """Add the user inputs. If the key exists add new values to the list, otherwise create a new key.
        Args:
            key (str): The key of the input.
            val (str): The value of the input.
        """
        if (
            key in self.body.keys()
            and self.body[key]
            and key != "projectName"
            and key != "hasDataAnalysisPlan"
            and key != "hasDataProcessor"
        ):
            if not (type(self.body[key]) == list):
                self.body[key] = [self.body[key]]
            self.body[key].append(val)
        else:
            self.body[key] = val

    ##### PROCESS GET FUNCTIONS

    def get_attribute(self, attribute_name: str):
        """Get the attribute of the object.
        Args:
            attribute_name (str): The attribute name.
        Returns:
            Any: The value of the attribute.
        """
        if hasattr(self, attribute_name):
            return getattr(self, attribute_name)
        else:
            raise AttributeError(
                f"'Process' object has no attribute '{attribute_name}'"
            )

    def get_attribute_all_data(self, attribute_name: str):
        """Get an attribute value in each of the data objects in the process.
        Args:
            attribute_name (str): The attribute name.
        Returns:
            List[Any]: The value of the attribute in each of the data objects.
        """
        attributes = []
        for data in self.data:
            attributes.append(data.get_attribute(attribute_name=attribute_name))
        return attributes

    def get_datacontrollers(self):
        """Get the data controllers as a list of dictionaries."""
        all_datacontrollers = []
        for person in self.datacontrollers:
            all_datacontrollers.append(person.get_person_as_dict())
        return all_datacontrollers

    def get_dataprocessors(self):
        """Get the data processors as a list of dictionaries."""
        all_dataprocessors = []
        for person in self.dataprocessors:
            all_dataprocessors.append(person.get_person_as_dict())
        return all_dataprocessors

    def get_data(self):
        """Get all datasets as a list of dictionaries."""
        all_data = []
        for data in self.data:
            all_data.append(data.get_data_as_dict())
        return all_data

    ##### PROCESS CONVERT TO DICT

    def convert_to_dict(self):
        """Convert the process object to a dictionary."""
        self.process_dict = {
            "data": [data.get_data_as_dict() for data in self.data],
            "data_names": self.data_names,
            "data_uris": self.data_uris,
            "datacontrollers_uris": self.get_attribute("datacontrollers_uris"),
            "data_type": self.data_type,
            "hasPurpose": self.hasPurpose,
            "hasProcessing": self.hasProcessing,
            "policy_elements": self.policy_elements,
            "inputs": self.body,
        }
