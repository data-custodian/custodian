class Person:
    """A class to represent a person."""

    def __init__(
        self,
        firstName: str = None,
        lastName: str = None,
        email: str = None,
        role: str = None,
    ):
        """Initialize the Person object.
        Args:
            firstName (str): The first name of the person.
            lastName (str): The last name of the person.
            email (str): The email of the person.
            role (str): The role of the person.
        """
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.role = role

    def get_person_as_dict(self):
        """Get the person as a dictionary.
        Returns:
            dict: The person's attributes as a dictionary of attributes.
        """
        return {
            "firstName": self.firstName,
            "lastName": self.lastName,
            "email": self.email,
            "role": self.role,
        }
