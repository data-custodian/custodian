from typing import List, Dict, Any
from datetime import datetime
from nicegui import ui
from playground.objects.process import Process
from playground.utils.utils_strings import (
    human_readable_string_formatter,
    remove_hashtag,
)
from playground.rdf.utils_rdf_read import (
    search_in_jsonld,
)


class Contract:
    """Class to represent a contract object.
    A Contract is composed of one or several processes.
    The contract is defined by a timeframe of validity, a URI, and a list of processes."""

    def __init__(
        self,
        uri: str = None,
        processes: List[Process] = [],
        start_date: datetime = None,
        end_date: datetime = None,
    ):
        """Initialize the Contract object.
        Args:
            uri (str): The URI of the contract.
            processes (List[Process]): The list of processes in the contract.
            start_date (datetime): The start date of the contract.
            end_date (datetime): The end date of the contract.
        """
        self.uri = uri
        self.processes = processes
        self.start_date = start_date
        self.end_date = end_date

    def add_uri(self, uri: str):
        """Add the URI to the contract.
        Args:
            uri (str): The URI of the contract.
        """
        self.uri = uri

    def add_process(self, process: Process):
        """Add a process to the contract.
        Args:
            process (Process): The process to add to the contract.
        """
        self.processes.append(process)

    def add_start_date(self, start_date):
        """Add the start date to the contract.
        Args:
            start_date (datetime): The start date of the contract.
        """
        self.start_date = start_date

    def add_end_date(self, end_date):
        """Add the end date to the contract.
        Args:
            end_date (datetime): The end date of the contract.
        """
        self.end_date = end_date

    def get_attribute(self, attribute_name: str):
        """Get the value of an attribute of the contract.
        Args:
            attribute_name (str): The name of the attribute to get.
        Returns:
            Any: The value of the attribute.
        """
        if hasattr(self, attribute_name):
            return getattr(self, attribute_name)
        else:
            raise AttributeError(
                f"'Process' object has no attribute '{attribute_name}'"
            )

    def get_attribute_all_processes(self, attribute_name: str):
        """Get the value of an attribute in each of the processes that compose the contract.
        Args:
            attribute_name (str): The name of the attribute which will be retrieved in each of the processes.
        Returns:
            List[Any]: The list of values of the attribute for each of the processes.
        """
        attributes = []
        for process in self.processes:
            attributes.append(process.get_attribute(attribute_name=attribute_name))
        return attributes

    def get_num_processes(self):
        """Get the number of processes in the contract.
        Returns:
            int: The number of processes in the contract.
        """
        return len(self.processes)


class ContractStatus(ui.label):
    """ "Change the color of the label representing the signed/unsigned/revoked status of a contract based on the value of the returned API text."""

    def __init__(self, positive_value):
        """Initialize the label with the positive value that will change the color of the label.
        Args:
            positive_value (str): The value that will change the label to a positive color.
        """
        super().__init__()
        self.positive_value = positive_value

    def _handle_text_change(self, text: str) -> None:
        """Change the color of the label based on the value of the text.
        Args:
            text (str): The text of the API response for the contract status.
        """
        super()._handle_text_change(text)
        if text == self.positive_value:
            self.classes(replace="text-positive")
        else:
            self.classes(replace="text-negative")


class ShowContract(ui.dialog):
    """Show the key-pair values defining a data request.
    The information is processed for the different main fields of the request
    (Context, DataControllers and DataProcessors, Dataset, and the rest of the details of the data request.)
    """

    def __init__(self, contract: Contract = None):
        """Initialize the ShowContract class and display the data request.
        Args:
            contract (Contract): The contract to display.
        """
        super().__init__(value=True)
        self.contract = contract
        with self, ui.card():
            ui.label("CONTRACT: " + contract.get_attribute("uri")).classes("text-lg")
            processes = contract.get_attribute("processes")
            # TIME
            self.__show_time__()
            for process in processes:
                # INTRODUCE NEW PROCESS
                self.__show_new_process__(process)
                # PURPOSE AND PROCESSING
                self.__show_purpose_processing__(process)
                # PERSONS
                self.__show_persons__(process)
                # DATA
                self.__show_data__(process)
                # PROCESS BODY INFO
                self.__show_body__(process)

    def __show_time__(self):
        """Show the timeframe of the contract."""
        with ui.row():
            ui.icon("clock").classes("text-2xl")
            ui.label("TIMEFRAME:")
        ui.label(
            "Start Date: "
            + self.contract.get_attribute("start_date").strftime("%Y-%m-%d %H:%M:%S")
        )
        ui.label(
            "End Date: "
            + self.contract.get_attribute("end_date").strftime("%Y-%m-%d %H:%M:%S")
        )

    def __show_new_process__(self, process):
        """Show the information of a process.
        Args:
            process (Process): The process to display.
        """
        ui.separator()
        ui.separator()
        with ui.row():
            ui.icon("transform").classes("text-2xl")
            body = process.get_attribute("body")
            project_name = search_in_jsonld([body], "projectName")
            ui.label("PROCESS:" + project_name).classes("text-lg")
        ui.separator()

    def __show_purpose_processing__(self, process):
        """Show the purpose and processing of a process.
        Args:
            process (Process): The process for which to display the purpose and processing.
        """
        with ui.row():
            ui.icon("notification_important").classes("text-2xl")
            ui.label("PURPOSE & PROCESSING:")

        ui.label("Purpose : " + process.get_attribute("hasPurpose"))
        ui.label("Processing : " + process.get_attribute("hasProcessing"))
        ui.separator()

    def __show_person__(self, person):
        """Show the information of a person.
        Args:
            person (Person): The person to display.
        """
        person_dict = person.get_person_as_dict()
        for k, v in person_dict.items():
            ui.label(human_readable_string_formatter(k) + ": " + v)

    def __show_persons__(self, process):
        """Show the data controllers and data processors of a process.
        Args:
            process (Process): The process for which to display the data controllers and data processors.
        """
        datacontrollers = process.get_attribute("datacontrollers")
        with ui.row():
            ui.icon("admin_panel_settings").classes("text-2xl")
            ui.label("DATA CONTROLLERS:")
        for person in datacontrollers:
            ui.icon("admin_panel_settings")
            self.__show_person__(person)
        dataprocessors = process.get_attribute("dataprocessors")
        with ui.row():
            ui.icon("people").classes("text-2xl")
            ui.label("DATA PROCESSORS:")
        for person in dataprocessors:
            ui.icon("people")
            self.__show_person__(person)
        ui.separator()

    def __show_data__(self, process):
        """Show the datasets of a process.
        Args:
            process (Process): The process for which to display the datasets.
        """
        all_data = process.get_attribute("data")
        with ui.row():
            ui.icon("folder").classes("text-2xl")
            ui.label("DATASETS:")
        for data in all_data:
            ui.icon("folder")
            data_dict = data.get_data_as_dict()
            for k, v in data_dict.items():
                ui.label(human_readable_string_formatter(k) + ": " + v)
        ui.separator()

    def __show_body__(self, process):
        """Show the body (policy elements) of a process.
        Args:
            process (Process): The process for which to display the body/ policy elements.
        """
        body = process.get_attribute("body")
        with ui.row():
            ui.icon("get_app").classes("text-2xl")
            ui.label("CONTRACT BODY:")
        for k, v in body.items():
            ui.label(human_readable_string_formatter(remove_hashtag(k)) + ": " + v)
