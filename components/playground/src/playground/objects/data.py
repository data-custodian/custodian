class Data:
    """Data object to store the data label/description and downloadURL"""

    def __init__(
        self, firstName: str = None, label: str = None, downloadURL: str = None
    ):
        """Initialize the Data object.
        Args:
            label (str): The label/description of the data.
            downloadURL (str): The download URL of the data.
        """
        self.label = label
        self.downloadURL = downloadURL

    def get_attribute(self, attribute_name: str):
        """Get the value of an attribute of the data.
        Args:
            attribute_name (str): The name of the attribute to get.
        Returns:
            Any: The value of the attribute.
        """
        if hasattr(self, attribute_name):
            return getattr(self, attribute_name)
        else:
            raise AttributeError(f"'Data' object has no attribute '{attribute_name}'")

    def get_data_as_dict(self):
        """Get the data as a dictionary.
        Returns:
            dict: The data objects attributes as a dictionary of attributes.
        """
        return {"label": self.label, "downloadURL": self.downloadURL}
