import jwt
from jwt import PyJWKClient


def jwt_decode_header(encoded: str):
    """Decodes the header of a JWT token. (Giving data like algorithm and token type as well as signature key: alg, typ, kid.). The decoding is in an unverified manner.

    Args:
        encoded (str): The encoded JWT token.

    Returns:
        token_header (dict): The decoded header of the JWT token.
    """
    token_header = jwt.get_unverified_header(encoded)
    return token_header


def jwt_decode_payload(encoded: str):
    """Decodes the payload of a JWT token without verifying JWT. (Giving data like issuer, groups, etc.). The decoding is in an unverified manner.

    Args:
        encoded (str): The encoded JWT token.

    Returns:
        payload (dict): The decoded payload of the JWT token.
    """
    payload = jwt.decode(
        jwt=encoded, algorithms=["RS256"], options={"verify_signature": False}
    )
    return payload


def jwt_decode_from_client(encoded: str, url: str, audience: str):
    """Decodes the payload of a JWT token using a client and verifying . (Giving data like issuer, groups, etc.)

    Args:
        encoded (str): The encoded JWT token.
        url (str): The URL of the client.
        audience (str): The audience of the client.

    Returns:
        payload (dict): The decoded payload of the JWT token.
    """
    jwks_client = PyJWKClient(url)
    signing_key = jwks_client.get_signing_key_from_jwt(encoded)
    payload = jwt.decode(
        encoded, signing_key.key, audience=audience, algorithms=["RS256", "HS256"]
    )
    return payload
