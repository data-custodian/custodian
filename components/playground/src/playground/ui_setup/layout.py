from nicegui import ui
from playground.ui_setup.logout import logout
from playground.ui_setup.home import home

important_field_style = "font-size: 150%; font-weight: 600; color: #92202A"
title_style = "font-size: 150%; font-weight: 500; color: #92202A"
big_writing_style = "font-size: 175%; font-weight: 800; color: #000000"
checkbox_style = "font-size: 100%; font-weight: 500; color: #6fa8dc"
writing_style = "font-size: 120%; font-weight: 500"


# icons :
color_icons = "#92202A"
size_icons = "250%"
classes_text_icons = "text-5xl"


def page_layout():
    """The layout of the page with the header and footer."""
    # HEADER
    with (
        ui.header(elevated=True)
        .style("background-color: #92202A")
        .classes("items-center justify-between")
    ):
        ui.image("data/images/custodian-logo-red.png").classes("w-80")
        ui.button(on_click=lambda: ui.navigate.to("/"), icon="home").props(
            "flat color=white"
        )
    # FOOTER
    with (
        ui.footer()
        .style("background-color: #92202A")
        .classes("items-center justify-between")
    ):
        ui.button(on_click=lambda: logout(), icon="logout").props("flat color=white")
        ui.button(on_click=lambda: home(), icon="home").props("flat color=white")


def main_layout():
    """The layout of the main page / landing page."""
    # WELCOME
    ui.label("Welcome to the Data Custodian Interface!").style(
        "font-size: 200%; font-weight: 300; color: #92202A"
    )
    ui.image("data/images/custodian-schema.png").style(
        "width: 600px; height: 260px;"
    ).classes("vertical-middle")
    ui.separator()
    ui.label("Menu : ").style("font-size: 150%; font-weight: 300; color: #92202A")
