from nicegui import ui
from playground.ui_setup.storage import reset


def home():
    """Resets the inputs and the URL button and opens the home page."""
    reset("inputs")
    reset("url_button")
    ui.navigate.to("/")
