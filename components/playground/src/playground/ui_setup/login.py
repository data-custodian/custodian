import os
from os.path import join, dirname
from dotenv import load_dotenv
from fastapi import Request
from starlette.middleware.base import BaseHTTPMiddleware
from nicegui import Client
from playground.ui_setup.storage import save_to_storage
from playground.ui_setup.utils_login_jwt import (
    jwt_decode_from_client,
)


dotenv_path = join(dirname(__file__), ".env")
load_dotenv(dotenv_path)
REALM_URL = os.environ.get("REALM_URL")
AUDIENCE = os.environ.get("AUDIENCE")


class AuthMiddleware(BaseHTTPMiddleware):
    """This middleware restricts access to all NiceGUI pages.
    It redirects the user to the login page if they are not authenticated.
    """

    async def dispatch(self, request: Request, call_next):
        """Dispatch the request and check if the user is authenticated.
        If the user is authenticated, the user information is saved in the session.
        Args:
            request (Request): The request object.
            call_next: The next function to call.
        Returns:
            The response of the request.
        """
        url = REALM_URL
        audience = AUDIENCE
        if request.url.path in Client.page_routes.values():
            # request.scope allows to see all the information about the request
            # SAVE HEADER and header reformatted for APIs
            header = request.headers["X-ID-Token"]
            save_to_storage("header", {"value": header})
            save_to_storage("header_reformatted", {"value": {"Authorization": header}})
            # DECODE JWT TOKEN
            jwt_token = header.split(" ")[1]
            decoded_jwt = jwt_decode_from_client(jwt_token, url, audience)
            # SAVE USER INFORMATION
            user_name = decoded_jwt["preferred_username"]
            save_to_storage("login_user_name", {"value": user_name})
            user_email = decoded_jwt["email"]
            save_to_storage("login_user_email", {"value": user_email})
            token_check = decoded_jwt["sub"]
            save_to_storage("user_hasIdentifier", {"value": token_check})
            full_name = decoded_jwt["name"]
            user_first_name = full_name.split(" ")[0]
            save_to_storage("login_user_firstName", {"value": user_first_name})
            user_last_name = full_name.split(" ")[1]
            save_to_storage("login_user_lastName", {"value": user_last_name})
            user_role = decoded_jwt["groups"]
            if "data-controller" in user_role:
                save_to_storage("user_role", {"value": "data-controller"})
            elif "data-processor" in user_role:
                save_to_storage("user_role", {"value": "data-processor"})
            else:
                print("Error: Role unknown")
        return await call_next(request)
