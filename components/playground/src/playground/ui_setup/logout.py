import glob
import os
from os.path import dirname, join

from dotenv import load_dotenv
from nicegui import ui

dotenv_path = join(dirname(__file__), ".env")
load_dotenv(dotenv_path)
REQUEST_PATH = os.environ.get("REQUEST_PATH", "data/requests")
REGISTER_DATA_PATH = os.environ.get("REGISTER_DATA_PATH", "data/register_data")
USER_DATA_PATH = os.environ.get("USER_DATA_PATH", "data/user")


def delete_storage(storage_folder):
    """Deletes the storage files.

    Args:
        storage_folder (str): The path to the storage folder.
    """
    for file in glob.glob(storage_folder + "storage-user-*"):
        os.remove(file)


def delete_files_in_folder(folder):
    """Deletes the requests files.

    Args:
        requests_folder (str): The path to the requests folder.
    """
    for filename in os.listdir(folder):
        os.remove(folder + "/" + filename)


def logout():
    """Logs out the user and deletes the requests and storage files."""
    requests_folder = os.getcwd() + "/" + REQUEST_PATH
    registered_data_folder = os.getcwd() + "/" + REGISTER_DATA_PATH
    user_data_folder = os.getcwd() + "/" + USER_DATA_PATH
    storage_folder = os.getcwd() + "/.nicegui/"
    delete_files_in_folder(requests_folder)
    delete_files_in_folder(registered_data_folder)
    delete_files_in_folder(user_data_folder)
    delete_storage(storage_folder)
    ui.navigate.to("/auth/logout")
