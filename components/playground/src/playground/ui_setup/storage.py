import json
from nicegui import app, ui


def reset(data_id):
    """This function resets the storage object.
    Args:
        data_id (str): The identifier of the data to be reset.
    """
    save_to_storage(data_id, {}, override=True)


def app_storage_is_set(value):
    """This function checks if the storage object is set.

    Args:
        value (str): The value to check in the storage.

    Returns:
        bool: True if the storage object is set, False otherwise.
    """
    storage_entry_for_value = app.storage.user.get(value)
    if storage_entry_for_value == "None" or not storage_entry_for_value:
        return False
    return True


def save_to_storage(data_id, data_dict, override=False):
    """This function saves the data to the storage.

    Args:
        data_id (str): The identifier of the data to be stored.
        data_dict (dict): The data to be stored.
        override (bool): The flag to override the data in the storage.
    """
    if app_storage_is_set(data_id) and override == False:
        # this allows to update an object if it is already in the storage
        data = get_from_storage(data_id)
        data.update(data_dict)
        data = json.dumps(data)
        app.storage.user[data_id] = data
    else:
        # this is to add a new object to the storage
        data_json = json.dumps(data_dict)
        app.storage.user[data_id] = data_json


def get_from_storage(data_id):
    """This function retrieves the data from the storage.

    Args:
        data_id (str): The identifier of the data to be retrieved.

    Returns:
        dict: The data retrieved from the storage.
    """
    try:
        object = app.storage.user.get(data_id)
        if app_storage_is_set(data_id) and object:
            return json.loads(object)
    except Exception as e:
        ui.notify(
            f"'{data_id}' could not be retrieved from storage. Exception occured: {e}",
            type="negative",
        )
