from nicegui import ui
from playground.ui_setup.storage import (
    save_to_storage,
    get_from_storage,
)
from playground.custodian_api import (
    api_knowledgebase_get_user,
)
from playground.utils.utils_process import add_to_process


def reset_user(fields):
    """This function resets the user fields.

    Args:
        fields (list): The list of fields to reset.
    """
    for i in range(0, len(fields)):
        field = fields[i]["label"]
        save_to_storage(data_id="user_" + field, data_dict={"value": ""})


def refresh_input_rows(row_inputs, fields, initial):
    """This function refreshes the input rows.

    Args:
        row_inputs (nicegui.ui.row): The row of inputs.
        fields (list): The list of fields.
        initial (bool): The flag to indicate if the field instantiation is the first or not.

    Returns:
        initial (bool): The flag to indicate if the field instantiation is the first or not.
    """
    reset_user(fields)
    row_inputs.clear()
    create_fields(row_inputs, fields)
    initial = False
    return initial


def create_field(field, label, initial=False):
    """This function creates a field and precompletes it if the information is available for the user logged in.

    Args:
        field (dict): The field to be created.
        label (str): The label of the field.
        initial (bool): The flag to indicate if the field instantiation is the first or not.
    """
    if initial == True:
        if get_from_storage("login_user_" + field["name"]):
            # if we have the info of the user, then we can prefill the fields
            ui.input(
                placeholder=label,
                value=get_from_storage("login_user_" + field["name"])["value"],
                on_change=lambda e: save_to_storage(
                    data_id="user_" + label, data_dict={"value": e.value}, override=True
                ),
            ).props("clearable")
        else:
            ui.input(
                placeholder=label,
                on_change=lambda e: save_to_storage(
                    data_id="user_" + label, data_dict={"value": e.value}, override=True
                ),
            ).props("clearable")
    else:
        ui.input(
            placeholder=label,
            on_change=lambda e: save_to_storage(
                data_id="user_" + label, data_dict={"value": e.value}, override=True
            ),
        ).props("clearable")


def create_fields(row_inputs, fields, initial=False):
    """This function creates the fields.

    Args:
        row_inputs (nicegui.ui.row): The row of inputs.
        fields (list): The list of fields.
        initial (bool): The flag to indicate if the field instantiation is the first or not.
    """
    with row_inputs:
        for field in fields:
            label = field["label"]
            create_field(field, label, initial)


def retrieve_user_data(fields, initial):
    """This function retrieves the user data.

    Args:
        fields (list): The list of fields.
        dialog (nicegui.ui.dialog): The dialog to be opened in case of an error.
        initial (bool): The flag to indicate if the field instantiation is the first or not.

    Returns:
        user_values (dict): The values of the user.
    """
    user_values = {}
    for i in range(0, len(fields)):
        field = fields[i]["label"]
        if (get_from_storage("user_" + field) is None) or (
            get_from_storage("user_" + field)["value"] == "" and initial == True
        ):
            val = get_from_storage("login_user_" + fields[i]["name"])["value"]
            user_values[field] = val
        else:
            val = get_from_storage("user_" + field)["value"]
            user_values[field] = val
    return user_values


def check_kb_for_user(email):
    """This function checks if the user is in the knowledge base.

    Args:
        email (str): The email of the user.

    Returns:
        bool: True if the user is in the knowledge base, False otherwise.
    """
    header = get_from_storage("header")["value"]
    resp = api_knowledgebase_get_user("email", email, header)
    if not resp:
        return False
    else:
        return True


def add_user(process_id, data_users_table, fields, row_inputs, initial):
    """This function adds a user to the data users table.

    Args:
        data_users_table (nicegui.ui.table): The table of data users.
        fields (list): The list of fields.
        row_inputs (nicegui.ui.row): The row of inputs.
        initial (bool): The flag to indicate if the field instantiation is the first or not.
    """
    with ui.dialog() as dialog, ui.card():
        ui.label(
            "That user is not in the Knowledge Base. Please make sure the user is registered in the system."
        )
        ui.button("I understand", on_click=dialog.close)
    num_cols = len(fields)
    user_values = retrieve_user_data(fields, initial)
    name_item_dict = {}
    for col in range(0, num_cols):
        item = user_values[fields[col]["label"]]
        name = fields[col]["name"]
        name_item_dict.update({name: item})
    email = name_item_dict["email"]

    if "hasDataProcessor" in get_from_storage("processes")[process_id]["inputs"].keys():
        data_processors = get_from_storage("processes")[process_id]["inputs"][
            "hasDataProcessor"
        ]
    else:
        data_processors = []
    if check_kb_for_user(email):
        # add a user to the list of data processors
        data_processors.append(name_item_dict)
        add_to_process(process_id, "hasDataProcessor", data_processors)
        data_users_table.add_rows(name_item_dict)
        initial = refresh_input_rows(row_inputs, fields, initial)
    else:
        initial = refresh_input_rows(row_inputs, fields, initial)
        dialog.open()
