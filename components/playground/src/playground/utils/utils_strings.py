import re


def snakemake_string(ex_string):
    """Converts a string to snakemake format.

    Args:
        ex_string (str): The string to convert.

    Returns:
        str: The string in snakemake format.
    """
    ex_string = ex_string.lower().title()
    return "".join(ex_string.split())


def remove_hashtag(word):
    """Removes the hashtag from a string.

    Args:
        word (str): The string to remove the hashtag from.

    Returns:
        str: The string without the hashtag.
    """
    return word.split("#")[1] if "#" in word else word


def remove_colon(word):
    """Removes the colon from a string.

    Args:
        word (str): The string to remove the colon from.

    Returns:
        str: The string without the colon.
    """
    return word.split(":")[1]


def human_readable_string_formatter(word):
    """Converts a string to human readable format. (If the string is in snakemake format)

    Args:
        word (str): The string to convert.

    Returns:
        str: The string in human readable format.
    """
    return (re.sub(r"([a-z](?=[A-Z])|[A-Z](?=[A-Z][a-z]))", r"\1 ", word)).title()


def naive_string_matching(text, pattern):
    """Naive string matching algorithm.

    Args:
        text (str): The text to search in.
        pattern (str): The pattern to search for.

    Returns:
        bool: True if the pattern is found in the text, False otherwise.
    """
    txt_len, pat_len = len(text), len(pattern)
    presence = False
    for s in range(txt_len - pat_len + 1):
        if pattern == text[s : s + pat_len]:
            presence = True
    return presence


def find_first_non_space(string_item):
    """Finds the first non space character in a string.

    Args:
        string_item (str): The string to search in.

    Returns:
        int: The index of the first non space character.
    """
    str_found = 0
    for splitted in string_item.split(" "):
        if splitted == "":
            str_found += 1
    return str_found
