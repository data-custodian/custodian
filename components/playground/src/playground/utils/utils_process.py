from playground.ui_setup.storage import (
    save_to_storage,
    get_from_storage,
)

from playground.objects.process import (
    Process,
)


def convert_to_dict(process):
    """Converts a process object to a dictionary.
    Args:
        process (Process): The process object.
    Returns:
        dict: The process object as a dictionary.
    """
    process.convert_to_dict()
    process_dict = process.get_attribute("process_dict")
    return process_dict


def add_to_process(process_id, key, value):
    """Adds a key-value pair to a process.
    It converts the stored process dictionary to a process object, adds the key-value pair and converts the process back into a dictionary so it can be stored in the nicegui storage.
    Args:
        process_id (str): The process ID to which the key-value pair needs to be added (to its inputs attribute).
        key (str): The key.
        value (str): The value.
    """
    processes = get_from_storage("processes")
    process_dict = processes[process_id]
    process = Process(init_dict=process_dict)
    process.add_inputs(key, value)
    processes[process_id] = convert_to_dict(process)
    save_to_storage(data_id="processes", data_dict=processes)


def clean_processes():
    """Cleans the processes dictionary in the nicegui storage by setting it to empty."""
    save_to_storage(data_id="processes", data_dict={}, override=True)
