from nicegui import ui
from playground.policies.policies_elements import (
    add_duration,
    add_project_name,
    add_plan,
    add_data_users,
    add_training,
    add_document,
    add_legal_measure,
    add_physical_measure,
    add_technical_measure,
    add_submit_button,
)
from playground.ui_setup.storage import (
    get_from_storage,
    save_to_storage,
)

from playground.utils.utils_process import (
    clean_processes,
    convert_to_dict,
)
from playground.ui_setup.layout import big_writing_style
from playground.objects.process import (
    Process,
)


def policy_elements_collider():
    """
    The policy_elements_collider function aims to collide the policy elements together if the data is defined with the same specifics (for example, same processing, same purpose, same data type).

    To do so, a Process object is created for each selected dataset and if the elements are the same for several datasets then the policy elements for these will be the same. Concretely, if there already exists a process with same characteristics, the dataset is added to its list of datasets. If the dataset has different characteristics (or processing or purpose are different) then a new Process is created with its own corresponding policy elements.
    """
    processes = {}
    process_num = 1
    selected_datametadata = get_from_storage("selected_datametadata")
    for new_data_name, new_datametadata in selected_datametadata.items():
        new_process = Process(
            data_names=[new_data_name],
            data_type=new_datametadata["type"],
            hasPurpose=get_from_storage(new_data_name)["hasPurpose"],
            hasProcessing=get_from_storage(new_data_name)["hasProcessing"],
            policy_elements=get_from_storage(new_data_name)["policy_elements"],
        )
        if not processes:
            processes[process_num] = new_process
            process_num += 1
        else:
            for process_id, process in processes.copy().items():
                if (
                    (
                        process.get_attribute("hasPurpose")
                        == new_process.get_attribute("hasPurpose")
                    )
                    and (
                        process.get_attribute("hasProcessing")
                        == new_process.get_attribute("hasProcessing")
                    )
                    and (
                        process.get_attribute("data_type")
                        == new_process.get_attribute("data_type")
                    )
                ):
                    process.add_data_name(new_process.get_attribute("data_names")[0])
                    processes[process_id] = process

                else:
                    processes[process_num] = new_process
                    process_num += 1
    for process_id, process in processes.copy().items():
        processes[process_id] = convert_to_dict(process)
    save_to_storage(data_id="processes", data_dict=processes)


def show_policies(display_col):
    """For each of the processes, the associated policy elements are rendered for the user to fill them out.
    Args:
        display_col (ui.column): The column to display the policy elements / the form for the user.
    """
    # ensure no legacy inputs from a previous request.
    if not (get_from_storage("processes") is None):
        clean_processes()
    policy_elements_collider()
    with display_col:
        display_col.clear()
        ui.separator()
        add_duration()
        processes = get_from_storage("processes")
        for process_id, process_dict in processes.copy().items():
            ui.separator()
            process = Process(init_dict=process_dict)
            data_names = " & ".join(process.get_attribute("data_names"))
            ui.label(data_names).style(big_writing_style)
            ui.separator()
            elements = process.get_attribute("policy_elements")
            for element_name, element_value in elements.copy().items():
                if element_name == "project_name":
                    add_project_name(process_id)
                elif element_name == "plan":
                    add_plan(process_id)
                elif element_name == "data_users":
                    add_data_users(process_id)
                elif element_name == "trainings":
                    for training in element_value:
                        add_training(process_id, training)
                elif element_name == "docs":
                    for doc in element_value:
                        add_document(process_id, doc)
                elif element_name == "legals":
                    for legal in element_value:
                        add_legal_measure(process_id, legal)
                elif element_name == "technicals":
                    for technical in element_value:
                        add_technical_measure(process_id, technical)
                elif element_name == "physicals":
                    for physical in element_value:
                        add_physical_measure(process_id, physical)
        add_submit_button()
