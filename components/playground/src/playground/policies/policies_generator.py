from nicegui import ui
from playground.ui_setup.storage import (
    save_to_storage,
    get_from_storage,
)
from playground.policies.policies_show import (
    show_policies,
)


def get_policy_elements(display_col):
    """This function gets the policy elements for the selected data based on the data type and calls the show_form function to show these policy elements and collect user input.
    Args:
        display_col (ui.column): The column to display the policy elements / the form for the user.
    """
    selected_datametadata = get_from_storage("selected_datametadata")
    for data_name, datametadata in selected_datametadata.items():
        if datametadata["type"] == "sensitive":
            get_sensitive_policy(data_name)
        elif datametadata["type"] == "non-sensitive":
            get_nonsensitive_policy(data_name)
    show_policies(display_col)


def get_nonsensitive_policy(data_name):
    """This function gets the policy elements for the selected NON-SENSITIVE data based on the data purpose and calls the save_to_storage function to save these policy elements.
    Args:
        data_name (str): The name of the data for which the data type will be inferred and the name is the key to save the policy elements in storage.
    """
    hasPurpose = get_from_storage(data_name)["hasPurpose"]
    if hasPurpose == "AcademicResearch":
        elements = {"project_name": [], "plan": [], "data_users": []}
    elif hasPurpose == "CommercialPurpose":
        elements = {
            "project_name": [],
            "plan": [],
            "data_users": [],
            "docs": ["Data Processing Agreement"],
            "legals": ["NDA", "Document Security"],
        }
    elif hasPurpose == "EnforceSecurity":
        elements = {
            "project_name": [],
            "plan": [],
            "trainings": ["Professional Training"],
            "docs": ["Data Processing Record", "PIA"],
        }
    if elements:
        elements = save_to_storage(
            data_id=data_name,
            data_dict={"policy_elements": elements},
        )


def get_sensitive_policy(data_name):
    """This function gets the policy elements for the selected SENSITIVE data based on the data processing and calls the save_to_storage function to save these policy elements.
    Args:
        data_name (str): The name of the data for which the data type will be inferred and the name is the key to save the policy elements in storage
    """
    hasProcessing = get_from_storage(data_name)["hasProcessing"]
    if hasProcessing == "Download":
        elements = {
            "project_name": [],
            "plan": [],
            "legals": ["Confidentiality Agreement"],
            "trainings": ["Data Protection Training", "Cybersecurity Training"],
            "technicals": ["Deidentification", "End to End Encryption"],
            "physicals": ["Physical Secure Storage"],
            "data_users": [],
        }
    elif hasProcessing == "Access":
        elements = {"project_name": [], "plan": [], "docs": ["Data Processing Policy"]}
    if elements:
        elements = save_to_storage(
            data_id=data_name,
            data_dict={"policy_elements": elements},
        )
