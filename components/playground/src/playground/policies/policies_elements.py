from datetime import datetime
from nicegui import ui
from playground.utils.utils_strings import (
    snakemake_string,
)
from playground.ui_setup.storage import (
    save_to_storage,
)
from playground.functionalities.request_validate import (
    submit_request,
)
from playground.utils.utils_dataprocessors import (
    add_user,
    create_fields,
)
from playground.ui_setup.layout import (
    title_style,
    checkbox_style,
)

from playground.utils.utils_process import add_to_process


def clean_contract_inputs():
    save_to_storage(data_id="contract", data_dict={}, override=True)


############################################################################################################
# DURATION


def save_date(date):
    """Saves the start and end date of the contract to the storage.
    Args:
        date (dict): The start and end date of the contract.
    """
    clean_contract_inputs()
    start_date = date["from"]
    end_date = date["to"]
    save_to_storage("contract", {"startDate": start_date, "endDate": end_date})


def add_duration():
    """Adds the duration of the contract to the form."""
    today_date = datetime.today().strftime("%Y/%m/%d")
    with ui.row():
        date = ui.date(on_change=lambda e: save_date(date.value)).props(
            f"""
                    range
                    default-year-month={today_date[:7]}
                    :options="date => date >= '{today_date}'"
                """
        )


############################################################################################################
# PROJECT NAME


def add_project_name(process_id):
    """Adds the project ID to the form.
    Args:
        process_id (str): The ID of the process for this input.
    """
    with ui.row():
        ui.label("Project Name").style(title_style)
        ui.icon("badge", color="#92202A", size="250%").classes("text-5xl")
        ui.input(
            label="Project Name",
            on_change=lambda e: add_to_process(process_id, "projectName", e.value),
        )
    ui.separator()


############################################################################################################
# PLAN


def add_plan(process_id):
    """Adds the data analysis plan to the form.
    Args:
        process_id (str): The ID of the process for this input.
    """
    with ui.row():
        ui.label("Data Analysis Plan").style(title_style)
        ui.icon("edit_note", color="#92202A", size="250%").classes("text-5xl")
    # plan = ui.textarea("Description of Data Use", value=" ", rows=10)
    ui.label("Please give a detailed plan for data analysis.")
    editor = ui.editor(
        placeholder="Type something here",
        on_change=lambda e: add_to_process(process_id, "hasDataAnalysisPlan", e.value),
    )
    ui.separator()


############################################################################################################
# DATA PROCESSORS


def add_data_users(process_id):
    """Adds the data users to the form based on their emails. Users need to be present in the Knowledge Base.
    Args:
        process_id (str): The ID of the process for this input.
    """
    fields = [{"name": "email", "label": "E-mail", "field": "email"}]
    with ui.row():
        ui.label("Data Processors").style(title_style)
        ui.icon("people", color="#92202A", size="250%").classes("text-5xl")
    ui.label(
        "Please add the e-mails of all data processors who will be working with or responsible for the data."
    )
    data_users_table = ui.table(columns=fields, rows=[], row_key="email").classes(
        "w-full"
    )
    # increment the data processors
    data_processors = []
    add_to_process(process_id, "hasDataProcessor", data_processors)
    row_inputs = ui.row()
    global initial
    initial = True
    create_fields(row_inputs, fields, initial)
    ui.button(
        "Add Data Processor",
        on_click=lambda: add_user(
            process_id, data_users_table, fields, row_inputs, initial
        ),
        color="#073763",
    ).classes("text-white")
    ui.separator()


############################################################################################################
# ORGANISATIONAL MEASURES: TRAININGS


def add_training(process_id, training):
    """Adds an arbitrary number of training defined by the data owner to the form.

    Args:
        process_id (str): The ID of the process for this input.
        training (list): A list of training that the data owner requires.
    """
    with ui.column():
        with ui.row():
            ui.label("Trainings").style(title_style)
            ui.icon("local_library", color="#92202A", size="250%").classes("text-5xl")
            with ui.row():
                checkbox = ui.checkbox(
                    "I have completed the " + training + " training.",
                    on_change=lambda e: add_to_process(
                        process_id,
                        "hasOrganisationalMeasure",
                        snakemake_string(training),
                    ),
                ).style(checkbox_style)
        ui.separator()


############################################################################################################
# ORGANISATIONAL MEASURES: DOCUMENTS


def add_document(process_id, document):
    """Adds an arbitrary number of document defined by the data owner to the form.

    Args:
        process_id (str): The ID of the process for this input.
        document (list): A list of document that the data owner requires.
    """
    with ui.row():
        with ui.row():
            ui.label("Documents").style(title_style)
            ui.icon("balance", color="#92202A", size="250%").classes("text-5xl")
            with ui.row():
                checkbox = ui.checkbox(
                    "Do you have the " + document + " ?",
                    on_change=lambda e: add_to_process(
                        process_id,
                        "hasOrganisationalMeasure",
                        snakemake_string(document),
                    ),
                ).style(checkbox_style)
    ui.separator()


############################################################################################################
# PHYSICAL MEASURES


def add_physical_measure(process_id, physical):
    """Adds the technical measures to the form.

    Args:
        process_id (str): The ID of the process for this input.
        physical (str): The physical measure that the data owner requires.
    """
    with ui.row():
        with ui.row():
            ui.label("Physical Measures").style(title_style)
            ui.icon("foundation", color="#92202A", size="250%").classes("text-5xl")
            with ui.row():
                checkbox = ui.checkbox(
                    "The following physical measure will be in place: "
                    + physical
                    + " .",
                    on_change=lambda e: add_to_process(
                        process_id, "hasPhysicalMeasure", snakemake_string(physical)
                    ),
                ).style(checkbox_style)
    ui.separator()


############################################################################################################
# LEGAL MEASURES


def add_legal_measure(process_id, legal):
    """Adds the technical measures to the form.

    Args:
        process_id (str): The ID of the process for this input.
        legal (str): The legal measure that the data owner requires.
    """
    with ui.row():
        with ui.row():
            ui.label("Legal Measures").style(title_style)
            ui.icon("gavel", color="#92202A", size="250%").classes("text-5xl")
            with ui.row():
                checkbox = ui.checkbox(
                    "I have signed the following legal measures: " + legal + " .",
                    on_change=lambda e: add_to_process(
                        process_id, "hasLegalMeasure", snakemake_string(legal)
                    ),
                ).style(checkbox_style)
    ui.separator()


############################################################################################################
# TECHNICAL MEASURES


def add_technical_measure(process_id, technical):
    """Adds the technical measures to the form.

    Args:
        process_id (str): The ID of the process for this input.
        technical (str): The technical measure that the data owner requires.
    """
    with ui.row():
        with ui.row():
            ui.label("Technical Measures").style(title_style)
            ui.icon("construction", color="#92202A", size="250%").classes("text-5xl")
        with ui.row():
            checkbox = ui.checkbox(
                "The following technical measure will be implemented: "
                + technical
                + " .",
                on_change=lambda e: add_to_process(
                    process_id, "hasTechnicalMeasure", snakemake_string(technical)
                ),
            ).style(checkbox_style)
    ui.separator()


############################################################################################################
# SUBMIT BUTTON


def add_submit_button():
    """Adds the submit button to the form. Clicking the button will create a turtle file and submit it to Custodian."""
    with ui.dialog() as dialog, ui.card():
        ui.button(
            "Custodian Error: There was an error uploading the data requests to the custodian.",
            on_click=lambda: ui.navigate.to("/"),
        )
    ui.button(
        "Submit Data Request",
        on_click=lambda: submit_request(dialog),
        color="#92202A",
    ).classes("text-white")
