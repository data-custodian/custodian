from nicegui import ui
from playground.ui_setup.layout import (
    important_field_style,
)
from playground.ui_setup.storage import (
    save_to_storage,
    get_from_storage,
)
from playground.utils.utils_strings import (
    snakemake_string,
)

from playground.ui_setup.layout import big_writing_style


def get_purpose_processing(type):
    """This function returns the purposes and processings based on the type of data.
    Args:
        type (str): The type of data (sensitive or non-sensitive).
    Returns:
        purposes (list): The list of purposes.
        processings (list): The list of processings.
    """
    if type == "sensitive":
        purposes = ["Academic Research"]
        processings = ["Download", "Access"]
    elif type == "non-sensitive":
        purposes = ["Academic Research", "Commercial Purpose", "Enforce Security"]
        processings = ["Download"]
    return purposes, processings


def select_purpose_processing(select_col, display_col, button_contract_generate):
    """This function creates the sensitive policies. The purpose is fixed and the processing can vary.
    Args:
        select_col (nicegui.ui.column): The column where the purpose and processing selection will be displayed.
        display_col (nicegui.ui.column): The column where the policies will be displayed.
        button_contract_generate (nicegui.ui.button): The button to generate the policies.
    """
    with select_col:
        selected_datametadata = get_from_storage("selected_datametadata")
        for data_name, datametadata in selected_datametadata.items():
            name = datametadata["name"]
            type = datametadata["type"]
            purposes, processings = get_purpose_processing(type)
            ui.label(name).style(big_writing_style)
            # Here purpose comes first
            ui.label("Indicate the PURPOSE behind your data request.").style(
                important_field_style
            )
            purpose_select(purposes, data_name, display_col)
            ui.space()
            # Then the processing
            ui.label("Indicate the USAGE of the data.").style(important_field_style)
            processing_select(
                processings, data_name, display_col, button_contract_generate
            )
            ui.separator()

        button_contract_generate.disable()


def purpose_select(purposes, data_name, display_col):
    """This function creates a dropdown menu to select the purpose of the data request.
    Args:
        purposes (list): The list of purposes.
        data_name (str): The name of the dataset.
        display_col (nicegui.ui.column): The column where the policies will be displayed.
    """
    ui.select(
        purposes,
        multiple=False,
        on_change=lambda e: purpose_change(data_name, display_col, e.value),
    )


def processing_select(processings, data_name, display_col, button_contract_generate):
    """This function creates a dropdown menu to select the processing of the data.
    Args:
        processings (list): The list of processings.
        data_name (str): The name of the dataset.
        display_col (nicegui.ui.column): The column where the policies will be displayed.
        button_contract_generate (nicegui.ui.button): The button to generate the policies.
    """
    ui.select(
        processings,
        multiple=False,
        on_change=lambda e: processing_change(
            data_name, display_col, button_contract_generate, e.value
        ),
    )


def purpose_change(data_name, display_col, purpose):
    """This function saves the purpose of the data request.
    Args:
        data_name (str): The name of the dataset.
        display_col (nicegui.ui.column): The column where the policies will be displayed.
        purpose (str): The purpose of the data request.
    """
    display_col.clear()
    if purpose:
        save_to_storage(
            data_id=data_name, data_dict={"hasPurpose": snakemake_string(purpose)}
        )


def processing_change(data_name, display_col, button_contract_generate, processing):
    """This function saves the processing of the data request.
    Args:
        data_name (str): The name of the dataset.
        display_col (nicegui.ui.column): The column where the policies will be displayed.
        button_contract_generate (nicegui.ui.button): The button to generate the policies.
        processing (str): The processing of the data request.
    """
    display_col.clear()
    if processing:
        save_to_storage(
            data_id=data_name, data_dict={"hasProcessing": snakemake_string(processing)}
        )
        enable_generate_contract(button_contract_generate)


def enable_generate_contract(button_contract_generate):
    """This function enables the button to generate the policies if all the data has been filled.
    Args:
        button_contract_generate (nicegui.ui.button): The button to generate the policies.
    """
    all_data_info = []
    selected_datametadata = get_from_storage("selected_datametadata")
    for data_name, _ in selected_datametadata.items():
        data_info = get_from_storage(data_name)
        if (
            (not data_info is None)
            and (not (data_info["hasProcessing"] is None))
            and (not (data_info["hasPurpose"] is None))
        ):
            this_data_info_present = True
        else:
            this_data_info_present = False
        all_data_info.append(this_data_info_present)
    if all(all_data_info):
        button_contract_generate.enable()
    else:
        button_contract_generate.disable()
