import urllib.parse
from datetime import datetime
from playground.rdf.utils_rdf_read import (
    split_from_prefix,
)
from playground.custodian_api import (
    api_knowledgebase_get_datametadata,
    api_knowledgebase_get_user,
)
from playground.ui_setup.storage import get_from_storage
from playground.objects.contract import Contract
from playground.objects.process import Process

from playground.utils.utils_strings import (
    remove_hashtag,
    human_readable_string_formatter,
)

#### Display JSON-LD file content


def process_val(val):
    """This function processes the value of a key in the JSON-LD file.
    It extracts the string value if the value is in a dictionary in a list.

    Args:
        val (list or str): The value which can have either type list (with a dict inside) or a string.

    Returns:
        val (str): The value of the key in a string format.
    """
    if isinstance(val, list):
        val = val[0]
        if "@value" in val.keys():
            return val["@value"]
        elif "@id" in val.keys():
            return val["@id"]
    return val


def get_data_content(data_uris):
    """This function extracts the content of a dataset metadata from JSON-LD metadata.
    From the URI, the JSON-LD metadata is retrieved through an API call to the knowledgebase.
    Then, the desired fields for displaying on the UI are extracted and saved in the dataset dictionary.

    Args:
        data_uris (list): The URIs of the datasets.

    Returns:
        data_list_dict (list): The list of dictionaries with the datasets metadata.
    """
    header = get_from_storage("header")["value"]
    data_list_dict = []

    for data_uri in data_uris:
        dataset_dict = {}
        dataset_uri = urllib.parse.quote(data_uri["@id"])
        data_jsonld = api_knowledgebase_get_datametadata("uri", dataset_uri, header)
        for key, val in data_jsonld[0].items():
            if "label" in key:
                dataset_dict["label"] = val[0]["@value"]
            elif "downloadURL" in key:
                dataset_dict["downloadURL"] = val[0]["@id"]
        data_list_dict.append(dataset_dict)
    return data_list_dict


def get_persons_content(persons_uri):
    """This function extracts the content of the data processors or data controllers from JSON-LD metadata.
    From the person's URI, the JSON-LD metadata is retrieved through an API call to the knowledgebase.
    Then, the desired fields for displaying on the UI are extracted and saved in the persons dictionary.

    Args:
        person_uris (list): The URIs of the data processors or data controllers.

    Returns:
        persons (list): The list of dictionaries with the data processors or data controllers metadata.
    """
    header = get_from_storage("header")["value"]
    persons = []
    for person_uri in persons_uri:
        person_dict = {}
        uri = urllib.parse.quote(person_uri["@id"])
        person = api_knowledgebase_get_user("uri", uri, header)
        for key, val in person[0].items():
            if "firstName" in key:
                person_dict["firstName"] = val[0]["@value"]
            elif "lastName" in key:
                person_dict["lastName"] = val[0]["@value"]
            elif "email" in key:
                person_dict["email"] = val[0]["@value"]
        persons.append(person_dict)
    return persons


def get_request_content(jsonld_input):
    """This function extracts the content of a data request from JSON-LD metadata.

    Args:
        jsonld_input (json-ld): The JSON-LD metadata of a data request.

    Returns:
        contract (Contract): The contract object with all processes, data, persons and body/policy elements.
    """
    contract = Contract(
        uri=None,
        processes=[],
        start_date=None,
        end_date=None,
    )
    for i in range(len(jsonld_input)):
        contract_subpart = jsonld_input[i]
        if "@type" in contract_subpart.keys():
            if "Contract" in contract_subpart["@type"][0]:
                contract.add_uri(split_from_prefix(contract_subpart["@id"]))
                for key, val in contract_subpart.items():
                    if "startDate" in key:
                        contract.add_start_date(
                            datetime.strptime(process_val(val), "%Y-%m-%dT%H:%M:%S")
                        )
                    if "endDate" in key:
                        contract.add_end_date(
                            datetime.strptime(process_val(val), "%Y-%m-%dT%H:%M:%S")
                        )
            if "#Process" in contract_subpart["@type"][0]:
                body_dict = {}
                for key, val in contract_subpart.items():
                    if "DataController" in key:
                        datacontrollers_uris = val
                        datacontrollers_list_dict = get_persons_content(
                            datacontrollers_uris
                        )
                    elif "DataProcessor" in key:
                        dataprocessors_uris = val
                        dataprocessors_list_dict = get_persons_content(
                            dataprocessors_uris
                        )
                    elif "hasData" == remove_hashtag(key):
                        data_uris = val
                        data_list_dict = get_data_content(data_uris)
                    elif "hasPurpose" in key:
                        purpose = human_readable_string_formatter(
                            remove_hashtag(process_val(val))
                        )
                    elif "hasProcessing" in key:
                        processing = human_readable_string_formatter(
                            remove_hashtag(process_val(val))
                        )
                    elif "hasMeasure" in key:
                        measures = [
                            human_readable_string_formatter(
                                remove_hashtag(process_val(measure["@id"]))
                            )
                            for measure in val
                        ]
                        body_dict["Measures"] = ", ".join(measures)
                    elif not ("@id" in key) and not ("@type" in key):
                        body_dict[key] = human_readable_string_formatter(
                            remove_hashtag(process_val(val))
                        )
                process = Process(
                    data_uris=data_uris,
                    hasPurpose=purpose,
                    hasProcessing=processing,
                    body=body_dict,
                    datacontrollers_uri=datacontrollers_uris,
                    dataprocessors_uri=dataprocessors_uris,
                    datacontrollers=[],
                    dataprocessors=[],
                    data=[],
                )
                process.add_persons(datacontrollers_list_dict, "DataController")
                process.add_persons(dataprocessors_list_dict, "DataProcessor")
                process.add_data(data_list_dict)
                contract.add_process(process)
    return contract
