import json
import os
from os.path import dirname, join

from dotenv import load_dotenv
from rdflib import Graph

dotenv_path = join(dirname(__file__), ".env")
load_dotenv(dotenv_path)
REGISTER_DATA_PATH = os.environ.get("REGISTER_DATA_PATH", "data/register_data")
SIGNATURE_PATH = os.environ.get("SIGNATURE_PATH", "data/signature")


def get_signature(datasignature):
    signature_jsonld = Graph()
    signature_jsonld.parse(data=datasignature[0], format="json-ld")
    signature_to_save = signature_jsonld.serialize(format="json-ld")
    with open(SIGNATURE_PATH + "/temp.json", "w", encoding="utf-8") as f:
        f.write(signature_to_save)
    with open(SIGNATURE_PATH + "/temp.json", "rb") as f:
        json_signature = f.read()
    return json_signature


def get_datametadata(datametadata):
    """Gets the data metadata in JSON-LD format.
    The datametadata is first parsed into a Graph then serialized to json-ld.
    The result is saved to a temporary file and then loaded back into a json object for further manipulations.

    Args:
        datametadata (dict): The data metadata in RDF format, which needs to be serialized.

    Returns:
        json_datametadata (dict): The data metadata in JSON-LD format.
    """
    datametadata_json = Graph()
    datametadata_json.parse(data=datametadata, format="json-ld")
    datametadata_to_save = datametadata_json.serialize(format="json-ld")
    with open(REGISTER_DATA_PATH + "/temp.json", "w", encoding="utf-8") as f:
        f.write(datametadata_to_save)
    with open(REGISTER_DATA_PATH + "/temp.json") as f:
        json_datametadata = json.load(f)
    return json_datametadata


def get_person_details(jsonld):
    """Gets the person details from the jsonld.

    Args:
        jsonld (list): The jsonld to search in. jsonld is a list of dictionaries.

    Returns:
        person (str): The person details (email, first name, last name)."""
    person_details = []
    person_details.append(search_in_jsonld(jsonld, "firstName")[0]["@value"])
    person_details.append(search_in_jsonld(jsonld, "lastName")[0]["@value"])
    person_details.append(search_in_jsonld(jsonld, "email")[0]["@value"])
    person = " ".join(person_details)
    return person


def search_in_jsonld(jsonld, keyword):
    """Searches for a keyword in a dictionary in JSON-LD.
    It returns the value associated to the key that contains the keyword

    Args:
        jsonld (list): The jsonld to search in. jsonld is a list of dictionaries
        keyword (str): The keyword to search for.

    Returns:
        val (str): The value associated to the key that contains the keyword.
    """
    for entity in jsonld:
        for key, val in entity.items():
            if keyword in key:
                return val


def split_from_prefix(rdf_uri):
    """Splits the URI from the prefix.
    Args:
        rdf_uri (str): The URI in RDF format.
    Returns:
        uri (str): The URI without the prefix.
    """
    uri = rdf_uri.split("/")[-1]
    uri = rdf_uri.split("#")[-1]
    return uri


def extract_id(data):
    """Extracts the id from the data metadata.

    Args:
        data (dict): The data metadata in JSON-LD format.

    Returns:
        data_id (str): The id of the data.
    """
    if type(data) == list:
        data_id = data[0]["@id"].split("/")[-1]
    else:
        data_id = data["@id"].split("/")[-1]
    data_id = data_id.split("#")[-1]
    return data_id
