import random

from rdflib import Graph, RDF, Literal, URIRef
from playground.custodian_api import (
    api_cms_post_datarequest,
    api_knowledgebase_post_data,
    api_knowledgebase_post_user,
)
from playground.rdf.utils_rdf_write import (
    define_prefixes,
    add_triples,
    add_data,
    create_person,
)


def write_contract_rdf(data_file_path, contract, processes, header):
    """Write the data request to a JSON-LD file.

    Args:
        data_file_path (str): The path to the JSON-LD file.
        contract (dict): The contract metadata.
        processes (dict): The dictionary of processes where each process is a dictionary.
        header (str): The authentification header of the user.
    """
    # Create a graph
    g = Graph()
    (
        g,
        dcat_list,
        dpv_list,
        dcat,
        _,
        dpv,
        _,
        _,
        rdf,
        _,
        sdcinst,
        sdcvoc,
        _,
        _,
        _,
        _,
    ) = define_prefixes(g)

    # Create a Contract
    contract_object = dpv.contract
    g.add((contract_object, rdf.type, dpv.Contract))
    # we need elements which are contract specific: the time span ?
    g = add_triples(
        g,
        contract,
        contract_object,
        dcat_list,
        dpv_list,
        dcat,
        dpv,
        sdcinst,
        sdcvoc,
        rdf,
    )

    for _, process in processes.items():
        # create the process
        random_id = random.randint(0, 1000000)
        process_id = sdcinst.Process + str(random_id)
        g.add((process_id, rdf.type, sdcvoc.Process))
        # Add the data URI
        for data_uri in process["data_uris"]:
            g.add((process_id, dpv.hasData, URIRef(data_uri)))
        # Add Data Controllers
        for datacontrollers_uri in process["datacontrollers_uris"]:
            g.add((process_id, dpv.hasDataController, URIRef(datacontrollers_uri)))
            human_involvement_unique_id = "HumanInvolvementForVerification" + str(
                random.randint(0, 1000000)
            )
            g.add(
                (
                    process_id,
                    URIRef(sdcvoc + "hasMeasure"),
                    URIRef(sdcinst + human_involvement_unique_id),
                )
            )
        # Add Purpose & Processing
        g.add((process_id, dpv.hasPurpose, URIRef(dpv + process["hasPurpose"])))
        g.add((process_id, dpv.hasProcessing, URIRef(dpv + process["hasProcessing"])))
        # Add all the relevant elements from inputs for that datametadata to the datahandling
        g = add_triples(
            g,
            process["inputs"],
            process_id,
            dcat_list,
            dpv_list,
            dcat,
            dpv,
            sdcinst,
            sdcvoc,
            rdf,
        )
        # Add process to the contract
        g.add((contract_object, dpv.hasProcess, process_id))

    g.serialize(destination=data_file_path, format="json-ld")
    api_cms_post_datarequest(data_file_path, header)


def write_data_rdf(data_file_path, datametadata, header):
    """Write the data to a JSON-LD format and post to the knowledge base.

    Args:
        data_file_path (str): The path to the JSON-LD file.
        datametadata (dict): The metadata of the data being requested.
        header (str): The authentification header of the user.
    """
    g = Graph()
    g, _, _, dcat, _, dpv, _, _, rdf, _, _, _, _, _, _, _ = define_prefixes(g)
    # Create the personalData / Dataset
    g, _ = add_data(g, datametadata, dcat, dpv, rdf)
    g.serialize(destination=data_file_path, format="json-ld")
    api_knowledgebase_post_data(data_file_path, header)


def write_person_rdf(data_file_path, user_data, role, header):
    """Write the person to a JSON-LD format and post to the knowledge base.

    Args:
        data_file_path (str): The path to the JSON-LD file.
        user_data (dict): The data of the user to be written to the JSON-LD file.
        role (str): The role of the user.
        header (str): The authentification header of the user.
    """
    g = Graph()
    (
        g,
        _,
        dpv_list,
        _,
        _,
        dpv,
        _,
        _,
        rdf,
        _,
        sdcinst,
        sdcvoc,
        _,
        _,
        _,
        _,
    ) = define_prefixes(g)
    g, person = create_person(g, user_data, role, dpv_list, dpv, rdf, sdcinst, sdcvoc)
    g.add((person, sdcvoc.id, Literal("_blanknode")))
    g.serialize(destination=data_file_path, format="json-ld")
    api_knowledgebase_post_user(data_file_path, header)
