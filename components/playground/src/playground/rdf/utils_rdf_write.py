from rdflib import Namespace
from rdflib import URIRef, Literal
from urllib.parse import quote
import random
from datetime import datetime

from playground.custodian_api import (
    api_knowledgebase_get_user,
)
from playground.ui_setup.storage import get_from_storage


def define_prefixes(g):
    """Define the prefixes and their namespaces for an RDF graph.
    The prefixes and namesapces are in alphabetical order.

    Args:
        g: RDF graph

    Returns:
        g: RDF graph with the prefixes and namespaces added.
        dpv_list: List of the DPV properties.
        dcat: Namespace for the DCAT vocabulary.
        dct: Namespace for the Dublin Core Terms vocabulary.
        dpv: Namespace for the DPV vocabulary.
        example: Namespace for the example vocabulary.
        foaf: Namespace for the FOAF vocabulary.
        rdf: Namespace for the RDF vocabulary.
        rdfs: Namespace for the RDFS vocabulary.
        sdcvoc: Namespace for the Swiss Data Custodian vocabulary.
        sh: Namespace for the SHACL vocabulary.
        skos: Namespace for the SKOS vocabulary.
        vcard: Namespace for the VCard vocabulary.
        xsd: Namespace for the XML Schema vocabulary.
    """
    dpv_list = [
        "hasIdentifier",
        # measures
        "hasOrganisationalMeasure",
        "hasLegalMeasure",
        "hasTechnicalMeasure",
        "hasPhysicalMeasure",
        # technical measures
        "Deidentification",
        "EndToEndEncryption",
        # organisational measures
        # trainings
        "DataProtectionTraining",
        "CybersecurityTraining",
        "ProfessionalTraining",
        "PIA",
        # documents
        "DataProcessingRecord",
        "DataProcessingAgreement",
        "DataProcessingPolicy",
        # physical measures
        "PhysicalSecureStorage",
        # legal measures
        "ConfidentialityAgreement",
        "DocumentSecurity",
        "NDA",
    ]

    dcat_list = ["startDate", "endDate"]

    # Define the namespaces
    dcat = Namespace("https://www.w3.org/ns/dcat#")
    dct = Namespace("http://purl.org/dc/terms/")
    # temporary:
    dpv = Namespace("https://w3id.org/dpv#")
    # it should be : dpv = Namespace("https://w3id.org/dpv#")
    example = Namespace("http://example.org/")
    foaf = Namespace("http://xmlns.com/foaf/0.1/")
    rdf = Namespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#")
    rdfs = Namespace("http://www.w3.org/2000/01/rdf-schema#")
    sdcinst = Namespace("https://swisscustodian.ch/inst#")
    sdcvoc = Namespace("https://swisscustodian.ch/doc/ontology#")
    sh = Namespace("http://www.w3.org/ns/shacl#")
    skos = Namespace("http://www.w3.org/2004/02/skos/core#")
    vcard = Namespace("http://www.w3.org/2006/vcard/ns#")
    xsd = Namespace("http://www.w3.org/2001/XMLSchema#")

    # Bind the namespaces to the graph
    g.bind("dcat", dcat)
    g.bind("dct", dct)
    g.bind("dpv", dpv)
    g.bind("ex", example)
    g.bind("foaf", foaf)
    g.bind("rdf", rdf)
    g.bind("rdfs", rdfs)
    g.bind("sdcinst", sdcinst)
    g.bind("sdcvoc", sdcvoc)
    g.bind("sh", sh)
    g.bind("skos", skos)
    g.bind("vcard", vcard)
    g.bind("xsd", xsd)

    return (
        g,
        dcat_list,
        dpv_list,
        dcat,
        dct,
        dpv,
        example,
        foaf,
        rdf,
        rdfs,
        sdcinst,
        sdcvoc,
        sh,
        skos,
        vcard,
        xsd,
    )


def create_person(g, dictionary, entity, dpv_list, dpv, rdf, sdcinst, sdcvoc):
    """Create a person in the RDF graph.
    This function is used to write a person into the Knowledge Base.

    Args:
        g: RDF graph
        dictionary: The person is a dictionary with its properties defined in key-value pairs.
        entity: The person's entity is the role or type (data processor or data controller).
        dpv_list: List of the DPV properties.
        dpv: Namespace for the DPV vocabulary.
        rdf: Namespace for the RDF vocabulary.
        sdcvoc: Namespace for the Swiss Data Custodian vocabulary.

    Returns:
        g: RDF graph with the person added.
        person: The person's URI (temporarily defined as email).
    """
    person = URIRef(sdcinst + quote(dictionary["email"]))
    g.add(((person), rdf.type, URIRef(dpv + entity)))
    for key2, value2 in dictionary.items():
        if key2 in dpv_list:
            g.add((person, URIRef(dpv + key2), Literal(value2)))
        else:
            g.add((person, URIRef(sdcvoc + key2), Literal(value2)))
    return g, person


def add_persons_list(g, object, persons, property, dpv):
    """Add a list of persons to the RDF graph. With each person's email, their URI is retrieved from the Knowledge Base.
    They are added as a property of an object through their URI.
    This function is used to write the dataset metadata to the Knowledge Base.

    Args:
        g: RDF graph
        object: The object to which the persons are added (example: dataHandling i.e. the data request or Data i.e. the dataset).
        persons: The list of persons to be added.
        property: The property under which the persons are added (hasDataController or hasDataProcessor) indicating the persons' role.
        dpv: Namespace for the DPV vocabulary.

    Returns:
        g: RDF graph with the persons added.
    """
    header = get_from_storage("header")["value"]
    for person_dictionary in persons:
        person_email = person_dictionary["email"]

        person = api_knowledgebase_get_user("email", person_email, header)
        person = person[
            0
        ]  # TODO validate that the person exists first (or use get instead)
        g.add((object, URIRef(dpv + property), URIRef(person["@id"])))
    return g


def add_data(g, datametadata, dcat, dpv, rdf):
    """Add the dataset metadata to the RDF graph as a PersonalData object.
    This function is used when writing the dataset's metadata to the Knowledge Base.

    Args:
        g: RDF graph
        datametadata: The metadata of the dataset.
        dcat: Namespace for the DCAT vocabulary.
        dpv: Namespace for the DPV vocabulary.
        rdf: Namespace for the RDF vocabulary.
        sdcvoc: Namespace for the Swiss Data Custodian vocabulary.

    Returns:
        g: RDF graph with the dataset metadata added as a PersonalData object.
    """
    data = dpv.Data
    download_url = datametadata["downloadURL"]
    datacontrollers = datametadata["hasDataController"]
    g.add((data, rdf.type, dcat.Dataset))
    g.add((data, dcat.downloadURL, URIRef(download_url)))
    g = add_persons_list(g, data, datacontrollers, "hasDataController", dpv)
    # ----------------------
    # USE-CASE SPECIFIC: define in data_extra_metadata.py
    try:
        from playground.use_case_specific.data_extra_metadata import (
            add_extra_data_metadata,
        )
    except:
        print(
            "In data_extra_metadata.py, define the function add_extra_data_metadata(g, data, datametadata)"
        )
    try:
        g = add_extra_data_metadata(g, data, datametadata)
    except:
        print(
            "The add_extra_data_metadata() function is defined incorrectly in the data_extra_metadata.py"
        )
    # ----------------------
    return g, data


def add_measure(g, object, val, sdcvoc, sdcinst):
    random_id = random.randint(0, 1000000)
    val_unique_id = val + str(random_id)
    g.add((object, URIRef(sdcvoc + "hasMeasure"), URIRef(sdcinst + val_unique_id)))


def add_triples(
    g, inputs, object, dcat_list, dpv_list, dcat, dpv, sdcinst, sdcvoc, rdf
):
    """Add triples to an object of the RDF graph.
    This function is used when writing the data request to the Contract database.
    Example: The data request RDF object (dataHandling) to which the data request fields (triples) are added.

    Args:
        g: RDF graph
        inputs: The fields/properties to add.
        object: The RDF object (example: dataHandling) to which the triples (example: data request properties) are added.
        dpv_list: List of the DPV properties.
        dpv: Namespace for the DPV vocabulary.
        sdcvoc: Namespace for the Swiss Data Custodian vocabulary.

    Returns:
        g: RDF graph with the triples added to the object.
    """
    for key_name, value in inputs.items():
        if "Date" in key_name:
            value = datetime.strptime(value, "%Y-%m-%d")
        if key_name in dpv_list:
            if type(value) == list:
                for val in value:
                    add_measure(g, object, val, sdcvoc, sdcinst)
            elif type(value) == str:
                add_measure(g, object, value, sdcvoc, sdcinst)
        elif key_name in dcat_list:
            g.add((object, URIRef(dcat + key_name), Literal(value)))
        else:
            if key_name == "hasDataProcessor":
                g = add_persons_list(g, object, value, "hasDataProcessor", dpv)
            else:
                g.add((object, URIRef(sdcvoc + key_name), Literal(value)))
    return g
