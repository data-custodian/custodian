import os
import random
from os.path import dirname, join

from dotenv import load_dotenv
from nicegui import ui
from playground.knowledgebase.register_user import register_person
from playground.rdf.parser_rdf_write import write_data_rdf
from playground.ui_setup.layout import title_style
from playground.ui_setup.storage import get_from_storage, save_to_storage
from playground.use_case_specific.data_extra_metadata import controlled_values

dotenv_path = join(dirname(__file__), ".env")
load_dotenv(dotenv_path)
REGISTER_DATA_PATH = os.environ.get("REGISTER_DATA_PATH", "data/register_data")


def show_data_metadata(datametadata, metadata_col):
    """This function shows the data metadata in the UI with the option of editing/adding it.

    Args:
        datametadata (dict): The metadata of the data being registered.
        metadata_col (ui.column): The column to display the metadata.
    """
    metadata_col.clear()
    with metadata_col:
        with ui.card().style("width: 1300px"):
            # LAYOUT
            with ui.row():
                ui.label("Metadata Overview & Edition").style(title_style)
                ui.icon("design_services", color="#92202A", size="250%").classes(
                    "text-5xl"
                )
            ui.label(
                "If everything looks good, you can register the data. Else, you can edit the metadata."
            ).style("color: #92202A; font-size: 100%; font-weight: 300")
            # SHOW METADATA
            with ui.row():
                ui.space()
                with ui.card().classes("no-shadow border-[2px]"):
                    for key, val in datametadata.items():
                        ui.label(f"{key}: {val}")
            # METADATA EDITOR
            with ui.row().style("justify-content: flex-start;"):
                dropdown = ui.dropdown_button(
                    "Choose attribute", auto_close=True, color="white"
                ).classes("text-grey")
                metadata = ui.input(placeholder="start typing").props(
                    "rounded outlined dense"
                )
                with dropdown:
                    for item in controlled_values:
                        create_dropdown_item(item, dropdown, metadata)
                metadata.disable()
                ui.button(
                    "Add Metadata",
                    on_click=lambda: register_metadata(
                        get_from_storage("register_metadata")["value"],
                        metadata.value,
                        datametadata,
                        metadata_col,
                    ),
                    color="white",
                ).classes("text-grey").bind_enabled(metadata, "value")
                ui.label("(Please seperate multiple values with a comma.)").style(
                    'color: "text-grey"; font-size: 85%; font-weight: 300'
                )
            ui.space()
            ui.space()
            # Error message
            with ui.dialog() as dialog_knowledgebase, ui.card():
                ui.button(
                    "Custodian Error: the data metadata could not be registered to the Knowledge Base.",
                    on_click=lambda: ui.navigate.to("/"),
                )
            # REGISTER BUTTON
            ui.button(
                "Register Data Metadata",
                on_click=lambda: submit_datametadata(
                    datametadata, dialog_knowledgebase
                ),
                color="#92202A",
            ).classes("text-white")


def create_dropdown_item(item, dropdown, metadata):
    """This function creates a dropdown item for the metadata editor.
    On click of this item, editing a metadata field is enabled.

    Args:
        item (str): The item to be added to the dropdown.
        dropdown (ui.dropdown_button): The dropdown to add the item to.
        metadata (ui.input): The input field to add the metadata to.
    """
    ui.item(item, on_click=lambda: key_register_metadata(item, dropdown, metadata))


def key_register_metadata(key, button, metadata):
    """This function enables the metadata editor for a specific metadata field by: 1. enabling a metadata input field to type the new value | 2. displaying the name of the metadata field being edited in a button.

    Args:
        key (str): The metadata field to be edited.
        button (ui.dropdown_button): The name of metadata field being changed.
        metadata (ui.input): The input field where the new value for the metadata can be input.
    """
    button.text = key
    metadata.enable()
    save_to_storage("register_metadata", {"value": key})


def register_metadata(key, val, datametadata, metadata_col):
    """This function registers the edits done to the metadata in the UI.
    If the input values are a list, it checks if the format can be processed correctly.

    Args:
        key (str): The metadata field which was edited.
        val (str): The new value for the metadata field.
        datametadata (dict): The metadata of the dataset being registered.
        metadata_col (ui.column): The column to display the metadata.
    """
    try:
        if "," in val:
            vals = val.split(",")
            list_value = []
            list_value.extend(vals)
            datametadata[key] = list_value
        else:
            datametadata[key] = val
        show_data_metadata(datametadata, metadata_col)
    except:
        ui.notify("The metadata could not be registered. Please check your input.")


def submit_datametadata(datametadata, dialog):
    """This function submits the data metadata to the Knowledge Base.
    It first registers the data controller, then -for the dataset- transforms its metadata to JSON-LD format and writes it to RDF.

    Args:
        datametadata (dict): The metadata of the dataset being registered.
        dialog (ui.dialog): The dialog to display an error message in case the metadata could not be registered.
    """
    # try:
    header = get_from_storage("header")["value"]
    # register data controller
    datametadata = register_person(datametadata, "hasDataController")
    # give random id and write to rdf
    id = random.randint(0, 100000000)
    data_file_path = REGISTER_DATA_PATH + "/datametadata_" + str(id) + ".json"
    write_data_rdf(data_file_path, datametadata, header)
    ui.navigate.to("/")
    # except:
    #     dialog.open()


def register_data(metadata_col):
    """This function displays the data metadata in the UI if the metadata has been successfully retrieved by an API call
    (see ReadMe for more specificites about the api_button set-up in the use_case_specific folder).
    The show function generates the metadata editor as well as the submit button to register the data in RDF to the Knowledge Base.
    Args:
        metadata_col (ui.column): The column to display the metadata.
    """
    if get_from_storage("api_button")["value"]:
        datametadata = get_from_storage("api_result")
        metadata_col.clear()
        show_data_metadata(datametadata, metadata_col)
