import os
from os.path import dirname, join

from dotenv import load_dotenv
from playground.rdf.parser_rdf_write import write_person_rdf
from playground.ui_setup.storage import get_from_storage

dotenv_path = join(dirname(__file__), ".env")
load_dotenv(dotenv_path)
USER_DATA_PATH = os.environ.get("USER_DATA_PATH", "data/user")


def get_user_info():
    """Get user information from the storage

    Returns:
        user: dictionary with user information
    """
    user = {}
    user["firstName"] = get_from_storage("login_user_firstName")["value"]
    user["lastName"] = get_from_storage("login_user_lastName")["value"]
    user["email"] = get_from_storage("login_user_email")["value"]
    user["hasIdentifier"] = get_from_storage("user_hasIdentifier")["value"]
    return user


def register_person(metadata, property_name):
    """Register a person in the metadata by getting all the user info.

    Args:
        metadata: dictionary with metadata
        property_name: key to store the user information (data processor or data controller)

    Returns:
        metadata: metadata dictionary with the added user information (list format)
    """
    user = get_user_info()
    metadata[property_name] = [user]
    return metadata


def register_user():
    """Register a user in the metadata by getting all the user info.
    Creates a temporary file defining the user before sending the user metadata to the knowledge base.
    """
    user = get_user_info()
    user_role = get_from_storage("user_role")["value"]
    if user_role == "data-controller":
        role = "DataController"
    if user_role == "data-processor":
        role = "DataProcessor"
    header = get_from_storage("header")["value"]
    data_file_path = os.getcwd() + "/" + USER_DATA_PATH + "/user_data.json"
    write_person_rdf(data_file_path, user, role, header)
