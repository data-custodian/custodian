from nicegui import ui
from playground.rdf.utils_rdf_read import (
    get_datametadata,
    extract_id,
)
from playground.ui_setup.storage import get_from_storage
from playground.custodian_api import (
    api_knowledgebase_get_user,
)


def get_user_id():
    """Gets the user UID from the Keycloak identifier saved in storage."""
    header = get_from_storage("header")["value"]
    user_hasIdentifier = get_from_storage("user_hasIdentifier")["value"]
    user = api_knowledgebase_get_user("id", user_hasIdentifier, header)
    uid_user = user[0]["@id"]
    return uid_user


def find_dataset(minimal_datametadata, keyword):
    """This function finds the dataset based on the keyword provided by the user.

    Args:
        minimal_datametadata (list): The minimal metadata of the datasets.
        keyword (str): The keyword provided by the user.

    Returns:
        selected_datametadata (list): The list of datasets where their metadata contains the keyword.
    """
    selected_datametadata = []
    for datametadata in minimal_datametadata:
        if keyword.lower() in datametadata["name"].lower():
            selected_datametadata.append(datametadata)
    return selected_datametadata


def prep_datametadata(all_datametadata):
    """Prepares the minimal metadata of the datasets.
    For each use_case, an extract_custom_metadata function should be defined in the utils_use_case.py

    Args:
        all_datametadata (list): The metadata of all the datasets.

    Returns:
        prepped_all_data (list): The minimal metadata of all the datasets.
    """
    prepped_all_data = []
    for datametadata in all_datametadata:
        datametadata_minimal = {}
        json_datametadata = get_datametadata(datametadata)
        datametadata_minimal["id"] = extract_id(json_datametadata)
        # ----------------------
        # USE-CASE SPECIFIC: define in utils_use_case.py
        try:
            from playground.use_case_specific.utils_use_case import (
                extract_custom_metadata,
                extract_name,
            )
        except:
            print(
                "In the utils_use_case.py you need to define the extract_custom_metadata() and extract_name() functions."
            )
        try:
            datametadata_minimal["name"] = extract_name(json_datametadata)
        except:
            print(
                "The extract_name() function is defined incorrectly in the utils_use_case.py"
            )
        try:
            datametadata_minimal = extract_custom_metadata(
                json_datametadata, datametadata_minimal
            )
        except:
            print(
                "The extract_custom_metadata() function is defined incorrectly in the utils_use_case.py"
            )
        # ----------------------
        prepped_all_data.append(datametadata_minimal)
    return prepped_all_data


def create_information_dialog(datametadata):
    """Creates a dialog box with the metadata of a dataset to show in the UI.

    Args:
        datametadata (dict): The metadata of the dataset.

    Returns:
        label_dialog: The dialog box with the description/label of the dataset to show in the UI.
    """
    with ui.dialog() as label_dialog, ui.card():
        ui.label("Data Description: ")
        ui.label(datametadata["label"])
        ui.button("Close", on_click=label_dialog.close, color="white").classes(
            "text-grey"
        )
    return label_dialog
