import os
from os.path import dirname, join

from dotenv import load_dotenv
from nicegui import app, ui
from playground.functionalities.audit_trail import show_audit_trail
from playground.functionalities.contracts_list import show_contracts
from playground.functionalities.data_registered_list import show_data_registered
from playground.functionalities.request_form import show_request_form
from playground.knowledgebase.register_user import register_user
from playground.ui_setup.layout import (
    classes_text_icons,
    color_icons,
    main_layout,
    page_layout,
    size_icons,
    title_style,
)
from playground.ui_setup.login import AuthMiddleware
from playground.ui_setup.storage import get_from_storage

dotenv_path = join(dirname(__file__), ".env")
load_dotenv(dotenv_path)
REQUEST_PATH = os.environ.get("REQUEST_PATH", "data/requests")
REGISTER_DATA_PATH = os.environ.get("REGISTER_DATA_PATH", "data/register_data")
USER_DATA_PATH = os.environ.get("USER_DATA_PATH", "data/user")


@ui.page("/data_register")
def data_register():
    """A page for registering data into the knowledge base."""
    page_layout()
    with ui.row():
        ui.icon("upload_file", color=color_icons, size=size_icons).classes(
            classes_text_icons
        )
        ui.label("Register Your Datasets into the Knowledgebase").style(title_style)
    # ------------------------------------------------------------------------------------------
    # USE_CASE_SPECIFIC: Define a Data Source - OPEN DATA SWISS example
    try:
        from playground.use_case_specific.data_source import data_source
    except:
        print(
            "In data_source.py define a data_source function to connect to your database."
        )
    try:
        data_source()
    except:
        print("The data_source() function is defined incorrectly in data_source.py")
    # ------------------------------------------------------------------------------------------


@ui.page("/data_registered_list")
def data_registered_list():
    """A page for showing all the data registered in the knowledge base."""
    page_layout()
    with ui.row():
        ui.icon("dataset", color=color_icons, size=size_icons).classes(
            classes_text_icons
        )
        ui.label("All Your Registered Datasets in the Knowledgebase").style(title_style)
    with ui.row():
        ui.link("To Register a new Dataset, click here.", "/data_register")
    show_data_registered()


@ui.page("/request_form")
def request_form():
    """A page with a form for the specific use-case."""
    page_layout()
    with ui.row():
        ui.icon("edit", color=color_icons, size=size_icons).classes(classes_text_icons)
        ui.label("Data Request Form").style(title_style)
    show_request_form()


@ui.page("/contracts_list")
def contracts_list():
    """A page where contracts for the logged in user are displayed."""
    page_layout()
    with ui.row():
        ui.icon("visibility", color=color_icons, size=size_icons).classes(
            classes_text_icons
        )
        ui.label("Here are all your contracts and their status.").style(title_style)
    ui.separator()
    with ui.dialog() as dialog, ui.card():
        ui.button(
            "Custodian Error: There was an error retrieving the contracts from the custodian.",
            on_click=lambda: ui.navigate.to("/"),
        )
    show_contracts(dialog)


@ui.page("/audit_trail")
def audit_trail():
    """A page for displaying the audit trail events."""
    # page_layout()
    with ui.row():
        ui.icon("follow_the_signs", color=color_icons, size=size_icons).classes(
            classes_text_icons
        )
        ui.label("Audit Trail").style(title_style)
    try:
        show_audit_trail()
    except:
        with ui.dialog() as dialog, ui.card():
            ui.button(
                "Custodian Error: There was an error retrieving the audit trail from the custodian.",
                on_click=lambda: ui.navigate.to("/"),
            )


@ui.page("/")
def menues():
    """A page to define menues based on user roles."""
    try:
        # Generate the layout of the page
        page_layout()
        main_layout()
        # Based on the user role, generate a menu
        user_role = get_from_storage("user_role")["value"]
        if user_role == "data-controller":
            with ui.row():
                ui.icon("visibility", color=color_icons, size=size_icons).classes(
                    classes_text_icons
                )
                ui.link("Data Requests", contracts_list).style(title_style)
            with ui.row():
                ui.icon("follow_the_signs", color=color_icons, size=size_icons).classes(
                    classes_text_icons
                )
                ui.link("Audit Trail", audit_trail).style(title_style)
            with ui.row():
                ui.icon("upload_file", color=color_icons, size=size_icons).classes(
                    classes_text_icons
                )
                ui.link("Register Data", data_register).style(title_style)
            with ui.row():
                ui.icon("dataset", color=color_icons, size=size_icons).classes(
                    classes_text_icons
                )
                ui.link("Overview Registered Data", data_registered_list).style(
                    title_style
                )
        if user_role == "data-processor":
            with ui.row():
                ui.icon("visibility", color=color_icons, size=size_icons).classes(
                    classes_text_icons
                )
                ui.link("My Data Requests", contracts_list).style(title_style)
            with ui.row():
                ui.icon("edit", color=color_icons, size=size_icons).classes(
                    classes_text_icons
                )
                ui.link("Create a Data Request", request_form).style(title_style)
        # Register the user who has just logged in
        register_user()
    except:
        print("Error: Login not performed yet.")


def main():
    """Main function to start the User Interface with an authentification middleware."""
    # Set Authentification Layer
    app.add_middleware(AuthMiddleware)
    # Create directories for temporary storage of data
    requests_path = REQUEST_PATH
    if not os.path.exists(requests_path):
        os.makedirs(requests_path)
    register_data_path = REGISTER_DATA_PATH
    if not os.path.exists(register_data_path):
        os.makedirs(register_data_path)
    user_data_path = USER_DATA_PATH
    if not os.path.exists(user_data_path):
        os.makedirs(user_data_path)
    # Create Menues
    menues()
    ui.run(port=5050, storage_secret="secret-key")


main()
