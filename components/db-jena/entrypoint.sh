#!/bin/sh
## Licensed under the terms of http://www.apache.org/licenses/LICENSE-2.0

cp /run/secrets/fuseki-users /fuseki/fuseki-users
## env | sort
exec "$JAVA_HOME/bin/java" "$JAVA_OPTIONS" -jar "${FUSEKI_DIR}/${FUSEKI_JAR}" "$@"
