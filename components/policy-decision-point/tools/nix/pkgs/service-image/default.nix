{
  lib,
  pkgs,
  service,
}:
let
  fs = lib.fileset;
  cnLib = lib.custodian;

  compPath = cnLib.components.getRootPath service.pname;

  # Add the sparql files to the image.
  sparqlFiles = fs.fromSource (compPath + "/sparql");
  auxSrc = fs.toSource {
    # Set the root in the generated `/nix/store/...` path to this folder.
    root = compPath;
    fileset = sparqlFiles;
  };
in
pkgs.dockerTools.buildLayeredImage {
  name = "custodian/${service.pname}-service";
  tag = service.version;

  contents = [
    service
  ];

  fakeRootCommands = ''
    ${pkgs.dockerTools.shadowSetup}
    mkdir -p /workspace
    ln -s "${auxSrc}/sparql" "/workspace/sparql"

    groupadd -r non-root
    useradd -r -g non-root non-root
    chown -R non-root:non-root /workspace
  '';
  enableFakechroot = true;

  config = {
    Entrypoint = [ "${service}/bin/${service.pname}" ];
    WorkingDir = "/workspace";
    Volumes = {
      "/workspace/config" = { };
    };
    Labels = {
      "org.opencontainers.image.source" = "https://gitlab.com/data-custodian/custodian";
      "org.opencontainers.image.description" = service.meta.description;
      "org.opencontainers.image.license" = service.meta.license.shortName;
      "org.opencontainers.image.version" = service.version;
    };
    User = "non-root";
  };
}
