{ pkgs, ... }:
rec {
  service = pkgs.callPackage ./service { };
  service-image = pkgs.callPackage ./service-image { inherit service; };
}
