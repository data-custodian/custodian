package pdp

import (
	"net/http"

	"github.com/gorilla/mux"
)

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

func NewRouter(h *Handler, componentURLPath string) *mux.Router {
	r := mux.NewRouter().StrictSlash(true)
	router := r.PathPrefix(componentURLPath).Subrouter()
	router.
		Methods("GET").
		Path("/token").
		Name("GetAuthorization").
		HandlerFunc(h.GetAuthorization)

	return router
}
