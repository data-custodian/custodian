package config

type Config struct {
	// Config struct for webapp config
	Server server `yaml:"server"`

	// FIXME: This should be removed and the public key should be retreived from the KMS
	// Location of the ECDSA Public Key
	CmsPublicKeyPath string `yaml:"cmsPublicKeyPath"`
	OIDC             oidc   `yaml:"oicd"`

	JWTPrivKey string   `yaml:"jwtPrivKey"`
	JWTPubKey  string   `yaml:"jwtPubKey"`
	RabbitMQ   rabbitmq `yaml:"rabbitmq"`
	Storage    storage  `yaml:"storage"`
}

type storage struct {
	Jena jena `yaml:"jena"`
}

type jena struct {
	Host           string `yaml:"host"`
	Scheme         string `yaml:"scheme"`
	CredentialFile string `yaml:"credentialFile"`
	CaTest         string `yaml:"caTest"`
}

type server struct {
	Hostname         string `yaml:"hostname"`
	Port             string `yaml:"port"`
	ComponentURLPath string `yaml:"componentURLPath"`
}

type oidc struct {
	// OpenID Connect issuer
	Issuer string `yaml:"issuer"`
	// OpenID Connect client ID
	ClientID string `yaml:"clientID"`
}

type rabbitmq struct {
	URL       string `yaml:"url"`
	QueueName string `yaml:"queueName"`

	Credentials rabbitMQCredentials `yaml:"credentials"`
}

type rabbitMQCredentials struct {
	Username string `yaml:"username"`
	Token    string `yaml:"token"`
}
