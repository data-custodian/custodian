package pdp

import (
	g "custodian/components/lib-common/pkg/graph"
	"custodian/components/lib-common/pkg/id"
)

const (
	revoke string = "REVOKE"
	valid  string = "VALID"
)

type Policy interface {
	IsContractSigned(contract id.ContractID, graph *g.Graph) (bool, error)
	IsWithinContractDateRange(contract id.ContractID, graph *g.Graph) (bool, error)
	HasUserAccess(contract id.ContractID, graph *g.Graph) (bool, error)
}

type DefaultPolicy struct {
	// empty
}

// validateStatusOfContract returns true if all value are "true".
func validateStatusOfContract(strings map[string]string) bool {
	for _, s := range strings {
		if s != valid && (s == "" || s == revoke) {
			return false
		}
	}

	return true
}

// IsContractSigned checks if the contract is signed by all data processor and data owner.
func (d DefaultPolicy) IsContractSigned(_contract id.ContractID, graph *g.Graph) (bool, error) {
	// TODO check that the user asking is actually in the contract?
	signaturesStatus, err := g.GetSignaturesStatus(graph)
	if err != nil {
		return false, err
	}
	contractValid := validateStatusOfContract(signaturesStatus)

	return contractValid, nil
}

// IsWithinContractDateRange TODO.
func (d DefaultPolicy) IsWithinContractDateRange(_contract id.ContractID, _graph *g.Graph) (bool, error) {
	return true, nil
}

// HasUserAccess TODO.
func (d DefaultPolicy) HasUserAccess(_contract id.ContractID, _graph *g.Graph) (bool, error) {
	// verify that the user and the data are in the contract
	// check constraints
	return true, nil
}
