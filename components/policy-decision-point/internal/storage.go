package pdp

import (
	"custodian/components/lib-common/pkg/graph"
	"custodian/components/lib-common/pkg/id"
)

type Storage interface {
	GetContractID(user id.GeneralID, processing string, dataset id.GeneralID) ([]id.ContractID, error)
	GetContractPolicy(*graph.Graph) string
	GetContract(contract id.ContractID) (*graph.Graph, error)
}
