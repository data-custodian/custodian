package pdp

import (
	"custodian/components/lib-common/pkg/graph"
	"custodian/components/lib-common/pkg/id"
	"custodian/components/lib-common/pkg/jena"
	"custodian/components/lib-common/pkg/log"
	pred "custodian/components/lib-common/pkg/predicates"
	tmpl "custodian/components/lib-common/pkg/template"
)

type JenaStorage struct {
	Jena jena.Jena
}

// GetContractID gets the contract id corresponding
// to `user`, `processing` and `dataset` id.
func (s *JenaStorage) GetContractID(
	user id.GeneralID,
	processing string,
	dataset id.GeneralID) (contractsID []id.ContractID, err error) {
	queryFilePath := "sparql/query-graph-uri.sparql.tmpl"
	data1 := map[string]interface{}{
		"Dpv":        pred.Dpv,
		"Processing": processing,
		"UserURI":    user.URI(),
		"DatasetURI": dataset.URI(),
	}
	query, err := tmpl.ReplaceInTemplate(queryFilePath, data1)
	if err != nil {
		return
	}
	log.Info("Get ContractID for user.", "user", user.URI(), "dataset", dataset.URI())
	resp, err := s.Jena.FetchFilteredTriples(jena.Contracts, string(query))
	if err != nil {
		return
	}
	for _, cURI := range resp {
		cID, e := id.NewContractIDFromURI(cURI)
		if e != nil {
			log.Error("could not create contract id", "error message", err)
		}
		contractsID = append(contractsID, cID)
	}

	return
}

// GetContractPolicy TODO.
/*
 * For now, policies have not been implemented yet so it will always return the "default policy"
 * Policies define criteria to allow or reject an access
 * For instance, data controller must have signed, user A must have completed security training ...
 */
func (s *JenaStorage) GetContractPolicy(_graph *graph.Graph) string {
	return "default"
}

func (s *JenaStorage) GetContract(contract id.ContractID) (*graph.Graph, error) {
	g, err := s.Jena.GetGraph(jena.Contracts, contract)
	if err != nil {
		return nil, err
	}

	return g, nil
}
