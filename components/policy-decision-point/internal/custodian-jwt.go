package pdp

import (
	"os"
	"time"

	"custodian/components/lib-common/pkg/log"

	"github.com/golang-jwt/jwt/v5"
)

const (
	fifteen = 15
)

// TokenClaims
// Content of a jwt from the custodian.
type TokenClaims struct {
	UserID         string   `json:"userid"`
	ContractID     string   `json:"contractid"`
	SignatureID    string   `json:"signatureid"`
	Actor          string   `json:"actor"`
	Scope          []string `json:"scope"`
	StandardClaims jwt.RegisteredClaims
}

// TODO: Token should have a short-time living period as it cannot be revoked easily.
// TODO: set sensitive data to nil after usage.
func generateToken(userID string, kid, jwtKeyPath string) (string, error) {
	claims := jwt.RegisteredClaims{
		ExpiresAt: jwt.NewNumericDate(time.Now().Add(fifteen * time.Minute)),
		IssuedAt:  jwt.NewNumericDate(time.Now()),
		NotBefore: jwt.NewNumericDate(time.Now()),
		Issuer:    "pdp.dev.swiss-custodian.ch",
		Subject:   userID,
		ID:        "signature",               // id of the contract ? of the signature or of the token?
		Audience:  []string{"somebody_else"}, // should we use that?
	}
	token := jwt.NewWithClaims(jwt.SigningMethodES256, claims)
	token.Header["kid"] = kid // FIXME: change config key name
	privateKeyData, err := os.ReadFile(jwtKeyPath)
	if err != nil {
		log.Error("error reading file", err)

		return "", err
	}
	privateKey, err := jwt.ParseECPrivateKeyFromPEM(privateKeyData)
	if err != nil {
		log.Error("error parsing private key", err)

		return "", err
	}
	signedToken, err := token.SignedString(privateKey)
	if err != nil {
		return "", err
	}

	return signedToken, nil
}
