package pdp

import (
	"custodian/components/lib-common/pkg/graph"
	utils "custodian/components/lib-common/pkg/http"
	"custodian/components/lib-common/pkg/id"
	"custodian/components/lib-common/pkg/jwt"
	"custodian/components/lib-common/pkg/log"
	mq "custodian/components/lib-common/pkg/rabbitmq"
	"net/http"

	"github.com/google/uuid"
)

const (
	defaultVal = "default"
)

// GetAuthorization verifies if a contract corresponds to the parameters, if it has been signed and if it is valid.
// @Title			Grant access to data
// @Description	    Decide if the current user has access to data or not
// @Resource		token
// @Param			processing	query   string   true   "Processing"                "Download"
// @Param			dataset	    query   string	 true	"ID of the dataset"	        Format(uuid)
// @Success         200	object	utils.ResponseStruct	"JWT"
// @Failure         400	object	utils.ErrorResponse	    "Bad request"
// @Router			/token [get].
//
//nolint:gocognit, funlen
func (h Handler) GetAuthorization(w http.ResponseWriter, r *http.Request) {
	if !jwt.CheckAuthTokenExists(r) {
		log.Error("Auth token was not provided.")
		utils.EncodeError(w, "No auth token provided", http.StatusUnauthorized)

		return
	}
	// TODO Check if the user already has a PDP JWT -> If so -> should he bypass ?
	// Extract data from URL
	userID, err := jwt.GetClaimFromJWT(r, "sub")
	if err != nil {
		log.Error("Could not extract user id from token.", "error message", err)
		utils.EncodeError(w, "User ID is missing or wrongly defined", http.StatusForbidden)

		return
	}
	u, err := uuid.Parse(userID)
	if err != nil {
		log.Error("Could not process user uuid from token,", "error message", err)
		utils.EncodeError(w, "User missing or wrongly defined", http.StatusBadRequest)

		return
	}
	user := id.NewGeneralID(u)
	processing, err := utils.GetParameterFromURL(r, "processing")
	if err != nil {
		log.Error("Could not get parameter processing from URL,", "error message", err)
		utils.EncodeError(
			w,
			"Parameter processing missing or wrongly defined",
			http.StatusBadRequest,
		)

		return
	}
	sanityCheck := utils.CheckWord(processing)
	if !sanityCheck {
		log.Error("Could not sanitize parameter processing.", "error message", err)
		utils.EncodeError(w, "Parameter processing is not correct", http.StatusBadRequest)

		return
	}
	datasetUUID, err := utils.GetParameterFromURL(r, "dataset")
	if err != nil {
		log.Error("Could not get parameter dataset from URL,", "error message", err)
		utils.EncodeError(w, "Parameter dataset missing or wrongly defined", http.StatusBadRequest)

		return
	}
	sanityCheck = utils.CheckUUID(datasetUUID)
	if !sanityCheck {
		log.Error("Could not sanitize parameter dataset.", "error message", err)
		utils.EncodeError(w, "Parameter dataset is not correct", http.StatusBadRequest)

		return
	}
	d, _ := uuid.Parse(datasetUUID)
	dataset := id.NewGeneralID(d)
	/*
	   		TODO : if the userID in the contract is not the same as the one in the jwt token
	   		we need to add here a method to translate/find the right uuid
	   		Otherwise, we can remove the user from the URL query and only use the one in the jwt token

	   		status, resp := getUserFromJWT(r)
	   		if status != http.StatusOK {
	   			encodeResponse(w, resp, status)
	   			return
	   		}

	   		tokenUser := resp
	   		// make the check that tokenUser == userID or make SPARQL query to retrieve the info
	              in the contract / Knowledge base
	*/
	// Find graph URI
	contractsID, err := h.Storage.GetContractID(user, processing, dataset)
	if err != nil {
		log.Error("Could not get contract URI.", "error message", err)
		utils.EncodeError(w, "Error with URI request", http.StatusBadRequest)

		return
	}
	if contractsID == nil {
		utils.EncodeError(w, "No contract found", http.StatusNotFound)

		return
	}
	isContractValid := false
	var dataControllerURI []string
	for _, contractID := range contractsID {
		// Retrieve contract as graph object
		contract, e := h.Storage.GetContract(contractID)
		if e != nil {
			log.Error("Could not serialize contract into graph object.", "error message", err)
			utils.EncodeError(w, "Error with contract request", http.StatusBadRequest)

			return
		}
		// Get contract policy
		// For now, policy is always "default"
		contractPolicy := h.Storage.GetContractPolicy(contract)

		var policy Policy
		if contractPolicy == defaultVal {
			policy = DefaultPolicy{}
		} else {
			utils.EncodeError(w, "Policy error", http.StatusBadRequest)

			return
		}
		// TODO : Check the contract is signed and not revoked
		// 1. Get CMS public key (TODO)
		// 2. Verify signature
		// Check if contract is signed
		isContractSigned, e := policy.IsContractSigned(contractID, contract)
		if e != nil {
			log.Error("Contract signature check failed.", "error message", err)
			utils.EncodeError(
				w,
				"An error occurred while checking the contract",
				http.StatusForbidden,
			)

			return
		}
		// Check if request is in date range
		isTimestampValid, e := policy.IsWithinContractDateRange(contractID, contract)
		if e != nil {
			log.Error("Contract time check failed.", "error message", err)
			utils.EncodeError(
				w,
				"An error occurred while checking the contract",
				http.StatusForbidden,
			)

			return
		}
		// Check if information given are in the contract
		hasUserAccess, e := policy.HasUserAccess(contractID, contract)
		if e != nil {
			log.Error("User access to data check failed.", "error message", err)
			utils.EncodeError(
				w,
				"An error occurred while checking the contract",
				http.StatusForbidden,
			)

			return
		}
		isContractValid = isContractValid || (isContractSigned && isTimestampValid && hasUserAccess)
		log.Info("Status of the contract for each test.",
			"signed", isContractSigned,
			"timestamp", isTimestampValid,
			"access", hasUserAccess)
		log.Info("Final status of the contract.", "valid", isContractValid)
		// Retrieve the URI of the data controllers and current user
		dataControllerURI = graph.GetDataControllerURI(contract)
	}
	if !isContractValid {
		log.Info("Contract is not valid.", "IsContractValid", isContractValid)
		err = mq.SendEvent(
			h.RabbitMQ,
			user,
			contractsID[0],
			dataControllerURI,
			dataset,
			"ACCESS DENIED",
			false,
			"The resource has not been accessed")
		if err != nil {
			log.Error("Could not connect to the Rabbit MQ.", "error message", err)
			utils.EncodeError(
				w,
				"An error occurred when sending the event",
				http.StatusInternalServerError,
			)

			return
		}
		utils.EncodeResponse(w, "Contract is not valid. "+
			"It has not been signed by everyone, has been revoked or is outdated")

		return
	}
	// ====================================================
	// Creating a custodian token so the user don't have to go through the whole process again
	// TODO check first if the user has a Custodian token and if it is valid
	signedToken, err := generateToken(userID, "123", h.JWTPrivKey)
	if err != nil {
		log.Error("Could not create the custodian token.", "error message", err)
		utils.EncodeError(w, "An internal error occurred", http.StatusInternalServerError)

		return
	}
	log.Info("Token successfully created.", "signedToken", signedToken)
	// ====================================================
	err = mq.SendEvent(
		h.RabbitMQ,
		user,
		contractsID[0],
		dataControllerURI,
		dataset,
		"ACCESS",
		true,
		"The resource has been accessed")
	if err != nil {
		log.Error("Could not connect to the Rabbit MQ.", "error message", err)
		utils.EncodeError(
			w,
			"An error occurred when sending the event",
			http.StatusInternalServerError,
		)

		return
	}
	utils.EncodeResponse(w, signedToken)
}
