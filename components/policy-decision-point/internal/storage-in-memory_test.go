package pdp

import (
	"custodian/components/lib-common/pkg/graph"
	utils "custodian/components/lib-common/pkg/http"
	"custodian/components/lib-common/pkg/id"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestGetContractID(t *testing.T) {
	db := InMemoryStorage{DB: map[id.ContractID]*graph.Graph{}}
	contractStr := `@prefix ns0: <https://w3id.org/dpv#> .
@prefix ns1: <https://swisscustodian.ch/doc/ontology#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
<https://swisscustodian.ch/inst#Process201229>
a <https://w3id.org/dpv#Process> ;
ns0:hasData <https://swisscustodian.ch/inst#d4156e8a-3a78-4087-9597-23a54016b615> ;
ns0:hasDataController <https://swisscustodian.ch/inst#791659e7-ff7d-4b40-abbb-952eb0d82b33> ;
ns0:hasDataProcessor <https://swisscustodian.ch/inst#6053c57d-17e6-476c-92e9-77736db38982> ;
ns0:hasProcessing ns0:Download ;
ns0:hasPurpose ns0:AcademicResearch ;
ns1:projectName "Project 1"^^xsd:string .
<https://swisscustodian.ch/inst#Contract123>
a ns0:Contract ;
ns0:hasProcess <https://swisscustodian.ch/inst#Process201229> .`
	c, _ := graph.SerializeToGraph([]byte(contractStr), utils.Turtle)
	u, _ := uuid.NewUUID()
	newID := id.NewContractID(u)
	db.DB[newID] = c
	user, _ := id.NewGeneralIDFromURI("https://swisscustodian.ch/inst#791659e7-ff7d-4b40-abbb-952eb0d82b33")
	data, _ := id.NewGeneralIDFromURI("https://swisscustodian.ch/inst#d4156e8a-3a78-4087-9597-23a54016b615")
	_, err := db.GetContractID(user, "Download", data)
	require.NoError(t, err)
	// assert.Equal(t, newID, contractID[0])
}

func TestGetContractPolicy(t *testing.T) {
	expectedPolicy := "default"
	db := InMemoryStorage{DB: map[id.ContractID]*graph.Graph{}}
	db.Create()
	// Take a contract
	var contract *graph.Graph
	for _, val := range db.DB {
		contract = val

		break
	}
	contractPolicy := db.GetContractPolicy(contract)
	assert.Equal(t, expectedPolicy, contractPolicy)
}

func TestGetContract(t *testing.T) {
	db := InMemoryStorage{DB: map[id.ContractID]*graph.Graph{}}
	db.Create()
	// Take a contract
	var contract id.ContractID
	var g *graph.Graph
	for i, val := range db.DB {
		contract = i
		g = val

		break
	}
	value, err := db.GetContract(contract)
	require.NoError(t, err)
	assert.Equal(t, g, value)
}
