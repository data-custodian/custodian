package pdp

import "custodian/components/lib-common/pkg/rabbitmq"

type Handler struct {
	Storage    Storage
	RabbitMQ   *rabbitmq.RabbitMQ
	JWTPrivKey string
	JWTPubKey  string
	CMSPubKey  string
}

func NewHandler(storage Storage, rabbitMQ *rabbitmq.RabbitMQ, jwtPrivKey, jwtPubKey, cmsPubKey string) *Handler {
	return &Handler{
		Storage:    storage,
		RabbitMQ:   rabbitMQ,
		JWTPrivKey: jwtPrivKey,
		JWTPubKey:  jwtPubKey,
		CMSPubKey:  cmsPubKey}
}
