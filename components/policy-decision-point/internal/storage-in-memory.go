package pdp

import (
	"custodian/components/lib-common/pkg/errors"
	"custodian/components/lib-common/pkg/graph"
	utils "custodian/components/lib-common/pkg/http"
	"custodian/components/lib-common/pkg/id"
	"custodian/components/lib-common/pkg/log"

	"github.com/google/uuid"
)

type InMemoryStorage struct {
	DB map[id.ContractID]*graph.Graph
}

func (m *InMemoryStorage) Create() {
	m.DB = make(map[id.ContractID]*graph.Graph)

	contractStr := "@prefix ns0: <https://w3id.org/dpv#> .\n" +
		"@prefix ns1: <https://swisscustodian.ch/vocab#> .\n" +
		"@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .\n" +
		"<https://swisscustodian.ch/inst#PhysicalSecureStorage303274> " +
		"a <http://w3id.org/dpv#PhysicalSecureStorage> .\n" +
		"<https://swisscustodian.ch/inst#Process201229>  a <http://w3id.org/dpv#Process> ;\n" +
		"ns0:hasData <https://swisscustodian.ch/inst#d4156e8a-3a78-4087-9597-23a54016b615> ;\n" +
		"ns0:hasDataController <https://swisscustodian.ch/inst#791659e7-ff7d-4b40-abbb-952eb0d82b33> ;\n" +
		"ns0:hasDataProcessor <https://swisscustodian.ch/inst#6053c57d-17e6-476c-92e9-77736db38982> ;\n" +
		"ns0:hasHumanInvolvement <https://swisscustodian.ch/inst#HumanInvolvementForVerification802391> ;\n" +
		"ns0:hasLegalMeasure <https://swisscustodian.ch/inst#ConfidentialityAgreement209327> ;\n" +
		"ns0:hasOrganisationalMeasure <https://swisscustodian.ch/inst#DataProtectionTraining749514>, " +
		"<https://swisscustodian.ch/inst#CybersecurityTraining420068> ;\n  " +
		"ns0:hasPhysicalMeasure <https://swisscustodian.ch/inst#PhysicalSecureStorage303274> ;\n" +
		"ns0:hasProcessing ns0:Download ;\n  ns0:hasPurpose ns0:AcademicResearch ;\n" +
		"ns0:hasTechnicalMeasure <https://swisscustodian.ch/inst#Deidentification607597>, " +
		"<https://swisscustodian.ch/inst#EndToEndEncryption757763> ;\n" +
		"ns1:hasDataAnalysisPlan \"Plan 1\"^^xsd:string ;\n" +
		"ns1:projectName \"Project 1\"^^xsd:string .\n" +
		"<https://swisscustodian.ch/inst#HumanInvolvementForVerification802391> " +
		"a ns0:HumanInvolvementForVerification .\n" +
		"<https://swisscustodian.ch/inst#ConfidentialityAgreement209327> a ns0:ConfidentialityAgreement .\n" +
		"<https://swisscustodian.ch/inst#DataProtectionTraining749514> a ns0:DataProtectionTraining .\n" +
		"<https://swisscustodian.ch/inst#CybersecurityTraining420068> a ns0:CybersecurityTraining .\n" +
		"<https://swisscustodian.ch/inst#Deidentification607597> a ns0:Deidentification .\n" +
		"<https://swisscustodian.ch/inst#EndToEndEncryption757763> a ns0:EndToEndEncryption .\n" +
		"<https://swisscustodian.ch/inst#Process907720>\n  a ns0:Process ;\n" +
		"ns0:hasData <https://swisscustodian.ch/inst#1e2c6dcd-9948-41a5-823f-92daa4d5870f> ;\n" +
		"ns0:hasDataController <https://swisscustodian.ch/inst#fbfc48e7-a6fc-46d3-a989-c729c75677a8> ;\n" +
		"ns0:hasDataProcessor <https://swisscustodian.ch/inst#0d6cc102-4116-4a98-ba83-c953987555d1> ;\n" +
		"ns0:hasHumanInvolvement <https://swisscustodian.ch/inst#HumanInvolvementForVerification428725> ;\n" +
		"ns0:hasProcessing ns0:Download ;\n  ns0:hasPurpose ns0:AcademicResearch ;\n" +
		"ns1:hasDataAnalysisPlan \"Plan 2\"^^xsd:string ;\n  ns1:projectName \"Project 2\"^^xsd:string .\n" +
		"<https://swisscustodian.ch/inst#HumanInvolvementForVerification428725> a " +
		"ns0:HumanInvolvementForVerification .\n<https://swisscustodian.ch/inst#Contract123>\n" +
		"a ns0:Contract ;\n" +
		"ns0:hasProcess <https://swisscustodian.ch/inst#Process907720>, " +
		"<https://swisscustodian.ch/inst#Process201229> ."
	c, _ := graph.SerializeToGraph([]byte(contractStr), utils.Turtle)
	u, _ := uuid.NewUUID()
	m.DB[id.NewContractID(u)] = c
}

// GetContractID gets the contract id corresponding
// to `user`, `processing` and `dataset` id.
func (m *InMemoryStorage) GetContractID(
	user id.GeneralID, processing string, dataset id.GeneralID) ([]id.ContractID, error) {
	// TODO
	var contracts []id.ContractID
	for i, val := range m.DB {
		d := graph.GetSubjectsByPredicate(val, "http://w3id.org/dpv#hasData", dataset.URI())
		p := graph.GetSubjectsByPredicate(
			val,
			"http://w3id.org/dpv#hasProcessing",
			"http://w3id.org/dpv#"+processing)

		up := graph.GetSubjectsByPredicate(val, graph.DataProcessor, user.URI())
		ud := graph.GetSubjectsByPredicate(val, graph.DataController, user.URI())
		if d != nil && p != nil && (up != nil || ud != nil) {
			contracts = append(contracts, i)
		}
	}
	log.Info("c", contracts)

	return contracts, nil
}

// GetContractPolicy
// TODO.
/*
 * For now, policies have not been implemented yet so it will always return the "default policy"
 * Policies define criteria to allow or reject an access
 * For instance, data controller must have signed, user A must have completed security training ...
 */
func (m *InMemoryStorage) GetContractPolicy(_graph *graph.Graph) string {
	return defaultVal
}

func (m *InMemoryStorage) GetContract(contract id.ContractID) (*graph.Graph, error) {
	if value, ok := m.DB[contract]; ok {
		return value, nil
	}

	return nil, errors.New("contract not found")
}
