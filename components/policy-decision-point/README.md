# Access Control

## Configuration

This folder contains the code for the Policy Decision Point service and the
Policy Administration Point service.

- Policy Decision Point: evaluates access requests from Policy Enforcement Point
  service against authorization policies (contracts) before granting access
  authorization.

When the command handler (Policy Enforcement Point) receives the task to access
through from the custodian, it must verify that the user signed a contract for
it.

For that, the Policy Enforcement Point service sends the user (`u`), processing
(`p`) and dataset (`d`) to the Policy Decision Point.

More information about the APIs can be found in `pdp/api/openapi.yml`.

The PDP will search for a contract and a signature matching the information
provided (`u` , `p`, `d`). If such contract id found, the PDP verifies its
validity before approving the action of the PEP.

---

# How to Run the Service With Docker Compose

Before running docker-compose, you must configure the environment variables in
the `docker-compose.yml` file.

Create a folder with the secret files and reference them in the
`docker-compose.yml` file.

Then, you can run the following command in a terminal:

```
docker compose up --build
```

## Endpoint test

```
curl -X GET "http://172.23.0.4:8011/acs/authorization?u=12bd62f8-7da5-49e3-8512-1abc0b603a48&p=download&d=http://example.org/%23MyDataSet1"
```

Dummy data

```
// We need to encode URL parameters for the graph URI, otherwise some may be cut out when decoding the URL
// For instance -> # replaced by %23

dummyUserID string = "12bd62f8-7da5-49e3-8512-1abc0b603a48"
dummyProcess string = "download"
dummyDatasetName string = "https://some.url.com/file"
```

## Endpoint implementation

The endpoint `/token` is a GET and parameters are passed by URL. This will
change in further implementation where the parameters will be passed through the
Keycloak token or the body request.

## Security known issues

- Currently, and for test purpose, the Keycloak token is used but not verified.
- Communication with the Jena DBs are through HTTP.

# Policy Decision Point

The Policy Decision Point (PDP) interprets the contracts and signature to decide
whether an actor has the right to access/process some data.

## Installation

### The Dependencies

The code has been built with `Go 1.19.2`, but should work with any version that
supports Go modules.

To install the dependencies, open a terminal in the `code` folder and type

```
go mod download
```

### Configuration

Create a `config.yml` file with the following content using the template
`config.docker.yml` or `config.local.yml.dist`

```sh
cp config.local.yml.dist config.yml
```

### Build and Run

To build and run the code, open a terminal in this folder
(`custodian/policy-decision-point`) and run the following command:

```
go build -o pdp cmd/main.go
./pdp
```

### Secrets

Create a `secrets.yaml` file with the following content using the template
`secrets.dist.yml`

```sh
cp secrets.dist.yml secrets.yml
```

Fill in the secrets according to your OIDC configuration.

## Usage

The list of endpoints and their description can be found in `api/swagger.yml`

## Spec

To make it work, you need

- a `user id` (this information is taken from the JWT token)
- a `dataset id` (the data the user wants to access)
- a processing (i.e. Download, Upload, what the user wants to do with the data)

The query looks like
`GET /acs/token?processing=Download&dataset=d4156e8a-3a78-4087-9597-23a54016b615`

The dataset is the UUID part of the URI (in other words, the prefix is removed)
Note that if you have multiple sources for datasets, the prefix may be needed
again in the future.

The processing name is case-sensitive and must match DPV spec

If more than one contract is found, they are all checked and if at least one is
valid, then the user has access to the data requested

`valid` means :

- the request is made within the time frame of the contract
- all the people involved in the contract have signed
- the user can indeed access this dataset through this medium (process checking)

If the contract has multiple processes, this is how it works: if `Process A` is
signed by everyone but `Process B` misses at least one signature, the contract
is not valid. Even if `Process A` and `Process` B do not share the same people /
data. You sign a Contract, not a Process.

A time frame is not in the contract yet, so for now the method returns true.

Process checking is not implemented yet, so it returns true. In this part, there
also will be checks if there are any constraints (but we do not have that yet)

## In-memory storage

In order to test the logic of the PDP alone - without all other components and
dependency to Jena -, it is possible to make it run as a standalone. There is an
in-memory storage implemented.

To make it run, you need to modify the `cmd/main.go` to use the proper storage

```
storage := pdp.JenaStorage{Jena: jena.New("config")}
//storage := pdp.InMemoryStorage{DB: map[id.ContractID]g.Graph{}}
//storage.Create()
```

Comment the first line and uncomment the two others. Then you can make it run as
explained above.

The in-memory storage implements the same methods as the jena storage + a
`Create()` method that populates the storage with one contract.

Values set for this particular contract are

`/acs/token?processing=Download&dataset=d4156e8a-3a78-4087-9597-23a54016b615`

with the token of Bob (Data controller)

`eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJXRGdUdXc4dnFscEdLUkU0aWFNSEZfOGRwM0tkMXlrcU1JT00wZVMzOU44In0.eyJleHAiOjE3MTA0MjYxNTAsImlhdCI6MTcxMDQyNTg1MCwiYXV0aF90aW1lIjoxNzEwNDI1ODUwLCJqdGkiOiIxN2QyYTNkZS0xZmEwLTRhNDktOWUxZi05NDEzNTVmNzVmYmQiLCJpc3MiOiJodHRwczovL2F1dGguZGV2LnN3aXNzY3VzdG9kaWFuLmNoL2F1dGgvcmVhbG1zL2N1c3RvZGlhbi1wbGF5Z3JvdW5kIiwiYXVkIjoiY3VzdG9kaWFuIiwic3ViIjoiNjA1M2M1N2QtMTdlNi00NzZjLTkyZTktNzc3MzZkYjM4OTgyIiwidHlwIjoiSUQiLCJhenAiOiJjdXN0b2RpYW4iLCJzZXNzaW9uX3N0YXRlIjoiZmVhOWUxOTQtZTM4My00ZThjLThiYjAtMjA1MjYzYzkxYzhjIiwiYXRfaGFzaCI6IkVGMUJ1NWg4aEV1MEtkOFl1YjhJeHciLCJhY3IiOiIxIiwic2lkIjoiZmVhOWUxOTQtZTM4My00ZThjLThiYjAtMjA1MjYzYzkxYzhjIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsIm5hbWUiOiJCb2IgRG9lIiwiZ3JvdXBzIjpbImRhdGEtb3duZXIiLCJkZWZhdWx0LXJvbGVzLWN1c3RvZGlhbi1wbGF5Z3JvdW5kIiwib2ZmbGluZV9hY2Nlc3MiLCJ1bWFfYXV0aG9yaXphdGlvbiJdLCJ1c2VyX3VyaSI6Imh0dHBzOi8vc3dpc3NkYXRhY3VzdG9kaWFuLmNoL2JvYiIsInByZWZlcnJlZF91c2VybmFtZSI6ImJvYiIsImdpdmVuX25hbWUiOiJCb2IiLCJmYW1pbHlfbmFtZSI6IkRvZSIsImVtYWlsIjoiYm9iQGV4YW1wbGUuY29tIn0.qC8zVrz89MOWzJJdxFwyvtemHh4z3VS3cVbTdd1xCfLq3sYCRLdVWpPydOeUWnxH6Tpn_kHSJGEN4GcOF985aUInk0VgdJXH1qiOYc6llOEqqlYq70WLORr8KFWjEjWI3yJMqhL_pNcs6YJyzDnrmDRT5l3xs9x9hEYuRGXfo1zuBn6NLokAipR3TiieOYlmCTsR_YnVNZ0FfNFlufiNd5JHKE8PM0x3cEhvot4g4Fw31SaGXrFpBjXWcQnItzI8Qdc0v4w8KSuTmpaTm6YgT1T2WjIRoQpMPBqmz0JMsitl7wk5nXfM9nwE3XAIBcMnfwJ_B0qqWsuLod1UsMelHg`
