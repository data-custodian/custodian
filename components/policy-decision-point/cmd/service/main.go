/**
 * Swiss Data Custodian provides mechanisms for contract management and access control.
 * Copyright (C) 2024 - Swiss Data Science Center
 * A partnership between École Polytechnique Fédérale de Lausanne (EPFL) and
 * Eidgenössische Technische Hochschule Zürich (ETHZ).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package main

import (
	cmc "custodian/components/lib-common/pkg/config"
	"custodian/components/lib-common/pkg/jena"
	"custodian/components/lib-common/pkg/rabbitmq"
	"net/http"
	"time"

	"custodian/components/lib-common/pkg/log"
	pdp "custodian/components/policy-decision-point/internal"
	"custodian/components/policy-decision-point/internal/config"
)

const (
	readTimeOut  = 5 * time.Minute
	writeTimeOut = 10 * time.Second
)

// @Version			0.1.0
// @Title				Custodian PDP API
// @Description		We offer some API that provides custodian services
// @ContactName		Custodian Team
// @ContactEmail		sdsc@epfl.ch
// @ContactURL			https://gitlab.com/data-custodian/custodian
// @TermsOfServiceUrl	https://gitlab.com/data-custodian/custodian
// @LicenseName		GNU AGPLv3
// @LicenseURL			https://gitlab.com/data-custodian/custodian/-/blob/develop/LICENSE.AGPL?ref_type=heads
// @Server				172.23.0.8:8011/acs
// @Security			AuthorizationHeader read write
// @SecurityScheme		AuthorizationHeader http bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9...
func main() {
	log.Setup()
	conf, err := cmc.LoadConfigs[config.Config]()
	if err != nil {
		log.ErrorE(err, "Failed to load configuration files.")

		return
	}
	storage := &pdp.JenaStorage{Jena: jena.New(
		conf.Storage.Jena.Host,
		conf.Storage.Jena.Scheme,
		conf.Storage.Jena.CredentialFile,
		conf.Storage.Jena.CaTest)}
	// storage := &pdp.InMemoryStorage{DB: map[id.ContractID]g.Graph{}}
	// storage.Create()
	// RabbitMR connection
	queue, err := rabbitmq.NewRabbitMQConnection(
		conf.RabbitMQ.URL,
		&conf.RabbitMQ.Credentials.Username,
		&conf.RabbitMQ.Credentials.Token,
	)
	if err != nil {
		log.ErrorE(err, "Failed to connect to RabbitMQ.")

		return
	}
	// Creates a handler that contains contextual information -> jena storage and some config information
	handler := pdp.NewHandler(
		storage,
		queue,
		conf.JWTPrivKey,
		conf.JWTPubKey,
		conf.CmsPublicKeyPath,
	)
	router := pdp.NewRouter(handler, conf.Server.ComponentURLPath)
	host := conf.Server.Hostname + ":" + conf.Server.Port
	server := &http.Server{
		Addr:         host,
		Handler:      router,
		ReadTimeout:  readTimeOut,
		WriteTimeout: writeTimeOut,
	}
	log.Info("Server is running at " + conf.Server.Port + " port.")
	err = server.ListenAndServe()
	if err != nil {
		return
	}
}
