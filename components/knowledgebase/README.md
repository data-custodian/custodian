# Disclamer

**This service is for development and tests only.** **There are no strong checks
on the data uploaded in the database** **This service will be removed in a
further release**

# Knowledge Base Manager (knowledgebase)

This folder contains the services to communicate with the knowledge base.

- knowledgebase-endpoint: REST API to upload data in the knowledge base

# How to Run the Service With Docker Compose

Before running docker-compose, you must configure the environment variables in
the `docker-compose.yml` file.

Specify the following environment variables for the contract-signature-endpoint:

- `SERVER_HOSTNAME`: the hostname to be used by the contract-endpoint
- `SERVER_PORT`: the port the contract-endpoint listens to

Create a folder with the secret files and reference them in the
`docker-compose.yml` file.

Then, you can run the following command in a terminal, from the repository
containing the `docker-compose.yml` file:

```
docker compose up
```

## API

### Data Metadata example

```
[
    {
    "@id": "https://swisscustodian.ch/vocab#PersonalData",
    "@type": [
      "https://www.w3.org/TR/vocab-dcat-2/#Dataset"
    ],
    "https://w3id.org/dpv#hasDataController": [
      {
        "@id": "https://swisscustodian.ch/vocab#bob%40example.com"
      }
    ],
    "http://www.w3.org/2000/01/rdf-schema#label": [
      {
        "@value": "Statistisches Jahrbuch der Schweiz 1920"
      }
    ],
    "https://www.w3.org/TR/vocab-dcat-2/#downloadURL": [
      {
        "@id": "https://opendata.swiss/en/dataset/statistisches-jahrbuch-der-schweiz-1920"
      }
    ],
    "https://www.w3.org/TR/vocab-dcat-2/#theme": [
      {
        "@id": "http://publications.europa.eu/resource/authority/data-theme/gove"
      }
    ]
  }

]
```

### Post a dataset

Accept text/turtle or application/ld+json

```
curl -X POST http://172.23.0.9:8004/knowledgebase/datasets-metadata -H "Content-Type:application/ld+json" -H 'Authorization: Bearer ...' -T "/path/to/dataset.jsonld"
```

### Get a dataset

```
curl -X GET http://172.23.0.9:8004/knowledgebase/datasets-metadata?url=url
```

### Get all dataset

```
curl -X GET http://172.23.0.9:8004/knowledgebase/datasets-metadata
```

### Get all dataset for dataowner

```
curl -X GET http://172.23.0.9:8004/knowledgebase/dataset/users -H 'Authorization: Bearer ...'
```

### Add a person

```
[ {
    "@id": "https://swisscustodian.ch/vocab#bob%40example.com",
    "@type": [
      "https://w3id.org/dpv#DataController"
    ],
    "https://w3id.org/dpv#hasIdentifier": [
      {
        "@value": "6053c57d-17e6-476c-92e9-77736db38983"
      }
    ],
    "https://swisscustodian.ch/vocab#email": [
      {
        "@value": "alice@example.com"
      }
    ],
    "https://swisscustodian.ch/vocab#firstName": [
      {
        "@value": "Alice"
      }
    ],
    "https://swisscustodian.ch/vocab#lastName": [
      {
        "@value": "Doey"
      }
    ]
  } ]
```

## Security known issues

- Currently, and for test purpose, the Keycloak token is used but not verified.
- Communication with the Jena DBs are through HTTP.

# Knowledge Base Webservice

**This is a temporary service to mimic the content of the knowledge base.**
**This service will be removed in a further release**

This REST API can be used to populate the knowledge base.

## Installation

### The Dependencies

The code has been built with `Go 1.22.0`, but should work with any version that
supports Go modules.

To install the dependencies, open a terminal in the `code` folder and type

```
go get
```

The signature endpoint uses either Pulsar or Kafka as message queue. Make sure
that the message queue you configured the code for (default is Pulsar) is
currently running before running the code.

### Configuration

Create a `config.yml` file with the following content using the template
`config.docker.yml` or `config.local.yml.dist`

```sh
cp config.local.yml.dist config.yml
```

### Build and Run

To build and run the code, open a terminal in the `custodian/knowledgebase`
folder and run the following command:

```
go build -o knowledgebase-endpoint code/main.go
./knowledgebase-endpoint
```

### Secrets

Create a `secrets.yaml` file with the following content using the template
`secrets.dist.yml`

```sh
cp secrets.dist.yml secrets.yml
```

Fill in the secrets according to your OIDC configuration.

## Usage

The list of endpoints and their description can be found in `api/openapi.yml`

## Documentation

To read the documentation you must install `godoc`. Open a terminal in the
`custodian/knowledgebase` folder or one of its subfolders, and run the following
command:

```
godoc -http=localhost:6060
```

The documentation for the structures and interfaces is accessible at
[http://localhost:6060/pkg/custodian/contract-manager/code/go/](http://localhost:6060/pkg/custodian/knowledgebase/code/go/)
