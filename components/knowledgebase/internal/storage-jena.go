package knowledgebase

import (
	"custodian/components/lib-common/pkg/errors"
	g "custodian/components/lib-common/pkg/graph"
	"custodian/components/lib-common/pkg/id"
	"custodian/components/lib-common/pkg/jena"
	"custodian/components/lib-common/pkg/log"
	pred "custodian/components/lib-common/pkg/predicates"
	tmpl "custodian/components/lib-common/pkg/template"
	"strconv"
)

type JenaStorage struct {
	Jena jena.Jena
}

func (s *JenaStorage) RemoveDataset(uri id.GeneralID) error {
	err := s.Jena.DeleteGraph(jena.KnowledgeBase, uri)
	if err != nil {
		return errors.AddContext(err, "data could not be deleted")
	}

	return nil
}

// GetDataset returns a dataset.
// the identifier can be either the id, the uri or the url of the dataset.
func (s *JenaStorage) GetDataset(identifier string, urlParam string) (*g.Graph, error) {
	var queryFilePath string
	switch urlParam {
	case "id":
		queryFilePath = "sparql/query-get-dataset-by-id.sparql.tmpl"
	case "url":
		queryFilePath = "sparql/query-get-dataset-by-url.sparql.tmpl"
	case "uri":
		queryFilePath = "sparql/query-get-dataset-by-uri.sparql.tmpl"
	}

	data1 := map[string]interface{}{
		"SwissCustodian": pred.SwissCustodian,
		"Dcat":           pred.Dcat,
		"Data":           identifier,
	}
	query, err := tmpl.ReplaceInTemplate(queryFilePath, data1)
	if err != nil {
		return nil, err
	}
	log.Info("Post query.", "query", string(query))
	graph, err := s.Jena.FetchGraph(jena.KnowledgeBase, string(query))
	if err != nil {
		return nil, err
	}

	return graph, nil
}

// Getuser returns a user.
// the identifier can be either the keycloack id, the uri or the email of the user.
func (s *JenaStorage) GetUser(identifier string, urlParam string, user id.GeneralID) (*g.Graph, error) {
	var queryFilePath string
	switch urlParam {
	case "email":
		queryFilePath = "sparql/query-get-user-by-email.sparql.tmpl"
	case "uri":
		queryFilePath = "sparql/query-get-user-by-uri.sparql.tmpl"
	case "id":
		queryFilePath = "sparql/query-get-user-by-id.sparql.tmpl"
	default:
		// id or user.URI()
		queryFilePath = "sparql/query-get-user-by-uri.sparql.tmpl"
	}
	var data1 map[string]interface{}
	if user.URI() != "" && urlParam == "" {
		data1 = map[string]interface{}{
			"SwissCustodian": pred.SwissCustodian,
			"Dpv":            pred.Dpv,
			"Data":           user.URI(),
		}
	} else {
		data1 = map[string]interface{}{
			"SwissCustodian": pred.SwissCustodian,
			"Dpv":            pred.Dpv,
			"Data":           identifier,
		}
	}
	query, err := tmpl.ReplaceInTemplate(queryFilePath, data1)
	if err != nil {
		return nil, err
	}
	log.Info("Post query.", "query", string(query))
	graph, err := s.Jena.FetchGraph(jena.KnowledgeBase, string(query))
	if err != nil {
		return nil, err
	}
	log.Info("Graph response.", "resp", graph)

	return graph, nil
}

func (s *JenaStorage) PostGraph(graph *g.Graph, dataID id.GeneralID) error {
	// TODO : check if graph is correct? shacl shapes, custodian names exist
	err := s.Jena.PostGraph(graph, dataID, jena.KnowledgeBase)
	if err != nil {
		return err
	}

	return nil
}

func (s *JenaStorage) GetAllDatasetForDataOwner(user id.GeneralID) (*g.Graph, error) {
	data := map[string]interface{}{
		"Dpv":  pred.Dpv,
		"Data": user.URI(),
	}
	query, err := tmpl.ReplaceInTemplate("sparql/query-get-dataset-by-user.sparql.tmpl", data)
	if err != nil {
		return nil, err
	}
	log.Info("Post query.", "query", string(query))
	graph, err := s.Jena.FetchGraph(jena.KnowledgeBase, string(query))
	if err != nil {
		return nil, errors.AddContext(err, "could not post query to retrieve datasets")
	}

	return graph, nil
}

func (s *JenaStorage) CheckIfExists(predicate string, object string) (bool, error) {
	data := map[string]interface{}{
		"Predicate": predicate,
		"Object":    object,
	}
	query, err := tmpl.ReplaceInTemplate("sparql/query-check-if-exists.sparql.tmpl", data)
	if err != nil {
		return false, err
	}
	log.Info("Post query.", "query", string(query))
	resp, err := s.Jena.FetchFilteredTriples(jena.KnowledgeBase, string(query))
	if err != nil {
		return false, errors.AddContext(err, "could not query if resource exists")
	}
	val, err := strconv.ParseBool(resp[0])
	if err != nil {
		err = errors.AddContext(err, "data could not be parsed to bool")

		return false, err
	}

	return val, nil
}

func (s *JenaStorage) GetAllDatasets() (*g.Graph, error) {
	data := map[string]interface{}{
		"Dcat": pred.Dcat,
	}
	query, err := tmpl.ReplaceInTemplate("sparql/query-get-all-datasets.sparql.tmpl", data)
	if err != nil {
		return nil, err
	}
	log.Info("Post query.", "query", string(query))
	graph, err := s.Jena.FetchGraph(jena.KnowledgeBase, string(query))
	if err != nil {
		return nil, errors.AddContext(err, "could not query all datasets")
	}

	return graph, nil
}
