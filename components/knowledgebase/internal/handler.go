package knowledgebase

import "custodian/components/lib-common/pkg/rabbitmq"

type Handler struct {
	Storage  Storage
	RabbitMQ *rabbitmq.RabbitMQ
}

func NewHandler(storage Storage, rabbitMQ *rabbitmq.RabbitMQ) *Handler {
	return &Handler{Storage: storage, RabbitMQ: rabbitMQ}
}
