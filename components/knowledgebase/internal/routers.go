package knowledgebase

import (
	"net/http"

	"github.com/gorilla/mux"
)

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

func NewRouter(h *Handler, componentURLPath string) *mux.Router {
	r := mux.NewRouter().StrictSlash(true)
	router := r.PathPrefix(componentURLPath).Subrouter()

	router.
		Methods("POST").
		Path("/datasets-metadata").
		Name("PostDataset").
		HandlerFunc(h.PostDataset)

	router.
		Methods("GET").
		Path("/datasets-metadata").
		Name("GetDataset").
		HandlerFunc(h.GetDataset)

	router.
		Methods("POST").
		Path("/users").
		Name("PostUser").
		HandlerFunc(h.PostUser)

	router.
		Methods("GET").
		Path("/users").
		Name("GetUser").
		HandlerFunc(h.GetUser)

	router.
		Methods("GET").
		Path("/datasets-metadata/user").
		Name("GetAllDataset").
		HandlerFunc(h.GetAllDatasetsForDataOwner)

	return router
}
