package knowledgebase

import (
	"custodian/components/lib-common/pkg/errors"
	g "custodian/components/lib-common/pkg/graph"
	"custodian/components/lib-common/pkg/id"
)

type InMemoryStorage struct {
	DBUsers    map[id.GeneralID]*g.Graph
	DBDatasets map[id.GeneralID]*g.Graph
}

func (m *InMemoryStorage) Create() {
	m.DBUsers = make(map[id.GeneralID]*g.Graph)
	m.DBDatasets = make(map[id.GeneralID]*g.Graph)
}

func (m *InMemoryStorage) RemoveDataset(uri id.GeneralID) error {
	if _, ok := m.DBDatasets[uri]; !ok {
		return errors.New("dataset does not exists")
	}
	delete(m.DBDatasets, uri)

	return nil
}

// GetDataset returns a dataset.
// the identifier can be either the id, the uri or the url of the dataset.
func (m *InMemoryStorage) GetDataset(_identifier string, _urlParam string) (*g.Graph, error) {
	// TODO
	return nil, errors.New("not implemented")
}

// Getuser returns a user.
// the identifier can be either the keycloak id, the uri or the email of the user.
func (m *InMemoryStorage) GetUser(
	_identifier string,
	_urlParam string,
	_user id.GeneralID,
) (*g.Graph, error) {
	// TODO
	return nil, errors.New("not implemented")
}

func (m *InMemoryStorage) PostGraph(graph *g.Graph, dataID id.GeneralID) error {
	// TODO : check if graph is correct? shacl shapes, custodian names exist
	isDataset := false
	for _, item := range graph.Graphs[g.DefaultName] {
		if item.Predicate.GetValue() == g.DataController {
			isDataset = true

			break
		}
	}
	if isDataset {
		if _, ok := m.DBDatasets[dataID]; ok {
			return errors.New("dataset already exists")
		}
		m.DBDatasets[dataID] = graph
	} else {
		if _, ok := m.DBUsers[dataID]; ok {
			return errors.New("user already exists")
		}
		m.DBUsers[dataID] = graph
	}

	return nil
}

// TODO : returns id or all graphs at once?
func (m *InMemoryStorage) GetAllDatasetForDataOwner(user id.GeneralID) ([]id.GeneralID, error) {
	var keys []id.GeneralID
	for _, val := range m.DBDatasets {
		for _, item := range val.Graphs[g.DefaultName] {
			if item.Predicate.GetValue() == g.DataController {
				if item.Object.GetValue() == user.URI() {
					k, _ := id.NewGeneralIDFromURI(item.Subject.GetValue())
					keys = append(keys, k)
				}
			}
		}
	}

	return keys, nil
}

func (m *InMemoryStorage) CheckIfExists(predicate string, object string) (bool, error) {
	for _, val := range m.DBDatasets {
		for _, item := range val.Graphs[g.DefaultName] {
			if item.Predicate.GetValue() == predicate && item.Object.GetValue() == object {
				return true, nil
			}
		}
	}
	for _, val := range m.DBUsers {
		for _, item := range val.Graphs[g.DefaultName] {
			if item.Predicate.GetValue() == predicate && item.Object.GetValue() == object {
				return true, nil
			}
		}
	}

	return false, nil
}

// TODO : same as getalldatasetsbydo.
func (m *InMemoryStorage) GetAllDatasets() ([]id.GeneralID, error) {
	var keys []id.GeneralID
	for i, _ := range m.DBDatasets {
		keys = append(keys, i)
	}

	return keys, nil
}
