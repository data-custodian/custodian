package knowledgebase

import (
	g "custodian/components/lib-common/pkg/graph"
	"custodian/components/lib-common/pkg/id"
)

type Storage interface {
	RemoveDataset(uri id.GeneralID) error

	GetDataset(identifier string, urlParam string) (*g.Graph, error)

	GetUser(data string, param string, user id.GeneralID) (*g.Graph, error)

	PostGraph(graph *g.Graph, dataID id.GeneralID) error

	GetAllDatasetForDataOwner(user id.GeneralID) (*g.Graph, error)

	// CheckIfExists Check if, in a graph, an object exists according to its predicate.
	CheckIfExists(predicate string, object string) (bool, error)

	GetAllDatasets() (*g.Graph, error)
}
