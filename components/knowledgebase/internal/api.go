package knowledgebase

import (
	auth "custodian/components/lib-common/pkg/authorization"
	g "custodian/components/lib-common/pkg/graph"
	utils "custodian/components/lib-common/pkg/http"
	"custodian/components/lib-common/pkg/id"
	"custodian/components/lib-common/pkg/jwt"
	"custodian/components/lib-common/pkg/log"
	pred "custodian/components/lib-common/pkg/predicates"
	mq "custodian/components/lib-common/pkg/rabbitmq"
	"net/http"
	"slices"

	"github.com/google/uuid"
)

// TODO : Sanitize user inputs

// GetAllDatasetsForDataOwner get all the datasets that belong to the current user
// @Summary		Get All Dataset for a data owner
// @Description	Get All Dataset for a data owner
// @Tags			Dataset
// @Security	Bearer
// @SecurityDefinitions api_key JWT
// @SecurityScheme api_key bearer
// @Param			Authorization	header		string	true	"Bearer <JWT Token>"	Format(JWT)
// Example(Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9...)
// @Success		200			{object}	utils.JSONLDResponseStruct	"OK"
// @Failure		400			{object}	utils.ErrorResponse	"Bad request"
// @Router			/datasets-metadata/user [get].
func (h Handler) GetAllDatasetsForDataOwner(w http.ResponseWriter, r *http.Request) {
	if !jwt.CheckAuthTokenExists(r) {
		utils.EncodeError(w, "No auth token provided.", http.StatusUnauthorized)

		return
	}
	userURI, err := jwt.GetUserID(r)
	if err != nil {
		log.ErrorE(err, "Could not extract user ID.")
		utils.EncodeError(w, "User ID is missing or wrongly defined.", http.StatusBadRequest)

		return
	}
	graph, err := h.Storage.GetAllDatasetForDataOwner(userURI)
	if err != nil {
		log.Error("error while getting dataset", "error message", err)
		utils.EncodeError(w, "Could not retrieve dataset.", http.StatusTeapot)

		return
	}
	// err, jsonld := serializeRDFToJson(resp)
	jsonld, err := g.GraphToJsonld(graph)
	if err != nil {
		log.Error("error while building graph", "error message", err)
		utils.EncodeError(w, "Could not get dataset.", http.StatusBadRequest)

		return
	}
	utils.EncodeJSONLDStruct(w, jsonld)
}

// GetDataset retrieve a dataset
// @Summary		Get a dataset
// @Description	Get a dataset. If not dataset is specified in parameter, get all datasets available
// @Tags			Dataset
// @Security	Bearer
// @SecurityDefinitions api_key JWT
// @SecurityScheme api_key bearer
// @Produces		text/turtle
// @Produces		application/ld+json
// @Param			Authorization	header		string	true	"Bearer <JWT Token>"	Format(JWT)
// Example(Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9...)
// @Param			id		query		string	false	"Dataset UUID"			Format(uuid)
// Example(123e4567-e89b-12d3-a456-426614174000)
// @Param			url		query		string	false	"URL of dataset"			Format(url)
// Example(http://mydataset.com/data1)
// @Success		200			{object}	utils.ResponseStruct	"OK"
// @Failure		400			{object}	utils.ErrorResponse	"Bad request"
// @Router			/datasets-metadata [get].
//
//nolint:funlen
func (h Handler) GetDataset(w http.ResponseWriter, r *http.Request) {
	if !jwt.CheckAuthTokenExists(r) {
		utils.EncodeError(w, "No auth token provided.", http.StatusUnauthorized)

		return
	}
	user, err := jwt.GetUserID(r)
	if err != nil {
		log.ErrorE(err, "Could not extract user ID.")
		utils.EncodeError(w, "User ID is missing or wrongly defined.", http.StatusBadRequest)

		return
	}
	acceptType := r.Header.Get("Accept")
	// TODO : check if multiple param?
	i := utils.DecodeParameter(r, "id")
	url := utils.DecodeParameter(r, "url")
	uri := utils.DecodeParameter(r, "uri")
	var graph *g.Graph

	switch {
	case i == "" && url == "" && uri == "":
		graph, err = h.Storage.GetAllDatasets()
		if err != nil {
			log.Error("error getting datasets", "error message", err)
			utils.EncodeError(w, "Could not retrieve dataset.", http.StatusBadRequest)

			return
		}
	case i != "":
		graph, err = h.Storage.GetDataset(i, "id")
		if err != nil {
			log.Error("error getting dataset id", "error message", err)
			utils.EncodeError(w, "Error when decoding dataset.", http.StatusBadRequest)

			return
		}
	case url != "":
		graph, err = h.Storage.GetDataset(url, "url")
		if err != nil {
			log.Error("error getting dataset url", "error message", err)
			utils.EncodeError(w, "Error when decoding dataset.", http.StatusBadRequest)

			return
		}
	default:
		graph, err = h.Storage.GetDataset(uri, "uri")
		if err != nil {
			log.Error("error getting dataset uri", "error message", err)
			utils.EncodeError(w, "Error when decoding dataset", http.StatusBadRequest)

			return
		}
	}
	// Check if the user can see this contract
	hasAccess := auth.HasDatasetAccess(user, graph)
	if !hasAccess {
		utils.EncodeError(w, "You cannot access this dataset.", http.StatusForbidden)

		return
	}
	// if accept header was specified, return turtle
	if acceptType == utils.Turtle {
		ttl, e := g.GraphToTurtle(graph)
		if e != nil {
			log.Error("error with graph", "error message", err)
			utils.EncodeError(w, "Could not get dataset.", http.StatusBadRequest)

			return
		}
		utils.EncodeTurtleStruct(w, ttl)

		return
	}

	// default return jsonld
	jsonld, err := g.GraphToJsonld(graph)
	if err != nil {
		log.Error("error with graph", "error message", err)
		utils.EncodeError(w, "Could not get dataset.", http.StatusBadRequest)

		return
	}

	utils.EncodeJSONLDStruct(w, jsonld)
}

// GetUser retrieve a user
// @Summary		Get a user
// @Description	Get a user. If no parameter is specified, get current user info
// @Tags			User
// @Security	Bearer
// @SecurityDefinitions api_key JWT
// @SecurityScheme api_key bearer
// @Produces		text/turtle
// @Produces		application/ld+json
// @Param			Authorization	header		string	true	"Bearer <JWT Token>"	Format(JWT)
// Example(Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9...)
// @Param			id		query		string	false	"User identifier - like keycloak"			Format(uuid)
// Example(123e4567-e89b-12d3-a456-426614174000)
// @Param			email		query		string	false	"email of user"			Format(url)
// Example(http://mydataset.com/data1)
// @Success		200			{object}	utils.ResponseStruct	"OK"
// @Failure		400			{object}	utils.ErrorResponse	"Bad request"
// @Router			/users [get].
//
//nolint:funlen
func (h Handler) GetUser(w http.ResponseWriter, r *http.Request) {
	if !jwt.CheckAuthTokenExists(r) {
		utils.EncodeError(w, "No auth token provided.", http.StatusUnauthorized)

		return
	}
	// TODO : check if multiple param?
	acceptType := r.Header.Get("Accept")
	user, err := jwt.GetUserID(r)
	if err != nil {
		log.ErrorE(err, "Could not extract user ID.")
		utils.EncodeError(w, "User ID is missing or wrongly defined.", http.StatusBadRequest)

		return
	}
	i := utils.DecodeParameter(r, "id")
	email := utils.DecodeParameter(r, "email")
	uri := utils.DecodeParameter(r, "uri")
	var graph *g.Graph
	switch {
	case i == "" && email == "" && uri == "":
		log.Info("current user")
		graph, err = h.Storage.GetUser("", "", user)
		if err != nil {
			log.Error("error getting user", "error message", err)
			utils.EncodeError(w, "Could not retrieve user.", http.StatusBadRequest)

			return
		}
	case i != "":
		log.Info("id")
		graph, err = h.Storage.GetUser(i, "id", user)
		if err != nil {
			log.Error("error getting user", "error message", err)
			utils.EncodeError(w, "Error when decoding user", http.StatusBadRequest)

			return
		}
	case email != "":
		log.Info("email")
		graph, err = h.Storage.GetUser(email, "email", user)
		if err != nil {
			log.Error("error getting user", err)
			utils.EncodeError(w, "Error when decoding user.", http.StatusBadRequest)

			return
		}
	default:
		log.Info("uri")
		graph, err = h.Storage.GetUser(uri, "uri", user)
		if err != nil {
			log.Error("error getting user", "error message", err)
			utils.EncodeError(w, "Error when decoding user.", http.StatusBadRequest)

			return
		}
	}
	// Check if the user can see this contract
	hasAccess := auth.HasUserAccess(user, graph)
	if !hasAccess {
		utils.EncodeError(w, "You cannot see this user.", http.StatusForbidden)

		return
	}
	// if accept header was specified, return turtle
	if acceptType == utils.Turtle {
		ttl, e := g.GraphToTurtle(graph)
		if e != nil {
			log.Error("error with graph", "error message", err)
			utils.EncodeError(w, "Could not get user.", http.StatusBadRequest)

			return
		}
		utils.EncodeTurtleStruct(w, ttl)

		return
	}
	// default return jsonld.
	jsonld, err := g.GraphToJsonld(graph)
	if err != nil {
		log.Error("error with graph", "error message", "error message", err)
		utils.EncodeError(w, "Could not get user.", http.StatusBadRequest)

		return
	}
	utils.EncodeJSONLDStruct(w, jsonld)
}

// PostDataset post the metadata of a dataset in the database
// @Summary		Upload a dataset
// @Description	Upload a dataset
// @Tags			Dataset
// @Security	Bearer
// @SecurityDefinitions api_key JWT
// @SecurityScheme api_key bearer
// @Param			Authorization	header		string	true	"Bearer <JWT Token>"	Format(JWT)
// Example(Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9...)
// @Consume		appliction/ld+json
// @Consume		text/turtle
// @Param			requestBody	body		string	true	"dataset (jsonld)"
// @Success		200			{object}	utils.UUIDResponseStruct	"UUID"
// @Failure		400			{object}	utils.ErrorResponse	"Bad request"
// @Failure		415			{object}	utils.ErrorResponse	"Unsupported Media Type"
// @Router			/datasets-metadata [post].
//
//nolint:funlen
func (h Handler) PostDataset(w http.ResponseWriter, r *http.Request) {
	if !jwt.CheckAuthTokenExists(r) {
		utils.EncodeError(w, "No auth token provided.", http.StatusUnauthorized)

		return
	}
	user, err := jwt.GetUserID(r)
	if err != nil {
		log.ErrorE(err, "Could not extract user ID.")
		utils.EncodeError(w, "User ID is missing or wrongly defined.", http.StatusBadRequest)

		return
	}
	newID := uuid.New()
	data := id.NewGeneralID(newID)
	contentType := r.Header.Get("Content-Type")
	if ok := utils.CheckContentType(contentType); !ok {
		log.Debug("Received content type.", "content-type", contentType)
		utils.EncodeError(
			w,
			"Unsupported media type. Only 'application/ld+json' and 'text/turtle' are accepted.",
			http.StatusUnsupportedMediaType,
		)

		return
	}
	// Get the contract through the body
	document := utils.DecodeBody(r)
	graph, _ := g.SerializeToGraph([]byte(document), contentType)
	log.Info("document", "d", document)
	log.Info("graph", "g", graph)
	var val bool
	defaultG := graph.Graphs[g.DefaultName]
	for _, data := range defaultG {
		if data.Predicate.GetValue() == pred.Dcat+"downloadURL" {
			val, err = h.Storage.CheckIfExists(data.Predicate.GetValue(), data.Object.GetValue())
			// TODO handle this error. What should happen here?

			break
		}
	}
	if err != nil {
		log.ErrorE(err, "Error while checking data.")
		utils.EncodeError(w, "Error while uploading data", http.StatusBadRequest)

		return
	}
	if val {
		utils.EncodeError(w, "Dataset already exists.", http.StatusBadRequest)

		return
	}
	// TODO check if follows ontology
	// TODO doc @id must be blank node (validate?)
	// set id to custodian uri + unique uuid
	// TODO replace blank nodes with custodian id
	// check if user is a dataController
	dataController := g.GetDataControllerURI(graph)
	log.Info("Data controller list.", "dc", dataController)
	if !slices.Contains(dataController, user.URI()) {
		utils.EncodeError(w, "Dataset does not belong to user.", http.StatusBadRequest)

		return
	}
	graph, err = g.ChangeInternalID(graph, data.URI())
	if err != nil {
		log.ErrorE(err, "Cannot change internal ID.")
		utils.EncodeError(w, "Internal error.", http.StatusInternalServerError)

		return
	}
	err = h.Storage.PostGraph(graph, data)
	if err != nil {
		log.ErrorE(err, "Graph POST failed.")
		utils.EncodeError(w, "Internal error.", http.StatusInternalServerError)
	}
	c, err := uuid.Parse("")
	if err != nil {
		log.ErrorE(err, "Failed to parse uuid .")
		utils.EncodeError(w, "An error occurred when parsing", http.StatusInternalServerError)

		return
	}
	contract := id.NewContractID(c)
	err = mq.SendEvent(h.RabbitMQ, user, contract,
		[]string{user.URI()}, data, "UPLOAD", true,
		"The resource has been uploaded.")
	if err != nil {
		log.ErrorE(err, "Failed sending event.")
		utils.EncodeError(w, "An error occurred when sending the event", http.StatusInternalServerError)

		return
	}
	dataID := graph.Graphs[g.DefaultName][0].Subject.GetValue()
	utils.EncodeResponse(w, dataID)
}

// PostUser upload a user in the database
// @Summary		Upload a user
// @Description	Upload a user
// @Tags			User
// @Security	Bearer
// @SecurityDefinitions api_key JWT
// @SecurityScheme api_key bearer
// @Param			Authorization	header		string	true	"Bearer <JWT Token>"	Format(JWT)
// Example(Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9...)
// @Consume		appliction/ld+json
// @Consume		text/turtle
// @Param			requestBody	body		string	true	"person (json-ld)"
// @Success		200			{object}	utils.UUIDResponseStruct	"UUID"
// @Failure		400			{object}	utils.ErrorResponse	"Bad request"
// @Failure		415			{object}	utils.ErrorResponse	"Unsupported Media Type"
// @Router			/users [post].
func (h Handler) PostUser(w http.ResponseWriter, r *http.Request) {
	if !jwt.CheckAuthTokenExists(r) {
		utils.EncodeError(w, "No auth token provided.", http.StatusUnauthorized)

		return
	}
	user, err := jwt.GetUserID(r)
	if err != nil {
		log.ErrorE(err, "Could not extract user ID.")
		utils.EncodeError(w, "User ID is missing or wrongly defined.", http.StatusBadRequest)

		return
	}
	contentType := r.Header.Get("Content-Type")
	if ok := utils.CheckContentType(contentType); !ok {
		log.Debug("Received content type.", "content-type", contentType)
		utils.EncodeError(
			w,
			"Unsupported media type. Only 'application/ld+json' and 'text/turtle' are accepted.",
			http.StatusUnsupportedMediaType,
		)

		return
	}
	// Get the contract through the body.
	document := utils.DecodeBody(r)
	graph, _ := g.SerializeToGraph([]byte(document), contentType)
	// TODO : validate shape.
	// return id.
	// Check if value are already present in the KB.
	// TODO : check if all triples are unique?
	var val bool
	defaultG := graph.Graphs[g.DefaultName]
	for _, data := range defaultG {
		if data.Predicate.GetValue() == pred.SwissCustodian+"hasIdentifier" {
			val, err = h.Storage.CheckIfExists(data.Predicate.GetValue(), data.Object.GetValue())
			// TODO handle this error.

			break
		}
	}
	if err != nil {
		log.Error("error while checking data", "error message", err)
		utils.EncodeError(w, "Error while uploading data.", http.StatusBadRequest)

		return
	}
	if val {
		utils.EncodeError(w, "Data already exists.", http.StatusBadRequest)

		return
	}
	graph, err = g.ChangeInternalID(graph, user.URI())
	if err != nil {
		log.ErrorE(err, "Cannot change internal ID.")
		utils.EncodeError(w, "Internal error.", http.StatusInternalServerError)

		return
	}
	// TODO check if follows ontology
	// TODO doc @id must be blank node (validate?)
	// set id to custodian uri + unique uuid
	// TODO replace blank nodes with custodian id
	err = h.Storage.PostGraph(graph, user)
	if err != nil {
		log.ErrorE(err, "Graph POST failed.")
		utils.EncodeError(w, "Internal error.", http.StatusInternalServerError)
	}
	contract := id.NewContractID(uuid.Nil)
	err = mq.SendEvent(h.RabbitMQ, user, contract,
		[]string{user.URI()}, user, "UPLOAD", true,
		"The resource has been uploaded.")
	if err != nil {
		log.ErrorE(err, "Failed sending event.")
		utils.EncodeError(w, "An error occurred when sending the event", http.StatusInternalServerError)

		return
	}
	dataID := graph.Graphs[g.DefaultName][0].Subject.GetValue()
	utils.EncodeResponse(w, dataID)
}
