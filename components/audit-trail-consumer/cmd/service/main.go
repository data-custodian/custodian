package main

import (
	"context"
	"custodian/components/audit-trail-consumer/internal/config"
	"custodian/components/audit-trail-consumer/internal/rabbitmq"
	cmc "custodian/components/lib-common/pkg/config"
	mongodb "custodian/components/lib-common/pkg/db/mongo"
	log "custodian/components/lib-common/pkg/log"
)

type Callback func() error

func cleanUp(callback Callback, comp string) {
	log.ErrorE(callback(), "Clean up failed.", "component", comp)
}

func main() {
	log.Setup()
	ctx := context.Background()

	conf, err := cmc.LoadConfigs[config.Config]()
	log.PanicE(err, "Could not load configs.")

	connection, err := mongodb.NewMongoDBConnection(ctx, &conf.MongoDB)
	log.PanicE(err, "Failed to connect to MongoDB")
	defer cleanUp(func() error { return connection.Disconnect(ctx) }, "MongoDB")

	queue, err := rabbitmq.NewRabbitMQConnection(&conf.RabbitMQ)
	log.PanicE(err, "Failed to connect to RabbitMQ")
	defer cleanUp(func() error { return queue.Close() }, "RabbitMQ")

	rabbitmq.HandleEvents(queue, connection, &conf.RabbitMQ)
}
