# Audit Trail Consumer

The events receiver picks up events from a rabbitmq message queue and add them
to a mongo db for permanent storage

## Installation

```
go get
```

### Configuration

The config file can be found in `config/config.yml`. For the new configuration
to take effect, you must rerun the service.

### Build and Run

To build and run the code, open a terminal in the
`custodian/audit-trail-consumer` folder and run the following command:

```
go build -o events-receiver cmd/main.go
./events-receiver
```

run a rabbit message queue

```
docker pull rabbitmq
docker run -d -p 15672:15672 -p 5672:5672 --hostname my-rabbit --name some-rabbit rabbitmq:3-management
```

Rabbit MQ will be served at http://localhost:15672/ Log in with `guest` `guest`

## Docker

```
docker build --tag events-receiver .
docker run -p 8081:8081 events-receiver
```
