package db

import (
	"context"
	"custodian/components/audit-trail-consumer/pkg/model"
	db "custodian/components/lib-common/pkg/db"
	log "custodian/components/lib-common/pkg/log"
)

func StoreEvent(ctx context.Context, d db.DBConnection, event model.Event) (err error) {
	log.Info("Adding event to collection", "event", event)

	return d.Store(ctx, event)
}
