package rabbitmq

import (
	"context"
	"custodian/components/audit-trail-consumer/internal/config"
	eventdb "custodian/components/audit-trail-consumer/internal/db"
	"custodian/components/audit-trail-consumer/pkg/model"
	db "custodian/components/lib-common/pkg/db"
	"custodian/components/lib-common/pkg/errors"
	"custodian/components/lib-common/pkg/log"
	"encoding/json"
	"net/url"

	amqp "github.com/rabbitmq/amqp091-go"
)

type RabbitMQ struct {
	conn    *amqp.Connection
	channel *amqp.Channel
}

type MQEvents <-chan amqp.Delivery

// buildURL creates a URL with credentials to connect to the MQ.
func buildURL(conf *config.RabbitMQ) (u *url.URL, err error) {
	u, err = url.Parse(conf.URL)
	if err != nil {
		return
	}
	userInfo := url.UserPassword(conf.Credentials.Username, conf.Credentials.Token)
	u.User = userInfo

	return
}

// NewRabbitMQConnection creates the RabbitMQ connection.
func NewRabbitMQConnection(conf *config.RabbitMQ) (*RabbitMQ, error) {
	log.Info("Connecting to RabbitMQ server.")

	url, err := buildURL(conf)
	if err != nil {
		return nil, err
	}

	conn, err := amqp.Dial(url.String())
	if err != nil {
		log.ErrorE(err, "Failed to connect to RabbitMQ server.", "url", conf.URL)

		return nil, err
	}

	log.Info("Open a RabbitMQ channel.")
	ch, err := conn.Channel()
	if err != nil {
		log.ErrorE(err, "Failed to create channel.")

		return nil, errors.Combine(err, conn.Close())
	}

	return &RabbitMQ{
		conn:    conn,
		channel: ch,
	}, nil
}

// Close closes the RabbitMQ connection and associated channel.
func (r *RabbitMQ) Close() (err error) {
	if r.conn != nil {
		e := r.conn.Close()
		err = errors.Combine(err, e)
	}

	if r.channel != nil {
		e := r.channel.Close()
		err = errors.Combine(err, e)
	}

	return
}

// Consume returns a channel which can be used to
// receive the events in the queue `queueName`.
func (r *RabbitMQ) Consume(queueName string) (MQEvents, error) {
	q, err := r.channel.QueueDeclare(
		queueName, // name
		false,     // durable
		false,     // delete when unused
		false,     // exclusive
		false,     // no-wait
		nil,       // arguments
	)

	if err != nil {
		log.ErrorE(err, "Failed to declare a queue.")

		return nil, err
	}

	ch, err := r.channel.Consume(
		q.Name, // queue
		"",     // consumer
		true,   // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)

	if err != nil {
		log.ErrorE(err, "Failed consume from queue.", "queue", queueName)

		return nil, err
	}

	return ch, nil
}

// TODO: We need to review this function again,
// to ensure consumed events on `StoreEvent` crashes are really persisted in RabbitMQ.
// Currently they are probably not because the ACK feature is wrongly used here.
func HandleEvents(queue *RabbitMQ, connection db.DBConnection, conf *config.RabbitMQ) {
	events, err := queue.Consume(conf.QueueName)
	log.PanicE(err, "Failed to consume events.")

	ctx := context.Background() // TODO: this should come form higher.
	go func() {
		for d := range events {
			log.Info("Received a message:", "body", d.Body)
			var event model.Event
			e := json.Unmarshal(d.Body, &event)
			if e != nil {
				log.Error("Failed to unmarshal message", "error", e)
			}

			e = eventdb.StoreEvent(ctx, connection, event)
			log.PanicE(e, "Could not store event to database.", "events", events)

			log.Info("Created event.", "event", event)
		}
	}()

	log.Info("[*] Waiting for messages. To exit press CTRL+C")
	forever := make(chan struct{})
	<-forever
}
