package rabbitmq

import (
	"custodian/components/audit-trail-consumer/internal/config"
	"testing"
)

func TestBuildURL(t *testing.T) {
	credentials := config.RabbitMQCredentials{Username: "testUser", Token: "testPassword"}
	conf := config.RabbitMQ{URL: "http://localhost:8080", Credentials: credentials}
	expected := "http://testUser:testPassword@localhost:8080"

	url, err := buildURL(&conf)
	if err != nil {
		t.Errorf("Got the following error while building the URL: %v", err)
	}

	if url.String() != expected {
		t.Errorf("Expected %v, got %v", expected, url.String())
	}
}
