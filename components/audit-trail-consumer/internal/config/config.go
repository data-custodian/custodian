package config

import (
	mongodb "custodian/components/lib-common/pkg/db/mongo"
)

type Config struct {
	//nolint:tagliatelle,nolintlint
	MongoDB mongodb.MongoDBConfig `yaml:"mongoDB"`
	//nolint:tagliatelle,nolintlint
	RabbitMQ RabbitMQ `yaml:"rabbitMQ"`
}

type RabbitMQ struct {
	URL       string `yaml:"connection"`
	QueueName string `yaml:"queueName"`

	Credentials RabbitMQCredentials `yaml:"credentials"`
}

type RabbitMQCredentials struct {
	Username string `yaml:"username"`
	Token    string `yaml:"token"`
}
