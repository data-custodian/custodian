package kms

import (
	g "custodian/components/lib-common/pkg/graph"
	"custodian/components/lib-common/pkg/id"
)

type Storage interface {
	GetUserKeys(user id.GeneralID) ([]id.GeneralID, error)
	GetKey(key id.GeneralID) (*g.Graph, error)
	UploadKey(keyGraph *g.Graph, key id.GeneralID) error
	RevokeKey(key id.GeneralID) error
}
