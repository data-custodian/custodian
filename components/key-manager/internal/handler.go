package kms

import "custodian/components/lib-common/pkg/rabbitmq"

type Handler struct {
	Storage    Storage
	RabbitMQ   *rabbitmq.RabbitMQ
	CMSPrivKey string
	CMSPubKey  string
}

func NewHandler(storage Storage, rabbitMQ *rabbitmq.RabbitMQ, cmsPrivKey, cmsPubKey string) *Handler {
	return &Handler{Storage: storage, RabbitMQ: rabbitMQ, CMSPrivKey: cmsPrivKey, CMSPubKey: cmsPubKey}
}
