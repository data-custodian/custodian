package kms

import (
	g "custodian/components/lib-common/pkg/graph"
	"custodian/components/lib-common/pkg/id"
	pred "custodian/components/lib-common/pkg/predicates"
	"time"

	"github.com/google/uuid"
	"github.com/piprate/json-gold/ld"
)

// createKeyObject creates a new key object.
func createKeyObject(keyPem string, user id.GeneralID) (id.GeneralID, *g.Graph) {
	keyID := uuid.New()
	key := id.NewGeneralID(keyID)
	dataset := ld.NewRDFDataset()
	var quads []*ld.Quad
	subject := ld.NewLiteral(key.URI(), "", "")
	quad := ld.NewQuad(
		subject,
		ld.NewIRI(g.RdfType),
		ld.NewIRI(KeyType),
		"",
	)
	quads = append(quads, quad)
	quad = ld.NewQuad(
		subject,
		ld.NewIRI(Controller),
		ld.NewIRI(user.URI()),
		"",
	)
	quads = append(quads, quad)
	quad = ld.NewQuad(
		subject,
		ld.NewIRI(PublicKeyPem),
		ld.NewLiteral(keyPem, g.DataTypeString, ""),
		"",
	)
	quads = append(quads, quad)
	quad = ld.NewQuad(
		subject,
		ld.NewIRI(Creation),
		ld.NewLiteral(time.Now().String(), g.DataTypeString, ""),
		"",
	)
	quads = append(quads, quad)
	// URI of the KMS where the key is stored. Useful if multiple Kms are working together,
	quad = ld.NewQuad(
		subject,
		ld.NewIRI(pred.SwissCustodian+StorageName),
		ld.NewIRI(KMSPrefix),
		"",
	)
	quads = append(quads, quad)
	(dataset).Graphs[g.DefaultName] = quads

	return key, dataset
}

// isRevoked checks if a key has a value revoked.
func isRevoked(key *g.Graph) bool {
	for _, item := range key.Graphs[g.DefaultName] {
		if item.Predicate.GetValue() == Revoked {
			return true
		}
	}

	return false
}
