package kms

import (
	"custodian/components/lib-common/pkg/errors"
	g "custodian/components/lib-common/pkg/graph"
	"custodian/components/lib-common/pkg/id"
	j "custodian/components/lib-common/pkg/jena"
	"custodian/components/lib-common/pkg/log"
	pred "custodian/components/lib-common/pkg/predicates"
	tmpl "custodian/components/lib-common/pkg/template"
	"time"

	"github.com/piprate/json-gold/ld"
)

type JenaStorage struct {
	Jena j.Jena
}

// GetUserKeys get all the public keys URI from the logged user.
func (s JenaStorage) GetUserKeys(user id.GeneralID) ([]id.GeneralID, error) {
	data := map[string]interface{}{
		"SecVocab": pred.SecVocab,
		"User":     user.URI(),
	}
	query, err := tmpl.ReplaceInTemplate("sparql/query-get-keys-by-user.sparql.tmpl", data)
	if err != nil {
		return nil, err
	}
	resp, err := s.Jena.FetchFilteredTriples(j.KnowledgeBase, string(query))
	if err != nil {
		return nil, err
	}
	var array []id.GeneralID
	for _, r := range resp {
		k, _ := id.NewGeneralIDFromURI(r)
		array = append(array, k)
	}

	return array, nil
}

// GetKey retrieve a public key based on an id.
func (s JenaStorage) GetKey(key id.GeneralID) (*g.Graph, error) {
	data := map[string]interface{}{
		"URI": key.URI(),
	}
	query, err := tmpl.ReplaceInTemplate("sparql/query-get-key-by-id.sparql.tmpl", data)
	if err != nil {
		return nil, err
	}
	graph, err := s.Jena.FetchGraph(j.KnowledgeBase, string(query))
	if err != nil {
		return nil, err
	}

	return graph, nil
}

// UploadKey uploads a public key.
func (s JenaStorage) UploadKey(keyGraph *g.Graph, key id.GeneralID) error {
	err := s.Jena.PostGraph(keyGraph, key, j.KnowledgeBase)
	if err != nil {
		return err
	}

	return nil
}

// RevokeKey revokes a key by adding a new triple.
func (s JenaStorage) RevokeKey(key id.GeneralID) error {
	// Check if key exists.
	graph, err := s.GetKey(key)
	if err != nil {
		return err
	}
	if isRevoked(graph) {
		return nil
		// return errors.New("key already revoked")
	}
	// Create revoke triple,
	subject := ld.NewIRI(key.URI())
	predicate := ld.NewIRI(Revoked)
	object := ld.NewLiteral(time.Now().String(), "http://www.w3.org/2001/XMLSchema#string", "")
	quad := ld.NewQuad(subject, predicate, object, "")
	triples := "<" + quad.Subject.GetValue() + "> <" + quad.Predicate.GetValue() + "> \"" + quad.Object.GetValue() + "\" ."
	log.Info("triples", "t", triples)
	data := map[string]interface{}{
		"RevokeTriple": triples,
	}
	query, err := tmpl.ReplaceInTemplate("sparql/query-revoke-key.sparql.tmpl", data)
	log.Info("triples", "t", string(query))
	if err != nil {
		return err
	}
	err = s.Jena.UpdateGraph(j.Contracts, string(query))
	if err != nil {
		return errors.AddContext(err, "could not post the signature")
	}
	err = s.Jena.UpdateGraph(j.KnowledgeBase, string(query))

	return err
}
