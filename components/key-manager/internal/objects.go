package kms

import pred "custodian/components/lib-common/pkg/predicates"

const (
	KMSPrefix    string = "https://swisscustodian.ch/kms/inst#"
	Revoked             = pred.SecVocab + "revoked"
	KeyType             = pred.SecVocab + "EcdsaSecp256k1VerificationKey2019"
	Controller          = pred.SecVocab + "controller"
	PublicKeyPem        = pred.SecVocab + "publicKeyPem"
	Creation            = pred.SecVocab + "created"
	StorageName  string = "kms"
)
