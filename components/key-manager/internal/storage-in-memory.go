package kms

import (
	"custodian/components/lib-common/pkg/errors"
	g "custodian/components/lib-common/pkg/graph"
	"custodian/components/lib-common/pkg/id"
	"custodian/components/lib-common/pkg/log"
	"time"

	"github.com/piprate/json-gold/ld"
)

type InMemoryStorage struct {
	DB map[id.GeneralID]*g.Graph
}

func (m InMemoryStorage) Create() {
	m.DB = make(map[id.GeneralID]*g.Graph)
	log.Info("Creating in-memory storage", "db", m.DB)
}

// GetUserKeys get all the public keys URI from the logged user.
func (m InMemoryStorage) GetUserKeys(user id.GeneralID) ([]id.GeneralID, error) {
	var keys []id.GeneralID
	for _, val := range m.DB {
		for _, item := range val.Graphs[g.DefaultName] {
			if item.Predicate.GetValue() == Controller {
				if item.Object.GetValue() == user.URI() {
					k, _ := id.NewGeneralIDFromURI(item.Subject.GetValue())
					keys = append(keys, k)

					break
				}
			}
		}
	}

	return keys, nil
}

// GetKey retrieve a public key based on an id.
func (m InMemoryStorage) GetKey(key id.GeneralID) (*g.Graph, error) {
	if value, ok := m.DB[key]; ok {
		return value, nil
	}

	return nil, errors.New("key not found")
}

// UploadKey uploads a public key.
func (m InMemoryStorage) UploadKey(keyGraph *g.Graph, key id.GeneralID) error {
	if _, ok := m.DB[key]; ok {
		return errors.New("key already exists")
	}
	m.DB[key] = keyGraph

	return nil
}

// RevokeKey revokes a key by adding a new triple.
func (m InMemoryStorage) RevokeKey(key id.GeneralID) error {
	value, ok := m.DB[key]
	if !ok {
		return errors.New("key not found")
	}
	revoke := isRevoked(value)
	if revoke {
		return errors.New("key already revoked")
	}
	quads := (value).Graphs[g.DefaultName]
	subject := ld.NewLiteral(key.URI(), "", "")
	quad := ld.NewQuad(
		subject,
		ld.NewLiteral(Revoked, "", ""),
		ld.NewLiteral(time.Now().String(), "http://www.w3.org/2001/XMLSchema#string", ""),
		"",
	)
	quads = append(quads, quad)
	(value).Graphs[g.DefaultName] = quads

	return nil
}
