package kms

import (
	"github.com/gorilla/mux"
)

func NewRouter(h *Handler, componentURLPath string) *mux.Router {
	r := mux.NewRouter().StrictSlash(true)
	router := r.PathPrefix(componentURLPath).Subrouter()
	router.
		Methods("POST").
		Path("/keys").
		Name("PostPublicKey").
		HandlerFunc(h.PostPublicKey)
	router.
		Methods("DELETE").
		Path("/keys/{id}").
		Name("RevokePublicKey").
		HandlerFunc(h.RevokePublicKey)
	router.
		Methods("GET").
		Path("/keys").
		Name("GetUserPublicKeys").
		HandlerFunc(h.GetUserPublicKeys)
	router.
		Methods("GET").
		Path("/keys/{id}").
		Name("GetPublicKey").
		HandlerFunc(h.GetPublicKey)

	return router
}
