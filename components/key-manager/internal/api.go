package kms

import (
	"custodian/components/lib-common/pkg/crypto"
	g "custodian/components/lib-common/pkg/graph"
	utils "custodian/components/lib-common/pkg/http"
	"custodian/components/lib-common/pkg/id"
	"custodian/components/lib-common/pkg/jwt"
	"custodian/components/lib-common/pkg/log"
	"net/http"

	"github.com/google/uuid"
)

const (
	MaxSizeContent = 10 * 1024
)

// GetPublicKey returns the public key with the given userid and kid.
// @Summary		Get a public key
// @Description	Get a public key
// @Resource	keys
// @Produce		application/ld+json
// @Param		id	path		string	true	"Key UUID"	"123e4567-e89b-12d3-a456-426614174000"
// @Success		200			object	utils.JSONLDResponseStruct	"key in JSON-LD"
// @Failure		400			object	utils.ErrorResponse	"Bad request"
// @Failure		401			object	utils.ErrorResponse	"Unauthorized"
// @Failure		422			object	utils.ErrorResponse	"Unprocessable Entity"
// @Failure		500			object	utils.ErrorResponse	"Internal Server Error"
// @Router			/keys/{id} [get].
func (h Handler) GetPublicKey(w http.ResponseWriter, r *http.Request) {
	if !jwt.CheckAuthTokenExists(r) {
		utils.EncodeError(w, "No auth token provided.", http.StatusUnauthorized)

		return
	}
	// Getting ID from URL
	keyUUID, err := utils.GetPathFromURL(r, "id")
	if err != nil {
		log.Error("error while getting key", "error message", err, "keyUUID", keyUUID)
		utils.EncodeError(w, "Key ID is missing or wrongly defined.", http.StatusBadRequest)

		return
	}
	k, err := uuid.Parse(keyUUID)
	if err != nil {
		log.Error("error while parsing key", "error message", err)
		utils.EncodeError(w, "Key ID is wrongly defined.", http.StatusUnprocessableEntity)

		return
	}
	key := id.NewGeneralID(k)
	log.Info("Get key.", "key id", key.URI())
	graph, err := h.Storage.GetKey(key)
	if err != nil {
		log.Error("error with key", "error message", err)
		utils.EncodeError(w, "An error occurred while getting the key.", http.StatusInternalServerError)

		return
	}
	jsonld, err := g.GraphToJsonld(graph)
	if err != nil {
		log.Error("error with graph", "error message", err)
		utils.EncodeError(w, "Could not get public key.", http.StatusInternalServerError)

		return
	}
	utils.EncodeJSONLDStruct(w, jsonld)
}

// GetUserPublicKeys get all the keys for the current user
// @Summary		Retrieve a list of UUIDs representing all keys belonging to the user currently logged in.
// @Description	Retrieve a list of UUIDs representing all keys belonging to the user currently logged in.
// @Resource	keys
// @Produce		json
// @Param		uri		query		string	true	"Key uri"			Format(uri)	Example(http://example.com/1234)
// @Success		200			object	utils.UUIDResponseStruct	"Array of user's public keys"
// @Failure		400			object	utils.ErrorResponse	"Bad request"
// @Failure		401			object	utils.ErrorResponse	"Unauthorized"
// @Failure		403			object	utils.ErrorResponse	"Forbidden"
// @Failure		500			object	utils.ErrorResponse	"Internal Server Error"
// @Router			/keys [get].
func (h Handler) GetUserPublicKeys(w http.ResponseWriter, r *http.Request) {
	if !jwt.CheckAuthTokenExists(r) {
		utils.EncodeError(w, "No auth token provided.", http.StatusUnauthorized)

		return
	}
	userURI, err := jwt.GetUserID(r)
	if err != nil {
		log.ErrorE(err, "Could not extract user ID.")
		utils.EncodeError(w, "Could not extract user ID.", http.StatusForbidden)

		return
	}
	keys, err := h.Storage.GetUserKeys(userURI)
	if err != nil {
		log.Error("could not get keys", "error message", err)
		utils.EncodeError(w, "An error occurred while getting the keys", http.StatusInternalServerError)

		return
	}
	var array []string
	for _, k := range keys {
		array = append(array, k.URI())
	}
	utils.EncodeUUIDResponse(w, array)
}

// RevokePublicKey revoke a public key
// @Summary		Revoke a key
// @Description	Revoke a key
// @Resource	keys
// @Param		id	path		string	true	"Key UUID"	"123e4567-e89b-12d3-a456-426614174000"
// @Success		200			object	utils.ResponseStruct	"OK"
// @Failure		400			object	utils.ErrorResponse	"Bad request"
// @Failure		401			object	utils.ErrorResponse	"Unauthorized"
// @Failure		403			object	utils.ErrorResponse	"Forbidden"
// @Failure		422			object	utils.ErrorResponse	"Unprocessable Entity"
// @Failure		500			object	utils.ErrorResponse	"Internal Server Error"
// @Router		/keys/{id} [delete].
func (h Handler) RevokePublicKey(w http.ResponseWriter, r *http.Request) {
	if !jwt.CheckAuthTokenExists(r) {
		utils.EncodeError(w, "No auth token provided.", http.StatusUnauthorized)

		return
	}
	keyUUID, err := utils.GetPathFromURL(r, "id")
	if err != nil {
		log.Error("error while getting key", "error message", err, "keyUUID", keyUUID)
		utils.EncodeError(w, "Key ID is missing or wrongly defined.", http.StatusBadRequest)

		return
	}
	k, err := uuid.Parse(keyUUID)
	if err != nil {
		log.Error("error while getting key", "error message", err, "keyUUID", k)
		utils.EncodeError(w, "Key ID is wrongly defined", http.StatusUnprocessableEntity)

		return
	}
	key := id.NewGeneralID(k)
	err = h.Storage.RevokeKey(key)
	if err != nil {
		log.Error("could not revoke key", "error message", err)
		utils.EncodeError(w, "An error occurred during the revocation process", http.StatusInternalServerError)

		return
	}
	utils.EncodeResponse(w, "Key revoked")
}

// PostPublicKey uploads a key
// @Summary		Upload a key
// @Description	Upload a key.
// @Resource	keys
// @Accept	    application/x-pem-file
// @Param		requestBody	body		utils.JSONLDResponseStruct	true "public ECC key (PEM format)"
// @Success		200			object	utils.ResponseStruct	"OK"
// @Failure		400			object	utils.ErrorResponse	"Bad request"
// @Failure		401			object	utils.ErrorResponse	"Unauthorized"
// @Failure		403			object	utils.ErrorResponse	"Forbidden"
// @Failure		415			object	utils.ErrorResponse	"Unsupported Media Type"
// @Failure		422			object	utils.ErrorResponse	"Unprocessable Entity"
// @Failure		500			object	utils.ErrorResponse	"Internal Server Error"
// @Router		/keys [post].
func (h Handler) PostPublicKey(w http.ResponseWriter, r *http.Request) {
	// Authentication.
	if !jwt.CheckAuthTokenExists(r) {
		utils.EncodeError(w, "No auth token provided.", http.StatusUnauthorized)

		return
	}
	// Content-Type.
	contentType := r.Header.Get("Content-Type")
	if ok := contentType == utils.Pem; !ok {
		log.Info("content type is not right", "content-type", contentType)
		utils.EncodeError(w, "Unsupported media type. Only '"+utils.Pem+"' is accepted.",
			http.StatusUnsupportedMediaType)

		return
	}
	// Content-Size check and retrieve key.
	keyPem, err := utils.DecodeBodyCheck(r, MaxSizeContent)
	if err != nil {
		log.Error("error while decoding body", "error message", err)
		utils.EncodeError(w, "An error occurred while decoding the request.", http.StatusUnprocessableEntity)

		return
	}
	_, err = crypto.VerifyPEM(keyPem)
	if err != nil {
		log.Error("PEM is malformed.", "error message", err)
		utils.EncodeError(w, "An error occurred with the key format", http.StatusUnprocessableEntity)

		return
	}
	// Get user.
	userURI, err := jwt.GetUserID(r)
	if err != nil {
		log.ErrorE(err, "Could not extract user ID.")
		utils.EncodeError(w, "Could not extract user ID.", http.StatusForbidden)

		return
	}
	// Create a new key ID.
	key, keyGraph := createKeyObject(keyPem, userURI)
	log.Info("Upload public key.", "id", key.URI())
	err = h.Storage.UploadKey(keyGraph, key)
	if err != nil {
		log.Error("could not upload the key.", "error message", err)
		utils.EncodeError(w, "An error occurred when uploading the key", http.StatusInternalServerError)

		return
	}
	log.Info("Key successfully uploaded.", "graph", keyGraph)
	utils.EncodeResponse(w, key.URI())
}
