# Key manager System (kms)

This folder contains the services handling consents.

- key-manager: REST API to upload, revoke or display a key

# How to Run the Service With Docker Compose

Before running docker-compose, you must configure the environment variables in
the `docker-compose.yml` file.

Specify the following environment variables for the contract-signature-endpoint:

- `SERVER_HOSTNAME`: the hostname to be used by the contract-endpoint
- `SERVER_PORT`: the port the contract-endpoint listens to

Create a folder with the secret files and reference them in the
`docker-compose.yml` file.

Then, you can run the following command in a terminal, from the repository
containing the `docker-compose.yml` file:

```
docker compose up
```

## API

#### Upload a key

```
curl -X POST http://0.0.0.0:8004/keys -H "Content-Type:application/ld+json" -T '/path/to/public/key/key.jsonld'
```

#### Revoke a key

```
curl -X DELETE http://0.0.0.0:8004/kms/keys/{uuid}
```

#### Get a key

```
curl -X GET http://0.0.0.0:8004/kms/keys/{uuid}
```

#### Get all user's keys

```
curl -X GET http://0.0.0.0:8004/kms/keys
```

## Security known issues

- Currently, and for test purpose, the Keycloak token is used but not verified.
- Communication with the Jena DBs are through HTTP.

# Signature Webservice

This REST API can be used to upload or revoke a key.

The webservice receives requests and passes them in the messages queue.

## Installation

### The Dependencies

The code has been built with `Go 1.19.2`, but should work with any version that
supports Go modules.

To install the dependencies, open a terminal in the `code` folder and type

```
go get
```

The signature endpoint uses either Pulsar or Kafka as message queue. Make sure
that the message queue you configured the code for (default is Pulsar) is
currently running before running the code.

### Configuration

Create a `config.yml` file with the following content using the template
`config.docker.yml` or `config.local.yml.dist`

```sh
cp config.local.yml.dist config.yml
```

### Build and Run

To build and run the code, open a terminal in the `custodian/contract-manager`
folder and run the following command:

```
go build -o key-manager cmd/main.go
./key-manager
```

### Secrets

Create a `secrets.yaml` file with the following content using the template
`secrets.dist.yml`

```sh
cp secrets.dist.yml secrets.yml
```

Fill in the secrets according to your OIDC configuration.

## Usage

The list of endpoints and their description can be found in `api/swagger.yml`

## In-memory storage

In order to test the logic of the KMS alone - without all other components and
dependency to Jena -, it is possible to make it run as a standalone. There is an
in-memory storage implemented.

To make it run, you need to modify the `cmd/main.go` to use the proper storage

```
storage := kms.JenaStorage{Jena: jena.New(conf.Jena.Path)}
//storage := kms.InMemoryStorage{DB: map[id.GeneralID]g.Graph{}}
//storage.Create()
```

Comment the first line and uncomment the two others. Then you can make it run as
explained above.

The in-memory storage implements the same methods as the jena storage.

Here is the token of Bob (Data controller) - for tests purposes

`eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJXRGdUdXc4dnFscEdLUkU0aWFNSEZfOGRwM0tkMXlrcU1JT00wZVMzOU44In0.eyJleHAiOjE3MTA0MjYxNTAsImlhdCI6MTcxMDQyNTg1MCwiYXV0aF90aW1lIjoxNzEwNDI1ODUwLCJqdGkiOiIxN2QyYTNkZS0xZmEwLTRhNDktOWUxZi05NDEzNTVmNzVmYmQiLCJpc3MiOiJodHRwczovL2F1dGguZGV2LnN3aXNzY3VzdG9kaWFuLmNoL2F1dGgvcmVhbG1zL2N1c3RvZGlhbi1wbGF5Z3JvdW5kIiwiYXVkIjoiY3VzdG9kaWFuIiwic3ViIjoiNjA1M2M1N2QtMTdlNi00NzZjLTkyZTktNzc3MzZkYjM4OTgyIiwidHlwIjoiSUQiLCJhenAiOiJjdXN0b2RpYW4iLCJzZXNzaW9uX3N0YXRlIjoiZmVhOWUxOTQtZTM4My00ZThjLThiYjAtMjA1MjYzYzkxYzhjIiwiYXRfaGFzaCI6IkVGMUJ1NWg4aEV1MEtkOFl1YjhJeHciLCJhY3IiOiIxIiwic2lkIjoiZmVhOWUxOTQtZTM4My00ZThjLThiYjAtMjA1MjYzYzkxYzhjIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsIm5hbWUiOiJCb2IgRG9lIiwiZ3JvdXBzIjpbImRhdGEtb3duZXIiLCJkZWZhdWx0LXJvbGVzLWN1c3RvZGlhbi1wbGF5Z3JvdW5kIiwib2ZmbGluZV9hY2Nlc3MiLCJ1bWFfYXV0aG9yaXphdGlvbiJdLCJ1c2VyX3VyaSI6Imh0dHBzOi8vc3dpc3NkYXRhY3VzdG9kaWFuLmNoL2JvYiIsInByZWZlcnJlZF91c2VybmFtZSI6ImJvYiIsImdpdmVuX25hbWUiOiJCb2IiLCJmYW1pbHlfbmFtZSI6IkRvZSIsImVtYWlsIjoiYm9iQGV4YW1wbGUuY29tIn0.qC8zVrz89MOWzJJdxFwyvtemHh4z3VS3cVbTdd1xCfLq3sYCRLdVWpPydOeUWnxH6Tpn_kHSJGEN4GcOF985aUInk0VgdJXH1qiOYc6llOEqqlYq70WLORr8KFWjEjWI3yJMqhL_pNcs6YJyzDnrmDRT5l3xs9x9hEYuRGXfo1zuBn6NLokAipR3TiieOYlmCTsR_YnVNZ0FfNFlufiNd5JHKE8PM0x3cEhvot4g4Fw31SaGXrFpBjXWcQnItzI8Qdc0v4w8KSuTmpaTm6YgT1T2WjIRoQpMPBqmz0JMsitl7wk5nXfM9nwE3XAIBcMnfwJ_B0qqWsuLod1UsMelHg`
