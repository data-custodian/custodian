package main

import (
	cmc "custodian/components/lib-common/pkg/config"
	"custodian/components/lib-common/pkg/jena"
	"custodian/components/lib-common/pkg/log"
	"custodian/components/lib-common/pkg/rabbitmq"
	kms "custodian/key-manager/internal"
	"custodian/key-manager/internal/config"
	"net/http"
	"time"
)

const (
	readTimeOut  = 5 * time.Minute
	writeTimeOut = 10 * time.Second
)

// @Version 0.1.0
// @Title Custodian KMS API
// @Description We offer some API that provides custodian services
// @ContactName Custodian Team
// @ContactEmail sdsc@epfl.ch
// @ContactURL https://gitlab.com/data-custodian/custodian
// @TermsOfServiceUrl https://gitlab.com/data-custodian/custodian
// @LicenseName GNU AGPLv3
// @LicenseURL https://gitlab.com/data-custodian/custodian/-/blob/develop/LICENSE.AGPL?ref_type=heads
// @Server 172.23.0.10:8099/kms
// @Security AuthorizationHeader read write
// @SecurityScheme AuthorizationHeader http bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9...

func main() {
	log.Setup()

	conf, err := cmc.LoadConfigs[config.Config]()
	if err != nil {
		log.ErrorE(err, "Failed loading config files.")

		return
	}

	storage := &kms.JenaStorage{Jena: jena.New(
		conf.Storage.Jena.Host,
		conf.Storage.Jena.Scheme,
		conf.Storage.Jena.CredentialFile,
		conf.Storage.Jena.CaTest)}
	// storage := kms.InMemoryStorage{DB: map[id.GeneralID]g.Graph{}}
	// storage.Create()

	// RabbitMR connection
	queue, err := rabbitmq.NewRabbitMQConnection(
		conf.Rabbitmq.URL,
		&conf.Rabbitmq.Credentials.Username,
		&conf.Rabbitmq.Credentials.Token)
	if err != nil {
		log.ErrorE(err, "Connection to RabbitMQ failed.")
	}

	// Creates a handler that contains contextual information -> jena storage and some config information
	handler := kms.NewHandler(storage, queue, conf.ECDSAKeys.CMSPrivateKey, conf.ECDSAKeys.CMSPublicKey)
	router := kms.NewRouter(handler, conf.Server.ComponentURLPath)

	// TODO: add SSL authentication (client AND server)

	host := conf.Server.Hostname + ":" + conf.Server.Port
	server := &http.Server{
		Addr:         host,
		Handler:      router,
		ReadTimeout:  readTimeOut,
		WriteTimeout: writeTimeOut,
	}
	log.Info("Server is running at " + conf.Server.Port + " port.")
	err = server.ListenAndServe()
	if err != nil {
		return
	}
}
