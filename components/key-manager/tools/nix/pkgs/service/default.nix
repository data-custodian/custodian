{ lib, pkgs }:
let
  cnPkgs = pkgs.custodian;
  cnLib = lib.custodian;

  compName = "key-manager";
in
cnPkgs.buildGoModule {
  inherit compName;
  pname = compName;
  version = cnLib.components.readVersion compName;

  src = cnLib.filesets.toSource [
    compName
    "lib-common"
  ];

  target = "service";
  vendorHash = "sha256-c+yocqXlzhDPGDYpx3XvNr9oiXrKvpBuVV055PUdbBc=";

  meta = {
    description = compName;
    homepage = "https://gitlab.com/data-custodian/custodian";
    license = lib.licenses.agpl3Plus;
    maintainers = [ "sdcs-ordes" ];
    mainProgram = compName;
  };
}
