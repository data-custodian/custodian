# Audit Trail

## About

The Audit Trail Component in Custodian stores gathers and stores events that
occur on resources and contracts and makes them available to the owners of the
resources. So it has exactly two tasks:

- pick up all events that need to be audited and are reported by other
  components od the Custodian and store them long term
- make the events avaliable to authorized users: to resource owners that can
  such monitor the usage of their resources

### Technologies

The Audit Trail component makes use the following technologies:

- **mongodb**: for the long term storage of events
- **rabbitMQ**: as an interface to the other Custodian components, that send
  their events to this message queue, where the Audit Trail component pick them
  up

The Audit Trail component consists of two microsrvices that take care of the two
tasks mentioned above

- the **events-api**: offeres an api to serve events to authorized users
- the **events-receiver**: collects events from a rabbit message queue and
  inserts them into a mongo db for permantent storage

Additionally there is an extra microservice to post events:

- the **events-sender**: sends mock events, so that the other services can be
  tested: this also serves as a model on how other components can implement
  sending events.

## Setup

Go to each Service and follow the setup instructions there.

Run Services in parallel (on 3 different terminals):

- First the **events-sender** needs to send mock events
- In parallel these events will be processed by the **events-receiver** and they
  will be added to the mongodb
- The **events-api** connects to the mongodb and shows events an authenticated
  user is authorized to see

# Events API

The events api is an api endpoint, that serves request coming through the
Custodian gateway reverse proxy.

- It assumes an authenticated user
- A filter is taken from the request claim
- a json response is returned with the events that the user is permitted to see
- the reponse is paginated
- the events are read from a mongo db

## Installation

```
go get
```

### Configuration

The config file can be found in `config/config.yml`. For the new configuration
to take effect, you must rerun the service.

### Build and Run

To build and run the code, open a terminal in the `custodian/audit-trail` folder
and run the following command:

```
go build -o events-api cmd/main.go
./events-api
```

run the gateway:

```
cd ../../components/gateway
go run main.go proxies.go middlewares.go config.go
```

## Docker

```
docker build --tag events-api .
docker run -p 8081:8081 events-api
```
