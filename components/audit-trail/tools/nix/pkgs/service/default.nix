{ lib, pkgs }:
let
  cnPkgs = pkgs.custodian;
  cnLib = lib.custodian;

  compName = "audit-trail";
in
cnPkgs.buildGoModule {
  inherit compName;
  pname = compName;
  version = cnLib.components.readVersion compName;

  src = cnLib.filesets.toSource [
    compName
    "lib-common"
  ];

  target = "service";

  vendorHash = "sha256-Sz8Ual7jAmkwc+S3SXV/4Wwg3IVcQTar+ncNpkZeufI=";

  meta = {
    description = compName;
    homepage = "https://gitlab.com/data-custodian/custodian";
    license = lib.licenses.agpl3Plus;
    maintainers = [ "sdcs-ordes" ];
    mainProgram = compName;
  };
}
