// The package provides the model for events
package model

import (
	"time"
)

// Event: an event that is captured by the Audit Trail.
type Event struct {
	CreatedAt    time.Time `bson:"created_at"`
	OwnerUserIDs []string  `bson:"owner_user_uris,omitempty"`
	ActionUserID string    `bson:"action_user_uri,omitempty"`
	ContractURI  string    `bson:"contract_uri,omitempty"`
	ResourceID   string    `bson:"resource_uri,omitempty"`
	ActionID     string    `bson:"action_uri,omitempty"`
	Success      bool      `bson:"success"`
	Message      string    `bson:"message,omitempty"`
}
