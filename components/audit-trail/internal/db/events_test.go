package db

import (
	"context"
	mongodb "custodian/components/lib-common/pkg/db/mongo"
	"testing"

	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/bson"
)

// TODO: This does not test FindEvents correctly because mongodb is complicated to mock
// Currently, it only checks that FindEvents does not panic for the wrong reason.
// This is use mostly for debugging.
func TestFindEvents(t *testing.T) {
	defer func() {
		if r := recover(); r == nil {
			t.Errorf("The code did not panic")
		} else {
			assert.NotEqual(t, "Not implemented", r)
		}
	}()

	ctx := context.Background()
	conn := mongodb.MongoDBConnection{Client: nil, Collection: nil}
	filter := bson.M{}
	sort := bson.D{}
	_, err := FindEvents(ctx, conn, filter, 0, 0, sort) // panic
	assert.NoError(t, err)                              // currently, this is never reached
}
