package db

import (
	"context"
	"custodian/components/audit-trail/pkg/model"
	db "custodian/components/lib-common/pkg/db"
	"custodian/components/lib-common/pkg/log"

	"go.mongodb.org/mongo-driver/bson"
)

func CountEvents(ctx context.Context, connection db.DBConnection, filter bson.M) (int64, error) {
	return connection.Count(ctx, filter)
}

func FindEvents(
	ctx context.Context,
	connection db.DBConnection,
	filter bson.M,
	limit int64,
	skip int64,
	sort bson.D,
) ([]model.Event, error) {
	return db.FindEntries[model.Event](ctx, connection, filter, limit, skip, sort)
}

func StoreEvent(ctx context.Context, d db.DBConnection, event model.Event) (err error) {
	log.Info("Adding event to collection", "event", event)

	return d.Store(ctx, event)
}
