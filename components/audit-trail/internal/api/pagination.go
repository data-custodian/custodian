package api

import (
	"custodian/components/audit-trail/internal/config"
	"errors"
	"fmt"
	"math"
)

// Pagination parameters returned in the API response.
type Pagination struct {
	// Current Page number
	Page int64 `json:"page,omitempty"`
	// requested number of records per page
	PerPage int64 `json:"perPage,omitempty"`
	// Total number of records for the filter
	TotalRecords int64 `json:"totalRecords,omitempty"`
	// Total number of pages
	TotalPages int64 `json:"totalPages,omitempty"`
	// Next Page or last page of the result
	NextPage int64 `json:"nextPage,omitempty"`
	// previous or fist page of the result
	PreviousPage int64 `json:"previousPage,omitempty"`
}

type PaginationConfig struct {
	Page    int64
	PerPage int64
	Count   int64
}

func newPaginationConfig(
	page int64,
	perPage int64,
	count int64,
	paginationDefaultConfig config.Pagination,
) PaginationConfig {
	if page == 0 {
		page = paginationDefaultConfig.Page
	}
	if perPage == 0 {
		perPage = paginationDefaultConfig.PerPage
	}

	return PaginationConfig{
		Page:    page,
		PerPage: perPage,
		Count:   count,
	}
}

// CalculatePagination calcultates the Pagination from page, perPage
// and count of records
// Return Pagination for the API and also Options that will be
// used for the MongoDb to limit the Find to retrieve the data.
func CalculatePagination(pc PaginationConfig) (limit int64, skip int64, pagination Pagination, err error) {
	if pc.Page < 1 {
		err = errors.New("pc.Page must be a positive integer")

		return
	}
	if pc.PerPage < 1 {
		err = errors.New("per pc.Page must be a positive integer")

		return
	}
	limit = pc.PerPage
	skip = pc.PerPage*pc.Page - pc.PerPage
	totalPages := int64(math.Ceil(float64(pc.Count) / float64(pc.PerPage)))
	if skip >= pc.Count {
		err = fmt.Errorf(
			"requested pc.Page number %d does not exist. Max pc.Page nr is %d",
			pc.Page, totalPages)

		return
	}
	nextPage := pc.Page + 1
	previousPage := pc.Page - 1
	if nextPage > totalPages {
		nextPage = totalPages
	}
	if previousPage < 1 {
		previousPage = 1
	}
	pagination = Pagination{
		TotalRecords: pc.Count,
		PerPage:      pc.PerPage,
		Page:         pc.Page,
		TotalPages:   totalPages,
		NextPage:     nextPage,
		PreviousPage: previousPage,
	}

	return
}
