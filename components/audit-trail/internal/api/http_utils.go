package api

import (
	"encoding/json"
	"net/http"
)

type ErrorResponse struct {
	Error Error `json:"error"`
}
type Error struct {
	StatusCode int    `json:"statusCode"`
	Message    string `json:"message"`
}

func encodeError(w http.ResponseWriter, text string, statusCode int) error {
	w.WriteHeader(statusCode)
	err := Error{StatusCode: statusCode, Message: text}
	data := ErrorResponse{err}
	e := json.NewEncoder(w)

	return e.Encode(data)
}
