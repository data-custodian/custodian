package api

import (
	"custodian/components/audit-trail/internal/config"
	"custodian/components/audit-trail/internal/db"
	"custodian/components/audit-trail/pkg/model"
	mongodb "custodian/components/lib-common/pkg/db/mongo"
	"custodian/components/lib-common/pkg/jwt"
	log "custodian/components/lib-common/pkg/log"
	"encoding/json"
	"net/http"

	"github.com/gorilla/schema"
	"go.mongodb.org/mongo-driver/bson"
)

// Paginated API Response.
type PaginatedResults struct {
	// Events as a List
	Events []model.Event `json:"events,omitempty"`
	// Pagination Parameters
	Pagination Pagination `json:"pagination,omitempty"`
}

// Query Parameters of the Request.
type RequestParams struct {
	// requested Page number
	Page int64 `schema:"page,default=1"`
	// requested results per page
	PerPage int64 `schema:"perPage,default=10"`
}

func FindEvents(
	connection mongodb.MongoDBConnection,
	paginationDefaultConfig config.Pagination,
) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		q := r.URL.Query()

		log.Info("NEW REQUEST: Request received:", "r.URL.Query()", q)

		pg := &RequestParams{}
		d := schema.NewDecoder()
		err := d.Decode(pg, q)
		if err != nil {
			e := encodeError(
				w,
				"Bad Request: Incorrect Pagination parameters: use 'page' and 'perPage'",
				http.StatusBadRequest,
			)

			log.ErrorE(err, "Could not JSON encode error message.", "error", e)

			return
		}

		userID, err := jwt.GetUserID(r)
		if err != nil {
			log.ErrorE(err, "Could not extract user ID")
			e := encodeError(w, "Access denied", http.StatusUnauthorized)

			log.ErrorE(err, "Could not JSON encode error message.", "error", e)

			return
		}

		userURI := userID.URI()

		// Mongo DB Access with a filter.
		filter := bson.M{"owner_user_uris": userURI}
		sort := bson.D{{Key: "created_at", Value: -1}}

		count, err := db.CountEvents(ctx, connection, filter)
		if err != nil {
			log.ErrorE(err, "Could not count events.")

			return
		}
		log.Info("Total Number of records received", "total number of records", count)

		if count == 0 {
			http.Error(w, "Events not found.", http.StatusNotFound)

			return
		}

		paginationConfig := newPaginationConfig(pg.Page, pg.PerPage, count, paginationDefaultConfig)
		// Calculate values for db options
		limit, skip, pagination, err := CalculatePagination(paginationConfig)
		if err != nil {
			log.ErrorE(err, "Could not calculate pagination.", "error", err.Error())
			e := encodeError(w, err.Error(), http.StatusBadRequest)

			log.ErrorE(err, "Could not JSON encode error message.", "error", e)

			return
		}
		log.Info("Pagination parameters for db:", "limit", limit, "skip", skip)

		results, err := db.FindEvents(ctx, connection, filter, limit, skip, sort)
		if err != nil {
			log.ErrorE(err, "FindEvents failed.")
			http.Error(w, "Events not found.", http.StatusNotFound)

			return
		}
		log.Debug("Received results:", "results", results)

		pagingatedResults := PaginatedResults{
			Events:     results,
			Pagination: pagination,
		}
		log.Debug("Paginated results:", " paginated results", pagingatedResults)

		err = json.NewEncoder(w).Encode(pagingatedResults)
		if err != nil {
			log.ErrorE(err, "Could not encode response.")
			http.Error(w, "Internal server error.", http.StatusInternalServerError)

			return
		}
	}
}
