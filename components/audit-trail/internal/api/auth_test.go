package api

import (
	"net/http"
	"testing"
)

const (
	dummyToken = "myToken"
)

func TestExtractAuthorizationToken(t *testing.T) {
	correctAuthHeader := http.Header{
		"Authorization": []string{oauthPrefixBearer + dummyToken},
	}
	token, err := extractAuthorizationToken(correctAuthHeader)
	if err != nil {
		t.Errorf("Expected nil error, got %v", err)
	} else if token != dummyToken {
		t.Errorf("Token does not match: expected %v, got %v", dummyToken, token)
	}

	missingPrefix := http.Header{
		"Authorization": []string{dummyToken},
	}
	_, err = extractAuthorizationToken(missingPrefix)
	if err == nil {
		t.Errorf("Expected an error for missing prefix, got 'nil'")
	}

	wrongPrefix := http.Header{
		"Authorization": []string{"wrong " + dummyToken},
	}
	_, err = extractAuthorizationToken(wrongPrefix)
	if err == nil {
		t.Errorf("Expected an error for wrong prefix, got 'nil'")
	}

	missingHeader := http.Header{}
	_, err = extractAuthorizationToken(missingHeader)
	if err == nil {
		t.Errorf("Expected error for missing header, got 'nil'")
	}
}
