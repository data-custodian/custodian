package api

import (
	"errors"
	"fmt"
	"net/http"
	"strings"
)

const (
	oauthPrefixBearer string = "Bearer "
)

// extractAuthorizationToken extracts the OAuth token from the Authorization Header.
func extractAuthorizationToken(h http.Header) (token string, err error) {
	auth := h.Get("Authorization")

	if auth == "" {
		return "", errors.New("empty 'Authorization' header")
	}

	if !strings.HasPrefix(auth, oauthPrefixBearer) {
		return "", fmt.Errorf("'%v' prefix is missing", oauthPrefixBearer)
	}

	return strings.TrimPrefix(auth, oauthPrefixBearer), nil
}

// TODO: implement OAuth authorization mechanism
// isUserAuthorized verifies that the token grants access to the user.
//
//nolint:revive,unused,nolintlint
func isUserAuthorized(userid string, token string) bool {
	panic("not implemented yet.")
}
