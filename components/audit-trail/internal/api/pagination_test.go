package api

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func assertPagination(t *testing.T, a, b Pagination) {
	assert.Equal(t, a.PerPage, b.PerPage)
	assert.Equal(t, a.TotalRecords, b.TotalRecords)
	assert.Equal(t, a.TotalPages, b.TotalPages)
	assert.Equal(t, a.NextPage, b.NextPage)
	assert.Equal(t, a.PreviousPage, b.PreviousPage)
}

func TestCalculatePagination(t *testing.T) {
	paginationConfig := PaginationConfig{
		Page:    2,
		PerPage: 10,
		Count:   100,
	}

	limit, skip, pagination, err := CalculatePagination(paginationConfig)
	require.NoError(t, err)

	expected := Pagination{
		Page:         2,
		PerPage:      10,
		TotalRecords: 100,
		TotalPages:   10,
		NextPage:     3,
		PreviousPage: 1,
	}

	assertPagination(t, expected, pagination)

	var expectedLimit, expectedSkip int64 = 10, 10

	assert.Equal(t, expectedLimit, limit)
	assert.Equal(t, expectedSkip, skip)
}
