// config package reads the config for the events api endpoint
// the config is read from either environment variables
// or from a config file
package config

import (
	mongodb "custodian/components/lib-common/pkg/db/mongo"
)

type Config struct {
	// Config struct for webapp config
	Server Server `yaml:"server"`

	// Config struct for mongodb
	//nolint:tagliatelle,nolintlint
	MongoDB mongodb.MongoDBConfig `yaml:"mongoDB"`

	OIDC OIDC `yaml:"oidc"`

	PaginationDefaults Pagination `yaml:"paginationDefaults"`
}

type Server struct {
	//	hostname string `yaml:"hostname"`
	Hostname string `yaml:"hostname"`
	//	host string `yaml:"port"`
	Port string `yaml:"port"`
}

type Collections struct {
	// mongodb collection events
	Events string `yaml:"events"`
}

type OIDC struct {
	// OpenID Connect issuer
	Issuer string `yaml:"issuer"`
	// OpenID Connect client ID
	ClientID string `yaml:"clientID"`
}

type Pagination struct {
	// OpenID Connect issuer
	Page int64 `yaml:"page"`
	// OpenID Connect client ID
	PerPage int64 `yaml:"perPage"`
}
