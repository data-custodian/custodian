package main

import (
	"context"
	api "custodian/components/audit-trail/internal/api"
	"custodian/components/audit-trail/internal/config"
	cmc "custodian/components/lib-common/pkg/config"
	mongodb "custodian/components/lib-common/pkg/db/mongo"
	log "custodian/components/lib-common/pkg/log"
	"net/http"
	"time"
)

const (
	readHeaderTimeout = 3 * time.Second
)

type Callback func() error

func cleanUp(callback Callback, comp string) {
	log.ErrorE(callback(), "Clean up failed.", "component", comp)
}

func main() {
	log.Setup()

	// Read config
	conf, err := cmc.LoadConfigs[config.Config]()
	log.PanicE(err, "Could not load configs.")

	ctx := context.Background()
	connection, err := mongodb.NewMongoDBConnection(ctx, &conf.MongoDB)
	log.PanicE(err, "Failed to connect to MongoDB")
	defer cleanUp(func() error { return connection.Disconnect(ctx) }, "MongoDB")

	http.HandleFunc("/events", api.FindEvents(connection, conf.PaginationDefaults))

	// Start API Server
	server := &http.Server{
		Addr:              conf.Server.Hostname + ":" + conf.Server.Port,
		ReadHeaderTimeout: readHeaderTimeout,
	}

	log.Info("Server is running at " + conf.Server.Port + " port.")
	err = server.ListenAndServe()
	log.PanicE(err, "Failed to start server.")
}
