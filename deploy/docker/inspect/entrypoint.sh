#!/usr/bin/env sh

set -e
set -u

apk add nmap-ncat figlet jq jwt-cli \
    --repository=https://dl-cdn.alpinelinux.org/alpine/edge/testing

response=$(
    printf "HTTP/1.1 200 OK\r\nContent-Type: text/plain\r\n\r\n"
    figlet "Redirect-Inspect"
    echo
    echo "You reached the 'redirect-inspect' endpoint for inspecting cookies."
    echo "Use 'just logs redirect-inspect'."
)

trap exit 0 INT EXIT

while true; do
    set +e

    log=$(echo "$response" | ncat -lvp "$PORT" 2>&1)
    echo "Log:"
    echo "$log"
    echo "======================================="

    access_token=$(echo "$log" |
        grep -E "^Authorization: Bearer " |
        sed -E "s/Authorization: Bearer //g")

    id_token=$(echo "$log" |
        grep -E "^X-Id-Token" |
        sed -E "s/X-Id-Token: Bearer //g")

    echo "ID Token:"
    echo "$id_token"
    echo "Access Token:"
    echo "$access_token"

    echo "ID Token Decoded:"
    jwt_id=$(echo "$id_token" | jwt decode --json -)

    echo "Access Token Decoded:"
    jwt_access=$(echo "$access_token" | jwt decode --json -)

    echo "JWT ID Token:"
    echo "$jwt_id" | jq

    echo "JWT Access Token:"
    echo "$jwt_access" | jq

done
