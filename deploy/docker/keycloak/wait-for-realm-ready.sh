#!/usr/bin/env sh

set -e
set -u

KC_HOST_URL="$1"
KC_REALM="$2"

trap exit 1 INT

for i in $(seq 1 1000); do
    echo "$i:: Check if realm $KC_REALM is up"

    if wget \
        "http://$KC_HOST_URL/realms/$KC_REALM/.well-known/openid-configuration" \
        -O -; then
        echo "Realm $KC_REALM is up and running."
        break
    else
        echo "Realm $KC_REALM is not yet up running -> waiting." >&2
    fi

    sleep 0.5
done
