#!/usr/bin/env bash

set -e
set -u

kc="/opt/keycloak/bin/kc.sh"

echo "[entrypoint]: Importing realm..."
"$kc" import \
    --file /opt/keycloak/data/import/realm.json \
    --override false

echo "[entrypoint]: Building keycloak"
# "$kc" build --features=preview,scripts

echo "[entrypoint]: Keycloak config"
"$kc" show-config

echo "[entrypoint]: Starting keycloak"
exec "$kc" "$@" # --features=preview,scripts
