# Launch Custodian with Docker

Secrets and configuration are hard-coded, do not use this for a deployment.

```shell
cp .env.dist .env
just up
```

Grab a coffee :)

This will run all components for custodian with `docker compose` (`podman` is
also possible with `just container_mgr=podman up`.

## Access URLS

Make sure to clean your cookies in the browser ([see here](#cookies-cleanup))
The following IP addresses are setup to access the different services:

- **Frontend `playground`** is accessible through the gateway on the host with

  - on the host **[http://localhost:8000](http://localhost:8000)**
  - directly by [http://172.24.0.20:5050](http://172.24.0.20:5050)

  The `${PLAYGROUND_LOCAL_PORT} := 8000` can be changed in [.env](./.env).

- **Frontend `keycloak`** is accessible

  - on the host **[http://localhost:8001](http://localhost:8001)**
  - directly by [http://172.23.0.2:8080](http://172.23.0.2:8080)

  The `${KEYCLOAK_LOCAL_PORT} := 8001` can be changed in [.env](./.env).

- **Backend `gateway`** is accessible

  - on the host **[http://localhost:8000](http://localhost:8000)**
  - directly by [http://172.24.0.2:8000](http://172.24.0.2:8000)

### IP/Port Mapping

| Component               | External                | Internal (also acc. on host) |
| ----------------------- | ----------------------- | ---------------------------- |
| `audit-trail`           | not-exposed             | `http://172.24.0.10:8080`    |
| `audit-trail-consumer`  | not-exposed             | `172.24.0.11`                |
| `contract-manager`      | not-exposed             | `http://172.24.0.12:8080`    |
| `key-manager`           | not-exposed             | `http://172.24.0.13:8080`    |
| `knowledgebase`         | not-exposed             | `http://172.24.0.14:8080`    |
| `policy-decision-point` | not-exposed             | `http://172.24.0.16:8080`    |
| ----------------------- | ----------------------- | -------------------------    |
| `playground`            | not-exposed             | `http://172.24.0.20:5050`    |
| ----------------------- | ----------------------- | -------------------------    |
| `db-mongo`              | not-exposed             | `172.24.0.100`               |
| `rabbitmq`              | not-exposed             | `172.24.0.101`               |
| `db-jena-proxy`         | not-exposed             | `172.24.0.102`               |
| ----------------------- | ----------------------- | -------------------------    |
| `gateway`               | `http://localhost:8000` | `http://172.24.0.2:8080`     |
| `keycloak`              | `http://localhost:8080` | `http://172.23.0.2:8080`     |

## Modify Keycloak Realm `data-custodian`

On startup `keycloak` will load the realm
[`data-custodian`](./keycloak/realm-data-custodian.json) if it does not yet
exists.

The file realm should contain two users `bob` and `alice` both with password
`aliceandbob`.

If you make changes and want to store it again do

```shell
just up keycloak
just export-keycloak
```

## Inspect Cookies in `redirect-inspect`

You can inspect cookies by setting the `redirect.url`
[`config.docker.yaml`](../../components/gateway/config/config.docker.yml) to
`redirect-inspect:5050` and running `just logs redirect-inspect`. Hit
`localhost:8000` and inspect the output when you logged into Keycloak.

## Cookies Clean-Up

To clean up cookies on tab close automatically use the
[browser extension](https://github.com/Cookie-AutoDelete/Cookie-AutoDelete) and
import the following
[settings file](../../tools/configs/cookies-autodelete/config.json) and
[core-settings](../../tools/configs/cookies-autodelete/core-settings.json).
Which will only delete all `localhost` cookies on tab close and leave anything
like it is.

## Keycloak Custom Mapper

Resources:

- https://github.com/mschwartau/keycloak-custom-protocol-mapper-example
- https://www.bityard.org/blog/2022/04/21/keycloak_script_mapper
