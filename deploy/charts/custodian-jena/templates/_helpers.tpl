{{/*
Expand the name of the chart.
*/}}
{{- define "custodian-jena.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "custodian-jena.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "custodian-jena.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "custodian-jena.labels" -}}
helm.sh/chart: {{ include "custodian-jena.chart" . }}
{{ include "custodian-jena.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "custodian-jena.selectorLabels" -}}
app.kubernetes.io/name: {{ include "custodian-jena.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "custodian-jena.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "custodian-jena.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
Create a line text with username:password from the jena secrets
*/}}
{{- define "jenaRWUser" -}}
{{ .Values.jena.userUsername }} {{ .Values.global.secrets.platforms.jena.userPassword }}
{{- end }}

{{/*
Create a three lines text with username:password from the jena secrets
*/}}
{{- define "jenaUsers" -}}
{{ .Values.jena.adminUsername }} {{ .Values.global.secrets.platforms.jena.adminPassword }}
{{ .Values.jena.userUsername }} {{ .Values.global.secrets.platforms.jena.userPassword }}
{{ .Values.jena.roUserUsername }} {{ .Values.global.secrets.platforms.jena.roUserPassword }}
{{- end }}

{{/*
Create the RabbitMQ URL
*/}}
{{- define "custodian.rabbitmqHostname" -}}
{{ .Values.auditTrailHostname }}
{{- end }}

{{/*
Create the Knowledge Base secrets file `secrets.yml`
*/}}
{{- define "knowledgebaseSecrets" -}}
rabbitMQ:
    credentials:
        username: {{ .Values.rabbitmq.credentials.username | quote }}
        token: {{ .Values.global.secrets.auditTrail.rabbitmq.password| quote }}
{{- end }}