# Swiss Data Custodian - Helm Charts

## Overview

These Helm charts are designed to deploy the Swiss Data Custodian on Kubernetes.
This README provides detailed instructions on how to deploy the solution using
this Helm chart.

## Prerequisites

Before you begin, ensure that you have the following installed:

- Kubernetes cluster (e.g., Minikube, kind, ...)
- Helm v3.x
- `kubectl` command-line tool
- Docker
- Keycloak instance running or a configured OpenID Connect endpoint

## Getting Started

### 1. Install Keycloak and Configure the Gateway

#### 1. Install and Access Keycloak

If you haven't already installed Keycloak, you can follow the official
[Keycloak Installation Guide](https://www.keycloak.org/docs/latest/getting_started/index.html)
to set it up.

Once Keycloak is running, access the Keycloak Admin Console.

#### 2. Create a Realm

- Log in to the Keycloak Admin Console.
- Click on `Add realm` and provide a name for your realm, e.g.,
  `swiss-data-custodian`.
- Click `Create`.

#### 3. Create a Client

- Inside your realm (`swiss-data-custodian`), navigate to `Clients` and click
  `Create`.
- Set `Client ID` to `custodian-client`.
- Set `Client Protocol` to `openid-connect`.
- Click `Save`.
- In the `Settings` tab, set `Access Type` to `confidential`.
- Under `Valid Redirect URIs`, add `http://localhost:8000/*` or the URL you will
  use to expose the Swiss Data Custodian.
- Click `Save`.

#### 4. Configure OpenID Connect Endpoint

- Still in the `Settings` tab of the `custodian-client`, note the `Client ID`,
  `Client Secret`, and `Issuer` values. You'll need these to configure the
  gateway.

#### 5. Configure the Gateway

Create a `gateway-values.yaml` file for the `custodian-gateway` chart with the
following minimal requirements:

```yaml
oidc:
  clientID: my_client
  clientSecret: my_client_secret
  issuer: http://keycloak:8080/realms/swiss-data-custodian
  callback: http://localhost:8000/auth/callback
  defaultPostLogoutRedirect: http://localhost:8000/
```

### 2. Generate Secrets

**Warning**: the use of the `secrets-generator` is meant for development and
testing only. For production, is it advised to manually create the
`custodian-secrets.yaml` file. A template `custodian-secrets.yaml.sample` is
present in this folder to help you with the structure.

Navigate to the `secrets-generator` directory and build the Docker image:

```bash
cd ../secrets-generator
docker build -t secrets-generator .
```

Go back to the Helm charts folder and run the Docker container to generate
`custodian-secrets.yaml`:

```bash
cd ../helm-chart
docker run -it -u $UID -v "${PWD}:/work" secrets-generator
```

This will create a `custodian-secrets.yaml` file in the root directory of the
repository.

More details about this tool can be found in the
[secrets-generator folder](https://gitlab.com/data-custodian/custodian/-/tree/develop/secrets-generator)

### 3. Deploy Helm Charts

Follow the deployment order specified below:

#### a. Deploy `custodian-jena`

```bash
helm install jena custodian-jena -f custodian-secrets.yaml
```

#### b. Deploy `custodian-audit-trail`

```bash
helm install audit-trail custodian-audit-trail -f custodian-secrets.yaml
```

#### c. Deploy `custodian-cms`

```bash
helm install cms custodian-cms -f custodian-secrets.yaml
```

#### d. Deploy `custodian-acs`

```bash
helm install acs custodian-acs -f custodian-secrets.yaml
```

#### e. Deploy `custodian-gateway`

```bash
helm install gateway custodian-gateway -f custodian-secrets.yaml
```

## Customization

You may need to customize some values in the `values.yaml` files to point to the
correct services based on the release names you choose. In particular, make sure
that the gateway `redirections` section points to the correct services names.

If you want to publicly expose the Swiss Data Custodian, you need to configure
the ingress properties of the gateway values file.

```yaml
ingress:
  enabled: false # Switch to `true` to create an ingress rule
  tlsCertificate: "gateway-tls"
  clusterIssuer: "" # Add your issuer
  ingressClassName: "nginx"
  hostname: "example.com" # Replace with your domain
```

## Cleanup

To uninstall all deployed Helm releases, run:

```bash
helm uninstall gateway acs cms audit-trail jena
```
