#! Common YAML functions for the rendering part.

load("@ytt:data", "data")


def undef():
    # Placeholder for values which should be defined.
    # Either injected by CI and the final rendering.
    return "custodian-undefined"

    end


#! Return the component name, optional with a postfix.
def resourceName(name, postfix=""):
    v = data.values
    return "-".join([v.release.name, name] + ([postfix] if postfix else []))

    end


#! TODO: documentation
def defineResourceName(component, name):
    return useResourceName(component, name)

    end


#! TODO: documentation
def useResourceName(component, name):
    v = data.values

    if name not in [n.strip() for n in component.resourceNames.splitlines()]:
        fail(
            (
                "This resource name '{}' must be defined "
                + "in 'schema.yaml::resourceNames' in {}."
            ).format(name, component.name)
        )
    end

    return "-".join([v.release.name, component.name, name])
    end


#! TODO: documentation
def defineResourceNameInternal(component, name):
    return useResourceNameInternal(component, name)


end


#! TODO: documentation
def useResourceNameInternal(component, name):
    v = data.values

    if name not in [n.strip() for n in component.resourceNamesInternal.splitlines()]:
        fail(
            (
                "This resource name '{}' must be defined "
                + "in 'schema.yaml::resourceNamesInternal' in {}."
            ).format(name, component.name)
        )
        end

    return "-".join([v.release.name, component.name, name])
    end


def resourceLabels(compSettings, postfix=""):
    v = data.values

    commitRef = undef()

    commitRef = (
        compSettings.sourceInfo.commitRef
        if hasattr(compSettings, "sourceInfo")
        else undef()
    )

    version = compSettings.version if hasattr(compSettings, "version") else undef()

    compName = resourceName(compSettings.name, postfix)

    return {
        # Custodian Labels.
        v.release.labelsPrefix + ".service": compName,
        v.release.labelsPrefix
        + ".source-info.repo-url": "gitlab.com--data-custodian--custodian",
        v.release.labelsPrefix + ".source-info.commit-ref": commitRef,
        #
        # Kubernetes Labels.
        "app.kubernetes.io/name": compName,
        "app.kubernetes.io/instance": compName,
        "app.kubernetes.io/version": version,
        "app.kubernetes.io/component": "microservice",
        "app.kubernetes.io/part-of": "custodian",
        "app.kubernetes.io/managed-by": "ytt",
    }
    end


def resourceSelectorLabel(name):
    v = data.values
    return {v.release.labelsPrefix + ".service": resourceName(name)}
    end
