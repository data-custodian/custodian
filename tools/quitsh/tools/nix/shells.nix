# Define different shells.
{
  lib,
  pkgs,
  cli,
  ...
}:
let
  go = pkgs.go_1_23;

  toolchains = {
    go = {
      inherit (pkgs) git;
      inherit go;
    };

    general = {
      # Essentials.
      inherit (pkgs) git just;

      # Main languages.
      inherit go;

      # Linting and LSP and debuggers.
      inherit (pkgs)
        delve
        gopls
        golines
        typos-lsp
        ;

      inherit cli;
    };

    ci = {
      inherit (pkgs) git git-lfs just;
      inherit cli;
    };
  };
in
{

  # Toolchain Shells ============================
  ci = pkgs.mkShell {
    QUITSH_TOOLCHAINS = "ci";
    packages = lib.attrValues toolchains.ci;
  };

  default = pkgs.mkShell {
    QUITSH_TOOLCHAINS = "default";
    packages = lib.attrValues toolchains.general;

    # To make CGO and the debugger delve work.
    # https://nixos.wiki/wiki/Go#Using_cgo_on_NixOS
    # Note: Due to warning when compilin `_FORTIFY_SOURCE`
    hardeningDisable = [ "fortify" ];
  };

  go = pkgs.mkShell {
    QUITSH_TOOLCHAINS = "go";
    packages = lib.attrValues toolchains.go;
  };

  # =============================================
}
