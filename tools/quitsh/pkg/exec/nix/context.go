package nix

import (
	"path"

	"github.com/sdsc-ordes/quitsh/pkg/debug"
	"github.com/sdsc-ordes/quitsh/pkg/exec"
	fs "github.com/sdsc-ordes/quitsh/pkg/filesystem"
	"github.com/sdsc-ordes/quitsh/pkg/log"
)

// NewCtxBuilder returns a new Nix command context builder.
func NewCtxBuilder() exec.CmdContextBuilder {
	return exec.NewCmdCtxBuilder().BaseCmd("nix")
}

// NewDevShellCtxBuilder returns a new command context builder which runs all
// commands over a Nix development shell.
func NewDevShellCtxBuilder(
	rootDir string,
	flakePath string,
	attrPath string,
) exec.CmdContextBuilder {
	return NewDevShellCtxBuilderI(rootDir, FlakeInstallable(flakePath, attrPath))
}

// NewDevShellCtxBuilderI, see `NewDevShellCtxBuilder`.
func NewDevShellCtxBuilderI(rootDir string, installable string) exec.CmdContextBuilder {
	debug.Assert(path.IsAbs(rootDir), "Devenv root must be absolute path.")

	b := exec.NewCmdCtxBuilder().
		BaseCmd("nix")

	devenvRoot := rootDir + "/.devenv/state/pwd"
	if fs.Exists(devenvRoot) {
		// We inject `--override-input` to set the `devenv-root` flake input (HACK).
		// This is currently needed for devenv to properly run in pure hermetic
		// mode while still being able to run processes & services and modify
		// (some parts) of the active shell.mkdir -p .devenv/state
		// See: https://github.com/cachix/devenv/issues/1461
		return b.
			BaseArgs(
				"develop",
				"--override-input", "devenv-root", "path:"+rootDir+"/.devenv/state/pwd",
				"--accept-flake-config",
				installable,
				"--command")
	} else {
		log.Warn("Devenv root file '%s' does not exist, " +
			"using '--no-pure-eval' for Nix shell.")

		return b.
			BaseArgs(
				"develop",
				"--no-pure-eval",
				"--accept-flake-config",
				installable,
				"--command")
	}
}

// WrapOverDevShell wraps a command context builder
// over a dev shell with `NewDevShellCtxBuilder`.
func WrapOverDevShell(
	ctxBuilder exec.CmdContextBuilder,
	rootDir string,
	flakePath string,
	attrPath string,
) exec.CmdContextBuilder {
	return WrapOverDevShellI(ctxBuilder, rootDir, FlakeInstallable(flakePath, attrPath))
}

// WrapOverDevShellI, see `WrapOverDevShellI`.
func WrapOverDevShellI(
	ctxBuilder exec.CmdContextBuilder,
	rootDir string,
	installable string,
) exec.CmdContextBuilder {
	devShell := NewDevShellCtxBuilderI(rootDir, installable).Build()

	return ctxBuilder.PrependCommand(devShell.BaseCmd(), devShell.BaseArgs()...)
}
