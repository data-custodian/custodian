package nix

import (
	"fmt"
)

// FlakeInstallable returns the attribute path `<flakePath>#<attrPath>`.
func FlakeInstallable(flakePath string, attrPath string) string {
	return fmt.Sprintf("%s#%s", flakePath, attrPath)
}
