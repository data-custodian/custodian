{
  lib,
  buildGo123Module,
  installShellFiles,
  testers,
  git,
  cnLib,
  self,
}:
let
  compName = "quitsh-custodian";
in
# NOTE: It can be that this derivation does not build anymore.
#       This is probably due to caching of the source directory.
#       Before debugging: Change the .component.yaml version and increment its patch number
#       to trigger another source hash.
buildGo123Module rec {
  pname = compName;
  version = cnLib.components.readVersion compName;
  src = cnLib.filesets.toSource [
    compName
    "quitsh"
  ];

  modRoot = cnLib.filesets.getRootPathRel compName;
  # This hash which is hard coded is baked into
  # hash=$(nix show-derivation "../nix#quitsh.goModules" | jq -r ".[] | .outputs.out.hash");
  # nix hash convert --hash-algo sha256 $hash
  # To check if a new hash must be here:
  # Delete the store path to `goModules`
  # nix store delete $(nix show-derivation "./tools/nix#quitsh.goModules" | jq -r ".[] | .outputs.out.path")
  # then build
  # or use:
  # https://github.com/msteen/nix-prefetch/issues/3
  vendorHash = "sha256-jGshOBPytOgHpmUp7n3FakWHT9+xF6sx7TAaOANg5g0=";
  proxyVendor = true;

  nativeBuildInputs = [ installShellFiles ];
  nativeCheckInputs = [ git ];

  ldflags =
    let
      modulePath = "custodian/tools/quitsh-custodian";
    in
    [
      "-s"
      "-w"
      "-X ${modulePath}/pkg/build.buildVersion=${version}"
    ];

  postInstall = ''
    installShellCompletion --cmd quitsh \
      --bash <($out/bin/quitsh completion bash) \
      --fish <($out/bin/quitsh completion fish) \
      --zsh <($out/bin/quitsh completion zsh)
  '';

  passthru.tests.version = testers.testVersion {
    package = self;
    command = "quitsh --version";
    inherit version;
  };

  meta = with lib; {
    description = "Tool to build/test/lint/deploy components in a monorepo - quit using `sh`.";
    homepage = "https://data-custodian.gitlab.io/custodian";
    license = licenses.agpl3Plus;
    maintainers = [ "gabyx" ];
    mainProgram = "quitsh";
  };
}
