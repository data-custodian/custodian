package main

import (
	cnCmd "custodian/tools/quitsh-custodian/cmd/quitsh/cmd"
	"custodian/tools/quitsh-custodian/pkg/build"
	cnConfig "custodian/tools/quitsh-custodian/pkg/config"
	"custodian/tools/quitsh-custodian/pkg/custodian"
	cnRunner "custodian/tools/quitsh-custodian/pkg/runner"
	"os"

	"github.com/sdsc-ordes/quitsh/pkg/cli"
	"github.com/sdsc-ordes/quitsh/pkg/common"
	"github.com/sdsc-ordes/quitsh/pkg/config"
	"github.com/sdsc-ordes/quitsh/pkg/log"
	"github.com/sdsc-ordes/quitsh/pkg/toolchain"
)

func main() {
	err := log.Setup("info") // Level will be set at startup.
	if err != nil {
		log.PanicE(err, "Could not setup logger.")
	}

	args := cnConfig.New()

	cli, err := cli.New(
		&args.Commands.Root,
		&args,
		cli.WithName("quitsh-custodian"),
		cli.WithVersion(build.Version()),
		cli.WithStages("lint", "build", "test", "image", "manifest", "deploy"),
		cli.WithComponentPatterns("!component-a"),
		cli.WithTargetToStageMapperDefault(),
		cli.WithToolchainDispatcherNix(
			custodian.FlakeDirRel,
			func(c config.IConfig) *toolchain.DispatchArgs {
				cc := common.Cast[*cnConfig.Config](c)

				return &cc.Commands.DispatchArgs
			},
		),
	)
	if err != nil {
		log.PanicE(err, "Could not initialize CLI app.")
	}

	// Enhance the CLI with our commands and runners.
	cnCmd.AddCommands(cli, &args)
	cnRunner.RegisterAll(
		&args.Build,
		&args.Lint,
		&args.Test,
		&args.Image,
		&args.Manifest,
		custodian.FlakeDirRel,
		cli.RunnerFactory())

	// Run the app.
	err = cli.Run()
	if err != nil {
		log.ErrorE(err, "Error occurred.")
		os.Exit(1)
	}
}
