package configimg

import (
	"custodian/tools/quitsh-custodian/cmd/quitsh/cmd/image"
	cnConfig "custodian/tools/quitsh-custodian/pkg/runner/config"

	"github.com/sdsc-ordes/quitsh/pkg/cli"
	"github.com/sdsc-ordes/quitsh/pkg/config"
	"github.com/sdsc-ordes/quitsh/pkg/log"

	"github.com/spf13/cobra"
)

// AddCmd adds the `image` subcommands.
func AddCmd(cl cli.ICLI, root *cobra.Command, imageSettings *cnConfig.ImageSettings) {
	var outputFile string

	configCmd := &cobra.Command{
		Use:   "image",
		Short: "Helper command to store settings into the global config. ",
		RunE: func(_cmd *cobra.Command, _args []string) error {
			return modifyConfig(cl, outputFile)
		},
	}

	image.SetImageFlags(configCmd, imageSettings)

	configCmd.Flags().StringVarP(&outputFile,
		"output", "o",
		"quitsh.yaml", "The output file to store the new config file.")

	root.AddCommand(configCmd)
}

func modifyConfig(cl cli.ICLI, outputFile string) error {
	log.Info("Config.", "config", cl.Config())
	log.Info("Write config to file.", "output", outputFile)

	return config.SaveInterfaceToFile(outputFile, cl.Config())
}
