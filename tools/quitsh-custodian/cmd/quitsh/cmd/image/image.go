package image

import (
	"custodian/tools/quitsh-custodian/pkg/runner/config"
	"fmt"
	"strings"

	"github.com/sdsc-ordes/quitsh/pkg/cli"
	"github.com/sdsc-ordes/quitsh/pkg/cli/general"
	"github.com/sdsc-ordes/quitsh/pkg/common"
	"github.com/sdsc-ordes/quitsh/pkg/component/stage"
	"github.com/sdsc-ordes/quitsh/pkg/dag"
	"github.com/sdsc-ordes/quitsh/pkg/image"
	"github.com/sdsc-ordes/quitsh/pkg/registry"
	"github.com/sdsc-ordes/quitsh/pkg/toolchain"

	"github.com/spf13/cobra"
)

type imgArgs struct {
	compArgs general.ComponentArgs
}

const longDesc = `
Build the OCI image for a component matching them by name patterns (glob).
`

func AddCmd(
	cl cli.ICLI,
	imageSettings *config.ImageSettings) {
	var args imgArgs
	imgType := imgTypesParseWrapper{values: &imageSettings.Build.ImageTypes}

	imageCmd := &cobra.Command{
		Use:   "image",
		Short: "Build the OCI image of a component.",
		Long:  longDesc,
		RunE: func(_ *cobra.Command, _args []string) error {
			return execute(cl, imageSettings, &args)
		},
	}

	SetImageFlags(imageCmd, imageSettings)

	imageCmd.Flags().
		VarP(&imgType,
			"image-types", "i",
			fmt.Sprintf("The image types to build (%s) (comma-separated).",
				image.GetImageTypesHelp()))

	imageCmd.Flags().
		StringArrayVarP(&args.compArgs.ComponentPatterns,
			"components", "c", nil, "Components matched by these patterns are built.")
	imageCmd.Flags().
		StringVar(&args.compArgs.ComponentDir,
			"component-dir", "", "Directory pointing to a component to build, instead of giving them by patterns.")
	imageCmd.MarkFlagsMutuallyExclusive("components", "component-dir")
	imageCmd.MarkFlagsOneRequired("components", "component-dir")

	cl.RootCmd().AddCommand(imageCmd)
}

// SetImageFlags setts all image settings which are set by the global config `imageSettings`.
func SetImageFlags(cmd *cobra.Command, imageSettings *config.ImageSettings) {
	cmd.Flags().
		VarP(&imageSettings.Build.BuildType,
			"build-type", "b",
			fmt.Sprintf("The build type (%v).", common.GetAllBuildTypes()),
		)

	cmd.Flags().
		BoolVar(&imageSettings.Push.Enable,
			"push", false,
			"If the image should be pushed to registry.")

	cmd.Flags().
		BoolVar(&imageSettings.Build.SkipBuild,
			"skip-build", false,
			"If the image should be not be build.")

	cmd.Flags().
		Var(&imageSettings.Push.RegistryType,
			"registry-type",
			fmt.Sprintf("The registry type specifying the registry to which the image is uploaded (%v).",
				registry.GetAllRegistryTypes()),
		)
	cmd.Flags().
		BoolVar(&imageSettings.Push.Force,
			"force", false,
			"Under the following condition you can force an upload\n"+
				"because we disallow the push to prevent accidents of overwriting:\n"+
				"- Release registry + !UseReleaseTag + image exist\n"+
				"Pushing/overwriting to temporary registry is always allowed.",
		)

	cmd.Flags().
		BoolVar(&imageSettings.Push.UseReleaseTag,
			"use-release-tag", false,
			"If the image tag should be a release tag \n"+
				"(e.g. semantic version: '1.2.3' instead of '1.2.3-<git-hash>').\n"+
				"On any other registry than 'release' this is ignored.")

	cmd.Flags().
		StringVar(&imageSettings.Push.CredentialsEnv.UserEnv,
			"credential-user-env", "",
			"The username environment variable for the registry to upload the image.")
	cmd.Flags().
		StringVar(&imageSettings.Push.CredentialsEnv.TokenEnv,
			"credential-token-env", "",
			"The token environment variable for the registry to upload the image.")
}

func execute(cl cli.ICLI, _ *config.ImageSettings, args *imgArgs) error {
	comps, all, rootDir, err := cl.FindComponents(&args.compArgs)
	if err != nil {
		return err
	}

	_, prios, err := dag.DefineExecutionOrderStage(all, comps, stage.Stage("image"), nil, rootDir)
	if err != nil {
		return err
	}

	var dispatcher toolchain.IDispatcher
	if !cl.RootArgs().SkipToolchainDispatch {
		dispatcher = cl.ToolchainDispatcher()
	}

	return dag.ExecuteDAG(
		prios,
		cl.RunnerFactory(),
		dispatcher,
		cl.Config(),
		rootDir,
	)
}

type imgTypesParseWrapper struct {
	values *[]image.ImageType
}

// Implementing pflag.Value interface.
func (i *imgTypesParseWrapper) String() string {
	return fmt.Sprintf("%q", *i.values)
}

// Implementing pflag.Value interface.
func (i *imgTypesParseWrapper) Set(s string) error {
	for _, v := range strings.Split(s, ",") {
		vv, err := image.NewImageType(strings.TrimSpace(v))
		if err != nil {
			return err
		}
		*i.values = append(*i.values, vv)
	}

	return nil
}

// Implementing pflag.Value interface.
func (i *imgTypesParseWrapper) Type() string {
	return "string"
}
