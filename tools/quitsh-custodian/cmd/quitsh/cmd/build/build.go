package build

import (
	"custodian/tools/quitsh-custodian/pkg/runner/config"
	"fmt"

	"github.com/sdsc-ordes/quitsh/pkg/cli"
	"github.com/sdsc-ordes/quitsh/pkg/cli/general"
	"github.com/sdsc-ordes/quitsh/pkg/common"
	"github.com/sdsc-ordes/quitsh/pkg/component/stage"
	"github.com/sdsc-ordes/quitsh/pkg/dag"

	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
)

type buildArgs struct {
	compArgs general.ComponentArgs
}

const longDescBuild = `
Build a component matching them by name patterns (glob).
`

func AddCmd(
	cl cli.ICLI,
	buildSettings *config.BuildSettings) {
	var args buildArgs

	buildCmd := &cobra.Command{
		Use:   "build",
		Short: "Build components.",
		Long:  longDescBuild,
		RunE: func(cmd *cobra.Command, _args []string) error {
			return runBuild(cl, cmd, &args, buildSettings)
		},
	}

	buildCmd.Flags().
		VarP(&buildSettings.BuildType,
			"build-type", "b",
			fmt.Sprintf("The build type (set by env. type if not set) (%v).", common.GetAllBuildTypes()),
		)
	buildCmd.Flags().
		VarP(&buildSettings.EnvironmentType,
			"env-type", "e", fmt.Sprintf("The environment type. (%s)", common.GetEnvTypesHelp()))

	buildCmd.Flags().
		BoolVar(&buildSettings.Coverage,
			"coverage", false, "Enable coverage for the build.")

	buildCmd.Flags().
		StringArrayVarP(&args.compArgs.ComponentPatterns,
			"components", "c", nil, "Components matched by these patterns are built.")
	buildCmd.Flags().
		StringVar(&args.compArgs.ComponentDir,
			"component-dir", "", "Directory pointing to a component to build, instead of giving them by patterns.")

	buildCmd.MarkFlagsMutuallyExclusive("components", "component-dir")
	buildCmd.MarkFlagsOneRequired("components", "component-dir")

	cl.RootCmd().AddCommand(buildCmd)
}

func adjustDefaultValues(cmd *cobra.Command, setts *config.BuildSettings) {
	// Adjust the `BuildType` if its not set.
	cmd.Flags().VisitAll(func(flag *pflag.Flag) {
		if flag.Name == "build-type" && !flag.Changed {
			setts.BuildType = common.NewBuildTypeFromEnv(setts.EnvironmentType)
		}
	})
}

func runBuild(
	cl cli.ICLI,
	cmd *cobra.Command,
	args *buildArgs,
	buildSettings *config.BuildSettings) error {
	adjustDefaultValues(cmd, buildSettings)

	comps, all, rootDir, err := cl.FindComponents(&args.compArgs)
	if err != nil {
		return err
	}

	_, prios, err := dag.DefineExecutionOrderStage(all, comps, stage.Stage("build"), nil, rootDir)
	if err != nil {
		return err
	}

	return dag.ExecuteDAG(
		prios,
		cl.RunnerFactory(),
		cl.ToolchainDispatcher(),
		cl.Config(),
		rootDir,
	)
}
