package test

import (
	"custodian/tools/quitsh-custodian/pkg/runner/config"
	"fmt"

	"github.com/sdsc-ordes/quitsh/pkg/cli"
	"github.com/sdsc-ordes/quitsh/pkg/cli/general"
	"github.com/sdsc-ordes/quitsh/pkg/common"
	"github.com/sdsc-ordes/quitsh/pkg/component/stage"
	"github.com/sdsc-ordes/quitsh/pkg/dag"
	"github.com/sdsc-ordes/quitsh/pkg/toolchain"

	"github.com/spf13/cobra"
)

type lintTest struct {
	compArgs general.ComponentArgs
}

const longDescTest = `
Lint a component matching them by name patterns (glob).
`

func AddCmd(
	cl cli.ICLI,
	testSettings *config.TestSettings) {
	var args lintTest

	testCmd := &cobra.Command{
		Use:   "test",
		Short: "Test components.",
		Long:  longDescTest,
		RunE: func(_cmd *cobra.Command, _args []string) error {
			return runBuild(cl, &args)
		},
	}

	testCmd.Flags().
		StringArrayVarP(&args.compArgs.ComponentPatterns,
			"components", "c", nil, "Components matched by these patterns are built.")
	testCmd.Flags().
		StringVar(&args.compArgs.ComponentDir,
			"component-dir", "", "Directory pointing to a component to build, instead of giving them by patterns.")

	testCmd.Flags().
		VarP(&testSettings.BuildType,
			"build-type", "b",
			fmt.Sprintf("The build type (set by env. type if not set) (%v).", common.GetAllBuildTypes()),
		)
	testCmd.Flags().
		BoolVar(&testSettings.ShowTestLog,
			"show-test-log", false, "Show the log of the tests.")

	testCmd.MarkFlagsMutuallyExclusive("components", "component-dir")
	testCmd.MarkFlagsOneRequired("components", "component-dir")

	cl.RootCmd().AddCommand(testCmd)
}

func runBuild(
	cl cli.ICLI,
	args *lintTest) error {
	comps, all, rootDir, err := cl.FindComponents(&args.compArgs)
	if err != nil {
		return err
	}

	_, prios, err := dag.DefineExecutionOrderStage(
		all, comps,
		stage.Stage("test"),
		nil, rootDir)

	if err != nil {
		return err
	}

	var dispatcher toolchain.IDispatcher
	if !cl.RootArgs().SkipToolchainDispatch {
		dispatcher = cl.ToolchainDispatcher()
	}

	return dag.ExecuteDAG(
		prios,
		cl.RunnerFactory(),
		dispatcher,
		cl.Config(),
		rootDir,
	)
}
