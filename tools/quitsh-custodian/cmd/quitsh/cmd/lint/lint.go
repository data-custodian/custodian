package lint

import (
	"github.com/sdsc-ordes/quitsh/pkg/cli"
	"github.com/sdsc-ordes/quitsh/pkg/cli/general"
	"github.com/sdsc-ordes/quitsh/pkg/component/stage"
	"github.com/sdsc-ordes/quitsh/pkg/dag"
	"github.com/sdsc-ordes/quitsh/pkg/toolchain"

	"github.com/spf13/cobra"
)

type lintArgs struct {
	compArgs general.ComponentArgs
}

const longDescLint = `
Lint a component matching them by name patterns (glob).
`

func AddCmd(cli cli.ICLI) {
	var args lintArgs

	lintCmd := &cobra.Command{
		Use:   "lint",
		Short: "Lint components.",
		Long:  longDescLint,
		RunE: func(_cmd *cobra.Command, _args []string) error {
			return runBuild(cli, &args)
		},
	}

	lintCmd.Flags().
		StringArrayVarP(&args.compArgs.ComponentPatterns,
			"components", "c", nil, "Components matched by these patterns are built.")
	lintCmd.Flags().
		StringVar(&args.compArgs.ComponentDir,
			"component-dir", "", "Directory pointing to a component to build, instead of giving them by patterns.")

	lintCmd.MarkFlagsMutuallyExclusive("components", "component-dir")
	lintCmd.MarkFlagsOneRequired("components", "component-dir")

	cli.RootCmd().AddCommand(lintCmd)
}

func runBuild(
	cl cli.ICLI,
	args *lintArgs) error {
	comps, all, repoRoot, err := cl.FindComponents(&args.compArgs)
	if err != nil {
		return err
	}

	_, prios, err := dag.DefineExecutionOrderStage(
		all, comps,
		stage.Stage("lint"),
		nil, repoRoot)

	if err != nil {
		return err
	}

	var dispatcher toolchain.IDispatcher
	if !cl.RootArgs().SkipToolchainDispatch {
		dispatcher = cl.ToolchainDispatcher()
	}

	return dag.ExecuteDAG(
		prios,
		cl.RunnerFactory(),
		dispatcher,
		cl.Config(),
		repoRoot,
	)
}
