package registry

import (
	regcleanup "custodian/tools/quitsh-custodian/cmd/quitsh/cmd/registry/clean-up"
	"errors"

	"github.com/spf13/cobra"
)

// AddCmd adds the `registry` subcommands to `root`.
func AddCmd(root *cobra.Command) {
	regCmd := &cobra.Command{
		Use:   "registry",
		Short: "Helper commands for the registry. ",
		RunE: func(cmd *cobra.Command, _args []string) error {
			_ = cmd.Help()

			return errors.New("no command given")
		},
	}

	regcleanup.AddCmd(regCmd)

	root.AddCommand(regCmd)
}
