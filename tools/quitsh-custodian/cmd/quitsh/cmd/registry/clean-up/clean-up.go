package regcleanup

import (
	"regexp"
	"slices"
	"strings"
	"sync"

	cnGitlab "custodian/tools/quitsh-custodian/pkg/custodian/gitlab"

	"github.com/sdsc-ordes/quitsh/pkg/errors"
	"github.com/sdsc-ordes/quitsh/pkg/log"
	"github.com/sdsc-ordes/quitsh/pkg/registry"

	"deedles.dev/xiter"
	gitlab "gitlab.com/gitlab-org/api/client-go"

	"github.com/go-playground/validator/v10"
	"github.com/spf13/cobra"
)

const longDescCleanup = `
Clean up the registry.
`

type patternConfig struct {
	includes []string `yamL:"includeRegexes"`
	excludes []string `yamL:"excludeRegexes"`
}

type pattern struct {
	full *regexp.Regexp

	// This is only the pattern part for the repository.
	repo *regexp.Regexp

	hasTagPattern bool
}

type cleanUpSetts struct {
	ProjectID int `validate:"required"`

	RegistryType registry.RegistryType
	Patterns     patternConfig

	TokenEnv string

	Force bool
}

func AddCmd(root *cobra.Command) {
	var setts cleanUpSetts

	ciCmd := &cobra.Command{
		Use:   "container-cleanup",
		Short: "Clean up the container registry.",
		Long:  longDescCleanup,
		RunE: func(_cmd *cobra.Command, _args []string) error {
			return runCleanUp(&setts)
		},
	}

	hint := "They get first matched against registry urls (`registry.gitlab.com/organization/project/bla`)\n" +
		"in which case the whole repository is delete."

	ciCmd.Flags().
		StringArrayVarP(&setts.Patterns.excludes,
			"exclude-regexes",
			"e",
			nil,
			"Regex patterns to match images to not delete.\n"+hint,
		)

	ciCmd.Flags().
		StringArrayVarP(&setts.Patterns.includes,
			"include-regexes",
			"i",
			nil,
			"Regex patterns to match images to delete.",
		)

	ciCmd.Flags().
		IntVarP(&setts.ProjectID,
			"project-id",
			"p",
			-1,
			"Project id (see gitlab.com).",
		)
	_ = ciCmd.MarkFlagRequired("project-id")

	ciCmd.Flags().
		BoolVar(&setts.Force,
			"force", false,
			"Enabling this does not do a dry-run and exit but actually cleans up.",
		)

	ciCmd.Flags().
		StringVar(&setts.TokenEnv,
			"credential-token-env", "",
			"The token environment variable for the registry to upload the image.")

	root.AddCommand(ciCmd)
}

type IncludeRe []pattern
type ExcludeRe []pattern

func compileRegexes(
	includes []string,
	excludes []string,
) (IncludeRe, ExcludeRe, error) {
	var incls IncludeRe
	var excls ExcludeRe

	compileRe := func(s string) (*regexp.Regexp, error) {
		// Match always from the start to end!
		if !strings.HasPrefix(s, "^") {
			s = "^" + s
		}
		if !strings.HasSuffix(s, "$") {
			s += "$"
		}
		r, e := regexp.Compile(s)
		if e != nil {
			return nil, errors.AddContext(e, "regex '%s' does not compile", r.String())
		}

		return r, nil
	}

	split := func(s string) (repo, full *regexp.Regexp, hasTag bool, err error) {
		full, e := compileRe(s)
		err = errors.Combine(err, e)

		colon := strings.LastIndex(s, ":")
		hasTag = colon >= 0

		// if we dont have a ":" take everything as its repo pattern.
		if colon < 0 {
			colon = len(s)
		}
		log.Debug("Compile repo regex.", "re", s[0:colon])
		repo, e = compileRe(s[0:colon])
		err = errors.Combine(err, e)

		return
	}

	for i := range includes {
		repo, full, hasTag, err := split(includes[i])
		if err != nil {
			return nil, nil, err
		}
		incls = append(incls, pattern{repo: repo, full: full, hasTagPattern: hasTag})
	}

	for i := range excludes {
		repo, full, hasTag, err := split(excludes[i])
		if err != nil {
			return nil, nil, err
		}
		excls = append(excls, pattern{repo: repo, full: full, hasTagPattern: hasTag})
	}

	return incls, excls, nil
}

type Client = gitlab.Client
type Tag = gitlab.RegistryRepositoryTag
type Repo = gitlab.RegistryRepository
type Response = gitlab.Response

type D struct {
	ID              int
	Location        string
	CreatedAt       string
	TagsCount       int
	TagsCountDelete int
	tagsDelete      []string
}

// TODO: Reduce cognitive complexity here.
//
//nolint:gocognit
func runCleanUp(setts *cleanUpSetts) error { //nolint:funlen
	err := validator.New().Struct(setts)
	if err != nil {
		return err
	}

	git, err := cnGitlab.NewClient(setts.TokenEnv)
	if err != nil {
		return err
	}

	log.Info("Getting all repositories in project")

	log.Info("Regex Patterns",
		"includes", setts.Patterns.includes,
		"excludes", setts.Patterns.excludes)
	incls, excls, err := compileRegexes(setts.Patterns.includes, setts.Patterns.excludes)
	if err != nil {
		return err
	}

	get := func(opt *gitlab.ListRegistryRepositoriesOptions) (
		[]*Repo, *Response, error) {
		return git.ContainerRegistry.ListProjectRegistryRepositories(setts.ProjectID, opt)
	}

	opt := &gitlab.ListRegistryRepositoriesOptions{}
	repos, err := cnGitlab.CollectAll(get, opt, &opt.ListOptions)
	if err != nil {
		return errors.AddContext(
			err,
			"could not get container repositories for project id '%v'",
			setts.ProjectID,
		)
	}

	// Filter non-delete scheduled repositories.
	repos = slices.Collect(
		xiter.Filter(slices.Values(repos), func(r *Repo) bool {
			if r.Status == nil {
				return true
			}

			return *r.Status != gitlab.ContainerRegistryStatusDeleteScheduled &&
				*r.Status != gitlab.ContainerRegistryStatusDeleteOngoing
		}),
	)

	log.Info("Repositories:", "count", len(repos))

	repos = filter(
		repos,
		incls,
		excls,
		func(r *Repo) string { return r.Location },
		true,
	)

	log.Info("Repositories filtered:", "count", len(repos))

	data := []D{}
	for i := range repos {
		d := D{
			ID:        repos[i].ID,
			Location:  repos[i].Location,
			CreatedAt: repos[i].CreatedAt.String(),
		}

		get := func(opt *gitlab.ListRegistryRepositoryTagsOptions) (
			[]*Tag, *Response, error) {
			return git.ContainerRegistry.ListRegistryRepositoryTags(setts.ProjectID, d.ID, opt)
		}

		opt := &gitlab.ListRegistryRepositoryTagsOptions{}
		tags, e := cnGitlab.CollectAll(get, opt, (*gitlab.ListOptions)(opt))
		if e != nil {
			return errors.AddContext(
				e,
				"could not get all tags in container repositories '%s' for project id '%v'",
				d.Location,
				setts.ProjectID,
			)
		}

		d.TagsCount = len(tags)
		if d.TagsCount == 0 {
			log.Debug("Repository contains not tags. Skip.", "location", d.Location)
		} else {
			tags = filter(
				tags,
				incls,
				excls,
				func(t *Tag) string {
					return t.Location
				},
				false,
			)

			d.tagsDelete = slices.Collect(
				xiter.Map(slices.Values(tags), func(t *Tag) string { return t.Name }),
			)
			log.Debug("Repository tags to delete.", "tags", d.tagsDelete)

			d.TagsCountDelete = len(d.tagsDelete)
		}

		data = append(data, d)
	}

	var reposToDelete []D
	var tagsToDelete []D

	for _, d := range data {
		if d.TagsCount == 0 || len(d.tagsDelete) == d.TagsCount {
			d.tagsDelete = nil
			reposToDelete = append(reposToDelete, d)
		} else if len(d.tagsDelete) != 0 {
			tagsToDelete = append(tagsToDelete, d)
		}
	}

	if setts.Force {
		log.Info("Going to FULLY delete repositories:", "repos", reposToDelete)

		e := deleteRepos(setts, git, reposToDelete)
		err = errors.Combine(err, e)
	} else {
		log.Info("[Dry Run] Would fully delete the following repositories:", "repos", reposToDelete)
	}

	if setts.Force {
		log.Info("Going to delete tags in repositories:", "repos", tagsToDelete)

		e := deleteTags(setts, git, tagsToDelete)
		err = errors.Combine(err, e)
	} else {
		log.Info("[Dry Run] Would delete tags in repositories:", "repos", tagsToDelete)
	}

	return err
}

func deleteTags(setts *cleanUpSetts, git *gitlab.Client, repos []D) (err error) {
	if !setts.Force {
		log.Info("[Dry Run] Use '--force' to delete this matches.")

		return nil
	}

	log.Info("Batch delete tags in repositories.", "count", len(repos))

	deleteTags := func(repo *D) {
		var wg sync.WaitGroup
		var mu sync.Mutex

		for _, tag := range repo.tagsDelete {
			wg.Add(1)

			go func() {
				defer wg.Done()
				resp, e := git.ContainerRegistry.DeleteRegistryRepositoryTag(
					setts.ProjectID,
					repo.ID,
					tag,
				)

				if e != nil || !cnGitlab.ResponseOk(resp.Response) {
					e = errors.AddContext(e,
						"could not delete tag '%s' (repo: '%v', id: '%v'), response: '%s'",
						tag,
						repo.Location,
						repo.ID,
						resp.Status)
					mu.Lock()
					err = errors.Combine(err, e)
					mu.Unlock()
				}
			}()
		}

		wg.Wait()
	}

	for _, repo := range repos {
		log.Info("Deleting repository tags.", "location", repo.Location, "repo", repo)
		deleteTags(&repo)
	}

	return
}

func deleteRepos(setts *cleanUpSetts, git *gitlab.Client, repos []D) (err error) {
	if !setts.Force {
		log.Info("[Dry Run] Use '--force' to delete this matches.")

		return nil
	}

	log.Info("Batch delete repositories.", "count", len(repos))
	for _, repo := range repos {
		log.Info("Deleting repository", "location", repo.Location, "repo", repo)

		resp, e := git.ContainerRegistry.DeleteRegistryRepository(setts.ProjectID, repo.ID)
		if e != nil || !cnGitlab.ResponseOk(resp.Response) {
			return errors.Combine(
				e,
				errors.New("could not delete repository '%s' (id: '%v'), response: '%s'",
					repo.Location,
					repo.ID,
					resp.Status),
			)
		}
	}

	return
}

// filter filters `refs` by include and exclude patterns.
//
//nolint:gocognit
func filter[T any](
	refs []T,
	incls IncludeRe,
	excls ExcludeRe,
	key func(T) string,
	onlyRepo bool,
) (res []T) {
	getRe := func(p pattern) *regexp.Regexp {
		if onlyRepo {
			return p.repo
		} else {
			return p.full
		}
	}

	for i := range refs {
		include := false
		exclude := false

		k := key(refs[i])

		for _, r := range incls {
			if getRe(r).MatchString(k) {
				include = true

				break
			}
		}

		for _, r := range excls {
			if onlyRepo && r.hasTagPattern {
				// If the exclude pattern has an additional tag pattern it
				// should not count as exclude already.
				// but later.
				continue
			}

			if getRe(r).MatchString(k) {
				exclude = true

				break
			}
		}

		if include && !exclude {
			res = append(res, refs[i])
		}
	}

	return res
}
