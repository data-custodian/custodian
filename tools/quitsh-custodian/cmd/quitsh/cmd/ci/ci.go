package ci

import (
	cigeneratejobs "custodian/tools/quitsh-custodian/cmd/quitsh/cmd/ci/generate-jobs"
	cimerge "custodian/tools/quitsh-custodian/cmd/quitsh/cmd/ci/merge"
	citriggermrs "custodian/tools/quitsh-custodian/cmd/quitsh/cmd/ci/trigger-merge-requests"
	"errors"

	"github.com/sdsc-ordes/quitsh/pkg/cli"

	"github.com/spf13/cobra"
)

// AddCmd adds the `ci` subcommands to `root`.
func AddCmd(cl cli.ICLI) {
	ciCmd := &cobra.Command{
		Use:   "ci",
		Short: "Helper commands for CI. ",
		RunE: func(cmd *cobra.Command, _args []string) error {
			_ = cmd.Help()

			return errors.New("no command given")
		},
	}

	cimerge.AddCmd(ciCmd)
	cigeneratejobs.AddCmd(cl, ciCmd)
	citriggermrs.AddCmd(ciCmd)

	cl.RootCmd().AddCommand(ciCmd)
}
