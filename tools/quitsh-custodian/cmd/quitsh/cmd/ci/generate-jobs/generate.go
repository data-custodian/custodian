package cigeneratejobs

import (
	"custodian/tools/quitsh-custodian/pkg/custodian/pipeline"

	"github.com/sdsc-ordes/quitsh/pkg/cli"

	"github.com/spf13/cobra"
)

const longDescGenerateJobs = `
Generate CI jobs for building.
`

type generateArgs struct {
	pipelineType         string
	output               string
	pipelineSettingsFile string
}

func AddCmd(cl cli.ICLI, root *cobra.Command) {
	var args generateArgs

	ciCmd := &cobra.Command{
		Use:   "generate-pipeline",
		Short: "Generate CI jobs pipeline.",
		Long:  longDescGenerateJobs,
		RunE: func(_cmd *cobra.Command, _args []string) error {
			return generateJobs(cl, &args)
		},
	}

	ciCmd.Flags().
		StringVarP(&args.pipelineType,
			"type", "t",
			"gitlab",
			"The pipeline type to generate jobs for. [gitlab]",
		)

	ciCmd.Flags().
		StringVarP(&args.output,
			"output", "o",
			"generated-jobs.yaml",
			"The output file to write the pipeline jobs.",
		)

	ciCmd.Flags().
		StringVarP(&args.pipelineSettingsFile,
			"pipeline-settings", "p",
			"pipeline-settings.yaml",
			"The serialization file (YAML) with the pipeline settings\n."+
				"You get this file from the pipeline artifact.",
		)

	root.AddCommand(ciCmd)
}

func generateJobs(cl cli.ICLI, args *generateArgs) error {
	return pipeline.Generate(cl, args.pipelineType, args.output, args.pipelineSettingsFile)
}
