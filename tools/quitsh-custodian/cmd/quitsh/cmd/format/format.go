package format

import (
	"custodian/tools/quitsh-custodian/pkg/custodian"
	"path/filepath"

	"github.com/sdsc-ordes/quitsh/pkg/errors"
	"github.com/sdsc-ordes/quitsh/pkg/exec/git"
	"github.com/sdsc-ordes/quitsh/pkg/exec/nix"
	fs "github.com/sdsc-ordes/quitsh/pkg/filesystem"
	"github.com/sdsc-ordes/quitsh/pkg/log"

	"github.com/spf13/cobra"
)

type formatArgs struct {
	failOnChange bool
}

const longDesc = `
Format the repository with 'nix run treefmt' ->.
`

func AddCmd(root *cobra.Command) {
	var fmtArgs formatArgs

	formatCmd := &cobra.Command{
		Use:   "format [paths...]",
		Short: "Format the files in paths (default '.')",
		Long:  longDesc,
		RunE: func(_cmd *cobra.Command, args []string) error {
			return execute(&fmtArgs, args)
		},
	}

	formatCmd.Flags().
		BoolVarP(&fmtArgs.failOnChange,
			"fail-on-change", "c", false, "Fail if any changes were made.")

	root.AddCommand(formatCmd)
}

func execute(c *formatArgs, paths []string) error {
	_, rootDir, err := git.NewCtxAtRoot(".")
	if err != nil {
		return err
	}

	if len(paths) == 0 {
		paths = append(paths, rootDir)
	}

	workDir := filepath.FromSlash(custodian.GetNixFlakeDir(rootDir))

	// Make all pathss relative to the working dir.
	for i := range paths {
		absP := fs.MakeAbsolute(paths[i])
		if !fs.Exists(absP) {
			return errors.New("path '%s' does not exist'", absP)
		}

		relPath, e := filepath.Rel(workDir, absP)
		if e != nil {
			return e
		}

		paths[i] = relPath
	}

	log.Info("Formatting paths.")
	log.Debug("Paths", "paths", paths)

	nixctx := nix.NewCtxBuilder().Cwd(workDir).Build()

	cmd := []string{"run",
		"--accept-flake-config",
		".#treefmt"}
	cmd = append(cmd, "--")
	cmd = append(cmd, paths...)
	if c.failOnChange {
		cmd = append(cmd, "--fail-on-change")
	}

	err = nixctx.Check(cmd...)
	if err != nil {
		return err
	}

	return nil
}
