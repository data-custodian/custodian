//go:build tools

// This exectuable can be used in`//go:generate` statements
// to dynamically generate a version.

package main

import (
	generateversion "custodian/tools/quitsh-custodian/cmd/quitsh/cmd/generate-version"
	"os"

	"github.com/sdsc-ordes/quitsh/pkg/log"
)

func main() {
	c, _ := os.Getwd()
	log.Setup("info")

	log.Info("Generate version.", "cwd", c)
	data := generateversion.GenerateVersionData{OutputDir: os.Args[1]}
	err := generateversion.Execute(&data)
	if err != nil {
		log.PanicE(err, "could not generate version file")
	}
}
