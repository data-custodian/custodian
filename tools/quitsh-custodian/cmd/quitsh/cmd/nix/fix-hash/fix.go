package fixhash

import (
	"bytes"
	"custodian/tools/quitsh-custodian/pkg/custodian"
	"iter"
	"maps"
	"os"
	"slices"
	"strings"
	"sync"

	"github.com/sdsc-ordes/quitsh/pkg/cli"
	strs "github.com/sdsc-ordes/quitsh/pkg/common/strings"
	"github.com/sdsc-ordes/quitsh/pkg/errors"
	"github.com/sdsc-ordes/quitsh/pkg/exec"
	"github.com/sdsc-ordes/quitsh/pkg/exec/git"
	"github.com/sdsc-ordes/quitsh/pkg/exec/nix"
	fs "github.com/sdsc-ordes/quitsh/pkg/filesystem"
	"github.com/sdsc-ordes/quitsh/pkg/log"

	"github.com/spf13/cobra"
)

const longDescLint = `
Fix hashes of Nix's fixed-output-derivation on components.
`

func AddCmd(cli cli.ICLI, parent *cobra.Command) {
	failIfChanges := false

	fixHashCmd := &cobra.Command{
		Use:   "fix-hash",
		Short: "Fix hash on fixed-output-derivations.",
		Long:  longDescLint,
		RunE: func(_cmd *cobra.Command, _args []string) error {
			return fixHash(cli, failIfChanges)
		},
	}

	fixHashCmd.Flags().
		BoolVarP(&failIfChanges,
			"fail-on-change", "c", false, "Fail if any changes were made.")

	parent.AddCommand(fixHashCmd)
}

// fixHash will evaluate over all Nix packages and check
// on fixed-output derivation the `outputHash` and recomputes it (sets it to `lib.flakeHash`)
// re builds the derivation and get `hash mismatch` errors to
// assemble a list of `old-hash -> new-hash` replacements over all Nix files in the repository
// This is a workaround to be able to easily update hashes like the `vendorHash`
// for example on `buildGoModule`:
//
// To check the outputHash on `goModules` fixed-output derivation:
// ```shell
// nix eval --expr "
//
//		let
//		  f = builtins.getFlake
//	       "git+file:///persist/repos/custodian?dir=tools/nix&ref=$(git branch --show)" +
//	       "&rev=$(git rev-parse HEAD)";
//
//		  drv = f.outputs.packages.x86_64-linux.quitsh;
//		in
//		drv.goModules.outputHash or null
//
// " --impure
// ```
// and to build the derivation with empty hash:
//
// ```shell
// nix eval --expr "
//
//		let
//		  f = builtins.getFlake
//	       "git+file:///persist/repos/custodian?dir=tools/nix&ref=$(git branch --show)" +
//	       "&rev=$(git rev-parse HEAD)";
//
//		  drv = f.outputs.packages.x86_64-linux.quitsh;
//		in
//		drv.goModules.overrideAttrs (f: b: { outputHash = \"sha256-AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=\"; })
//
// " --impure
// ```.
func fixHash(cli cli.ICLI, failIfChanges bool) error {
	rootDir := cli.RootDir()
	gitx := git.NewCtx(rootDir)
	nixx := nix.NewCtxBuilder().Cwd(rootDir).Build()

	packages, err := nix.GetFlakePackages(nixx, custodian.GetNixFlakeDir(rootDir))
	if err != nil {
		return err
	}

	hashOldNew := make(map[string]string)

	err = addGoModulesReplacements(gitx, nixx, packages, hashOldNew)
	if err != nil {
		return err
	}

	err = searchAndReplace(rootDir, hashOldNew)
	if err != nil {
		return err
	}

	if len(hashOldNew) != 0 && failIfChanges {
		return errors.New("not all hashes in files are up to date")
	}

	return nil
}

const getOutputHash = `
	let
	    f = builtins.getFlake "git+file:///{{ .RepoPath }}?dir={{ .FlakeRelDir }}";
	    drv = f.outputs.{{ .AttrPath }};
	in
	drv.goModules.outputHash or "none"
	`

const buildFixedOutputDrv = `
	let
	    f = builtins.getFlake "git+file:///{{ .RepoPath }}?dir={{ .FlakeRelDir }}";
	    drv = f.outputs.{{ .AttrPath }};
	in
	drv.goModules.overrideAttrs (f: b: { outputHash = "sha256-AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA="; })
	`

func addGoModulesReplacements(
	gitx git.Context,
	nixx *exec.CmdContext,
	packages map[string]nix.Package,
	hashOldNew map[string]string,
) error {
	hasChanges, err := gitx.HasChanges(gitx.Cwd())
	if err != nil {
		return err
	}

	if hasChanges {
		log.Error("You must have not unstaged changes before running this function!")

		return errors.New("have unstaged changes")
	}

	updateMutex := sync.Mutex{}

	fix := func(p nix.Package) error {
		data := map[string]string{
			"AttrPath":    p.AttrPath,
			"RepoPath":    gitx.Cwd(),
			"FlakeRelDir": custodian.FlakeDirRel,
		}

		oldHash, e := nix.EvalTemplate(
			nixx,
			getOutputHash,
			data,
			nix.WithEvalImpure(),
			nix.WithEvalOutputRaw(),
		)
		if e != nil {
			return e
		}

		if oldHash == "none" {
			log.Info("Package has no 'goModules' derivation -> Skip", "package", p.AttrPath)

			return nil
		}

		log.Info("Build fixed-output derivation to get new hash", "package", p.AttrPath)

		res, e := exec.WithTemplate(
			nixx,
			buildFixedOutputDrv,
			data,
			func(c *exec.CmdContext, file string) (string, error) {
				return c.GetCombined("build", "--file", file)
			},
		)
		if e == nil || !strings.Contains(res, "mismatch") || !strings.Contains(res, "hash") {
			return errors.New(
				"Building with empty hash should result in an hash mismatch error!, out:\n%s",
				res,
			)
		}

		var newHash string
		for _, l := range strs.SplitLines(res) {
			if strings.Contains(l, "got:") {
				newHash = strings.TrimSpace(strings.Replace(l, "got:", "", 1))
			}
		}

		if oldHash != newHash {
			log.Warn(
				"Fixed output derivation requires new hash.",
				"package",
				p.AttrPath,
				"oldHash",
				oldHash,
				"newHash",
				newHash,
			)

			updateMutex.Lock()
			defer updateMutex.Unlock()

			hashOldNew[oldHash] = newHash
		} else {
			log.Info("Fixed output derivation has current hash.", "package", p.AttrPath, "hash", newHash)
		}

		return nil
	}

	return runConcurrent(maps.Values(packages), fix)
}

func searchAndReplace(rootDir string, hashOldNew map[string]string) error {
	if len(hashOldNew) == 0 {
		return nil
	}

	log.Info("Hashes to update:", "hashes", hashOldNew)

	files, _, err := fs.FindFiles(
		rootDir,
		[]string{"*.nix"},
		nil,
		fs.WithMatchFileNameOnly(),
		fs.WithPathFilterDefault(),
	)
	if err != nil {
		return errors.AddContext(err, "Could not find all Nix files")
	}

	log.Info("Replace over Nix fs.", "count", len(files))

	correct := func(file string) error {
		bs, e := os.ReadFile(file)
		if e != nil {
			return e
		}

		replaced := false
		for old, new := range hashOldNew {
			sub := []byte(old)
			idx := bytes.Index(bs, sub)
			if idx >= 0 {
				replaced = true
				copy(bs[idx:idx+len(sub)], []byte(new))
			}
		}

		if replaced {
			err = os.WriteFile(file, bs, fs.DefaultPermissionsFile)
			if err != nil {
				return err
			}
		}

		return e
	}

	return runConcurrent(slices.Values(files), correct)
}

// runConcurrent runs over all items in parallel and collects errors.
func runConcurrent[T any](items iter.Seq[T], run func(p T) error) error {
	wg := sync.WaitGroup{}
	errChan := make(chan error)

	// Process in parallel.
	for p := range items {
		wg.Add(1)
		go func() {
			defer wg.Done()
			errChan <- run(p)
		}()
	}

	// Wait on a separate Go routine, for finishing.
	// The close the channel, that the below code, finishes.
	go func() {
		wg.Wait()
		close(errChan)
	}()

	var err error
	for e := range errChan {
		if e != nil {
			err = errors.Combine(err, e)
		}
	}

	return err
}
