package nix

import (
	fixhash "custodian/tools/quitsh-custodian/cmd/quitsh/cmd/nix/fix-hash"
	"errors"

	"github.com/sdsc-ordes/quitsh/pkg/cli"

	"github.com/spf13/cobra"
)

// AddCmd adds the `ci` subcommands to `root`.
func AddCmd(cl cli.ICLI) {
	nixCmd := &cobra.Command{
		Use:   "nix",
		Short: "Helper commands for Nix. ",
		RunE: func(cmd *cobra.Command, _args []string) error {
			_ = cmd.Help()

			return errors.New("no command given")
		},
	}

	fixhash.AddCmd(cl, nixCmd)

	cl.RootCmd().AddCommand(nixCmd)
}
