package cmd

import (
	cnBuildCmd "custodian/tools/quitsh-custodian/cmd/quitsh/cmd/build"
	cnCICmd "custodian/tools/quitsh-custodian/cmd/quitsh/cmd/ci"
	cnConfigImgCmd "custodian/tools/quitsh-custodian/cmd/quitsh/cmd/config/image"
	cnFormatCmd "custodian/tools/quitsh-custodian/cmd/quitsh/cmd/format"
	cnGenVersionCmd "custodian/tools/quitsh-custodian/cmd/quitsh/cmd/generate-version"
	cnImageCmd "custodian/tools/quitsh-custodian/cmd/quitsh/cmd/image"
	cnLintCmd "custodian/tools/quitsh-custodian/cmd/quitsh/cmd/lint"
	cnManifestCmd "custodian/tools/quitsh-custodian/cmd/quitsh/cmd/manifest"
	cnNix "custodian/tools/quitsh-custodian/cmd/quitsh/cmd/nix"
	cnRegistryCmd "custodian/tools/quitsh-custodian/cmd/quitsh/cmd/registry"
	cnSetupCmd "custodian/tools/quitsh-custodian/cmd/quitsh/cmd/setup"
	cnTestCmd "custodian/tools/quitsh-custodian/cmd/quitsh/cmd/test"
	"custodian/tools/quitsh-custodian/pkg/config"

	"github.com/sdsc-ordes/quitsh/pkg/cli"
	cleanCmd "github.com/sdsc-ordes/quitsh/pkg/cli/cmd/clean"
	configCmd "github.com/sdsc-ordes/quitsh/pkg/cli/cmd/config"
	execRunnerCmd "github.com/sdsc-ordes/quitsh/pkg/cli/cmd/exec-runner"
	execTargetCmd "github.com/sdsc-ordes/quitsh/pkg/cli/cmd/exec-target"
	listCmd "github.com/sdsc-ordes/quitsh/pkg/cli/cmd/list"
)

func AddCommands(cl cli.ICLI, conf *config.Config) {
	// Commands from Quitsh
	confCmd := configCmd.AddCmd(cl.RootCmd(), conf)
	cnConfigImgCmd.AddCmd(cl, confCmd, &conf.Image)

	execRunnerCmd.AddCmd(cl, cl.RootCmd(), &conf.Commands.DispatchArgs)
	execTargetCmd.AddCmd(cl, cl.RootCmd())
	listCmd.AddCmd(cl, cl.RootCmd())
	cleanCmd.AddCmd(cl)

	// Own customized commands.
	cnSetupCmd.AddCmd(cl.RootCmd())

	cnBuildCmd.AddCmd(cl, &conf.Build)
	cnLintCmd.AddCmd(cl)
	cnTestCmd.AddCmd(cl, &conf.Test)
	cnImageCmd.AddCmd(cl, &conf.Image)
	cnManifestCmd.AddCmd(cl, &conf.Manifest)

	cnNix.AddCmd(cl)
	cnRegistryCmd.AddCmd(cl.RootCmd())
	cnCICmd.AddCmd(cl)
	cnFormatCmd.AddCmd(cl.RootCmd())
	cnGenVersionCmd.AddCmd(cl.RootCmd())
}
