package manifest

import (
	"custodian/tools/quitsh-custodian/pkg/runner/config"
	"fmt"

	"github.com/sdsc-ordes/quitsh/pkg/cli"
	"github.com/sdsc-ordes/quitsh/pkg/cli/general"
	"github.com/sdsc-ordes/quitsh/pkg/common"
	"github.com/sdsc-ordes/quitsh/pkg/component/stage"
	"github.com/sdsc-ordes/quitsh/pkg/dag"
	"github.com/spf13/cobra"
)

type renderArgs struct {
	compArgs general.ComponentArgs
}

func AddCmd(cl cli.ICLI, conf *config.ManifestSettings) {
	var args renderArgs
	renderCmd := &cobra.Command{
		Use:     "manifest",
		Aliases: []string{"render"},
		Short:   "Render the manifest file.",
		Long:    "Render the manifest file.",
		PreRunE: func(_cmd *cobra.Command, _args []string) error {
			return nil
		},
		RunE: func(_cmd *cobra.Command, _args []string) error {
			return execute(cl, &args)
		},
	}

	renderCmd.Flags().
		StringVarP(&conf.Domain, "domain", "d", "swisscustodian.ch", "The domain used for deployment.")
	renderCmd.Flags().
		VarP(&conf.EnvironmentType,
			"env-type", "e", fmt.Sprintf("The environment type. (%s)", common.GetEnvTypesHelp()))
	renderCmd.Flags().StringVarP(&conf.OutputDir,
		"output-dir",
		"o",
		"",
		"The directory used to store the rendered manifests.",
	)

	renderCmd.Flags().
		StringArrayVarP(&args.compArgs.ComponentPatterns,
			"components", "c", nil, "Components matched by these patterns are built.")

	cl.RootCmd().AddCommand(renderCmd)
}

func execute(cl cli.ICLI, args *renderArgs) error {
	comps, all, rootDir, err := cl.FindComponents(&args.compArgs)
	if err != nil {
		return err
	}

	_, prios, err := dag.DefineExecutionOrderStage(
		all,
		comps,
		stage.Stage("manifest"),
		nil,
		rootDir,
	)
	if err != nil {
		return err
	}

	return dag.ExecuteDAG(
		prios,
		cl.RunnerFactory(),
		cl.ToolchainDispatcher(),
		cl.Config(),
		rootDir,
	)
}
