package cngitlab

import (
	cnCI "custodian/tools/quitsh-custodian/pkg/custodian/ci"

	"github.com/sdsc-ordes/quitsh/pkg/ci"
	"github.com/sdsc-ordes/quitsh/pkg/common"
	"github.com/sdsc-ordes/quitsh/pkg/errors"
	gitlab "gitlab.com/gitlab-org/api/client-go"
)

// NewClient returns a Gitlab client.
func NewClient(token string) (git *gitlab.Client, err error) {
	creds, err := common.NewCredentialsTokenOnly(token)
	if err != nil {
		return
	}

	url := "https://" + cnCI.RepoDomain + "/api/v4"
	urlOpt := gitlab.WithBaseURL(url)

	if ci.IsRunning() {
		git, err = gitlab.NewJobClient(
			creds.Token(),
			urlOpt,
		)
	} else {
		git, err = gitlab.NewClient(
			creds.Token(),
			urlOpt,
		)
	}

	if err != nil {
		err = errors.AddContext(err,
			"could not create client for '%v'.", url)
	}

	return
}
