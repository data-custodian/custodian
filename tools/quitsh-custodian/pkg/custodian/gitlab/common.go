package cngitlab

import (
	"net/http"

	gitlab "gitlab.com/gitlab-org/api/client-go"
)

func ResponseOk(resp *http.Response) bool {
	return resp.StatusCode/100 == 2 //nolint:mnd
}

// CollectAll calls `getter` (HTTP request)
// by exhausting all pagination.
// The `listOpts` must be a subfield in `opt`.
func CollectAll[T any, O any](
	getter func(opt *O) ([]T, *gitlab.Response, error),
	opt *O, listOpts *gitlab.ListOptions) ([]T, error) {
	var list = []T{}

	listOpts.Page = 1
	listOpts.PerPage = 100

	for {
		res, resp, err := getter(opt)
		if err != nil {
			return nil, err
		}
		list = append(list, res...)

		// Exit the loop when we've seen all pages.
		if resp.NextPage == 0 {
			break
		}

		// Update the page number to get the next page.
		listOpts.Page = resp.NextPage
	}

	return list, nil
}
