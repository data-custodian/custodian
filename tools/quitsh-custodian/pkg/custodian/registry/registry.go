package custodianregistry

import (
	"path"
)

const custodianRegistry = "registry.gitlab.com"
const custodianImageBasePath = "data-custodian/custodian/nix"

// NewRegistryBaseName returns the image base name.
func NewRegistryBaseName() (domain string, baseName string) {
	domain = custodianRegistry
	baseName = path.Join(custodianRegistry, custodianImageBasePath)

	return
}
