package custodian

import "path"

const FlakeDirRel = "tools/nix"

// GetNixFlakeDir returns the flake directory
// in `tools/nix` relative to the root dir.
func GetNixFlakeDir(rootDir string) string {
	return path.Join(rootDir, FlakeDirRel)
}
