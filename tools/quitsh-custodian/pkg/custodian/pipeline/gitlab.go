package pipeline

import (
	"io"

	"github.com/sdsc-ordes/quitsh/pkg/component/stage"
	"github.com/sdsc-ordes/quitsh/pkg/dag"
	"github.com/sdsc-ordes/quitsh/pkg/log"

	"gopkg.in/yaml.v3"
)

type GitlabJobRun struct {
	Name   string `yaml:"name,omitempty"`
	Script string `yaml:"script"`
}

type GitlabJobArtifact struct {
	Paths []string `yaml:"paths,omitempty"`
}

type GitlabJob struct {
	TargetNode *dag.TargetNode `yaml:"-"`
	Name       string          `yaml:"-"`
	StageType  stage.Stage     `yaml:"-"`

	Tags  []string `yaml:"tags"`
	Stage string   `yaml:"stage"`
	Needs []string `yaml:"needs,omitempty"`

	BeforeScript []string `yaml:"before_script,omitempty"` //nolint:tagliatelle,nolintlint
	Script       []string `yaml:"script,omitempty"`
	// Currently not used, its anyway experimental.
	Run []GitlabJobRun `yaml:"run,omitempty"`

	Rules []*yaml.Node `yaml:"rules,omitempty"`
	When  string       `yaml:"when,omitempty"`

	Artifacts *GitlabJobArtifact `yaml:"artifacts,omitempty"`

	AllowFailure bool `yaml:"allow_failure,omitempty"` //nolint:tagliatelle,nolintlint
}

type GitlabJobMap map[string]GitlabJob

type GitlabPipeline struct {
	// Representing other custom stuff added to the yaml.
	other []*yaml.Node `yaml:"ignore"`

	Stages []string
	Jobs   map[string]*GitlabJob `yaml:",inline"`
}

// NewGitlabPipeline creates a new Gitlab Pipeline.
func NewGitlabPipeline() GitlabPipeline {
	return GitlabPipeline{
		Jobs:  make(map[string]*GitlabJob),
		other: GetGeneralDefaults()}
}

// Write writes the pipeline to the writer `w`.
func (p *GitlabPipeline) Write(w io.Writer) error {
	var document yaml.Node
	err := document.Encode(p)
	if err != nil {
		return err
	}

	document.HeadComment = "Automatically generated pipeline with `quitsh ci generate-pipeline`."
	document.Content = append(p.other, document.Content...)

	enc := yaml.NewEncoder(w)
	defer enc.Close()

	return enc.Encode(document)
}

// DefaultRules returns the default rule, which includes jobs on
// - a merge-request and
// - on the main branch and
// - on tags.
func DefaultRules() []*yaml.Node {
	rules := `
- if: "$CI_MERGE_REQUEST_ID || $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH || $CI_COMMIT_TAG"
`

	// Parse the compile-time YAML string into a yaml.Node
	var node yaml.Node
	err := yaml.Unmarshal([]byte(rules), &node)
	log.PanicE(err, "Cannot unmarshal inline YAML.")

	return node.Content[0].Content
}

// ReleaseRules returns the default rule when deploying should be active.
func ReleaseRules() []*yaml.Node {
	rules := `
- if: "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH || $CI_COMMIT_TAG =~ /v[0-9]+.[0-9]+[0-9]+.*/"
`

	// Parse the compile-time YAML string into a yaml.Node
	var node yaml.Node
	err := yaml.Unmarshal([]byte(rules), &node)
	log.PanicE(err, "Cannot unmarshal inline YAML.")

	return node.Content[0].Content
}

// GetDefaultRule returns the default rule, which includes jobs on a merge-request and
// on the main branch.
func GetGeneralDefaults() []*yaml.Node {
	rule := `
variables:
  FF_TIMESTAMPS: true
  FF_GITLAB_REGISTRY_HELPER_IMAGE: "true"
  GIT_DEPTH: 10
  GIT_SUBMODULE_STRATEGY: recursive
  GIT_SUBMODULE_FORCE_HTTPS: "true"

default:
  interruptible: true
`

	// Parse the compile-time YAML string into a yaml.Node
	var node yaml.Node
	err := yaml.Unmarshal([]byte(rule), &node)
	log.PanicE(err, "Cannot unmarshal inline YAML.")

	return node.Content[0].Content
}
