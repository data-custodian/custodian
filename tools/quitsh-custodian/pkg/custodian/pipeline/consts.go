package pipeline

const releaseBranch = "main"
const RegistryTokenEnv = "CI_REGISTRY_PASSWORD"
const RegistryUserEnv = "CI_REGISTRY_USER"

func IsReleaseBranch(branch string) bool {
	return branch == releaseBranch
}
