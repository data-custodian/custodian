package pipeline

import (
	"bytes"
	"fmt"
	"iter"
	"maps"
	"os"
	"path"
	"slices"

	"github.com/sdsc-ordes/quitsh/pkg/ci"
	"github.com/sdsc-ordes/quitsh/pkg/ci/pipeline"
	"github.com/sdsc-ordes/quitsh/pkg/cli"
	"github.com/sdsc-ordes/quitsh/pkg/cli/general"
	mapss "github.com/sdsc-ordes/quitsh/pkg/common/map"
	"github.com/sdsc-ordes/quitsh/pkg/common/set"
	"github.com/sdsc-ordes/quitsh/pkg/component"
	"github.com/sdsc-ordes/quitsh/pkg/component/stage"
	"github.com/sdsc-ordes/quitsh/pkg/dag"
	"github.com/sdsc-ordes/quitsh/pkg/errors"
	"github.com/sdsc-ordes/quitsh/pkg/exec/git"
	"github.com/sdsc-ordes/quitsh/pkg/exec/shell"
	fs "github.com/sdsc-ordes/quitsh/pkg/filesystem"
	"github.com/sdsc-ordes/quitsh/pkg/log"

	"deedles.dev/xiter"
)

func Generate(
	cl cli.ICLI,
	pipelineType string,
	outputFile string,
	pipelineSettingsFile string,
) error {
	gitx, _, err := git.NewCtxAtRoot(".")
	if err != nil {
		return err
	}

	if pipelineType == "gitlab" {
		return generateGitlabPipeline(cl, gitx, outputFile, pipelineSettingsFile)
	}

	return errors.New("Only 'gitlab' is currently supported as pipeline type.")
}

//nolint:funlen
func generateGitlabPipeline(
	cl cli.ICLI,
	gitx git.Context,
	outputFile string,
	pipelineSettingsFile string,
) error {
	log.Info("Generating pipeline ...")
	pl := NewGitlabPipeline()

	setts, err := LoadSettings(pipelineSettingsFile)
	if err != nil {
		return err
	}

	if ci.IsRunning() {
		log.Info("Pipeline Settings", "general", setts.General)
		log.Info("Custodian Attributes", "attributes", setts.Attrs)
		log.Info("Custodian Settings", "settings", setts.Custom)
	}

	var changes []string
	if !setts.Attrs.Build.Lazy {
		log.Info("Build all targets specified, no change detection.")
	} else {
		log.Info("Compute changes on pipeline.")
		changes, err = pipeline.GetChangesOnPipeline(gitx, &setts.General, "origin")
		if err != nil {
			return err
		}
		log.Info("Changes on pipeline.", "changes", changes)
	}

	versionTag := git.GetVersionFromTag(setts.General.Git.Ref)

	// Do we deploy (upload container images).
	doDeploy := (setts.General.Type == pipeline.BranchPipeline &&
		IsReleaseBranch(setts.General.Git.Ref)) ||
		(setts.General.Type == pipeline.TagPipeline &&
			versionTag != nil)

	// Is the deploy using full release versions.
	doRelease := doDeploy && versionTag != nil

	log.Info(
		"Deploy Status:",
		"versionTag",
		versionTag,
		"doDeploy",
		doDeploy,
		"doRelease",
		doRelease,
	)

	comps, _, rootDir, err := cl.FindComponents(
		&general.ComponentArgs{ComponentPatterns: []string{"*", "!component-a"}},
	)
	if err != nil {
		return err
	}

	jobs := generateGlobalJobs()
	if e := mapss.CopyUnique(pl.Jobs, jobs); e != nil {
		return errors.New("jobs with names '%v' already existing.", e)
	}

	log.Info("Generate jobs for comps.", "count", len(comps))
	jobs, e := generateComponentJobs(
		rootDir,
		comps,
		&setts,
		changes,
		doDeploy,
		doRelease,
	)
	if e != nil {
		return e
	}

	if e := mapss.CopyUnique(pl.Jobs, jobs); e != nil {
		return errors.New("jobs with names '%v' already existing.", e)
	}

	pl.Stages, err = generateStages(cl, pl.Jobs)
	if err != nil {
		return err
	}
	assignMissingStepNames(maps.Values(pl.Jobs))

	log.Info("Write pipeline file.", "path", outputFile)
	buf := bytes.NewBuffer(nil)
	if err = pl.Write(buf); err != nil {
		return err
	}

	if ci.IsRunning() {
		log.Info("Pipeline file content:", "content", "\n---\n"+buf.String()+"\n---\n")
	}

	err = os.MkdirAll(path.Dir(outputFile), fs.DefaultPermissionsFile)
	if err != nil {
		return err
	}

	err = os.WriteFile(outputFile, buf.Bytes(), fs.DefaultPermissionsFile)
	if err != nil {
		return err
	}

	return nil
}

// generateStages generates all used stage names (sorted according to their succession)
// from the jobs.
func generateStages(cl cli.ICLI, jobs map[string]*GitlabJob) ([]string, error) {
	log.Info("Generating stage names.")

	definedStages := append(
		cl.Stages(),
		stage.StagePrio{Stage: "cleanup", Priority: 100}, //nolint:mnd
	)

	allstages := set.NewUnordered[stage.StagePrio]()
	for i := range jobs {
		// Set the stage name for each job.
		jobs[i].Stage = jobs[i].StageType.String()

		pr, exists := definedStages.Find(jobs[i].StageType)
		if !exists {
			return nil, errors.New(
				"stage name '%v' for job '%v' does not exists in defined set '%v'",
				jobs[i].StageType,
				definedStages,
				jobs[i].Name,
			)
		}
		allstages.Insert(pr)
	}

	sorted := xiter.SortedFunc(allstages.Values(), func(a, b stage.StagePrio) int {
		// Sort first according to step, then according to stage name.
		return a.Less(&b)
	})

	allStages := slices.Collect(xiter.Map(
		sorted,
		func(s stage.StagePrio) string { return s.Stage.String() }))

	return allStages, nil
}

func generateGlobalJobs() map[string]*GitlabJob {
	log.Info("Generate global jobs (format).")

	defaultRules := DefaultRules()
	releaseRules := ReleaseRules()

	jobs := map[string]*GitlabJob{
		"format": {
			Tags: []string{"nix", "linux"},
			BeforeScript: []string{
				"just ci quitsh ci merge-to-target-branch",
			},
			Script: []string{
				"just ci quitsh setup",
				"just ci quitsh format --fail-on-change"},
			StageType: "lint",
			Rules:     defaultRules,
			When:      "always",
		},
		"nix-hashes (FOD)": {
			Tags: []string{"nix", "linux"},
			BeforeScript: []string{
				"just ci quitsh ci merge-to-target-branch",
			},
			Script:    []string{"just ci quitsh nix fix-hash --fail-on-change"},
			StageType: "lint",
			Rules:     defaultRules,
			When:      "always",
		},
		"clean-scratch": {
			Tags:      []string{"nix", "linux"},
			Script:    []string{"rm -rf /scratch/${CI_PIPELINE_ID}"},
			StageType: "cleanup",
			Rules:     defaultRules,
			When:      "always",
		},
		"trigger-merge-requests": {
			Tags: []string{"nix", "linux"},
			Script: []string{
				`just ci quitsh ci trigger-merge-requests ` +
					`--force` +
					`--project-id "${CI_PROJECT_ID}" ` +
					`--credential-token-env 'CI_JOB_TOKEN'`},
			StageType:    "cleanup",
			AllowFailure: true,
			Rules:        releaseRules,
		},
	}

	return jobs
}

func generateComponentJobs(
	rootDir string,
	comps []*component.Component,
	setts *CISettings,
	changes []string,
	doDeploy bool,
	doRelease bool,
) (map[string]*GitlabJob, error) {
	log.Info("Generate job for components.")

	targets, prios, err := dag.DefineExecutionOrder(comps, nil, changes, rootDir)
	if err != nil {
		return nil, err
	}

	log.Info(prios.Format())

	jobs := make(map[string]*GitlabJob)

	defaultRules := DefaultRules()
	releaseRules := ReleaseRules()

	for _, target := range targets {
		if !target.Inputs.Changed {
			continue
		}
		jobName := target.Target.ID.String()

		if _, exists := jobs[jobName]; exists {
			return nil, errors.New("cannot add job '%s', already exists!", jobName)
		}

		// Define dependency.
		var needs []string
		for _, d := range target.Target.Dependencies {
			needs = append(needs, d.String())
		}

		// Special Settings
		allowFailure := false
		rules := &defaultRules
		stageType := target.Target.Stage
		var preCmds []string
		configFile := ""

		switch stageType {
		case "lint":
			if target.Comp.Name() == "lib-common" {
				allowFailure = true
			}
		case "build":
		case "test":
		case "image":
			configFile, preCmds, stageType = setImageArgumentsToConfig(
				doDeploy,
				doRelease,
				&setts.Attrs,
			)
		case "deploy":
			rules = &releaseRules
		case "manifest":
			rules = &defaultRules
		default:
			return nil, errors.New("stage name '%v' is unknown", stageType)
		}

		// NOTE: Skipping some jobs:
		//       One can only skip jobs in DAG on Gitlab by not connecting
		//       them and setting them to `when: manual`.
		//       E.g. if I want to take out all build steps (on merge-requests) and make
		//       them optional, one needs to remove these nodes from the graph and add them back
		//       with `when: manual` which makes them optional.

		job := GitlabJob{
			TargetNode: target,
			Name:       jobName,
			StageType:  stageType,
			Tags:       []string{"nix", "linux"},
			Needs:      needs,
			BeforeScript: append([]string{
				"just ci quitsh ci merge-to-target-branch",
			}, preCmds...),
			Script: []string{fmt.Sprintf(
				"just ci quitsh --config '%s' exec-target '%v'",
				configFile,
				target.Target.ID,
			)},
			Rules:        *rules,
			AllowFailure: allowFailure,
		}

		modifyJob(rootDir, doDeploy, &job, target)

		jobs[job.Name] = &job
	}

	assignDummyDependencies(jobs)

	return jobs, nil
}

// assignDummyDependencies links together all jobs in accordance to the stages
// on the same component
// A target A::a in stage 'build' depends on all targets `A::*` in stage 'lint'
// for example.
func assignDummyDependencies(jobs map[string]*GitlabJob) {
	findPrevJobs := func(job *GitlabJob) (prevs []*GitlabJob) {
		for _, j := range jobs {
			if j.TargetNode == nil {
				continue
			}

			// If its the same component but is on the previous stage, then
			// this is a previous target.
			if j.TargetNode.Comp.Name() == job.TargetNode.Comp.Name() &&
				j.TargetNode.Target.StagePrio.IsBefore(job.TargetNode.Target.StagePrio) {
				prevs = append(prevs, j)
			}
		}

		return prevs
	}

	for _, job := range jobs {
		if job.TargetNode == nil {
			continue
		}
		prevs := findPrevJobs(job)

		// Job `job` needs all previous `prevs` jobs.
		for _, p := range prevs {
			if p.Name == "" {
				log.Panic("Name of job not defined.", "job", p)
			}

			job.Needs = append(job.Needs, p.Name)
		}
	}
}

func modifyJob(
	rootDir string,
	doDeploy bool,
	job *GitlabJob,
	target *dag.TargetNode,
) {
	if doDeploy && target.Target.ID == "custodian-docs::build" {
		// We need to move the public to the global folder.
		// See:
		// https://gitlab.com/groups/gitlab-org/-/epics/10126
		job.StageType = "deploy"
		job.Script = append(
			job.Script,
			shell.CmdToString("mkdir", "-p", "public"),
			shell.CmdToString(
				"mv",
				path.Join(target.Comp.OutBuildDir("docs")),
				path.Join(rootDir, "public/custodian"),
			),
		)

		if job.Artifacts == nil {
			job.Artifacts = &GitlabJobArtifact{}
		}

		job.Artifacts.Paths = append(job.Artifacts.Paths, "public")
	}
}

func setImageArgumentsToConfig(
	doDeploy bool,
	onReleaseTag bool,
	attrs *CustodianPplAttrs,
) (configFile string, cmds []string, stageType stage.Stage) {
	configFile = "quitsh.yaml"
	args := []string{"ci", "quitsh", "config", "image", configFile}

	stageType = stage.Stage("image")

	if doDeploy {
		stageType = stage.Stage("deploy")

		args = append(
			args,
			"--push",
			"--registry-type",
			"release",
			"--credential-user-env",
			RegistryUserEnv,
			"--credential-token-env",
			RegistryTokenEnv,
		)

		if onReleaseTag {
			args = append(args, "--use-release-tag")
		}
	} else if attrs.Images.Push {
		args = append(args,
			"--push",
			"--registry-type",
			"temporary", // WARN: this is important, do not change this!
			"--credential-user-env",
			RegistryUserEnv,
			"--credential-token-env",
			RegistryTokenEnv)

		if attrs.Images.UseReleaseTag {
			args = append(args, "--use-release-tag")
		}
	}

	cmds = append(cmds, shell.CmdToStringF("just", args...))

	return "quitsh.yaml", cmds, stageType
}

func assignMissingStepNames(jobs iter.Seq[*GitlabJob]) {
	for job := range jobs {
		for stepIdx := range job.Run {
			step := &job.Run[stepIdx]
			if step.Name == "" {
				step.Name = fmt.Sprintf(
					"step_%v",
					stepIdx,
				)
			}
		}
	}
}
