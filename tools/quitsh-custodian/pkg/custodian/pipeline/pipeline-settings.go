package pipeline

import (
	"os"
	"path"

	"github.com/sdsc-ordes/quitsh/pkg/ci"
	"github.com/sdsc-ordes/quitsh/pkg/ci/pipeline"
	"github.com/sdsc-ordes/quitsh/pkg/common"
	"github.com/sdsc-ordes/quitsh/pkg/errors"
	"github.com/sdsc-ordes/quitsh/pkg/log"
)

// CustodianPplAttrs are the custom CI attributes for Custodian.
//
// A YAML block like:
// ```
//
//	```yaml {ci}
//	pushImages: true
//	...
//	```
//
// ```
// can be put inside the merge-request description or
// the commit message or the tag message (annotated tag),
// depending on the pipeline mode:
//
//   - On a branch pipeline only the commit message is consider.
//   - On a merge-request pipeline the merge-request description
//     and the commit message are consider
//     (in this increasing priority order).
//   - On a tag pipeline the commit-message and the tag-message
//     are considered (in this increasing priority order)
type CustodianPplAttrs struct {
	Images CustodianPplImages `yaml:"images"`
	Build  CustodianPplBuild  `yaml:"build"`
}

type CustodianPplImages struct {
	Push          bool `yaml:"push"`
	UseReleaseTag bool `yaml:"useReleaseTag"`
}

type CustodianPplBuild struct {
	Lazy bool `yaml:"lazy"`
}

// CIAttributes interface.
func (c *CustodianPplAttrs) Merge(other pipeline.PipelineAttributes) {
	o := common.Cast[*CustodianPplAttrs](other)

	c.Images.Push = c.Images.Push || o.Images.Push
	c.Images.UseReleaseTag = c.Images.UseReleaseTag || o.Images.UseReleaseTag

	c.Build.Lazy = c.Build.Lazy || o.Build.Lazy
}

// CIAttributes interface.
func (c CustodianPplAttrs) Clone() pipeline.PipelineAttributes {
	return &c
}

// CustodianPplCustom represents some custom data we
// parse for the Custodian pipeline.
type CustodianPplCustom struct {
	ScratchDir string `yaml:"scratchDir"`
}

// LoadFromEnv loads Custodian pipeline settings from the environment.
func (c *CustodianPplCustom) LoadFromEnv() error {
	scratchKey := os.Getenv("CI_PIPELINE_ID")
	if scratchKey != "" {
		c.ScratchDir = path.Join("/scratch", scratchKey)
	} else {
		return errors.New("env. variable CI_PIPELINE_ID is not defined")
	}

	return nil
}

type CISettings struct {
	General pipeline.PipelineSettings `yaml:"general"`
	Attrs   CustodianPplAttrs         `yaml:"attrs"`
	Custom  CustodianPplCustom        `yaml:"custom"`
}

func (c *CISettings) Init() error { return nil }

// LoadSettings loads all pipeline settings for the Custodin pipeline.
// If CI is running it loads the settings from the environment and
// writes the settings to the `file` otherwise
// it loads it from the given file.
func LoadSettings(file string) (settings CISettings, err error) {
	if ci.IsRunning() {
		log.Info("Loading pipeline settings from environment.")

		err = settings.Custom.LoadFromEnv()
		if err != nil {
			err = errors.AddContext(
				err,
				"could not load custodian pipeline settings from environment",
			)

			return
		}

		settings.General, err = pipeline.
			NewPipelineSettingsLoaderGitlab(&settings.Attrs).
			LoadFromEnv(nil)
		if err != nil {
			return
		}

		err = pipeline.WritePipelineSettingsToFile(&settings, file)
	} else {
		log.Info("Loading pipeline settings from file.", "file", file)
		settings, err = pipeline.NewPipelineSettingsFromFile[CISettings](file)

		if err != nil {
			log.Error("You need to provide the pipeline settings copied from " +
				"the output of the Job 'generate-pipeline' or write your own.")
		}
	}

	return
}
