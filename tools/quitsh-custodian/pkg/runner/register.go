package runner

import (
	"custodian/tools/quitsh-custodian/pkg/runner/config"
	containerfilerunner "custodian/tools/quitsh-custodian/pkg/runner/containerfile"
	docsphinxrunner "custodian/tools/quitsh-custodian/pkg/runner/doc-sphinx"
	gorunner "custodian/tools/quitsh-custodian/pkg/runner/go"
	yttrunner "custodian/tools/quitsh-custodian/pkg/runner/manifest-ytt"
	nixrunner "custodian/tools/quitsh-custodian/pkg/runner/nix"

	"github.com/sdsc-ordes/quitsh/pkg/errors"
	"github.com/sdsc-ordes/quitsh/pkg/log"
	"github.com/sdsc-ordes/quitsh/pkg/runner/factory"
	gorunnerQuitsh "github.com/sdsc-ordes/quitsh/pkg/runner/go"
)

func RegisterAll(
	buildSettings *config.BuildSettings,
	lintSettings *config.LintSettings,
	testSettings *config.TestSettings,
	imageSettings *config.ImageSettings,
	manifestSettings *config.ManifestSettings,
	flakeDirRel string,
	factory factory.IFactory,
) {
	log.Trace("Register all runners.")
	var err error
	e := gorunnerQuitsh.RegisterBuild(buildSettings.WrapToIBuildSettings(), factory, true)
	err = errors.Combine(err, e)

	e = gorunnerQuitsh.RegisterTest(testSettings.WrapToITestSettings(), factory, true)
	err = errors.Combine(err, e)

	e = gorunner.Register(lintSettings, factory)
	err = errors.Combine(err, e)

	e = nixrunner.Register(flakeDirRel, imageSettings, factory)
	err = errors.Combine(err, e)

	e = containerfilerunner.Register(imageSettings, factory)
	err = errors.Combine(err, e)

	e = docsphinxrunner.Register(factory)
	err = errors.Combine(err, e)

	e = yttrunner.Register(factory, manifestSettings)
	err = errors.Combine(err, e)

	if err != nil {
		log.PanicE(err, "Could not register runners")
	}
}
