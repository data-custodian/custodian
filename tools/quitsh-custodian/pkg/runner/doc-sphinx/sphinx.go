package docsphinxrunner

import (
	"github.com/sdsc-ordes/quitsh/pkg/exec/python"
	fs "github.com/sdsc-ordes/quitsh/pkg/filesystem"
	"github.com/sdsc-ordes/quitsh/pkg/runner"
)

const DocSphinxRunnerID = "custodian::doc-sphinx"

type DocSphinxRunner struct{}

func NewSphinxBuildRunner() (runner.IRunner, error) {
	return &DocSphinxRunner{}, nil
}

func (*DocSphinxRunner) ID() runner.RegisterID {
	return DocSphinxRunnerID
}

func (*DocSphinxRunner) Run(ctx runner.IContext) error {
	log := ctx.Log()
	comp := ctx.Component()

	config := comp.Config()
	log.Info("Starting Sphinx build documentation.", "component", config.Name)

	venv := comp.RelPath(".venv")
	cmdCtx := python.NewVEnvCtxBuilder(venv, nil).Cwd(comp.Root()).Build()

	log.Info("Creating python venv.", "path", comp.Root())
	if !fs.Exists(venv) {
		err := cmdCtx.Check("uv", "venv")
		if err != nil {
			return err
		}
	}

	out := comp.OutBuildDocsDir()

	log.Info("Starting sphinx build.")
	err := cmdCtx.Chain().
		Check("uv", "pip", "install", ".").
		Check("uv", "run", "sphinx-build", "src", out).
		Error()

	if err != nil {
		return err
	}

	log.Info("Built sphinx documentation. Success.", "output", out)

	return nil
}
