package ytt

import (
	"github.com/creasty/defaults"
	"github.com/go-playground/validator/v10"
	"github.com/sdsc-ordes/quitsh/pkg/component/step"
)

type RunnerConfigYTT struct {
}

func (c *RunnerConfigYTT) Validate() error {
	return validator.New().Struct(c)
}

func UnmarshalYTTConfig(raw step.AuxConfigRaw) (step.AuxConfig, error) {
	config := &RunnerConfigYTT{}
	err := defaults.Set(config)
	if err != nil {
		return nil, err
	}

	// Deserialize if we have something.
	if raw.Unmarshal != nil {
		err = raw.Unmarshal(&config)
		if err != nil {
			return nil, err
		}
	}

	err = config.Validate()
	if err != nil {
		return nil, err
	}

	return config, nil
}
