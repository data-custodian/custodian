package ytt

import (
	"custodian/tools/quitsh-custodian/pkg/runner/config"

	"github.com/sdsc-ordes/quitsh/pkg/errors"
	"github.com/sdsc-ordes/quitsh/pkg/log"
	"github.com/sdsc-ordes/quitsh/pkg/runner"
	"github.com/sdsc-ordes/quitsh/pkg/runner/factory"
)

// Register registers the runners in the factory.
func Register(
	factory factory.IFactory,
	manifestSettings *config.ManifestSettings,
) (err error) {
	log.Trace("Register runner.", "id", YTTRunnerID)
	e := factory.Register(
		YTTRunnerID,
		runner.RunnerData{
			Creator: func(runnerConfig any) (runner.IRunner, error) {
				return NewYTTRunner(runnerConfig, manifestSettings)
			},
			RunnerConfigUnmarshal: UnmarshalYTTConfig,
			DefaultToolchain:      "manifest-ytt",
		})

	err = errors.Combine(err, e)
	e = factory.RegisterToKey(runner.NewRegisterKey("manifest", "ytt"), YTTRunnerID)
	err = errors.Combine(err, e)

	return err
}
