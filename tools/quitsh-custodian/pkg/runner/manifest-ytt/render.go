package ytt

import (
	"custodian/tools/quitsh-custodian/pkg/runner/config"
	"path"

	"github.com/sdsc-ordes/quitsh/pkg/common"
	cg "github.com/sdsc-ordes/quitsh/pkg/config"
	"github.com/sdsc-ordes/quitsh/pkg/exec"
	fs "github.com/sdsc-ordes/quitsh/pkg/filesystem"
	"github.com/sdsc-ordes/quitsh/pkg/runner"
)

const (
	YTTRunnerID          = "custodian::manifest-ytt"
	externalSrcsFilename = "external.yaml"
)

type externalSources struct {
	Sources []string `yaml:"srcs"`
}

type release struct {
	Name      string `yaml:"name"`
	Namespace string `yaml:"namespace"`
}

type instanceConfig struct {
	domain  string
	envType string
}

// DeploymentDir returns the deployment directory for that component.
func (i *instanceConfig) DeploymentDir(compDir string, paths ...string) string {
	return path.Join(append([]string{compDir, "deployments", i.domain, i.envType}, paths...)...)
}

type componentDirs struct {
	compRoot    string
	outBuildDir string
	commonLib   string
}

type deploymentValues struct {
	Release release `yaml:"release"`
}

func (e externalSources) Init() error {
	return nil
}

func (d deploymentValues) Init() error {
	return nil
}

type ManifestYTTRunner struct {
	runnerConfig *RunnerConfigYTT
	settings     *config.ManifestSettings
}

func NewYTTRunner(
	runnerConfig runner.ISettings,
	settings runner.ISettings,
) (runner.IRunner, error) {
	return &ManifestYTTRunner{
		runnerConfig: common.Cast[*RunnerConfigYTT](runnerConfig),
		settings:     common.Cast[*config.ManifestSettings](settings),
	}, nil
}

func (*ManifestYTTRunner) ID() runner.RegisterID {
	return YTTRunnerID
}

// Run renders YTT manifests.
func (r *ManifestYTTRunner) Run(ctx runner.IContext) error {
	log := ctx.Log()
	comp := ctx.Component()

	config := comp.Config()
	log.Info("Starting YTT render.", "component", config.Name)

	fs.AssertDirs(comp.OutBuildDir())

	manifestsDir := path.Join([]string{ctx.Root(), "deploy", "manifests"}...)

	compDirs := componentDirs{
		compRoot:    comp.Root(),
		outBuildDir: comp.OutBuildDir(),
		commonLib:   path.Join(manifestsDir, "common"),
	}

	instConfig := instanceConfig{
		domain:  r.settings.Domain,
		envType: r.settings.EnvironmentType.ShortString(),
	}

	// Add the common lib and the component files.
	basicSrcs := append(
		[]string{"-f", compDirs.commonLib},
		defineCompSources(comp.Root(), instConfig)...)

	// Add all helm dependencies, by rendering the helm charts
	// to `helmSrc`.
	helmSrcs, err := processHelm(compDirs, &instConfig, basicSrcs)
	if err != nil {
		return err
	}
	log.Info("Helm sources:", "args", helmSrcs)

	// Define the external sources this component depends on.
	extSrcs, err := defineExternalSources(comp.Root(), manifestsDir, instConfig)
	if err != nil {
		return err
	}
	log.Info("External sources:", "args", extSrcs)

	var yttOutputDir string
	if r.settings.OutputDir != "" {
		yttOutputDir = path.Join(r.settings.OutputDir, comp.Name())
	} else {
		yttOutputDir = path.Join(comp.OutBuildDir(), "manifests")
	}

	var args []string
	args = append(args, basicSrcs...)
	args = append(args, extSrcs...)
	args = append(args, helmSrcs...)
	args = append(args, "-f", comp.RelPath("src/"))
	args = append(args, "--output-files", yttOutputDir)

	log.Info("Running YTT command.", "args", args)

	ytt := exec.NewCmdCtxBuilder().BaseCmd("ytt").
		Cwd(comp.Root()).Build()

	return ytt.Check(args...)
}

func readDeploymentInfo(filename string) (deployment deploymentValues, err error) {
	return cg.LoadFromFile[deploymentValues](filename)
}

func defineCompSources(compDir string, instConfig instanceConfig) []string {
	return []string{
		"-f", path.Join(compDir, "schema.yaml"),
		"-f", instConfig.DeploymentDir(compDir),
	}
}

func defineExternalSources(
	compRoot string,
	manifestsDir string,
	instConfig instanceConfig,
) (args []string, err error) {
	var extSrcs externalSources

	file := path.Join(compRoot, externalSrcsFilename)
	if fs.Exists(file) {
		extSrcs, err = cg.LoadFromFile[externalSources](file)
		if err != nil {
			return
		}
	}

	for _, compName := range extSrcs.Sources {
		extSrcArgs := defineCompSources(path.Join(manifestsDir, compName), instConfig)
		args = append(args, extSrcArgs...)
	}

	return
}
