package ytt

import (
	"os"
	"path"

	"github.com/sdsc-ordes/quitsh/pkg/exec"
	fs "github.com/sdsc-ordes/quitsh/pkg/filesystem"
)

const (
	helmChartFolder = "helm-chart"
)

func renderHelmValuesFile(
	compRoot string,
	buildDir string,
	args []string,
) (outFile string, err error) {
	ytt := exec.NewCmdCtxBuilder().BaseCmd("ytt").
		Cwd(compRoot).Build()

	helmValuesInput := path.Join(compRoot, helmChartFolder, "values.yaml")
	args = append(args,
		"-f", helmValuesInput,
	)

	var helmValues string
	helmValues, err = ytt.Get(args...)
	if err != nil {
		return
	}

	outFile = path.Join(buildDir, ".helm-values.yaml")

	err = os.WriteFile(outFile, []byte(helmValues), fs.StrictPermissionsFile)

	return
}

func helmDepUpdate(helm *exec.CmdContext, helmFolder string) error {
	return helm.Check(
		"dependency",
		"update",
		helmFolder,
	)
}

func renderHelmTemplate(
	compRoot string,
	buildDir string,
	deployment deploymentValues,
	valuesArgs []string,
) (outDir string, err error) {
	outFile, err := renderHelmValuesFile(compRoot, buildDir, valuesArgs)
	if err != nil {
		return
	}

	args := []string{
		"template",
		deployment.Release.Name,
		path.Join(compRoot, helmChartFolder),
		"--namespace", deployment.Release.Namespace,
		"-f", outFile,
	}

	outDir = path.Join(buildDir, "helm")

	args = append(args,
		"--output-dir", outDir,
	)
	helm := exec.NewCmdCtxBuilder().BaseCmd("helm").Cwd(compRoot).Build()

	err = helmDepUpdate(helm, path.Join(compRoot, helmChartFolder))
	if err != nil {
		return
	}

	err = helm.Check(args...)

	return
}

func hasHelmCharts(compRoot string) bool {
	return fs.Exists(path.Join(compRoot, helmChartFolder))
}

func processHelm(
	compDirs componentDirs,
	instConfig *instanceConfig,
	valuesArgs []string,
) (helmSrcs []string, err error) {
	if !hasHelmCharts(compDirs.compRoot) {
		return
	}

	releaseValuesFilename := instConfig.DeploymentDir(compDirs.commonLib, "release.yaml")

	var deployment deploymentValues
	deployment, err = readDeploymentInfo(releaseValuesFilename)
	if err != nil {
		return
	}

	outDir, err := renderHelmTemplate(
		compDirs.compRoot,
		compDirs.outBuildDir,
		deployment,
		valuesArgs,
	)
	helmSrcs = append(helmSrcs, "-f", outDir)

	return
}
