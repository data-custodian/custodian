package containerfilerunner

import (
	"custodian/tools/quitsh-custodian/pkg/runner/config"

	"github.com/sdsc-ordes/quitsh/pkg/errors"
	"github.com/sdsc-ordes/quitsh/pkg/log"
	"github.com/sdsc-ordes/quitsh/pkg/runner"
	"github.com/sdsc-ordes/quitsh/pkg/runner/factory"
)

// Register registers the runners in the factory.
func Register(
	imageSettings *config.ImageSettings,
	factory factory.IFactory,
) (err error) {
	log.Trace("Register runner.", "id", ContainerfileRunnerID)

	e := factory.Register(
		ContainerfileRunnerID,
		runner.RunnerData{
			Creator: func(runnerConfig any) (runner.IRunner, error) {
				return NewContainerfileBuildRunner(runnerConfig, imageSettings)
			},
			RunnerConfigUnmarshal: UnmarshalImageConfig,
			DefaultToolchain:      "image-containerfile",
		})
	err = errors.Combine(err, e)
	e = factory.RegisterToKey(
		runner.NewRegisterKey("image", "containerfile"),
		ContainerfileRunnerID,
	)
	err = errors.Combine(err, e)

	return
}
