package containerfilerunner

import (
	cnImages "custodian/tools/quitsh-custodian/pkg/image"
	"custodian/tools/quitsh-custodian/pkg/runner/config"
	"fmt"
	"os"
	"path"

	"github.com/sdsc-ordes/quitsh/pkg/common"
	"github.com/sdsc-ordes/quitsh/pkg/debug"
	"github.com/sdsc-ordes/quitsh/pkg/errors"
	"github.com/sdsc-ordes/quitsh/pkg/exec"
	fs "github.com/sdsc-ordes/quitsh/pkg/filesystem"
	"github.com/sdsc-ordes/quitsh/pkg/image"
	"github.com/sdsc-ordes/quitsh/pkg/log"
	"github.com/sdsc-ordes/quitsh/pkg/runner"
)

const ContainerfileRunnerID = "custodian::image-containerfile"

type ContainerfileBuildRunner struct {
	runnerConfig *RunnerConfigContainerfile
	settings     *config.ImageSettings
}

// NewContainerfileBuildRunner constructs a new ContainerfileBuildRunner with its own config.
func NewContainerfileBuildRunner(conf any, settings runner.ISettings) (runner.IRunner, error) {
	debug.Assert(conf != nil, "config is nil")

	return &ContainerfileBuildRunner{
		runnerConfig: common.Cast[*RunnerConfigContainerfile](conf),
		settings:     common.Cast[*config.ImageSettings](settings),
	}, nil
}

func (*ContainerfileBuildRunner) ID() runner.RegisterID {
	return ContainerfileRunnerID
}

func (r *ContainerfileBuildRunner) Run(ctx runner.IContext) error {
	log := ctx.Log()
	comp := ctx.Component()
	config := comp.Config()
	log.Info("Starting Containerfile image build for component.", "component", config.Name)

	if r.settings.Build.SkipBuild && !r.settings.Push.Enable {
		return errors.New("you need at least to build or push the images")
	}

	debug.Assert(r.settings.Push.RegistryDomain != "",
		"registry domain should not be empty")

	// Either build the images given on the command line
	// or build the images given in the component config.
	var imageTypes []image.ImageType
	if r.settings.Build.ImageTypes != nil {
		imageTypes = r.settings.Build.ImageTypes
	} else {
		imageTypes = image.GetAllImageTypes()
	}

	pkgs := []cnImages.ImagePackage{}
	for i := range imageTypes {
		p := comp.ImagesContainerfile(imageTypes[i])
		if !fs.Exists(p) {
			continue
		}

		baseName := cnImages.NewImagePackageName(comp.Name(), imageTypes[i])
		imageRef, e := image.NewImageRef(
			ctx.Git(),
			r.settings.Push.RegistryBaseName,
			baseName,
			comp.Version(),
			r.settings.Push.RegistryType,
			r.settings.Push.UseReleaseTag,
		)
		if e != nil {
			return e
		}

		pkgs = append(pkgs,
			cnImages.ImagePackage{
				ContainerFile: p,
				Version:       comp.Version().String(),
				ImageFile:     comp.OutImageDir(baseName),
				ImageRef:      image.ImageRefField{Ref: imageRef},
			})
	}

	if len(pkgs) == 0 {
		return errors.New("no image container files found in '%s' for types %q",
			comp.ImagesDir(), imageTypes)
	}

	if !r.settings.Build.SkipBuild {
		err := buildImages(ctx.Root(), pkgs)
		if err != nil {
			return err
		}
	}

	if r.settings.Push.Enable {
		err := cnImages.UploadImages(r.settings, pkgs)
		if err != nil {
			return err
		}
	}

	return nil
}

func buildImages(
	rootDir string,
	pkgs []cnImages.ImagePackage,
) error {
	// We build with a special storage driver,
	// all buildah commands need to know this.
	buildctx := exec.NewCmdCtxBuilder().
		Cwd(rootDir).
		BaseCmd("buildah").
		Env(
			"BUILDAH_FORMAT=docker",
			"BUILDAH_ISOLTAION=chroot",
			"STORAGE_DRIVER=vfs").
		Build()

	for i := range pkgs {
		log.Info("Building image.",
			"image",
			pkgs[i].ImageFile,
			"ref", pkgs[i].ImageRef.Ref.String())

		cmd := []string{
			"build",
			"--build-arg", fmt.Sprintf("BUILD_VERSION=%v", pkgs[i].Version),
			"-f", pkgs[i].ContainerFile,
			"-t", pkgs[i].ImageRef.Ref.String(),
			"--isolation=chroot",
			"--storage-driver=vfs",
			"--format=docker",
			rootDir,
		}

		err := buildctx.Check(cmd...)
		if err != nil {
			return err
		}

		dest := pkgs[i].ImageFile
		err = os.MkdirAll(path.Dir(dest), fs.DefaultPermissionsDir)
		if err != nil {
			return err
		}

		if fs.Exists(dest) {
			err = os.Remove(dest)
			if err != nil {
				return err
			}
		}

		cmd = []string{
			"push",
			"--storage-driver=vfs",
			"--format=docker",
			pkgs[i].ImageRef.Ref.String(),
			"docker-archive://" + dest,
		}

		err = buildctx.Check(cmd...)
		if err != nil {
			return err
		}
	}

	return nil
}
