package nixrunner

import (
	"custodian/tools/quitsh-custodian/pkg/runner/config"

	"github.com/sdsc-ordes/quitsh/pkg/errors"
	"github.com/sdsc-ordes/quitsh/pkg/log"
	"github.com/sdsc-ordes/quitsh/pkg/runner"
	"github.com/sdsc-ordes/quitsh/pkg/runner/factory"
)

// Register registers the runners in the factory.
func Register(
	flakeRelDir string,
	imageSettings *config.ImageSettings,
	factory factory.IFactory,
) (err error) {
	log.Trace("Register runner.", "id", NixImageRunnerID)

	e := factory.Register(
		NixImageRunnerID,
		runner.RunnerData{
			Creator: func(runnerConfig any) (runner.IRunner, error) {
				return NewNixImageRunner(flakeRelDir, runnerConfig, imageSettings)
			},
			RunnerConfigUnmarshal: UnmarshalImageConfig,
			DefaultToolchain:      "image-nix",
		})
	err = errors.Combine(err, e)
	e = factory.RegisterToKey(runner.NewRegisterKey("image", "nix"), NixImageRunnerID)
	err = errors.Combine(err, e)

	return
}
