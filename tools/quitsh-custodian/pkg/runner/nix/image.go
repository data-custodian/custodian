package nixrunner

import (
	"custodian/tools/quitsh-custodian/pkg/custodian"
	cnNix "custodian/tools/quitsh-custodian/pkg/exec/nix"
	cnImages "custodian/tools/quitsh-custodian/pkg/image"
	"custodian/tools/quitsh-custodian/pkg/runner/config"
	"fmt"
	"os"
	"path"

	"github.com/sdsc-ordes/quitsh/pkg/common"
	"github.com/sdsc-ordes/quitsh/pkg/debug"
	"github.com/sdsc-ordes/quitsh/pkg/errors"
	"github.com/sdsc-ordes/quitsh/pkg/exec"
	"github.com/sdsc-ordes/quitsh/pkg/exec/nix"
	fs "github.com/sdsc-ordes/quitsh/pkg/filesystem"
	"github.com/sdsc-ordes/quitsh/pkg/image"
	"github.com/sdsc-ordes/quitsh/pkg/log"
	"github.com/sdsc-ordes/quitsh/pkg/runner"
)

const NixImageRunnerID = "custodian::image-nix"

type NixImageRunner struct {
	flakeDirRel  string
	runnerConfig *RunnerConfigImage
	settings     *config.ImageSettings
}

// NewNixImageRunner constructs a new NixImageRunner with its own config.
func NewNixImageRunner(
	flakeDirRel string,
	conf any,
	settings runner.ISettings,
) (runner.IRunner, error) {
	debug.Assert(conf != nil, "config is nil")

	return &NixImageRunner{
		flakeDirRel:  flakeDirRel,
		runnerConfig: common.Cast[*RunnerConfigImage](conf),
		settings:     common.Cast[*config.ImageSettings](settings),
	}, nil
}

func (*NixImageRunner) ID() runner.RegisterID {
	return NixImageRunnerID
}

func (r *NixImageRunner) Run(ctx runner.IContext) error {
	log := ctx.Log()
	comp := ctx.Component()
	config := comp.Config()
	log.Info("Starting Nix image build for component.", "component", config.Name)

	if r.settings.Build.SkipBuild && !r.settings.Push.Enable {
		return errors.New("you need at least to build or push the images")
	}

	debug.Assert(r.settings.Push.RegistryDomain != "",
		"registry domain should not be empty")

	nixctx := nix.NewCtxBuilder().Cwd(ctx.Root()).Build()

	fs.AssertDirs(comp.OutImageDir())

	// Either build the images given on the command line
	// or build the images given in the component config.
	var imageTypes []image.ImageType
	if r.settings.Build.ImageTypes != nil {
		imageTypes = r.settings.Build.ImageTypes
	} else {
		imageTypes = image.GetAllImageTypes()
	}

	flakePath := custodian.GetNixFlakeDir(ctx.Root())
	packages, err := nix.GetFlakePackages(nixctx, flakePath)
	if err != nil {
		return err
	}

	pkgs := []cnImages.ImagePackage{}

	for i := range imageTypes {
		installable, packageName := cnNix.NixComponentImageInstallable(
			flakePath, config.Name, imageTypes[i])

		if _, exists := packages[packageName]; !exists {
			continue
		}

		imageBaseName := cnImages.NewImagePackageName(comp.Name(), imageTypes[i])
		imageRef, e := image.NewImageRef(
			ctx.Git(),
			r.settings.Push.RegistryBaseName,
			imageBaseName,
			comp.Version(),
			r.settings.Push.RegistryType,
			r.settings.Push.UseReleaseTag,
		)
		if e != nil {
			return e
		}

		pkgs = append(
			pkgs,
			cnImages.ImagePackage{
				Component:      config.Name,
				Version:        comp.Version().String(),
				ImageType:      imageTypes[i],
				NixInstallable: installable,
				ImageFile:      comp.OutImageDir(imageBaseName),
				ImageRef:       image.ImageRefField{Ref: imageRef}},
		)
	}

	if len(pkgs) == 0 {
		return errors.New("no image installables found with `%s-<image-type>-image`",
			config.Name)
	}

	if !r.settings.Build.SkipBuild {
		err = buildImages(nixctx, pkgs)
		if err != nil {
			return err
		}
	}

	if r.settings.Push.Enable {
		err = cnImages.UploadImages(r.settings, pkgs)
		if err != nil {
			return err
		}
	}

	return err
}

func buildImages(
	nixctx *exec.CmdContext,
	pkgs cnImages.ImagePackages,
) error {
	installables := []string{}

	for i := range pkgs {
		log.Info("Building image.",
			"image",
			pkgs[i].ImageFile,
			"ref", pkgs[i].ImageRef.Ref.String())

		installables = append(installables, pkgs[i].NixInstallable)
	}

	outLink := path.Join(path.Dir(pkgs[0].ImageFile), "."+path.Base(pkgs[0].ImageFile))

	cmd := []string{
		"build",
		"-L",
		"--accept-flake-config",
		"--json",
		"--out-link",
		outLink,
	}
	cmd = append(cmd, installables...)
	err := nixctx.Check(cmd...)
	if err != nil {
		return err
	}

	// Rename all results links for better user friendliness.
	// see `nix build`.
	for i := range pkgs {
		oldPath := outLink
		if i != 0 {
			oldPath += fmt.Sprintf("-%v", i)
		}

		err = os.Rename(oldPath, pkgs[i].ImageFile)
		if err != nil {
			return err
		}
	}

	return err
}
