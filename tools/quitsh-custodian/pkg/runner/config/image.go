package config

import (
	custodianregistry "custodian/tools/quitsh-custodian/pkg/custodian/registry"

	"github.com/sdsc-ordes/quitsh/pkg/common"
	"github.com/sdsc-ordes/quitsh/pkg/image"
	"github.com/sdsc-ordes/quitsh/pkg/registry"
)

type ImageSettings struct {
	Build ImageSettingsBuild `yaml:"build"`
	Push  ImageSettingsPush  `yaml:"push"`

	Args []string `yaml:"args"` // Additional arguments forwarded to the tool.
}

type ImageSettingsBuild struct {
	SkipBuild bool `yaml:"skip"`

	ImageTypes []image.ImageType `yaml:"imageTypes,omitempty"`

	// Currently only `release` is supported.
	BuildType common.BuildType `yaml:"buildType"`
}

type ImageSettingsPush struct {
	Enable bool `yaml:"enable"`

	// The registry name specifying either `release` or `temporary` to use to
	// upload the image to.
	RegistryType registry.RegistryType `yaml:"registryName"`

	// The default domain and base name.
	RegistryDomain   string `yaml:"-"`
	RegistryBaseName string `yaml:"-"`

	// If the image should have a release tag (only `<version>)
	// or use `<version>-<commitSHA>`.
	UseReleaseTag  bool                  `yaml:"useReleaseTag"`
	CredentialsEnv common.CredentialsEnv `yaml:"credentialsEnv"`
	// Under the following condition you can force an upload
	// because we disallow to prevent accidents of overwriting:
	// - Push + Release + image does not exist yet!
	// - Push + Debug + and image does exist.
	Force bool `yaml:"force"`
}

// SetDefaults implements the `defaults.Setter` interface.
func (s *ImageSettingsPush) SetDefaults() {
	s.RegistryDomain, s.RegistryBaseName =
		custodianregistry.NewRegistryBaseName()
}

// ResolveCredentials resolves the credentials from the env. variables.
func (s *ImageSettingsPush) ResolveCredentials() (common.Credentials, error) {
	return common.NewCredentials(s.CredentialsEnv)
}
