package config

import "github.com/sdsc-ordes/quitsh/pkg/common"

type ManifestSettings struct {
	Domain          string                 `yaml:"domain"`
	EnvironmentType common.EnvironmentType `yaml:"environmentType"`

	OutputDir string `yaml:"outputDir"`

	Args []string `yaml:"args"` // Additional arguments forwarded to the tool.
}
