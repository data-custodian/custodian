package config

type YTTSettings struct {
	// Additional arguments forwarded to the test tool.
	Args []string `yaml:"args"`
}

// NewYTTSettings constructs a new build setting.
func NewYTTSettings(
	args []string,
) YTTSettings {
	return YTTSettings{
		Args: args,
	}
}
