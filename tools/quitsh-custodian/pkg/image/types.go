package image

import (
	"github.com/sdsc-ordes/quitsh/pkg/image"
)

type ImagePackages []ImagePackage

type ImagePackage struct {
	Component string `yaml:"component"`
	Version   string `yaml:"version"`

	// For Nix build.
	ImageType      image.ImageType `yaml:"imageType"`
	NixInstallable string          `yaml:"nixAttribute"`

	// For normal Containerfile build.
	ContainerFile string              `yaml:"containerFile,omitempty"`
	ImageFile     string              `yaml:"src"`
	ImageRef      image.ImageRefField `yaml:"imageRef"`
}
