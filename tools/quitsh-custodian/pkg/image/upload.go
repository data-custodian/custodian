package image

import (
	"custodian/tools/quitsh-custodian/pkg/runner/config"
	"strings"
	"time"

	"github.com/sdsc-ordes/quitsh/pkg/ci"
	"github.com/sdsc-ordes/quitsh/pkg/errors"
	"github.com/sdsc-ordes/quitsh/pkg/exec"
	"github.com/sdsc-ordes/quitsh/pkg/exec/skopeo"
	"github.com/sdsc-ordes/quitsh/pkg/log"
	"github.com/sdsc-ordes/quitsh/pkg/registry"
)

func UploadImages(sett *config.ImageSettings, pkgs []ImagePackage) error {
	var err error

	creds, err := sett.Push.ResolveCredentials()
	if err != nil {
		return err
	}

	skopeo := skopeo.NewCtx()
	if creds.Token() != "" {
		log.Info("Skopeo login.")
		logout, e := skopeo.Login(creds, sett.Push.RegistryDomain)
		if e != nil {
			return e
		}

		defer func() {
			le := logout()
			err = errors.Combine(err, le)
		}()
	}

	for i := range pkgs {
		src := pkgs[i].ImageFile
		dest := "docker://" + pkgs[i].ImageRef.Ref.String()

		exists := true
		log.Info("Checking if image already exists.")

		cmd := []string{"inspect"}
		cmd = append(cmd, dest)

		// When repository does not exist yet ->
		// we get exit code 1, an we need to handle
		// this case somehow.
		// TODO: Use a better tool/approach to check if the image is there, registry agnostic
		// (its not about gitlab!)
		// The workaround below is really ugly and prone to break!
		resp, e := skopeo.GetCombinedWithEC(
			func(cmdError *exec.CmdError) error {
				if cmdError != nil && cmdError.ExitCode() == 2 {
					exists = false

					return nil
				}

				return cmdError
			},
			cmd...)
		if e != nil {
			if !strings.Contains(resp, "invalid status code from registry 403") &&
				!strings.Contains(resp, "requested access to the resource is denied") {
				return errors.AddContext(
					e,
					"could not check if image '%s' exists:\nstdout/err:\n%s",
					dest,
					resp,
				)
			}
			// we have 403 which is ok, the registry does not yet exist.
			exists = false
		}

		log.Info("Image seems not to exist.")
		e = assertUseForceForPush(sett, exists, dest)
		if e != nil {
			return e
		}

		log.Info("Uploading image.", "src", src, "dest", dest)
		cmd = []string{
			"--insecure-policy",
			"copy",
		}
		cmd = append(cmd, "docker-archive://"+src, dest)

		// TODO: Sign the image with cosign
		// https://gitlab.com/data-custodian/custodian/-/issues/200
		// https://docs.gitlab.com/ee/ci/yaml/signing_examples.html
		// https://docs.sigstore.dev/cosign/signing/signing_with_containers/
		e = skopeo.Check(cmd...)

		if e != nil {
			e = errors.AddContext(e, "could not upload image '%s' -> '%s'", src, dest)
			err = errors.Combine(err, e)
		}
	}

	return err
}

func assertUseForceForPush(
	sett *config.ImageSettings,
	exists bool,
	imageName string,
) error {
	if sett.Push.RegistryType == registry.RegistryTemp {
		// To the test registry, allow anything.
		return nil
	}

	// Any other registry...
	if !exists { //nolint:nestif // intentional.
		if sett.Push.UseReleaseTag && !sett.Push.Force {
			log.Error("The image does NOT EXIST and you want to push a RELEASE image!"+
				"WARNING: Luke you can use the force (--force) flag to proceed.",
				"image", imageName)

			return errors.New("image push denied")
		}

		log.Info(
			"Going to push image to registry.",
			"registry",
			sett.Push.RegistryType.String(),
			"image",
			imageName,
		)
		if !ci.IsRunning() {
			log.Warn("Waiting 10secs to proceed. -> Cancel now it that is a mistake.")
			time.Sleep(10 * time.Second) //nolint:mnd
		}
	} else {
		if sett.Push.UseReleaseTag {
			log.Error("The image EXISTS and you want to OVERWRITE a release image! "+
				"This is not allowed for safety, the image needs to be deleted manually!",
				imageName)

			return errors.New("image push denied")
		} else if !sett.Push.Force {
			log.Error("The image EXISTS and you want to OVERWRITE a image! "+
				"WARNING: Luke you can use the force (--force) flag to proceed.",
				"image", imageName)

			return errors.New("image push denied")
		}

		log.Warn("The image EXISTS and you are going to OVERWRITING an image!",
			"registry",
			sett.Push.RegistryType.String(),
			"image", imageName)

		if !ci.IsRunning() {
			log.Warn("Waiting 10secs to proceed. -> Cancel now it that is a mistake.")
			time.Sleep(10 * time.Second) //nolint:mnd
		}
	}

	return nil
}
