#!/usr/bin/env bash

mkdir -p config

read -r -p "Country: " COUNTRY_NAME
read -r -p "State or Province: " STATE_OR_PROVINCE_NAME
read -r -p "Locality: " LOCALITY_NAME
read -r -p "Organization: " ORGANIZATION_NAME
read -r -p "Common Name: " COMMON_NAME

cat <<EOF | envsubst >config/cert_ext.cnf.sample
[req]
default_bit = 4096
distinguished_name = req_distinguished_name
prompt = no

[req_distinguished_name]
countryName             = $COUNTRY_NAME
stateOrProvinceName     = $STATE_OR_PROVINCE_NAME
localityName            = $LOCALITY_NAME
organizationName        = $ORGANIZATION_NAME
EOF

cat <<EOF | envsubst >config/custodianCA_cert.cnf
[req]
distinguished_name = req_distinguished_name
x509_extensions = v3_ca
prompt = no

[req_distinguished_name]
countryName             = $COUNTRY_NAME
stateOrProvinceName     = $STATE_OR_PROVINCE_NAME
localityName            = $LOCALITY_NAME
organizationName        = $ORGANIZATION_NAME
commonName              = $COMMON_NAME

[ v3_ca ]
basicConstraints=critical,CA:TRUE
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid:always,issuer
EOF

cat <<EOF | envsubst >config/custodianCA.cnf.sample
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names

[alt_names]
DNS.1 = localhost
EOF
