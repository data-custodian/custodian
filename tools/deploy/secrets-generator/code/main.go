package main

import (
	"fmt"
	"math/rand"
	"os"

	"gopkg.in/yaml.v2"
)

type secretsStore struct {
	Secrets secrets `yaml:"secrets"`
}

type sslSecrets struct {
	Cert string `yaml:"cert"`
	Key  string `yaml:"key"`
}

type ecdsaSecrets struct {
	Public  string `yaml:"public"`
	Private string `yaml:"private"`
}

type secrets struct {
	Acs         acsSecrets        `yaml:"acs"`
	Cms         cmsSecrets        `yaml:"cms"`
	AuditTrail  auditTrailSecrets `yaml:"auditTrail"`
	CustodianCA string            `yaml:"custodianCA"`
	Platforms   platforms         `yaml:"platforms"`
}

type keycloak struct {
	Auth       keycloakAuth `yaml:"auth"`
	Postgresql postgresql   `yaml:"postgresql"`
}

type keycloakAuth struct {
	AdminPassword string `yaml:"adminPassword"`
}

type postgresql struct {
	Auth postgresqlAuth `yaml:"auth"`
}

type postgresqlAuth struct {
	Password        string `yaml:"password"`
	PostgresPasswor string `yaml:"postgresPassword"`
}

type acsSecrets struct {
	PDP    serviceSecrets `yaml:"pdp"`
	JwtKey string         `yaml:"jwtKey"`
}

type cmsSecrets struct {
	ContractSignature serviceSecrets `yaml:"contractSignature"`
}

type auditTrailSecrets struct {
	MongoDB  mongoDB  `yaml:"mongodb"`
	RabbitMQ rabbitMQ `yaml:"rabbitmq"`
}

type mongoDB struct {
	RootPassword string `yaml:"rootPassword"`
	UserPassword string `yaml:"userPassword"`
}

type rabbitMQ struct {
	Password string `yaml:"password"`
}

type serviceSecrets struct {
	Ssl   sslSecrets   `yaml:"ssl"`
	Ecdsa ecdsaSecrets `yaml:"ecdsa"`
}

type platforms struct {
	HAProxy HAProxy `yaml:"haproxy"`
	Jena    Jena    `yaml:"jena"`
}

type HAProxy struct {
	Ssl string `yaml:"ssl"`
}

type Jena struct {
	AdminPassword  string `yaml:"adminPassword"`
	UserPassword   string `yaml:"userPassword"`
	ROUserPassword string `yaml:"roUserPassword"`
}

const (
	passwordCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_+-=[]{}|;':\",<>/?~"
	passwordLength     = 20
)

func ReadFile(filename string) string {
	data, err := os.ReadFile(filename)
	if err != nil {
		panic(err)
	}
	return string(data)
}

func generatePassword() string {
	b := make([]byte, passwordLength)
	for i := range b {
		b[i] = passwordCharacters[rand.Intn(len(passwordCharacters))]
	}
	return string(b)
}

func main() {

	keycloakConfig := keycloak{
		Auth: keycloakAuth{
			AdminPassword: generatePassword(),
		},
		Postgresql: postgresql{
			Auth: postgresqlAuth{
				Password:        generatePassword(),
				PostgresPasswor: generatePassword(),
			},
		},
	}

	haProxyCertificates := ReadFile("secrets/platforms/haproxy/ssl/custodian-platforms-haproxy.crt") + ReadFile("secrets/platforms/haproxy/ssl/custodian-platforms-haproxy.key")

	secretsStore := secretsStore{
		Secrets: secrets{
			Acs: acsSecrets{
				PDP: serviceSecrets{
					Ssl: sslSecrets{
						Cert: ReadFile("secrets/policy-decision-point/ssl/custodian-acs-pdp.crt"),
						Key:  ReadFile("secrets/policy-decision-point/ssl/custodian-acs-pdp.key"),
					},
					Ecdsa: ecdsaSecrets{
						Public:  ReadFile("secrets/policy-decision-point/ecdsa/public.pem"),
						Private: ReadFile("secrets/policy-decision-point/ecdsa/private.pem"),
					},
				},
				JwtKey: generatePassword(),
			},
			Cms: cmsSecrets{
				ContractSignature: serviceSecrets{
					Ssl: sslSecrets{
						Cert: ReadFile("secrets/cms/ssl/custodian-cms.crt"),
						Key:  ReadFile("secrets/cms/ssl/custodian-cms.key"),
					},
					Ecdsa: ecdsaSecrets{
						Public:  ReadFile("secrets/cms/ecdsa/public.pem"),
						Private: ReadFile("secrets/cms/ecdsa/private.pem"),
					},
				},
			},
			AuditTrail: auditTrailSecrets{
				MongoDB: mongoDB{
					RootPassword: generatePassword(),
					UserPassword: generatePassword(),
				},
				RabbitMQ: rabbitMQ{
					Password: generatePassword(),
				},
			},
			CustodianCA: ReadFile("secrets/ca/custodianCA.crt"),
			Platforms: platforms{
				HAProxy: HAProxy{
					Ssl: haProxyCertificates,
				},
				Jena: Jena{
					AdminPassword:  generatePassword(),
					UserPassword:   generatePassword(),
					ROUserPassword: generatePassword(),
				},
			},
		},
	}

	newFile := make(map[string]interface{})
	newFile["global"] = secretsStore // We need the secrets to be set as global values in case we deploy only a few services of the Custodian
	newFile["custodian-keycloak"] = keycloakConfig

	yamlData, err := yaml.Marshal(&newFile)
	if err != nil {
		fmt.Printf("Error while Marshaling. %v", err)
	}

	fmt.Println()
	fmt.Println("########################################################################")
	fmt.Println("# Use the following YAML file to configure your dev Custodian instance #")
	fmt.Println("########################################################################")
	fmt.Println()

	fmt.Println("# This file was generated using the secrets generator provided by the Swiss Data Custodian")
	fmt.Println(string(yamlData))
}
