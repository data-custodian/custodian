#!/bin/bash

set -e
set -u

bash scripts/configure_ca.sh

mkdir -p .tmp

cp config/* .tmp/
cp scripts/* .tmp/

cd .tmp
bash setup_ssl_certificates.sh
bash setup_ecdsa_keys.sh
mv secrets ../secrets
cd ..

rm -r .tmp
