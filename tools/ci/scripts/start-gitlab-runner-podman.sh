#!/usr/bin/env bash
# shellcheck disable=SC1090,SC1091,SC2015
#
# Start a gitlab runner in a podman container which is configured
# configured as a docker executor with podman.
#
# Do this by first visiting `CI/CD Settings` page and
# creating a `linux` runner which gives you a `<token>` needed
# for this script. Select to run also `untagged jobs`.
#
# This creates a container which runs the Gitlab runner
# which will execute jobs over the `podman` executor.
#
# This container is based on [`pipglr`](https://gitlab.com/qontainers/pipglr)
# which uses two container volumes (`pipglr-storage` and `pipglr-cache`)
# which are attached and contains
# `podman` (user `podman`) and `gitlab-runner` (user `runner`)
# to have a rootless container experience which provides much more security.
# The two volumes contain the images created/built by CI. The volumes can safely
# be wiped if space is needed.
#
# The podman socket created inside this container
# will be mounted to each job container the `gitlab-runner` creates.
# This makes also use of caching directly possible which is cool.
#
# Usage:
# ```shell
# start-gitlab-runner-podman.sh [--force] [<token>]
# ```
# Read token from stdin.
# ```shell
# start-gitlab-runner-podman.sh [--force] -
#
# The container uses two users `runner` and `podman`.
# To inspect use `podman log --since 0 custodian-ci-podman`
#
# Usage in Pipeline:
#
# A job which uses `podman` (linked to podman) to run/build images.
# The gitlab-runner cannot serve `services` statements as in the
# `start-gitlab-runner-docker.sh` (uses `docker`
# `--link` which is anyway deprecated)
#
# ```yaml
# podman-remote-run-build:
#   image: quay.io/podman/stable:latest
#   variables:
#     CONTAINER_HOST: unix://var/run/podman.sock
#   script:
#     - podman info
#     - podman run alpine:latest cat /etc/os-release
#     - podman build -f Dockerfile .
# ```
#
# The following (custom build image) also works:
#
# ```yaml
# podman-remote-alpine-run-build:
#   image: alpine:latest
#   variables:
#     CONTAINER_HOST: unix://var/run/podman.sock
#   script:
#     - apk add podman
#     - podman info
#     - podman run alpine:latest cat /etc/os-release
#     - podman build -f Dockerfile .
# ```

# TODO: This script should be put into `quitsh`.

set -e
set -u

ROOT=$(git rev-parse --show-toplevel)
. "$ROOT/tools/ci/scripts/general.sh"

force="false"
config_dir="$ROOT/.gitlab/local/config"
config_file="$config_dir/config.toml"
runner_name="custodian-ci-podman"
cores=$(nproc) || ci::die "Can not get number of processors."
max_jobs="$cores" # includes hyper-threading
# image="registry.gitlab.com/qontainers/pipglr-alpine:latest"
# Or use the fedora version
image="registry.gitlab.com/qontainers/pipglr:latest"

function clean_up() {
    if [ -f "$config_file" ]; then
        rm -rf "$config_file"
    fi
}

trap clean_up EXIT
function modify_toml() {
    local key="$1"
    local value="$2"
    local type="${3:-json}"

    podman run --rm -v "$config_file:/config.toml" \
        "ghcr.io/tomwright/dasel" put -f /config.toml \
        -t "$type" \
        -s "$key" \
        -v "$value" ||
        ci::die "Could not set gitlab runner config key '$key' to '$value'"
}

function register_runner() {
    ci::print_info "Registering gitlab-runner ..."
    local token="${1:-}"

    if [ "$token" = "-" ] || [ -z "$token" ]; then
        read -rs -p "Enter Gitlab Runner Token: " token ||
            ci::die "Could not read token from stdin."
    fi

    podman secret rm REGISTRATION_TOKEN &>/dev/null || true
    echo "$token" | podman secret create REGISTRATION_TOKEN - ||
        ci::die "Could not set registration token secret."

    # Register Gitlab runner.
    (cd "$config_dir" &&
        touch config.toml &&
        podman container runlabel register "$image") ||
        ci::die "Could not register gitlab-runner."

    podman secret rm config.toml &>/dev/null || true
    podman secret create config.toml "$config_file" ||
        ci::die "Could not create config.toml secret."

    # Modify Gitlab runner config secret.
    modify_config
}

# Dump the config, modify and reset the config.
function modify_config() {
    ci::print_info "Modify the Gitlab runner config."

    mkdir -p "$(dirname "$config_file")" &&
        podman container runlabel dumpconfig "$image" >"$config_file" &&
        podman secret rm config.toml || ci::die "Could not dump and delete the secret"

    modify_toml ".concurrent" "$max_jobs"
    modify_toml ".runners.first().docker.pull_policy" \
        '["always"]'

    # This is only needed if direct access to podman is
    # needed inside the `jobs`.
    # modify_config ".runners.first().docker.volumes.append()" \
    #     "/home/runner/podman.sock:/var/run/podman.sock:rw" string

    modify_toml ".runners.first().docker.image" \
        "alpine:latest" string

    modify_toml ".runners.first().pre_build_script" \
        "echo 'Prebuild Script'" string

    ci::print_info "Config file:" \
        "$(sed 's/token\s*=.*/token = ***/g' "$config_dir/config.toml")"

    ci::print_info "Storing Gitlab runner config."
    podman secret create config.toml "$config_file" ||
        ci::die "Could not create config.toml secret."

    rm "$config_file"
}

function remove_volumes() {
    ci::print_info "Do you want to remove the volumes?"
    read -r -p "Answer (y/N):" answer
    answer=${answer:-n}

    if [[ $answer != "y" ]]; then
        return 0
    fi

    ci::print_info "Removing volumes."
    podman volume rm pipglr-cache || true
    podman volume rm pipglr-storage || true
}

function assert_volumes() {
    ci::print_info "Asserting needed volumes ..."

    local volumes
    volumes=$(podman volume list --format="{{ .Name }}") ||
        ci::die "Could not get volumes"

    if ! echo "$volumes" | grep -q "pipglr-storage"; then
        podman container runlabel setupstorage "$image"
    fi

    if ! echo "$volumes" | grep -q "pipglr-cache"; then
        podman container runlabel setupcache "$image"
    fi
}

function start_runner() {
    ci::print_info "Start runner '$runner_name' ..."

    # Run the Gitlab runner. We cannot user `podman container runlabel run "$image"`
    # because we need to set some cpu constraints.
    # Delete an already existing container and create a new one.
    podman container rm "$runner_name" &>/dev/null || true
    podman run -dt --name "$runner_name" \
        --cpus "$cores" \
        --secret config.toml,uid=1001,gid=1001 \
        -v pipglr-storage:/home/podman/.local/share/containers \
        -v pipglr-cache:/cache \
        --systemd true --privileged \
        --device /dev/fuse "$image"

}

function create() {
    rm -rf "$config_dir" >/dev/null || true
    mkdir -p "$config_dir"

    register_runner "$@"
    remove_volumes
    assert_volumes

    start_runner
}

function stop() {
    if is_running; then
        ci::print_info "Stop runner '$runner_name' ..."
        podman stop "$runner_name"
    fi

    if is_exited; then
        # shellcheck disable=SC2046
        podman rm $(podman ps -a -q)
    fi
}

function is_running() {
    [ "$(podman inspect -f '{{.State.Status}}' "$runner_name" 2>/dev/null || true)" = 'running' ] || return 1
    return 0
}

function is_exited() {
    [ "$(podman inspect -f '{{.State.Status}}' "$runner_name" 2>/dev/null || true)" = 'exited' ] || return 1
    return 0
}

if [ "${1:-}" = "--force" ]; then
    force="true"
    shift 1
fi

if [ "$force" = "true" ]; then
    stop
fi

if ! is_running; then
    create "$@"
else
    ci::die "Gitlab runner '$runner_name' is already running. Use '--force' to restart it."
fi
