#!/usr/bin/env bash

set -e
set -u

ROOT=$(git rev-parse --show-toplevel)
. "$ROOT/tools/ci/scripts/general.sh"

cd "$ROOT/tools/nix/quitsh"

if false; then
    podman pull "registry.gitlab.com/data-custodian/custodian/acs-pdp:28ce396"
    podman pull "registry.gitlab.com/data-custodian/custodian/acs/pdp:3b1b725"
    podman pull "registry.gitlab.com/data-custodian/custodian/acs/pdp:b78be0a"
    podman pull "registry.gitlab.com/data-custodian/custodian/audit-trail:7c4965d"
    podman pull "registry.gitlab.com/data-custodian/custodian/audit-trail:80dfb28"
    podman pull "registry.gitlab.com/data-custodian/custodian/audit-trail-consumer:7c4965d"
    podman pull "registry.gitlab.com/data-custodian/custodian/audit-trail-consumer:80dfb28"
    podman pull "registry.gitlab.com/data-custodian/custodian/audit-trail/events-api:3b1b725"
    podman pull "registry.gitlab.com/data-custodian/custodian/audit-trail/events-api:f42b292"
    podman pull "registry.gitlab.com/data-custodian/custodian/audit-trail/events-receiver:3b1b725"
    podman pull "registry.gitlab.com/data-custodian/custodian/audit-trail/events-receiver:f42b292"
    podman pull "registry.gitlab.com/data-custodian/custodian/cms-contract-signature-endpoint:28ce396"
    podman pull "registry.gitlab.com/data-custodian/custodian/cms/contract-signature-endpoint:8e58ad8"
    podman pull "registry.gitlab.com/data-custodian/custodian/cms/contract-signature-endpoint:b78be0a"
    podman pull "registry.gitlab.com/data-custodian/custodian/contract-manager:7c4965d"
    podman pull "registry.gitlab.com/data-custodian/custodian/db-jena:28ce396"
    podman pull "registry.gitlab.com/data-custodian/custodian/db-jena:7c4965d"
    podman pull "registry.gitlab.com/data-custodian/custodian/gateway:28ce396"
    podman pull "registry.gitlab.com/data-custodian/custodian/gateway:7c4965d"
    podman pull "registry.gitlab.com/data-custodian/custodian/gateway/reverse-proxy:072366f"
    podman pull "registry.gitlab.com/data-custodian/custodian/gateway/reverse-proxy:3b1b725"
    podman pull "registry.gitlab.com/data-custodian/custodian/knowledgebase:7c4965d"
    podman pull "registry.gitlab.com/data-custodian/custodian/knowledgebase-knowledgebase-endpoint:28ce396"
    podman pull "registry.gitlab.com/data-custodian/custodian/knowledgebase/knowledgebase-endpoint:3b1b725"
    podman pull "registry.gitlab.com/data-custodian/custodian/platform/jena-docker-config:3b1b725"
    podman pull "registry.gitlab.com/data-custodian/custodian/platform/jena-docker-config:b78be0a"
    podman pull "registry.gitlab.com/data-custodian/custodian/policy-decision-point:7c4965d"
fi

patterns=(
    # Include scratch space.
    -i "registry.gitlab.com/data-custodian/custodian/.*/cache:.*"
    -i "registry.gitlab.com/data-custodian/custodian/temp/.*"
    -i "registry.gitlab.com/data-custodian/custodian/nix/temp/.*"

    # Exclude all CI images.
    -e "registry.gitlab.com/data-custodian/custodian/ci/.*"

    # Inlclude repos which still contain a few needed tags.
    # -i "registry.gitlab.com/data-custodian/custodian/acs-pdp.*"
    # -i "registry.gitlab.com/data-custodian/custodian/acs/pdp.*"
    #
    # -i "registry.gitlab.com/data-custodian/custodian/audit-trail.*"
    # -i "registry.gitlab.com/data-custodian/custodian/audit-trail-consumer.*"
    # -i "registry.gitlab.com/data-custodian/custodian/audit-trail/events-api.*"
    # -i "registry.gitlab.com/data-custodian/custodian/audit-trail/events-receiver.*"
    #
    # -i "registry.gitlab.com/data-custodian/custodian/cms-contract-signature-endpoint.*"
    # -i "registry.gitlab.com/data-custodian/custodian/cms/contract-signature-endpoint.*"
    # -i "registry.gitlab.com/data-custodian/custodian/contract-manager.*"
    # -i "registry.gitlab.com/data-custodian/custodian/db-jena.*"
    # -i "registry.gitlab.com/data-custodian/custodian/gateway.*"
    # -i "registry.gitlab.com/data-custodian/custodian/gateway/reverse-proxy.*"
    # -i "registry.gitlab.com/data-custodian/custodian/knowledgebase.*"
    # -i "registry.gitlab.com/data-custodian/custodian/knowledgebase-knowledgebase-endpoint.*"
    # -i "registry.gitlab.com/data-custodian/custodian/knowledgebase/knowledgebase-endpoint.*"
    #
    # -i "registry.gitlab.com/data-custodian/custodian/platform/jena-docker-config.*"
    # -i "registry.gitlab.com/data-custodian/custodian/policy-decision-point.*"

    ### Excludes do not change!!!!
    -e "registry.gitlab.com/data-custodian/custodian/acs-pdp:28ce396"
    -e "registry.gitlab.com/data-custodian/custodian/acs/pdp:3b1b725"
    -e "registry.gitlab.com/data-custodian/custodian/acs/pdp:b78be0a"

    -e "registry.gitlab.com/data-custodian/custodian/audit-trail:7c4965d"
    -e "registry.gitlab.com/data-custodian/custodian/audit-trail:80dfb28"

    -e "registry.gitlab.com/data-custodian/custodian/audit-trail-consumer:7c4965d"
    -e "registry.gitlab.com/data-custodian/custodian/audit-trail-consumer:80dfb28"

    -e "registry.gitlab.com/data-custodian/custodian/audit-trail/events-api:3b1b725"
    -e "registry.gitlab.com/data-custodian/custodian/audit-trail/events-api:f42b292"

    -e "registry.gitlab.com/data-custodian/custodian/audit-trail/events-receiver:3b1b725"
    -e "registry.gitlab.com/data-custodian/custodian/audit-trail/events-receiver:f42b292"

    -e "registry.gitlab.com/data-custodian/custodian/cms-contract-signature-endpoint:28ce396"
    -e "registry.gitlab.com/data-custodian/custodian/cms/contract-signature-endpoint:8e58ad8"
    -e "registry.gitlab.com/data-custodian/custodian/cms/contract-signature-endpoint:b78be0a"

    -e "registry.gitlab.com/data-custodian/custodian/contract-manager:7c4965d"

    -e "registry.gitlab.com/data-custodian/custodian/db-jena:28ce396"
    -e "registry.gitlab.com/data-custodian/custodian/db-jena:7c4965d"

    -e "registry.gitlab.com/data-custodian/custodian/gateway:28ce396"
    -e "registry.gitlab.com/data-custodian/custodian/gateway:7c4965d"

    -e "registry.gitlab.com/data-custodian/custodian/gateway/reverse-proxy:072366f"
    -e "registry.gitlab.com/data-custodian/custodian/gateway/reverse-proxy:3b1b725"

    -e "registry.gitlab.com/data-custodian/custodian/knowledgebase:7c4965d"

    -e "registry.gitlab.com/data-custodian/custodian/knowledgebase-knowledgebase-endpoint:28ce396"
    -e "registry.gitlab.com/data-custodian/custodian/knowledgebase/knowledgebase-endpoint:3b1b725"

    -e "registry.gitlab.com/data-custodian/custodian/platform/jena-docker-config:3b1b725"
    -e "registry.gitlab.com/data-custodian/custodian/platform/jena-docker-config:b78be0a"

    -e "registry.gitlab.com/data-custodian/custodian/policy-decision-point:7c4965d"
)

just run registry container-cleanup "$@" \
    --project-id "14747939" \
    --credential-token-env "CI_TOKEN" \
    "${patterns[@]}"
