#!/usr/bin/env bash
# shellcheck disable=SC1090,SC1091
#
# Create a normal gitlab runner in a Docker container.
#
# Do this by first visiting `CI/CD Settings` page and
# creating a `linux` runner which gives you a `<token>` needed
# for this script. Select to run also `untagged jobs`.
#
# Usage:
# ```shell
# start-gitlab-runner-docker.sh [--force] [<token>]
# ```
# Read token from stdin.
# ```shell
# start-gitlab-runner-docker.sh [--force] -
#
# The container uses two users `runner` and `docker`.
# To inspect use `docker log --since 0 custodian-ci-docker`
#
set -e
set -u

ROOT=$(git rev-parse --show-toplevel)
. "$ROOT/tools/ci/scripts/general.sh"

force="false"
config_dir="$ROOT/.gitlab/local/config"
runner_name="custodian-ci-docker"
cores=$(nproc) || ci::die "Can not get number of processors."
max_jobs="$cores" # includes hyper-threading

function create() {
    local token="${1:?First argument must be the runner token.}"

    rm -rf "$config_dir" >/dev/null || true
    mkdir -p "$config_dir"

    docker run -d \
        --cpus "$cores" \
        --name "$runner_name" \
        --restart always \
        -v /var/run/docker.sock:/var/run/docker.sock \
        -v "$config_dir":/etc/gitlab-runner \
        gitlab/gitlab-runner:latest || ci::die "Could not create gitlab-runner"

    docker exec -it "$runner_name" gitlab-runner register \
        --non-interactive \
        --url "https://gitlab.com" \
        --token "$token" \
        --executor docker \
        --description "custodian-ci" \
        --docker-image "alpine:latest" \
        --docker-privileged \
        --docker-volumes "/certs/client" || ci::die "Could not start gitlab runner"

    # Set concurrency.
    docker exec -it "$runner_name" \
        sed -i "s/concurrent =.*/concurrent = $max_jobs/" \
        "/etc/gitlab-runner/config.toml" || ci::die "Could not set concurrency."

    docker exec -it "$runner_name" gitlab-runner start || ci::die "Could not start runner."
}

function stop() {
    if is_running; then
        docker stop "$runner_name"
        # shellcheck disable=SC2046
        docker rm $(docker ps -a -q)
    fi
}

function is_running() {
    [ "$(docker inspect -f '{{.State.Running}}' "$runner_name" 2>/dev/null || true)" = 'true' ] || return 1
    return 0
}

if [ "${1:-}" = "--force" ]; then
    force="true"
    shift 1
fi

if [ "$force" = "true" ]; then
    stop
fi

if ! is_running; then
    create "$@"
else
    print_info "Gitlab runner '$runner_name' is already running. Restart it."
    docker restart "$runner_name" || ci::die "Could not restart gitlab runner"
fi
