#!/usr/bin/env bash

set -e
set -u

ROOT=$(git rev-parse --show-toplevel)
. "$ROOT/tools/ci/scripts/general.sh"

if ! brew --version &>/dev/null; then
    ci::print_info "Install brew."
    NON_INTERACTIVE=1 bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

    ci::print_info "Put brew into bashrc."
    test -d ~/.linuxbrew && eval "$(~/.linuxbrew/bin/brew shellenv)"
    test -d /home/linuxbrew/.linuxbrew && eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
    echo "eval \"\$($(brew --prefix)/bin/brew shellenv)\"" >>~/.bashrc
fi

ci::print_info "Install podman."
brew install podman
brew install slirp4netns

ci::print_info "Check if the user has rights for CPU and CPUSET limit delegation."
if ! grep "cpu" "/sys/fs/cgroup/user.slice/user-$(id -u).slice/user@$(id -u).service/cgroup.controllers" >/dev/null; then
    ci::print_info "Enabling CPU and CPUSET limit delegation for all users."
    sudo mkdir -p /etc/systemd/system/user@.service.d/

    cat <<"EOF" | sudo tee /etc/systemd/system/user@.service.d/delegate.conf
[Service]
Delegate=memory pids cpu cpuset
EOF

fi

ci::print_info "Set podman storage conf"
cat <<"EOF" >~/.config/containers/storage.conf
# This file is the configuration file for all tools
# that use the containers/storage library. The storage.conf file
# overrides all other storage.conf files. Container engines using the
# container/storage library do not inherit fields from other storage.conf
# files.
#
#  Note: The storage.conf file overrides other storage.conf files based on this precedence:
#      /usr/containers/storage.conf
#      /etc/containers/storage.conf
#      $HOME/.config/containers/storage.conf
#      $XDG_CONFIG_HOME/containers/storage.conf (If XDG_CONFIG_HOME is set)
# See man 5 containers-storage.conf for more information
# The "container storage" table contains all of the server options.
[storage]

# Default Storage Driver, Must be set for proper operation.
driver = "overlay"
EOF
