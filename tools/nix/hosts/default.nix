{ inputs, outputs }:
let
  nixosConfigurations = {
    # This is the VM used for the gitlab-runner running in engine.switch.ch.
    #
    # To apply the VM use:
    # ```shell
    #    nix run github:nix-community/nixos-anywhere \
    #      --flake ./tools/nix#gitlab-runner \
    #       <ip>
    # ```
    #
    # The `hardware-configuration.nix` was created using an additional:
    #
    # ```shell
    # --generate-hardware-config nixos-generate-config ./hardware-configuration.nix
    # ```
    gitlab-runner = import ./gitlab-runner { inherit inputs outputs; };
  };

  deploy = {
    nodes.gitlab-runner = {
      hostname = "gitlab-runner-nixos";
      fastConnection = true;
      profiles = {
        system = {
          user = "root";
          sshUser = "root";
          path = inputs.deploy-rs.lib.x86_64-linux.activate.nixos nixosConfigurations.gitlab-runner;
        };
      };
    };
  };

in
{
  inherit nixosConfigurations deploy;
}
