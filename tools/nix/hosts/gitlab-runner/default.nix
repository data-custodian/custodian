{ inputs, outputs, ... }:
inputs.nixpkgs.lib.nixosSystem {
  system = "x86_64-linux";
  modules = [
    inputs.disko.nixosModules.disko
    ./configuration.nix
    ./hardware-configuration.nix
  ];

  specialArgs = {
    inherit inputs outputs;
  };
}
