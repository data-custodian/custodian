{ config, ... }:
let
  sshKeys = import ./secrets/sshkeys.nix;
in
{
  # These are our own settings (defined in `nixos/settings.nix`).
  settings = {
    user = {
      name = "ci";
      inherit sshKeys;
    };

    root = {
      sshKeys = sshKeys;
    };

    timezone = "Europe/Zurich";
    hostName = "vm-gitlab-runner";
  };

}
