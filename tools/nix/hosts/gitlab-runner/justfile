set positional-arguments
set fallback
set shell := ["bash", "-cue"]
comp_dir := justfile_directory()
root_dir := `git rev-parse --show-toplevel`

# Deploy Commands =============================================================

# Build the VM.
build:
    cd "{{root_dir}}" && \
       nix build ./tools/nix#nixosConfigurations.gitlab-runner.config.system.build.toplevel

# Initialize the new VM by overwriting it with NixOS.
init-instance host="debian@gitlab-runner-nixos":
    cd "{{root_dir}}" && \
    nixos-anywhere --flake "./tools/nix#gitlab-runner" "{{host}}"

# Deploy the NixOS configuration to the VM (use --force to really do it).
deploy *args:
    #!/usr/bin/env bash
    # Checks are skipped because, some flakes are not really compatible yet.
    args=()
    [ "$1" = "--force" ] || args=("--dry-activate")
    cd "{{root_dir}}" && \
        deploy --debug-logs \
            --skip-checks \
            "${args[@]}" ./tools/nix#gitlab-runner

# Update root files
update-root-files:
    #!/usr/bin/env bash
    cd "{{root_dir}}"
    function cleanup() {
        podman container kill temp-buildah &>/dev/null || true
        podman container rm temp-buildah &>/dev/null || true
    }
    trap cleanup EXIT
    cleanup
    podman run -d --name temp-buildah quay.io/buildah/stable:latest
    echo "Writing files to {{comp_dir}}/root/etc ..."
    podman cp temp-buildah:/etc/containers "{{comp_dir}}/root/etc/"
    podman cp temp-buildah:/run/secrets "{{comp_dir}}/root/run/"
    find "{{comp_dir}}/root/etc" -type d -empty -delete

# Secrets Commands ============================================================

# Edit the secret with your SSH key `identidy`.
edit-secret sshkey="~/.ssh/id_rsa":
    cd "{{comp_dir}}/secrets" && \
    agenix -i "{{sshkey}}" -e gitlab-runner-token-config.age

# Reencrypt all keys in `./secrets`
# Needed when the list of recipients (age) change
# (the user who can decrypt the secrets).
reencrypt-secrets sshkey="~/.ssh/id_rsa":
    cd "{{comp_dir}}/secrets" && agenix -i "{{sshkey}}" --rekey


# VM Commands =================================================================
# Show the history of the NixOS on the machine.
history: check-on-vm
    #!/usr/bin/env bash
    set -eu
    echo "History in 'system' profile:"
    nix profile history --profile /nix/var/nix/profiles/system

# Clean the VMs nix store (not the same as the jobs /nix/store !)
gc: check-on-vm
    echo "Remove all generations older than 7 days"
    sudo nix profile wipe-history --profile /nix/var/nix/profiles/system --older-than 7d

    echo "Garbage collect all unused nix store entries"
    sudo nix store gc --debug

# Prune stuff which lets the runner continue running, also does not kill jobs.
podman-gc:
    # Cleans everything except (nix-daemon-container which runs and its volumes)
    # Only dangling images.
    podman system prune --volumes
    just podman-remove-images

# Recreate the nix-daemon-store.
podman-recreate-nix-daemon-store:
    cd "{{comp_dir}}" && ./scripts/recreate-nix-daemon-store.sh
# Remove all containers (but not systemd spawnd containers: nix-daemon-container).
[private]
podman-remove-containers: check-on-vm
    echo "Remove all cointainers (except nix-daemon-container)"
    podman container rm --force --filter "label!=PODMAN_SYSTEMD_UNIT"

# Remove podman images (but only ones with no label `no-prune`)
[private]
podman-remove-images: check-on-vm
     podman image prune --filter "label!=no-prune"

# Remove all volumes (but not nix-daemon-* volumes)
[private]
podman-remove-volumes: check-on-vm
    podman volume ls --format "{{{{ .Name }}" | \
        grep -v "nix-daemon" | \
        xargs -I {} podman volume rm {}
## ==============================================

# Show some status on the VM.
status: check-on-vm
    #!/usr/bin/env bash
    set -eu

    echo "===================="
    echo "Disk Space:"
    df -H | grep -E "/root|overlay"
    echo "===================="

    echo "===================="
    echo "Podman Images:"
    podman images
    echo "===================="

    echo "===================="
    echo "Volumes (nix-daemon-container)"
    podman container inspect -f "{{{{ json .Mounts }}" nix-daemon-container |
        jq '.[] | select(.Type == "volume") | .Name, .Destination'
    echo "===================="

    echo "===================="
    echo "Memory Usage:"
    free -h
    echo "===================="

    echo "===================="
    echo "Gitlab Runner Config"
    cat /var/lib/gitlab-runner/.gitlab-runner/config.toml
    echo "===================="

# Show the Gitlab Runner log.
log: check-on-vm
    journalctl -f -u gitlab-runner.service

# =============================================================================

[private]
check-on-vm:
    #!/usr/bin/env bash
    set -eu
    [ "$(hostname)" = "vm-gitlab-runner" ] || {
        echo "ERROR: You do not seem to be logged in on the VM."
        exit 1
    }
