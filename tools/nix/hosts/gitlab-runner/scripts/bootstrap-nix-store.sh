#!/usr/bin/env bash

# Since we mount the volumes from `nix-daemon-container`
# ('/nix/store': podman volume of the Nix store) to the Gitlab Runner base image
# the initial `/nix/store` paths get lost.
#
# This script bootstraps the initial podman volume in `nix-daemon-container`
# with some tools which are in `/nix/store` in the Gitlab Runner job image.
# by copying them.

set -e
set -u

# exec >~/bootstrap-nix-store.output.log 2>&1

USE_ALPINE_BASE="@use-alpine-base@"

BASE_IMG="local/nix"
if [ "$USE_ALPINE_BASE" = "true" ]; then
    BASE_IMG="local/alpine"
fi

for _ in {1..5}; do
    NIX_STORE_VOLUME_ID=$(podman container inspect -f "{{ json .Mounts }}" nix-daemon-container |
        jq -r '.[] | select(.Type == "volume" and .Destination == "/nix/store") | .Name')

    [ -n "$NIX_STORE_VOLUME_ID" ] || {
        echo "Could not determine podman volume with nix store in 'nix-daemon-container' -> Sleep 2 and retry." >&2

        sleep 2

        continue
    }
done

[ -n "$NIX_STORE_VOLUME_ID" ] || {
    echo "Could not determine podman volume with nix store in 'nix-daemon-container'." >&2
    exit 1
}

CMD=$(
    cat <<EOF
pkgs=(@bootstrap-pkgs@)
nix --extra-experimental-features nix-command copy --to /nix-custom "\${pkgs[@]}"
EOF
)

echo "Podman images:"
podman images

echo "Copy pkgs to 'nix-daemon-container' volume ('$NIX_STORE_VOLUME_ID', '$BASE_IMG')"
podman run \
    -v "$NIX_STORE_VOLUME_ID:/nix-custom/nix/store" \
    "$BASE_IMG" \
    bash -c "$CMD"

echo "Successfully copied bootstrap packages."
