# Gitlab Runner Module
#
# This module will add a Gitlab-Runner
# configured similar to https://wiki.nixos.org/wiki/Gitlab_runner
# with a nix-daemon running in a podman container `nix-daemon-container`.
#
# - The volumes from the daemon container will get mounted to
#   each Job container which Gitlab starts.
#
# - The `/nix/store` inside the job container (either image `alpineImage` or `nixImage`)
#   will be read-only and nix can only store stuff into this path by using the
#   `NIX_DAEMON` env. variable which lets it communicate through the
#   mounted daemon socket.
#
# - Since the `/nix/store` path of the job container is mounted over from the
#   volume of `nix-daemon-container`, the initial `bootstrapPkgs` are lost. Therefore
#   a systemd service `bootstrap-nix-store` is started before the Gitlab runner starts to
#   copy essential store paths to the volume attached to `nix-daemon-container`
#   (see `scripts/bootstrap-nix-store.sh`)
#
# - There is also a prebuild script which is started on every job.
#   See `scripts/prebuild.sh`
#
# Debugging on the VM:
# - You can use `journalclt -u gitlab-runner.service`
#   and `journalclt -u bootstrap-nix-store.service`
# - You can use `systemctl show bootstrap-nix-store.service` to
#   to inspect the systemd configuration for this service.
# - Also inside `~/custodian` (clone this repo there)
#   you can run `just status` to get some information about the
#   running stuff.
# - You can clean all logs with `journalclt --vacuume-time=1s`
# - Also you can run `btop` on the machine to inspect the performance.
#
# TODO: use `.execConfig` on `gitlab-runner.service` created
#       to set `.execConfig.User = 'ci'`
#       and see if we can start it with out requiring root.
{
  config,
  lib,
  pkgs,
  inputs,
  ...
}:
let
  fs = lib.fileset;

  # Either we use a Nix as the base image or Alpine.
  useAlpineBase = true;
  bootstrapPkgs = [
    pkgs.just
    pkgs.cachix
  ];
  baseImage = if useAlpineBase then "local/alpine" else "local/nix";
  baseImageService =
    if useAlpineBase then "podman-alpine-container.service" else "podman-nix-container.service";

  preBuild =
    lib.replaceStrings
      [
        "@nix-pkg-path@"
        "@use-alpine-base@"
      ]
      [
        "${pkgs.nix}"
        (if useAlpineBase then "true" else "false")

      ]
      (builtins.readFile ./scripts/prebuild.sh);
  preBuildScript = pkgs.writeScriptBin "gitlab-runner-pre-build-script" preBuild;

  bootstrapNixStore =
    lib.replaceStrings
      [
        "@use-alpine-base@"
        "@bootstrap-pkgs@"
      ]
      [
        (if useAlpineBase then "true" else "false")
        (lib.concatStringsSep " " (lib.map (p: "\"${p}\"") bootstrapPkgs))
      ]
      (builtins.readFile ./scripts/bootstrap-nix-store.sh);
  bootstrapNixStoreScript = pkgs.writeScriptBin "bootstrap-nix-store" bootstrapNixStore;

  noPruneLabels = {
    no-prune = "true";
  };

  # This is the Nix base image.
  # git+file:///home/nixos/Desktop/Repos/custodian?dir=tools/nix#nixosConfigurations.gitlab-runner.config.system.bu👀
  nixImageBase = import (inputs.nix.outPath + "/docker.nix") {
    pkgs = pkgs;
    name = "local/nix-base";
    tag = "latest";

    bundleNixpkgs = false;

    extraPkgs = [
      pkgs.cachix
      preBuildScript
    ];

    nixConf = {
      cores = "0";
      experimental-features = [
        "nix-command"
        "flakes"
      ];
    };
  };

  # This derivation will contain a folder `/etc`
  # If its added to `contents` all files in the
  # derivation will be symlinked in `/`.
  auxRootFiles = fs.toSource {
    root = ./root;
    fileset = fs.gitTracked ./root/etc;
  };

  nixImage = pkgs.dockerTools.buildLayeredImage {
    fromImage = nixImageBase;
    name = "local/nix";
    tag = "latest";
    contents = [
      auxRootFiles
    ];
    config = {
      Labels = noPruneLabels;
    };
    maxLayers = 104;
  };

  # This is the daemon image which provides the store
  # as volumes.
  nixDaemonImage = pkgs.dockerTools.buildLayeredImage {
    fromImage = nixImage;
    name = "local/nix-daemon";
    tag = "latest";

    config = {
      Volumes = {
        "/nix/store" = { };
        "/nix/var/nix/db" = { };
        "/nix/var/nix/daemon-socket" = { };
      };
      Labels = noPruneLabels;
    };
    maxLayers = 105;
  };

  # Update with:
  # ```shell
  # nix run "github:nixos/nixpkgs/nixos-unstable#nix-prefetch-docker" -- \
  #   --image-name alpine --image-tag latest
  # ```
  alpineBase = pkgs.dockerTools.pullImage {
    imageName = "alpine";
    imageDigest = "sha256:beefdbd8a1da6d2915566fde36db9db0b524eb737fc57cd1367effd16dc0d06d";
    sha256 = "0gf7wbjp37zbni3pz8vdgq1mss6mz69wynms0gqhq7lsxfmg9xj9";
    finalImageName = "alpine";
    finalImageTag = "latest";
  };

  # This is the analog image to `local/nix` but alpine based.
  alpineImage = (
    pkgs.dockerTools.buildLayeredImage {
      fromImage = alpineBase;
      name = "local/alpine";
      tag = "latest";

      contents = [
        auxRootFiles # Will link all paths at /nix/store/...-source/etc/... in /etc/...
        pkgs.nix
        pkgs.cacert
        pkgs.coreutils
        pkgs.findutils
        pkgs.git
        pkgs.openssh
        pkgs.bash

        preBuildScript
      ] ++ bootstrapPkgs;

      config = {
        Labels = noPruneLabels;
      };

      # Only if `build buildLayeredImage`.
      maxLayers = 15;
    }
  );

  nixDaemonContainer = {
    imageFile = nixDaemonImage;
    image = "local/nix-daemon:latest";
    # I cannot denote these volumes because they overmount the
    # shit which is in the image.
    # TODO: make a systemd service which starts before
    # that and creates some volumes and inits these from the image.
    # volumes = [
    #   "nix-daemon-store:/nix/store"
    #   "nix-daemon-db:/nix/var/nix/db"
    #   "nix-daemon-socket:/nix/var/nix/daemon-socket"
    # ];
    cmd = [
      "nix"
      "daemon"
    ];
  };

  nixContainer = {
    imageFile = nixImage;
    image = "local/nix:latest";
    cmd = [
      "true"
    ];
  };

  alpineContainer = {
    imageFile = alpineImage;
    image = "local/alpine:latest";
    cmd = [
      "true"
    ];
  };

  envNix = {
    NIX_REMOTE = "daemon";
  };

  envAlpine = {
    NIX_REMOTE = "daemon";
    USER = "root";
    PATH = "/nix/var/nix/profiles/default/bin:/nix/var/nix/profiles/default/sbin:/bin:/sbin:/usr/bin:/usr/sbin";
    SSL_CERT_FILE = "${pkgs.cacert}/etc/ssl/certs/ca-bundle.crt";
    NIX_SSL_CERT_FILE = "${pkgs.cacert}/etc/ssl/certs/ca-bundle.crt";
  };

in
{
  # Common containers.
  virtualisation.containers.storage.settings = {
    storage = {
      driver = "overlay";
      graphroot = "/var/lib/containers/storage";
      runroot = "/run/containers/storage";
    };
    # Does not work currently.
    # storage.options.overlay.mountopt = "nodev,metacopy=on";
  };

  virtualisation.oci-containers = {
    backend = "podman";
    containers =
      {
        nix-daemon-container = nixDaemonContainer;
      }
      //
      # Workaround to add the images to the registry.
      (
        if useAlpineBase then
          {
            alpine-container = alpineContainer;
          }
        else
          {
            nix-container = nixContainer;
          }
      );
  };

  # Define the Gitlab Runner.
  services.gitlab-runner = {
    enable = true;

    settings = {
      log_level = "info";
      # See https://stackoverflow.com/a/55173132/293195
      concurrent = 16;

      check_interval = 2;
    };

    gracefulTermination = true;

    services.nix-runner = {
      description = "Nix Runner (NixOS)";

      registrationFlags = [
        "--docker-volumes"
        "gitlab-runner-cache:/scratch"
        "--docker-volumes-from"
        "nix-daemon-container:ro"

        "--docker-pull-policy"
        "if-not-present"

        "--docker-allowed-pull-policies"
        "if-not-present"

        "--docker-host"
        "unix:///var/run/podman/podman.sock"

        "--docker-network-mode"
        "host"
      ];

      authenticationTokenConfigFile = config.age.secrets.gitlab-runner-token-config.path;

      executor = "docker";
      dockerImage = baseImage;
      dockerAllowedImages = [ baseImage ];
      dockerPrivileged = false;
      requestConcurrency = 4;

      environmentVariables = if useAlpineBase then envAlpine else envNix;
      preBuildScript = "${preBuildScript}/bin/gitlab-runner-pre-build-script";
    };
  };

  # Install some bootstrap script before Gitlab Runner starts
  # to populate the nix store in the podman volume
  # with the essential packages needed.
  # Inspect with `systemctl show gitlab-runner.service`
  # Requires: Ensure it starts only after the bootstrap.
  systemd.services.gitlab-runner.after = [ "bootstrap-nix-store.service" ];

  systemd.services.bootstrap-nix-store = {
    description = "bootstrap-nix-store";

    path = [
      pkgs.nix
      pkgs.bash
      pkgs.jq
      pkgs.podman
    ];

    restartIfChanged = true;

    # If `nix-daemon-container` is started, start the bootstrap.
    wantedBy = [ "podman-nix-daemon-container.service" ];
    # Start the bootstrap after `nix-daemon-container` and after the `baseImageService`.
    after = [
      "podman-nix-daemon-container.service"
      baseImageService
    ];
    # If ensure that the bootstrap is restarted when `nix-daemon-container` is.
    partOf = [ "podman-nix-daemon-container.service" ];

    script = ''
      ${bootstrapNixStoreScript}/bin/bootstrap-nix-store
    '';

    serviceConfig = {
      Type = "oneshot";
      SupplementaryGroups = "podman";
      User = "root";
      StandardOutput = "journal";
      StandardError = "journal";
    };
  };

}
