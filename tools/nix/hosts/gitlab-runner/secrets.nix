{ ... }:
{
  age.secrets.gitlab-runner-token-config = {
    file = ./secrets/gitlab-runner-token-config.age;
    path = "/run/secret/gitlab-runner-token-config";
    mode = "600";
    owner = "ci";
    group = "users";
  };
}
