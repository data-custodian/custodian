# This file is not included in NixOS.
# This is only used with `agenix` CLI.
let
  sshKeys = import ./sshkeys.nix;

  # Use `ssh-keyscan <ip>` to get the public keys of the VM.
  # The VM needs to have access to decrypt the secrets.
  vmPublicKey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPxjMLCiiTy4lFsk2TqVb88/iUFLAK/qsJBkXXLU4f86";
in
{
  # All these SSH keys can decrypt the `gitab-runner-token-config.age` file.
  # Use `agenix -e gitlab-runner-token-config.age` to edit the file
  "gitlab-runner-token-config.age".publicKeys = sshKeys ++ [ vmPublicKey ];
}
