{
  modulesPath,
  outputs,
  inputs,
  nix,
  ...
}:
{
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
    (modulesPath + "/profiles/qemu-guest.nix")

    inputs.home-manager.nixosModules.home-manager
    inputs.agenix.nixosModules.default
    ./disk-config.nix

    # Load our settings.
    ./boot.nix
    ./system.nix
    ./settings.nix
    ./secrets.nix
    ./runner.nix

  ] ++ outputs.nixosModules.all;

  nixpkgs = {
    overlays = [
      # Add here overlays if you need them.
    ];
  };

  system.stateVersion = "24.11";
}
