{ inputs }:
# The overlay function: To override Nixpkgs `pkgs`.
# E.g. to pin formatters or build tools
# to exactly what we want. Otherwise they
# follow the `nixpkgs` or `nixpkgs-stable` releases.
pkgsFinal: pkgsPrev:
let
  cn = import ./custodian {
    inherit inputs;
    rootDir = ../..;
    pkgs = pkgsFinal;
  };
in
{
  # We inject all our custodian packages in namespace `custodian`.
  custodian = cn.pkgs;

  # Add out custodian lib to the
  # existing lib under the namespace `custodian`.
  lib = pkgsPrev.lib.extend (final: prev: prev // { custodian = cn.lib; });
}
