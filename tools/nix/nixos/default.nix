let
  networking = import ./networking.nix;
  nix = import ./nix.nix;
  packages = import ./packages.nix;
  services = import ./services.nix;
  settings = import ./settings.nix;
  time = import ./time.nix;
  user = import ./user.nix;
  virtualization = import ./virtualization.nix;
  home-manager = import ./home-manager.nix;
in
{
  inherit
    networking
    nix
    packages
    services
    settings
    time
    user
    virtualization
    home-manager
    ;

  all = [
    networking
    nix
    packages
    services
    settings
    time
    user
    virtualization
    home-manager
  ];
}
