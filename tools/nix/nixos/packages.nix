{ pkgs, lib, ... }:
{
  # Packages
  environment.systemPackages = with pkgs; [
    coreutils
    findutils
    zsh
    curl
    git

    jq
    btop
    tree
    neovim
    tmux
    just

    podman

    gitlab-runner
  ];

  programs.zsh.enable = true;
}
