{
  config,
  inputs,
  outputs,
  ...
}:
{
  home-manager = {
    useGlobalPkgs = true;
    useUserPackages = true;

    users.${config.settings.user.name} = import (inputs.self + /home/home.nix) {
      username = config.settings.user.name;
    };

    users.root = import (inputs.self + /home/home.nix) {
      username = "root";
    };

    extraSpecialArgs = {
      inherit inputs outputs;
    };

    backupFileExtension = "backup";
  };
}
