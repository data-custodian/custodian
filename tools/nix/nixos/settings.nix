# Own settings to build the NixOS configurations.
{ lib, ... }:
with lib;
{
  # Some option declarations which can be used to specify
  # in `config.settings.???`
  options = {
    settings = {
      # The default user.
      user = {
        name = mkOption {
          description = "The main user name.";
          default = "nixos";
          type = types.str;
        };

        sshKeys = mkOption {
          description = "All keys which have access over SSH to this user account.";
          default = [ ];
          type = types.listOf types.str;
        };
      };

      # The root user.
      root = {
        name = "root";
        sshKeys = mkOption {
          description = "All keys which have access over SSH to root account.";
          default = [ ];
          type = types.listOf types.str;
        };
      };

      timezone = mkOption {
        description = "The timezone to use.";
        default = "Europe/Zurich";
        type = types.str;
      };

      hostName = mkOption {
        description = "The hostname.";
        default = "vm";
        type = types.str;
      };
    };
  };

}
