{ config, ... }:
{
  users.users.${config.settings.user.name}.extraGroups = [
    "docker"
    "podman"
    "libvirtd"
  ];

  virtualisation.docker = {
    enable = false;
    enableOnBoot = true;

    # rootless = {
    #   enable = false;
    #   setSocketVariable = true;
    # };

    autoPrune = {
      dates = "weekly";
      flags = [
        "--filter label!=no-prune"
        "--volumes"
      ];
    };
  };

  virtualisation.podman = {
    enable = true;

    # Create a `docker` alias for podman, to use it as a drop-in replacement
    # dockerCompat = true;
    dockerSocket = {
      enable = true;
    };

    # Required for containers under podman-compose to be able to talk to each other.
    defaultNetwork.settings.dns_enabled = true;

    autoPrune = {
      dates = "weekly";
      flags = [
        "--filter label!=no-prune"
        "--volumes"
      ];
    };
  };

  virtualisation.containers = {
    enable = true;
  };
}
