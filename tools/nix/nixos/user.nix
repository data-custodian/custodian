{
  config,
  lib,
  pkgs,
  ...
}:
{
  ### User Settings ==========================================================
  users.users.root = {
    shell = pkgs.zsh;
    useDefaultShell = false;
    isSystemUser = true;
    openssh.authorizedKeys.keys = config.settings.root.sshKeys;
  };

  users.users.${config.settings.user.name} =
    {
      shell = pkgs.zsh;
      uid = 1000;

      createHome = true;

      linger = true; # Start systemd units at boot, rather than at login.
      useDefaultShell = false;

      isNormalUser = true;

      extraGroups = [
        "disk"
        "network"
        "systemd-journal"
      ];

      openssh.authorizedKeys.keys = config.settings.user.sshKeys;

      # Extent the user `uid/gid` ranges to make podman work better.
      # This is for using https://gitlab.com/qontainers/pipglr
    }
    // lib.optionalAttrs config.virtualisation.podman.enable {
      subUidRanges = [
        {
          startUid = 100000;
          count = 65539;
        }
      ];
      subGidRanges = [
        {
          startGid = 100000;
          count = 65539;
        }
      ];
    };
  # ===========================================================================
}
