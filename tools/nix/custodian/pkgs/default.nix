# Define the `custodian` pkgs module.
{
  pkgs,
  lib,
  inputs,
  cnLib,
  cnQuitsh,
  ...
}:
let
  # Define some pinned versions for build tools used.
  go = pkgs.go_1_23;
  python = pkgs.python312;

  shells = (import ./shells) {
    inherit pkgs lib;
    inherit inputs;
    inherit cnQuitsh;
    pythonDrv = python;
    goDrv = go;
  };

  # Load all components packages by loading
  # `<compPath>/tools/nix/pkgs/default.nix` which must define a
  # an AttrSet of derivations.
  # The result is `{ compName : { pkg-a: derivation-a, pkgs-b: ...}, ...}`
  comp-packages = cnLib.common.attrs.filterEmpty (
    builtins.mapAttrs (
      compName: comp:
      let
        pkgsPath = comp.path + "/tools/nix/pkgs";
        exists = builtins.pathExists pkgsPath;
        p = import pkgsPath { inherit lib pkgs; };
      in
      lib.optionalAttrs exists p
    ) cnLib.components.all
  );

  comp-pkgs-flattened = cnLib.common.attrs.flattenPkgs comp-packages;
in
{
  # Build support functions.
  buildGoModule = pkgs.callPackage ./build-support/go/module.nix { inherit go cnLib cnQuitsh; };

  # Define all component derivations.
  inherit comp-packages comp-pkgs-flattened;

  # Shells derivations.
  inherit shells;

  # The CI Tool derivation.
  quitsh-custodian = cnQuitsh;
}
