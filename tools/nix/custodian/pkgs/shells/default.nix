# Define different shells.
{
  pkgs,
  lib,
  inputs,
  goDrv,
  pythonDrv,
  cnQuitsh,
  ...
}:
let
  toolchains = {
    go = {
      env = {
        QUITSH_TOOLCHAINS = "go";
      };

      packages = with pkgs; [
        git
        goDrv
      ];

    };

    go-lint = {
      env = {
        QUITSH_TOOLCHAINS = "go-lint";
      };

      packages = with pkgs; [
        git
        golangci-lint
        goDrv
      ];
    };

    doc-sphinx = {
      env = {
        QUITSH_TOOLCHAINS = "doc-sphinx";
      };
      packages = with pkgs; [
        git
        uv
        pythonDrv
      ];
    };

    doc-mkdocs = {
      env = {
        QUITSH_TOOLCHAINS = "doc-sphinx";
      };

      packages = with pkgs; [
        git
        uv
        pythonDrv
      ];
    };

    python-playground = {
      env = {
        QUITSH_TOOLCHAINS = "python-playground";
      };

      packages = with pkgs; [
        git
        uv
        pythonDrv
      ];
    };

    image-containerfile = {
      env = {
        QUITSH_TOOLCHAINS = "image-containerfile";
      };
      packages = with pkgs; [
        git
        buildah
        skopeo
      ];
    };

    image-nix = {
      env = {
        QUITSH_TOOLCHAINS = "image-nix";
      };

      packages = with pkgs; [
        git
        skopeo
      ];
    };

    manifest-ytt = {
      env = {
        QUITSH_TOOLCHAINS = "manifest-ytt";
      };

      packages = with pkgs; [
        ytt
        kubernetes-helm
      ];
    };

    general = {
      env = {
        QUITSH_TOOLCHAINS = "general";
      };

      packages = with pkgs; [
        # Essentials.
        git
        just

        # Main languages.
        goDrv

        # Linting and LSP and debuggers.
        delve
        gopls
        golines
        gotools
        golangci-lint
        golangci-lint-langserver
        typos-lsp
        ytt
        kbld
        kubernetes-helm

        # Web-Traffic
        httpie

        # Inspect/upload images.
        dive
        skopeo

        # Validation
        check-jsonschema

        # Deployment tool.
        nixos-anywhere
        deploy-rs
        agenix

        # Our script tool as an alias over `just quitsh`
        (pkgs.writeScriptBin "quitsh" ''
          #!/usr/bin/env sh
          root=$(${pkgs.git}/bin/git rev-parse --show-toplevel) || {
            echo "Could not determine repo. root dir." >&2
          }
          cd "$root" && ${pkgs.just}/bin/just quitsh "$@"
        '')
      ];

      languages.go = {
        enableHardeningWorkaround = true;
      };

      enterShell = ''
        just setup
      '';
    };

    ci = {
      env = {
        QUITSH_TOOLCHAINS = "ci";
      };

      packages = with pkgs; [
        git
        git-lfs
        just
        cnQuitsh
      ];
    };
  };

  makeShell =
    toolchain:
    inputs.devenv.lib.mkShell {
      inherit pkgs inputs;
      modules = [
        (
          { ... }:
          {
            devenv.flakesIntegration = true;
            # This is currently needed for devenv to properly run in pure hermetic
            # mode while still being able to run processes & services and modify
            # (some parts) of the active shell.
            devenv.root = builtins.readFile inputs.devenv-root.outPath;
          }
        )
        ({ ... }: toolchain)

      ];
    };

  shells = lib.attrsets.mapAttrs (name: toolchain: makeShell toolchain) toolchains;
in
shells
// {
  default = shells.general;
}
