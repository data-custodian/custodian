# Define the `custodian` lib modules.
{
  pkgs,
  lib,
  rootDir,
  rootFileset,
}:
let
  cnCommon = {
    yaml = import ./yaml.nix { inherit pkgs; };
    attrs = import ./attrs.nix { inherit lib; };
    pkgsets = import ./pkgsets.nix { inherit lib; };
  };

  cnComponents = (import ./components) { inherit lib cnCommon rootDir; };

  cnFilesets = (import ./filesets.nix) {
    inherit
      lib
      cnComponents
      rootDir
      rootFileset
      ;
  };
in
{
  filesets = cnFilesets;
  common = cnCommon;
  components = cnComponents;
}
