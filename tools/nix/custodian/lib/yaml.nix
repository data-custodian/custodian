{ pkgs, ... }:
let
  # Read a YAML file into a Nix datatype using IFD
  # (Import From Derivation).
  #
  # Similar to:
  # > builtins.fromJSON (builtins.readFile ./somefile)
  # but takes an input file in YAML instead of JSON.
  #
  # readYAML :: Path -> a
  #
  # where `a` is the Nixified version of the input file.
  read =
    path:
    let
      jsonOutputDrv = pkgs.runCommand "from-yaml" {
        nativeBuildInputs = [ pkgs.remarshal ];
      } "remarshal -if yaml -i \"${path}\" -of json -o \"$out\"";
    in
    builtins.fromJSON (builtins.readFile jsonOutputDrv);
in
{
  inherit read;
}
