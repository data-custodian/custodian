{
  lib,
  cnCommon,
  rootDir,
}:
let
  configFileName = ".component.yaml";

  # All query functions.
  queries = (import ./queries.nix) { inherit lib cnCommon configFileName; };

  components = queries.getComponents { path = rootDir; };
in
{
  inherit configFileName;
  inherit queries;

  # All components found in `rootDir`.
  all = components;

  getRootPathRel = compName: (lib.getAttr compName components).pathRel;
  getRootPath = compName: (lib.getAttr compName components).path;

  /*
    Get the version of a component.

    # Inputs
    `compRoot`: The component source directory where `.component.yaml` resides.

    # Type
    ```
    readVersion :: Path -> String
    ```
    # Examples
    ```nix
    readVersion "./components/a"
    => "3.0.1"
    ```
  */
  readVersion = compName: components.${compName}.config.version;

  /*
    Returns the build directory in the components directory.

    # Inputs
    `compRoot` : The component directory.

    # Type
    ```
    getBuildDir :: String -> String
    ```
    # Examples
    ```nix
    getBuildDir "./components/a"
    => ./components/a/.output/build
    ```
  */
  getBuildDir =
    compRoot:
    lib.concatStringsSep "/" [
      compRoot
      ".output"
      "build"
    ];
}
