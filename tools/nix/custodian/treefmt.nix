{ pkgs, ... }:
{
  # Used to find the project root
  # For worktrees we need either `.git` or a file.
  projectRootFile = "CHANGELOG.md";

  settings.global = {
    excludes = [
      "external/**/*"
      "**/vendor/**/*"
    ];
  };

  # Enable the following formatters.
  programs.gofmt.enable = true;
  programs.goimports.enable = true;

  # Markdown, JSON, YAML, etc.
  programs.prettier.enable = true;
  settings.formatter.prettier.excludes = [
    "deploy/charts/**/templates/*"
    "components/playground/helm-chart/*"
    "components/playground/docs/*"
    ".golangci.yaml" # this is a symlink, which prettier cannot deal with
    ".yamllint.yaml" # this is a symlink, which prettier cannot deal with
  ];

  programs.ruff-format.enable = true;

  # Shellscripts (which we should not have!)
  programs.shfmt = {
    enable = true;
    indent_size = 4;
  };
  programs.shellcheck = {
    enable = true;
  };
  settings.formatter.shellcheck = {
    options = [
      "-e"
      "SC1091"
    ];
  };

  # Nix.
  programs.nixfmt.enable = true;

  # Lua.
  programs.stylua.enable = true;

  # Typos. TODO: Make this work only for markdown, its destructive in other formats.
  # programs.typos.enable = true;
}
