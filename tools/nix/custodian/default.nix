{
  inputs,
  pkgs,
  rootDir,
  ...
}:
let
  lib = pkgs.lib;
  fs = lib.fileset;

  rootFileset = fs.gitTracked rootDir;

  # The library with common Nix functionality.
  cnLib = (import ./lib/default.nix) {
    inherit
      lib
      pkgs
      rootDir
      rootFileset
      ;
  };

  # The tool which acts as a helper to build stuff.
  cnQuitsh = pkgs.callPackage ../../quitsh-custodian/tools/nix/package {
    self = cnQuitsh;
    inherit cnLib;
  };

  # Configure formatter.
  treefmtEval = inputs.treefmt-nix.lib.evalModule pkgs ./treefmt.nix;
  treefmt = treefmtEval.config.build.wrapper;

  # All packages/build-support stuff
  # exposed in this module.
  cnPackages =
    (import ./pkgs) {
      inherit
        lib
        pkgs
        inputs
        cnLib
        cnQuitsh
        ;
    }
    # Add packages ...
    // {
      inherit treefmt;
    };
in
{
  lib = cnLib;
  pkgs = cnPackages;
}
