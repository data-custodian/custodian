{
  description = "custodian";

  nixConfig = {
    extra-trusted-substituters = [
      # Nix community's cache server
      "https://nix-community.cachix.org"
    ];
    extra-trusted-public-keys = [
      "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
    ];

    allow-import-from-derivation = "true";
  };

  inputs = {
    # Nixpkgs
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

    # You can access packages and modules from different nixpkgs revs
    # at the same time. Here's an working example:
    nixpkgsStable.url = "github:nixos/nixpkgs/nixos-24.11";
    # Also see the 'stable-packages' overlay at 'overlays/default.nix'.

    nix = {
      url = "github:nixos/nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # The devenv module to create good development shells.
    devenv = {
      url = "github:cachix/devenv";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    devenv-root = {
      url = "file+file:///dev/null";
      flake = false;
    };

    # Encrypted secrets.
    agenix = {
      url = "github:ryantm/agenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # Format the repo with nix-treefmt.
    treefmt-nix = {
      url = "github:numtide/treefmt-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # Declarative Disk partitioning for VMs.
    disko = {
      url = "github:nix-community/disko";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    deploy-rs = {
      url = "github:serokell/deploy-rs";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # Home-Manager for NixOS.
    home-manager = {
      url = "github:nix-community/home-manager/master";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };
  outputs =
    inputs:
    let
      inherit (inputs.self) outputs;
      inherit (inputs.nixpkgs) lib;

      supportedSystems = [
        "x86_64-linux"
        "aarch64-darwin"
        "x86_64-darwin"
        "aarch64-linux"
      ];

      # Function to import nixpkgs and apply overlays to it.
      loadNixpgs =
        system:
        import inputs.nixpkgs {
          inherit system;
          overlays = [
            # Load deploy-rs from this input.
            (pkgsFinal: pkgsPrev: { deploy-rs = inputs.deploy-rs.packages.${system}.default; })
            (pkgsFinal: pkgsPrev: { agenix = inputs.agenix.packages.${system}.default; })
            (import ./overlay.nix { inherit inputs; }) # Use our custodian overlay.
          ];
        };

      # Nixpkgs instantiated for supported system types.
      nixpkgsFor = lib.genAttrs supportedSystems (system: loadNixpgs system);

      # Function which generates an attribute set '{ x86_64-linux = func {inherit lib pkgs}; ... }'.
      forAllSystems =
        func:
        lib.genAttrs supportedSystems (
          system:
          let
            pkgs = nixpkgsFor.${system};
            lib = pkgs.lib;
          in
          func { inherit lib pkgs; }
        );

      hosts = import ./hosts { inherit inputs outputs; };
    in
    {
      # All flakes output.

      lib = forAllSystems (
        { pkgs, lib, ... }:
        {
          custodian = {
            lib = lib.custodian;
            # TODO: Should not be here, just for convenience.
            pkgs = pkgs.custodian;
          };
        }
      );

      packages = forAllSystems (
        { pkgs, ... }:
        {
          treefmt = pkgs.custodian.treefmt;
          inherit (pkgs.custodian) quitsh-custodian;
        }
        // pkgs.custodian.comp-pkgs-flattened
      );

      formatter = forAllSystems ({ pkgs, ... }: pkgs.custodian.treefmt);
      devShells = forAllSystems ({ pkgs, ... }: pkgs.custodian.shells);

      nixosModules = import ./nixos;
      inherit (hosts) nixosConfigurations deploy;
    };
}
