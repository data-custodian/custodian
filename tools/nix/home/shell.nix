{ pkgs, ... }:
{
  xdg = {
    enable = true;
    configFile.p10k = {
      source = ./shell/.p10k.zsh;
      target = "zsh/.p10k.zsh";
    };
    configFile.keybindings = {
      source = ./shell/.zshrc-keybindings.zsh;
      target = "zsh/.zshrc-keybindings.zsh";
    };
  };

  programs.fzf = {
    enable = true;
    enableZshIntegration = true;
  };

  programs.zsh = {
    enable = true;

    enableCompletion = false;
    syntaxHighlighting.enable = false;

    dotDir = ".config/zsh";

    history = {
      path = "$ZDOTDIR/.zsh_history";
      size = 10000;
      share = true;
    };

    initExtraFirst = ''
      local ZVM_LAZY_KEYBINDINGS=true
      # Somehow zsh-vi-mode overwrites CTRL+R. https://github.com/jeffreytse/zsh-vi-mode/issues/242
      local ZVM_INIT_MODE=sourcing
    '';

    initExtra = ''
      # Init Extra -----------------------
      if [ -f "$ZDOTDIR/.p10k.zsh" ]; then
          source "$ZDOTDIR/.p10k.zsh"
      fi

      source "$ZDOTDIR/.zshrc-keybindings.zsh"

      echo "Welcome to"
      ${pkgs.figlet}/bin/figlet "GitlabRunner"
      # ----------------------------------
    '';

    antidote = {
      enable = true;
      plugins = [
        "romkatv/zsh-defer"

        # OMZ Plugins.
        "ohmyzsh/ohmyzsh path:plugins/gitfast"
        "ohmyzsh/ohmyzsh path:plugins/colored-man-pages"
        "ohmyzsh/ohmyzsh path:plugins/command-not-found"

        # Enable VIM mode
        "jeffreytse/zsh-vi-mode"

        "romkatv/powerlevel10k"

        # Completion Plugins.
        "zsh-users/zsh-completions"
        "sorin-ionescu/prezto path:modules/completion"

        # These popular core plugins should be loaded at the end
        "zsh-users/zsh-autosuggestions kind:defer"
        "zsh-users/zsh-syntax-highlighting kind:defer"
        "zsh-users/zsh-history-substring-search kind:defer"

        # Ctrl+R
        "z-shell/zsh-navigation-tools"
      ];
    };
  };

  programs.direnv = {
    enable = true;
    enableZshIntegration = true;
  };
}
