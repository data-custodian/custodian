# Use this to configure your home environment (it replaces ~/.config/nixpkgs/home.nix)
# Search for all options using: https://mipmip.github.io/home-manager-option-search
{ username }:
{
  config,
  osConfig,
  ...
}:
{
  imports = [
    ./shell.nix
    ./session.nix
  ];

  home = {
    inherit username;
  };

  # Enable home-manager.
  programs.home-manager.enable = true;

  # https://nixos.wiki/wiki/FAQ/When_do_I_update_stateVersion
  home.stateVersion = "24.11";
}
