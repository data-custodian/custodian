# The Swiss Data Custodian

## Introduction

Swiss Data Custodian is a comprehensive governance framework leveraging semantic
digital contracts to ensure secure, compliant, and transparent data sharing.
With its modular architecture, the platform facilitates contract lifecycle
management and precise access controls, effectively bridging the gap between
privacy regulations and technical implementation.

Navigating data privacy has its intricacies, and the Swiss Data Custodian
streamlines this journey with privacy-centric governance. Its capability to set
policies, spanning from core infrastructure to applications and research data
handling, underscores its versatility. The essence of this system lies in its
fine-grained access control, effectively safeguarding sensitive assets. Here,
digital semantic contracts prescribe the terms, fostering transparency and a
shared understanding among all parties. Whether addressing GDPR or the Swiss
Federal Act on Data Protection (nFADP) mandates, the Swiss Data Custodian
clarifies the path to compliance. Comprehensive audit trails offer valuable
insights into every data interaction, positioning organizations a step ahead in
an ever-shifting regulatory environment.

The Swiss Data Custodian aims to unlock sensitive data value through:

- **Policy Enforcement:** Implement detailed and context-aware data handling
  policies, ensuring stringent privacy compliance.

- **Semantic Oversight:** Augment existing architectures with a potent semantic
  contract-driven layer, enhancing clarity in data management.

- **Transparent Accountability:** Capture every data touchpoint, retaining an
  enduring record of access and signature validations.

## Facilitating Privacy Compliance

In adherence to critical privacy protection regulations, including the Federal
Act on Data Protection (FADP) and the General Data Protection Regulation (GDPR),
the Swiss Data Custodian enhances privacy compliance through the following key
aspects:

- **Data Subject Centricity:** Prioritize the rights and autonomy of data
  subjects, ensuring that they have control, transparency, and understanding of
  how their data is used.

- **Controller Compliance:** Equip data controllers with the tools they need to
  define, enforce, and verify processing activities align with the legal
  framework, ensuring that data subjects' rights are upheld.

- **Processor Accountability:** Streamline the responsibilities of data
  processors with clear contract management and audit trails, ensuring that
  processing is done securely, transparently, and in line with the controller's
  instructions.

## Getting Started

Read our documentation on
[Custodian Documentation](https://data-custodian.gitlab.io/custodian).

## Contributing

Please read the [contribution guidelines](/CONTRIBUTING.md).

## Development Setup

Read the [development setup guide here](./docs/development/README.md).

## License Information

Copyright © 2019-2024 Swiss Data Science Center (SDSC),
[www.datascience.ch](https://www.datascience.ch/). All rights reserved.

The SDSC operates as a 'Société Simple' (einfache Gesellschaft) under Swiss law,
jointly established and legally represented by the École Polytechnique Fédérale
de Lausanne (EPFL) and the Eidgenössische Technische Hochschule Zürich (ETH
Zürich). This copyright encompasses all materials, software, documentation, and
other content created and developed by the SDSC.

The Swiss Data Custodian software is distributed as open-source under the AGPLv3
license or any later version. Details about the license can be found in the
`LICENSE.AGPL` file included within the distribution package.

If the AGPLv3 license does not accommodate your project or business needs,
alternative licensing options are available to meet specific requirements. These
arrangements can be facilitated through the EPFL Technology Transfer Office. For
more information, kindly visit their official website at
[tto.epfl.ch](https://tto.epfl.ch/) or direct your inquiries via email to
[info.tto@epfl.ch](mailto:info.tto@epfl.ch).

Please note that this software should not be used to harm any individual or
entity. Users and developers must adhere to ethical guidelines and use the
software responsibly and legally.
