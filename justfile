set positional-arguments
set shell := ["bash", "-cue"]
comp_dir := justfile_directory()
root_dir := `git rev-parse --show-toplevel`
flake_dir := "./tools/nix"

# Default target if you do not specify a target.
default:
    just --list

# Enter a Nix development shell.
develop *args: set-devenv-pwd
    #!/usr/bin/env bash
    set -eu
    cd "{{root_dir}}"
    args=("$@")
    [ "${#args[@]}" != 0 ] || args="$SHELL"
    nix develop \
        --accept-flake-config \
        --override-input devenv-root "path:.devenv/state/pwd" \
        "{{flake_dir}}#default" \
        --command "${args[@]}"

# Run quitsh (by compiling it directly and executing it).
quitsh *args:
    just -f "{{root_dir}}/tools/quitsh-custodian/justfile" run "$@"

# Clean cleans the components output folders.
clean comppattern *args:
    cd "{{root_dir}}" && \
    just quitsh clean --components "{{comppattern}}" {{args}}

# Format the whole repository.
format *args:
    cd "{{root_dir}}" && \
    just quitsh format {{args}}

# List components.
list *args:
    cd "{{root_dir}}" && \
    just quitsh list {{args}}

# Lint components by pattern `comppattern`.
lint comppattern *args:
    cd "{{root_dir}}" && \
    just quitsh lint --components "{{comppattern}}" {{args}} && \
    just quitsh nix fix-hash

# Build components by pattern `comppattern`.
build comppattern *args:
    cd "{{root_dir}}" && \
    just quitsh build --components "{{comppattern}}" {{args}}

# Test components by pattern `comppattern`.
test comppattern *args:
    cd "{{root_dir}}" && \
    just quitsh test --components "{{comppattern}}" {{args}}

# Render all manifest by pattern `comppattern`.
manifest comppattern *args:
    cd "{{root_dir}}" && \
    just quitsh manifest --components "{{comppattern}}" {{args}}

## Packaging ===================================================================
# Build the image of the component by pattern `comppattern`.
alias build-image := package-image
package-image comppattern *args:
    cd "{{root_dir}}" && \
    just quitsh image --components "{{comppattern}}" {{args}}

## CI =========================================================================
# Run command over CI shell.
ci *args: set-devenv-pwd
    cd "{{root_dir}}" && \
        nix develop \
        --accept-flake-config \
        --override-input devenv-root "path:.devenv/state/pwd" \
        "{{flake_dir}}#ci" --command "$@"

# Clean up the registry on certain images.
ci-registry-cleanup projectID tokenEnv *args:
    cd "{{root_dir}}" && \
    quitsh \
      registry container-cleanup \
      --project-id "{{projectID}}" \
      --credential-token-env "{{tokenEnv}}" \
      -i "registry.gitlab.com/data-custodian/custodian/nix-temporary/.*" \
      {{args}}

## Nix Raw Tooling ================================================================
# Set the direnv root file which records the project directory.
[private]
set-devenv-pwd:
    #!/usr/bin/env bash
    set -eu
    cd "{{root_dir}}"
    # This is currently needed for devenv to properly run in pure hermetic
    # mode while still being able to run processes & services and modify
    # (some parts) of the active shell.mkdir -p .devenv/state
    # See: https://github.com/cachix/devenv/issues/1461
    mkdir -p .devenv/state
    if [ ! -f .devenv/state/pwd ] || [ "$(pwd)" != "$(cat .devenv/state/pwd)" ]; then
        pwd > .devenv/state/pwd
    fi

# Build a package in the `packages` output of `flake.nix`
alias build-nix := package-nix
package-nix attrname *args:
    cd "{{root_dir}}" && \
    mkdir -p .output/package && \
    outlink=".output/package/{{attrname}}" && \
    nix build -L "{{flake_dir}}#{{attrname}}" --out-link "$outlink" --json

package-nix-show:
    #!/usr/bin/env bash
    # We cannot use `nix flake show` due to other systems where
    # IFD (import-from-derivation -> from-yaml Nix derivation)
    # does not work.
    set -e
    set -u
    system=$(nix eval --impure --raw --expr builtins.currentSystem)
    nix eval --json "{{flake_dir}}#packages.$system" | jq

# Enter a `nix` interactive interpreters with loaded `flake.nix`.
# Inspect the `packages` variable by tab-completion.
nix-repl *args:
    cd "{{root_dir}}" && \
    nix repl --extra-experimental-features 'repl-flake' "{{flake_dir}}"

# Deploy the Gitlab runner.
nix-deploy-gitlab-runner *args:
    cd "{{root_dir}}" && just deploy {{args}}

# Update an input of the flake.
nix-flake-update input:
    nix flake update --flake "{{flake_dir}}" "{{input}}"
## ============================================================================

## Miscellaneous ==============================================================
# Setup development files (default done in `.envrc`).
setup *args:
    cd "{{root_dir}}" && \
    just quitsh setup "$@"

## ============================================================================
