Notable changes introduced in the Swiss Data Custodian releases are documented
in this file

# [0.2.0] - 2024-07-08

Replacement of JSON contracts with RDF contracts

## Documentation

- update readme
- update documentation source files in `docs`
- update API documentation in the different services

## Features

- add code base for the RDF contract management and signature (`cms`)
- add code base for the RDF contract interpretation (`acs`)
- add reusable code for the interaction with the Jena API
- add Dockerfiles and Helm charts for Jena and HAProxy
- update all Docker Compose files and Helm charts

# [0.1.0] - 2023-11-08

Initial release.

## Documentation

- add contributing guidelines
- add documentation source files in `docs`
- add license
- add API documentation in `swagger`

## Features

- add code base for the Access Control System (ACS)
- add code base for the Contract Management System (CMS)
- add code base for the Audit Trail
- add code base for the Key Management System (KMS)
- add code base for the gateway
- add a `secret-generator` for development/test deployment
- add a Dockerfile for each microservice
- add Docker Compose files to locally run the components
- add Helm charts for Kubernetes deployment
- upload and interprete JSON contracts (Swiss Data Custodian contracts)
- sign contracts with ECDSA
- flexible authentication using KeyCloak and OpenID Connect
